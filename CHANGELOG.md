# Changelog

## Release version 0.4.1 (25 sept. 2018)

* Update to latest version of dependencies for coherency
* Another fix to consider communication task nodes of BasicLeafTaskNodes in the HTGDefaultVisitor
* Fixed problem in HTG switches/visitors, which did not treat BasicLeafTaskNodes as BasicBlock and ignored the content of CommunicationNodes in some cases
* Update target to latest dependencies
---

## Release version 0.4.0 (20 sept. 2018)

* Merge branch 'new_for_loop_representation' into 'develop'
* Hack disable test to enable correct build
* Fix build inside eclipse
* Fix gecos parallel build
* - Turned ProfilingInformation into an interface - HTGExtraction now copies BasicBlock labels
* changed method to get get children tasks
* Added tasks for DoWhile, Switch and Case
* Fixed uninitialized LoopIn/LoopOut nodes for while task nodes
* - Added HTGFactory methods - Fixed HTGDefaultSwitch to also visit LoopIn/LoopOut nodes - Fixed HTG Dotty export to output LoopIn/LoopOut nodes
* Added factory methods for further task dependency types
* Small fixes to the new HTG model Add new model for while-loops Improved support for C99 loops
* adapt build and clear control flow add new representation of WhileTaskNode change LoopIn/Out to CommunicationNode
* Merge branch 'new_for_loop_representation' of gitlab.inria.fr:gecos/gecos-parallel into new_for_loop_representation
* fix containment of init basic block
* improved dependency handling
* Changed model and creation of for tasks Added different kinds of task dependencies
* fix containment of init basic block
* improved dependency handling
* Changed model and creation of for tasks Added different kinds of task dependencies
---

## Release version 0.3.1 (20 avril 2018)

* [Fix] missing the setting of depth attribute for CommunicationNodes in HTaskGraphFactory
* [Add] paralle.demo plugin with demo scripts for task extraction flow with MPI codegen and SDSoC codegen
* [Mod] HTGUtils.getAllProcessingResourcesFor() now also consider the specified task's in/out nodes PRs
* [Refactor] InsertSynchronuousCommunications to allow for ather strategies to be used
* minor cleanup and logging improvement

---


## Release version 0.3.0 (30 mars 2018)

* [Add] simple SDSoC backend using block outlining.
* [Fix] concurrent access to block children list in HTGExtraction.
* [Releng] update eclipse dependencies to releases/oxygen/201803211000
* [Releng] update gecos-core to v0.5.0, gecos-framework to v1.2.0, gecos-testframework to v2.0.0
* [CI] Fix Jenkins pipeline configuration:
  Merge Build and Test stages to avoid incorrect dependency resolution through the local maven cache
* [CI] gitlab-ci can now use variable 'JENKINS\_FAIL\_MODE' to specify the behaviour depending on test results.
  JENKINS\_FAIL\_MODE can be optionally defined as secret variable in the gitlab project CI/CD settings 
  as comma-separated list of one or more of the following modes:
  - 'FAIL' : fail in case any test has failed
  - 'SKIP' : fail in case any test wad skiped
  - 'NO\_TEST' : fail in case no test was run

---


## Release version 0.2.0 (09 janv. 2018)

* [Revert] HTG model changes regarding `CommunicationNode`:
* [Update] gecos dependencies to latest release versions.
* [New] support for inserting communication profiling instructions.
  - `CommunicationNode` now contains a `CompositeBlock`.
  - `HTGCom` inherits `BasicBlock`.
* [Fix] Parallel Mode visitors.
* [Mod] Changes to `Parallel` model:
  - `Communication` is now a `BasicBlock` (was `Block`).
* [CI] deploying a release version now automatically triggers an integration build/test in a dedicated branch in 'gecos-composite'.

---


## Release version 0.1.0 (24 Nov 2017)

Migrate HTG/Parallel IR plugins from GeCoS svn: 
 _/gecos/trunk/gecos/htg/_ (rev 17733) and _/gecos/trunk/gecos/extra/fr.irisa.**.scheduling_ (rev 17328).

* [Mod] Convert `Htashgraph`, `Parallel` and `Architecture` models to Xcore:
  - Disabled 'Operation Reflection'.
  - Regenerate edit and editor plugins.
* [Mod] `HTaskGraph` model changes:
  - Move `ProcessingResource`s reference to `TaskNode`.
  - Add some new operations.
  - `CommunicationNode` contains a list of `HTGCom` instead of a `CompositeBlock`.
  - `HTGCom` is no longer a `Block`.
* [Mod] changes to `HTGExtraction`:
  - Merging dependencies with the same use between 2 tasks into a unique dependency is now enabled by default.
  - Skip *wrapper* `CompositeBlocks and *dummy* `BasicBlock`s added by ComputeSSA:
    Dummy BasicBlocks are now considered as intrinsic part of the Control Task to which they are associated.
  - Cleanup/fix support for processing resource pragmas.
* [Mod] Changes to `Parallel` model:
  - `Receive` can now have multiple sends (used to model a receive from potentially mutiple senders).
* [Mod] Minor modifications of HTG/parallel dotty exporters.
* [Fix] HTG/parallel communications placement (using full control tasks duplication):
  - All control tasks are now duplicated in HTG on the communicating processing resources.
  - `HTGToParallelModelConvertor` now creates parallel communications by visiting HTG CommunicationNodes (now treated more like tasks).
* [Fix] MPI code generator send/recv order:
  - Use a unique MPI TAG for each send/recvs pair (with standard-mode, blocking MPI_Send/MPI_recv).
* [Fix] dangling references in `ParallelApplication` to objects in HTG (e.g. Symbols/Types):
  - This is a temporary fix by adding HTG to the containment structure of the ParallelApp via a annotation.
* [Fix] dangling references in `HTaskGraph` to `Architecture` (PrecessingResource):
  - This is a temporary fix by adding the Architecture to the containment structure of HTG

---

