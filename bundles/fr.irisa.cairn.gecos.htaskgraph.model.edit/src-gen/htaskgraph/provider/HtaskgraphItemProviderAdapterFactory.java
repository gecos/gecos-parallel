/**
 */
package htaskgraph.provider;

import htaskgraph.util.HtaskgraphAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HtaskgraphItemProviderAdapterFactory extends HtaskgraphAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.TaskDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskDependencyItemProvider taskDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.TaskDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTaskDependencyAdapter() {
		if (taskDependencyItemProvider == null) {
			taskDependencyItemProvider = new TaskDependencyItemProvider(this);
		}

		return taskDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.GlueTaskDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GlueTaskDependencyItemProvider glueTaskDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.GlueTaskDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGlueTaskDependencyAdapter() {
		if (glueTaskDependencyItemProvider == null) {
			glueTaskDependencyItemProvider = new GlueTaskDependencyItemProvider(this);
		}

		return glueTaskDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.TaskOutputDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskOutputDependencyItemProvider taskOutputDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.TaskOutputDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTaskOutputDependencyAdapter() {
		if (taskOutputDependencyItemProvider == null) {
			taskOutputDependencyItemProvider = new TaskOutputDependencyItemProvider(this);
		}

		return taskOutputDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.TaskAntiDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskAntiDependencyItemProvider taskAntiDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.TaskAntiDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTaskAntiDependencyAdapter() {
		if (taskAntiDependencyItemProvider == null) {
			taskAntiDependencyItemProvider = new TaskAntiDependencyItemProvider(this);
		}

		return taskAntiDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.TaskTrueDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskTrueDependencyItemProvider taskTrueDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.TaskTrueDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTaskTrueDependencyAdapter() {
		if (taskTrueDependencyItemProvider == null) {
			taskTrueDependencyItemProvider = new TaskTrueDependencyItemProvider(this);
		}

		return taskTrueDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.TaskControlDependency} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskControlDependencyItemProvider taskControlDependencyItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.TaskControlDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTaskControlDependencyAdapter() {
		if (taskControlDependencyItemProvider == null) {
			taskControlDependencyItemProvider = new TaskControlDependencyItemProvider(this);
		}

		return taskControlDependencyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.CommunicationNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationNodeItemProvider communicationNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.CommunicationNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCommunicationNodeAdapter() {
		if (communicationNodeItemProvider == null) {
			communicationNodeItemProvider = new CommunicationNodeItemProvider(this);
		}

		return communicationNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.LeafTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafTaskNodeItemProvider leafTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.LeafTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLeafTaskNodeAdapter() {
		if (leafTaskNodeItemProvider == null) {
			leafTaskNodeItemProvider = new LeafTaskNodeItemProvider(this);
		}

		return leafTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.BasicLeafTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicLeafTaskNodeItemProvider basicLeafTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.BasicLeafTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBasicLeafTaskNodeAdapter() {
		if (basicLeafTaskNodeItemProvider == null) {
			basicLeafTaskNodeItemProvider = new BasicLeafTaskNodeItemProvider(this);
		}

		return basicLeafTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.ClusterTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClusterTaskNodeItemProvider clusterTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.ClusterTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createClusterTaskNodeAdapter() {
		if (clusterTaskNodeItemProvider == null) {
			clusterTaskNodeItemProvider = new ClusterTaskNodeItemProvider(this);
		}

		return clusterTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.IfTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfTaskNodeItemProvider ifTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.IfTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createIfTaskNodeAdapter() {
		if (ifTaskNodeItemProvider == null) {
			ifTaskNodeItemProvider = new IfTaskNodeItemProvider(this);
		}

		return ifTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.ForTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForTaskNodeItemProvider forTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.ForTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createForTaskNodeAdapter() {
		if (forTaskNodeItemProvider == null) {
			forTaskNodeItemProvider = new ForTaskNodeItemProvider(this);
		}

		return forTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.WhileTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhileTaskNodeItemProvider whileTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.WhileTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWhileTaskNodeAdapter() {
		if (whileTaskNodeItemProvider == null) {
			whileTaskNodeItemProvider = new WhileTaskNodeItemProvider(this);
		}

		return whileTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.ProcedureTaskNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureTaskNodeItemProvider procedureTaskNodeItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.ProcedureTaskNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createProcedureTaskNodeAdapter() {
		if (procedureTaskNodeItemProvider == null) {
			procedureTaskNodeItemProvider = new ProcedureTaskNodeItemProvider(this);
		}

		return procedureTaskNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.CoreLevelWCET} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoreLevelWCETItemProvider coreLevelWCETItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.CoreLevelWCET}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCoreLevelWCETAdapter() {
		if (coreLevelWCETItemProvider == null) {
			coreLevelWCETItemProvider = new CoreLevelWCETItemProvider(this);
		}

		return coreLevelWCETItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.WCETInformation} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WCETInformationItemProvider wcetInformationItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.WCETInformation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWCETInformationAdapter() {
		if (wcetInformationItemProvider == null) {
			wcetInformationItemProvider = new WCETInformationItemProvider(this);
		}

		return wcetInformationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.HTGSend} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HTGSendItemProvider htgSendItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.HTGSend}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createHTGSendAdapter() {
		if (htgSendItemProvider == null) {
			htgSendItemProvider = new HTGSendItemProvider(this);
		}

		return htgSendItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link htaskgraph.HTGRecv} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HTGRecvItemProvider htgRecvItemProvider;

	/**
	 * This creates an adapter for a {@link htaskgraph.HTGRecv}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createHTGRecvAdapter() {
		if (htgRecvItemProvider == null) {
			htgRecvItemProvider = new HTGRecvItemProvider(this);
		}

		return htgRecvItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (taskDependencyItemProvider != null) taskDependencyItemProvider.dispose();
		if (glueTaskDependencyItemProvider != null) glueTaskDependencyItemProvider.dispose();
		if (taskOutputDependencyItemProvider != null) taskOutputDependencyItemProvider.dispose();
		if (taskAntiDependencyItemProvider != null) taskAntiDependencyItemProvider.dispose();
		if (taskTrueDependencyItemProvider != null) taskTrueDependencyItemProvider.dispose();
		if (taskControlDependencyItemProvider != null) taskControlDependencyItemProvider.dispose();
		if (communicationNodeItemProvider != null) communicationNodeItemProvider.dispose();
		if (leafTaskNodeItemProvider != null) leafTaskNodeItemProvider.dispose();
		if (basicLeafTaskNodeItemProvider != null) basicLeafTaskNodeItemProvider.dispose();
		if (clusterTaskNodeItemProvider != null) clusterTaskNodeItemProvider.dispose();
		if (ifTaskNodeItemProvider != null) ifTaskNodeItemProvider.dispose();
		if (forTaskNodeItemProvider != null) forTaskNodeItemProvider.dispose();
		if (whileTaskNodeItemProvider != null) whileTaskNodeItemProvider.dispose();
		if (procedureTaskNodeItemProvider != null) procedureTaskNodeItemProvider.dispose();
		if (coreLevelWCETItemProvider != null) coreLevelWCETItemProvider.dispose();
		if (wcetInformationItemProvider != null) wcetInformationItemProvider.dispose();
		if (htgSendItemProvider != null) htgSendItemProvider.dispose();
		if (htgRecvItemProvider != null) htgRecvItemProvider.dispose();
	}

}
