/**
 */
package htaskgraph.provider;


import gecos.blocks.BlocksFactory;

import htaskgraph.HtaskgraphFactory;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.LeafTaskNode;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link htaskgraph.LeafTaskNode} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LeafTaskNodeItemProvider extends TaskNodeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafTaskNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LeafTaskNode.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LeafTaskNode"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		LeafTaskNode leafTaskNode = (LeafTaskNode)object;
		return getString("_UI_LeafTaskNode_type") + " " + leafTaskNode.getNumber();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LeafTaskNode.class)) {
			case HtaskgraphPackage.LEAF_TASK_NODE__BLOCK:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createCommunicationNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createLeafTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createBasicLeafTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createClusterTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createIfTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createForTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createWhileTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createProcedureTaskNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createHTGSend()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 HtaskgraphFactory.eINSTANCE.createHTGRecv()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createBasicBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createCompositeBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createIfBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createWhileBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createDoWhileBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createForBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createForC99Block()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createSimpleForBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createSwitchBlock()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK,
				 BlocksFactory.eINSTANCE.createCaseBlock()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == HtaskgraphPackage.Literals.TASK_NODE__INPUT_NODE ||
			childFeature == HtaskgraphPackage.Literals.TASK_NODE__OUTPUT_NODE ||
			childFeature == HtaskgraphPackage.Literals.LEAF_TASK_NODE__BLOCK;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
