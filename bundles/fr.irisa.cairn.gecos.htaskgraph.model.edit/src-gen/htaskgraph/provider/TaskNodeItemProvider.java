/**
 */
package htaskgraph.provider;


import gecos.annotations.AnnotationsFactory;
import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import htaskgraph.HtaskgraphFactory;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskNode;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link htaskgraph.TaskNode} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TaskNodeItemProvider extends ItemProviderAdapter {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addParentPropertyDescriptor(object);
			addNumberPropertyDescriptor(object);
			addDepthPropertyDescriptor(object);
			addSuccsPropertyDescriptor(object);
			addProcessingResourcesPropertyDescriptor(object);
			addMemoryResourcesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Parent feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_parent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_parent_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__PARENT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_number_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_number_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__NUMBER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Depth feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDepthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TaskNode_depth_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TaskNode_depth_feature", "_UI_TaskNode_type"),
				 HtaskgraphPackage.Literals.TASK_NODE__DEPTH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Succs feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuccsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TaskNode_succs_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TaskNode_succs_feature", "_UI_TaskNode_type"),
				 HtaskgraphPackage.Literals.TASK_NODE__SUCCS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Processing Resources feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessingResourcesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TaskNode_processingResources_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TaskNode_processingResources_feature", "_UI_TaskNode_type"),
				 HtaskgraphPackage.Literals.TASK_NODE__PROCESSING_RESOURCES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Memory Resources feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMemoryResourcesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TaskNode_memoryResources_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TaskNode_memoryResources_feature", "_UI_TaskNode_type"),
				 HtaskgraphPackage.Literals.TASK_NODE__MEMORY_RESOURCES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AnnotationsPackage.Literals.ANNOTATED_ELEMENT__ANNOTATIONS);
			childrenFeatures.add(HtaskgraphPackage.Literals.TASK_NODE__PREDS);
			childrenFeatures.add(HtaskgraphPackage.Literals.TASK_NODE__INPUT_NODE);
			childrenFeatures.add(HtaskgraphPackage.Literals.TASK_NODE__OUTPUT_NODE);
			childrenFeatures.add(HtaskgraphPackage.Literals.TASK_NODE__PROFILING_INFORMATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		TaskNode taskNode = (TaskNode)object;
		return getString("_UI_TaskNode_type") + " " + taskNode.getNumber();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TaskNode.class)) {
			case HtaskgraphPackage.TASK_NODE__NUMBER:
			case HtaskgraphPackage.TASK_NODE__DEPTH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case HtaskgraphPackage.TASK_NODE__ANNOTATIONS:
			case HtaskgraphPackage.TASK_NODE__PREDS:
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(AnnotationsPackage.Literals.ANNOTATED_ELEMENT__ANNOTATIONS,
				 AnnotationsFactory.eINSTANCE.create(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createTaskDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createGlueTaskDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createTaskOutputDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createTaskAntiDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createTaskTrueDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PREDS,
				 HtaskgraphFactory.eINSTANCE.createTaskControlDependency()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__INPUT_NODE,
				 HtaskgraphFactory.eINSTANCE.createCommunicationNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__OUTPUT_NODE,
				 HtaskgraphFactory.eINSTANCE.createCommunicationNode()));

		newChildDescriptors.add
			(createChildParameter
				(HtaskgraphPackage.Literals.TASK_NODE__PROFILING_INFORMATION,
				 HtaskgraphFactory.eINSTANCE.createWCETInformation()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == HtaskgraphPackage.Literals.TASK_NODE__INPUT_NODE ||
			childFeature == HtaskgraphPackage.Literals.TASK_NODE__OUTPUT_NODE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return HtaskgraphEditPlugin.INSTANCE;
	}

}
