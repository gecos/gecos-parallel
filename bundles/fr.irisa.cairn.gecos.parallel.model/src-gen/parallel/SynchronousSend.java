/**
 */
package parallel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronous Send</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getSynchronousSend()
 * @model
 * @generated
 */
public interface SynchronousSend extends SendCommunication, ParallelModelVisitable {
} // SynchronousSend
