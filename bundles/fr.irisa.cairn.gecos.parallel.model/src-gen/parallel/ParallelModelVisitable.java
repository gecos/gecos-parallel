/**
 */
package parallel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getParallelModelVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ParallelModelVisitable extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(ParallelModelVisitor visitor);

} // ParallelModelVisitable
