/**
 */
package parallel;

import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksVisitor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getCommunication()
 * @model abstract="true"
 * @generated
 */
public interface Communication extends BasicBlock, ParallelModelVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.ProcedureSet%&gt; _containingProcedureSet = this.getContainingProcedureSet();\nreturn ((&lt;%parallel.Process%&gt;) _containingProcedureSet);'"
	 * @generated
	 */
	parallel.Process getContiningProcess();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((visitor instanceof &lt;%parallel.ParallelModelVisitor%&gt;))\n{\n\tthis.accept(((&lt;%parallel.ParallelModelVisitor%&gt;) visitor));\n}\nelse\n{\n\tfinal &lt;%parallel.ParallelModelVisitor%&gt; adapter = &lt;%fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorsRegistry%&gt;.INSTANCE.&lt;&lt;%parallel.ParallelModelVisitor%&gt;, &lt;%gecos.blocks.BlocksVisitor%&gt;&gt;getBlocksAdapter(visitor, &lt;%parallel.ParallelModelVisitor%&gt;.class);\n\tif ((adapter == null))\n\t{\n\t\tsuper.accept(visitor);\n\t}\n\telse\n\t{\n\t\tthis.accept(adapter);\n\t}\n}'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // Communication
