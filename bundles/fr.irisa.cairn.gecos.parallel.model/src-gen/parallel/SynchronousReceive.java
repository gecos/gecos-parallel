/**
 */
package parallel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronous Receive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getSynchronousReceive()
 * @model
 * @generated
 */
public interface SynchronousReceive extends ReceiveCommunication, ParallelModelVisitable {
} // SynchronousReceive
