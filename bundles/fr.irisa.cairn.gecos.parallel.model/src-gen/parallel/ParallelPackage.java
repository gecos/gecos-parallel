/**
 */
package parallel;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see parallel.ParallelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false'"
 * @generated
 */
public interface ParallelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "parallel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/parallel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "parallel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ParallelPackage eINSTANCE = parallel.impl.ParallelPackageImpl.init();

	/**
	 * The meta object id for the '{@link parallel.ParallelModelVisitor <em>Model Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.ParallelModelVisitor
	 * @see parallel.impl.ParallelPackageImpl#getParallelModelVisitor()
	 * @generated
	 */
	int PARALLEL_MODEL_VISITOR = 0;

	/**
	 * The number of structural features of the '<em>Model Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_MODEL_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link parallel.ParallelModelVisitable <em>Model Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.ParallelModelVisitable
	 * @see parallel.impl.ParallelPackageImpl#getParallelModelVisitable()
	 * @generated
	 */
	int PARALLEL_MODEL_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Model Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_MODEL_VISITABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link parallel.impl.ParallelApplicationImpl <em>Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.ParallelApplicationImpl
	 * @see parallel.impl.ParallelPackageImpl#getParallelApplication()
	 * @generated
	 */
	int PARALLEL_APPLICATION = 2;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_APPLICATION__SCOPE = CorePackage.PROCEDURE_SET__SCOPE;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_APPLICATION__ANNOTATIONS = CorePackage.PROCEDURE_SET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_APPLICATION__USES = CorePackage.PROCEDURE_SET__USES;

	/**
	 * The feature id for the '<em><b>Processes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_APPLICATION__PROCESSES = CorePackage.PROCEDURE_SET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_APPLICATION_FEATURE_COUNT = CorePackage.PROCEDURE_SET_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link parallel.impl.ProcessImpl <em>Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.ProcessImpl
	 * @see parallel.impl.ParallelPackageImpl#getProcess()
	 * @generated
	 */
	int PROCESS = 3;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__SCOPE = CorePackage.PROCEDURE_SET__SCOPE;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__ANNOTATIONS = CorePackage.PROCEDURE_SET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__USES = CorePackage.PROCEDURE_SET__USES;

	/**
	 * The feature id for the '<em><b>Process Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__PROCESS_RESOURCE = CorePackage.PROCEDURE_SET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__RANK = CorePackage.PROCEDURE_SET_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_FEATURE_COUNT = CorePackage.PROCEDURE_SET_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link parallel.impl.CommunicationImpl <em>Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.CommunicationImpl
	 * @see parallel.impl.ParallelPackageImpl#getCommunication()
	 * @generated
	 */
	int COMMUNICATION = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__ANNOTATIONS = BlocksPackage.BASIC_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PARENT = BlocksPackage.BASIC_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__NUMBER = BlocksPackage.BASIC_BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__INSTRUCTIONS = BlocksPackage.BASIC_BLOCK__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__DEF_EDGES = BlocksPackage.BASIC_BLOCK__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__OUT_EDGES = BlocksPackage.BASIC_BLOCK__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__USE_EDGES = BlocksPackage.BASIC_BLOCK__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__IN_EDGES = BlocksPackage.BASIC_BLOCK__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__LABEL = BlocksPackage.BASIC_BLOCK__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__FLAGS = BlocksPackage.BASIC_BLOCK__FLAGS;

	/**
	 * The number of structural features of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_FEATURE_COUNT = BlocksPackage.BASIC_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link parallel.impl.SendImpl <em>Send</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.SendImpl
	 * @see parallel.impl.ParallelPackageImpl#getSend()
	 * @generated
	 */
	int SEND = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__ANNOTATIONS = COMMUNICATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__PARENT = COMMUNICATION__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__NUMBER = COMMUNICATION__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__INSTRUCTIONS = COMMUNICATION__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__DEF_EDGES = COMMUNICATION__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__OUT_EDGES = COMMUNICATION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__USE_EDGES = COMMUNICATION__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__IN_EDGES = COMMUNICATION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__LABEL = COMMUNICATION__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__FLAGS = COMMUNICATION__FLAGS;

	/**
	 * The feature id for the '<em><b>Receives</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__RECEIVES = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__MESSAGE = COMMUNICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comm Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND__COMM_TYPE = COMMUNICATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Send</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link parallel.impl.ReceiveImpl <em>Receive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.ReceiveImpl
	 * @see parallel.impl.ParallelPackageImpl#getReceive()
	 * @generated
	 */
	int RECEIVE = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__ANNOTATIONS = COMMUNICATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__PARENT = COMMUNICATION__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__NUMBER = COMMUNICATION__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__INSTRUCTIONS = COMMUNICATION__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__DEF_EDGES = COMMUNICATION__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__OUT_EDGES = COMMUNICATION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__USE_EDGES = COMMUNICATION__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__IN_EDGES = COMMUNICATION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__LABEL = COMMUNICATION__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__FLAGS = COMMUNICATION__FLAGS;

	/**
	 * The feature id for the '<em><b>Sends</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__SENDS = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__MESSAGE = COMMUNICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comm Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE__COMM_TYPE = COMMUNICATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Receive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link parallel.impl.SendReceiveImpl <em>Send Receive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.SendReceiveImpl
	 * @see parallel.impl.ParallelPackageImpl#getSendReceive()
	 * @generated
	 */
	int SEND_RECEIVE = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__ANNOTATIONS = COMMUNICATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__PARENT = COMMUNICATION__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__NUMBER = COMMUNICATION__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__INSTRUCTIONS = COMMUNICATION__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__DEF_EDGES = COMMUNICATION__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__OUT_EDGES = COMMUNICATION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__USE_EDGES = COMMUNICATION__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__IN_EDGES = COMMUNICATION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__LABEL = COMMUNICATION__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE__FLAGS = COMMUNICATION__FLAGS;

	/**
	 * The number of structural features of the '<em>Send Receive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_RECEIVE_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link parallel.impl.MessageImpl <em>Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.impl.MessageImpl
	 * @see parallel.impl.ParallelPackageImpl#getMessage()
	 * @generated
	 */
	int MESSAGE = 8;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ADDRESS = 0;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TAG = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TYPE = 2;

	/**
	 * The feature id for the '<em><b>Count</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__COUNT = 3;

	/**
	 * The number of structural features of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link parallel.CommType <em>Comm Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see parallel.CommType
	 * @see parallel.impl.ParallelPackageImpl#getCommType()
	 * @generated
	 */
	int COMM_TYPE = 9;


	/**
	 * Returns the meta object for class '{@link parallel.ParallelModelVisitor <em>Model Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Visitor</em>'.
	 * @see parallel.ParallelModelVisitor
	 * @generated
	 */
	EClass getParallelModelVisitor();

	/**
	 * Returns the meta object for class '{@link parallel.ParallelModelVisitable <em>Model Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Visitable</em>'.
	 * @see parallel.ParallelModelVisitable
	 * @generated
	 */
	EClass getParallelModelVisitable();

	/**
	 * Returns the meta object for class '{@link parallel.ParallelApplication <em>Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application</em>'.
	 * @see parallel.ParallelApplication
	 * @generated
	 */
	EClass getParallelApplication();

	/**
	 * Returns the meta object for the containment reference list '{@link parallel.ParallelApplication#getProcesses <em>Processes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Processes</em>'.
	 * @see parallel.ParallelApplication#getProcesses()
	 * @see #getParallelApplication()
	 * @generated
	 */
	EReference getParallelApplication_Processes();

	/**
	 * Returns the meta object for class '{@link parallel.Process <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process</em>'.
	 * @see parallel.Process
	 * @generated
	 */
	EClass getProcess();

	/**
	 * Returns the meta object for the reference '{@link parallel.Process#getProcessResource <em>Process Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process Resource</em>'.
	 * @see parallel.Process#getProcessResource()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_ProcessResource();

	/**
	 * Returns the meta object for the attribute '{@link parallel.Process#getRank <em>Rank</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rank</em>'.
	 * @see parallel.Process#getRank()
	 * @see #getProcess()
	 * @generated
	 */
	EAttribute getProcess_Rank();

	/**
	 * Returns the meta object for class '{@link parallel.Communication <em>Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication</em>'.
	 * @see parallel.Communication
	 * @generated
	 */
	EClass getCommunication();

	/**
	 * Returns the meta object for class '{@link parallel.Send <em>Send</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Send</em>'.
	 * @see parallel.Send
	 * @generated
	 */
	EClass getSend();

	/**
	 * Returns the meta object for the reference list '{@link parallel.Send#getReceives <em>Receives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Receives</em>'.
	 * @see parallel.Send#getReceives()
	 * @see #getSend()
	 * @generated
	 */
	EReference getSend_Receives();

	/**
	 * Returns the meta object for the containment reference '{@link parallel.Send#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Message</em>'.
	 * @see parallel.Send#getMessage()
	 * @see #getSend()
	 * @generated
	 */
	EReference getSend_Message();

	/**
	 * Returns the meta object for the attribute '{@link parallel.Send#getCommType <em>Comm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comm Type</em>'.
	 * @see parallel.Send#getCommType()
	 * @see #getSend()
	 * @generated
	 */
	EAttribute getSend_CommType();

	/**
	 * Returns the meta object for class '{@link parallel.Receive <em>Receive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receive</em>'.
	 * @see parallel.Receive
	 * @generated
	 */
	EClass getReceive();

	/**
	 * Returns the meta object for the reference list '{@link parallel.Receive#getSends <em>Sends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sends</em>'.
	 * @see parallel.Receive#getSends()
	 * @see #getReceive()
	 * @generated
	 */
	EReference getReceive_Sends();

	/**
	 * Returns the meta object for the containment reference '{@link parallel.Receive#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Message</em>'.
	 * @see parallel.Receive#getMessage()
	 * @see #getReceive()
	 * @generated
	 */
	EReference getReceive_Message();

	/**
	 * Returns the meta object for the attribute '{@link parallel.Receive#getCommType <em>Comm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comm Type</em>'.
	 * @see parallel.Receive#getCommType()
	 * @see #getReceive()
	 * @generated
	 */
	EAttribute getReceive_CommType();

	/**
	 * Returns the meta object for class '{@link parallel.SendReceive <em>Send Receive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Send Receive</em>'.
	 * @see parallel.SendReceive
	 * @generated
	 */
	EClass getSendReceive();

	/**
	 * Returns the meta object for class '{@link parallel.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message</em>'.
	 * @see parallel.Message
	 * @generated
	 */
	EClass getMessage();

	/**
	 * Returns the meta object for the containment reference '{@link parallel.Message#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Address</em>'.
	 * @see parallel.Message#getAddress()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Address();

	/**
	 * Returns the meta object for the attribute '{@link parallel.Message#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag</em>'.
	 * @see parallel.Message#getTag()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Tag();

	/**
	 * Returns the meta object for the reference '{@link parallel.Message#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see parallel.Message#getType()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Type();

	/**
	 * Returns the meta object for the containment reference '{@link parallel.Message#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Count</em>'.
	 * @see parallel.Message#getCount()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Count();

	/**
	 * Returns the meta object for enum '{@link parallel.CommType <em>Comm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comm Type</em>'.
	 * @see parallel.CommType
	 * @generated
	 */
	EEnum getCommType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ParallelFactory getParallelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link parallel.ParallelModelVisitor <em>Model Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.ParallelModelVisitor
		 * @see parallel.impl.ParallelPackageImpl#getParallelModelVisitor()
		 * @generated
		 */
		EClass PARALLEL_MODEL_VISITOR = eINSTANCE.getParallelModelVisitor();

		/**
		 * The meta object literal for the '{@link parallel.ParallelModelVisitable <em>Model Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.ParallelModelVisitable
		 * @see parallel.impl.ParallelPackageImpl#getParallelModelVisitable()
		 * @generated
		 */
		EClass PARALLEL_MODEL_VISITABLE = eINSTANCE.getParallelModelVisitable();

		/**
		 * The meta object literal for the '{@link parallel.impl.ParallelApplicationImpl <em>Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.ParallelApplicationImpl
		 * @see parallel.impl.ParallelPackageImpl#getParallelApplication()
		 * @generated
		 */
		EClass PARALLEL_APPLICATION = eINSTANCE.getParallelApplication();

		/**
		 * The meta object literal for the '<em><b>Processes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_APPLICATION__PROCESSES = eINSTANCE.getParallelApplication_Processes();

		/**
		 * The meta object literal for the '{@link parallel.impl.ProcessImpl <em>Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.ProcessImpl
		 * @see parallel.impl.ParallelPackageImpl#getProcess()
		 * @generated
		 */
		EClass PROCESS = eINSTANCE.getProcess();

		/**
		 * The meta object literal for the '<em><b>Process Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__PROCESS_RESOURCE = eINSTANCE.getProcess_ProcessResource();

		/**
		 * The meta object literal for the '<em><b>Rank</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS__RANK = eINSTANCE.getProcess_Rank();

		/**
		 * The meta object literal for the '{@link parallel.impl.CommunicationImpl <em>Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.CommunicationImpl
		 * @see parallel.impl.ParallelPackageImpl#getCommunication()
		 * @generated
		 */
		EClass COMMUNICATION = eINSTANCE.getCommunication();

		/**
		 * The meta object literal for the '{@link parallel.impl.SendImpl <em>Send</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.SendImpl
		 * @see parallel.impl.ParallelPackageImpl#getSend()
		 * @generated
		 */
		EClass SEND = eINSTANCE.getSend();

		/**
		 * The meta object literal for the '<em><b>Receives</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEND__RECEIVES = eINSTANCE.getSend_Receives();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEND__MESSAGE = eINSTANCE.getSend_Message();

		/**
		 * The meta object literal for the '<em><b>Comm Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEND__COMM_TYPE = eINSTANCE.getSend_CommType();

		/**
		 * The meta object literal for the '{@link parallel.impl.ReceiveImpl <em>Receive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.ReceiveImpl
		 * @see parallel.impl.ParallelPackageImpl#getReceive()
		 * @generated
		 */
		EClass RECEIVE = eINSTANCE.getReceive();

		/**
		 * The meta object literal for the '<em><b>Sends</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEIVE__SENDS = eINSTANCE.getReceive_Sends();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEIVE__MESSAGE = eINSTANCE.getReceive_Message();

		/**
		 * The meta object literal for the '<em><b>Comm Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECEIVE__COMM_TYPE = eINSTANCE.getReceive_CommType();

		/**
		 * The meta object literal for the '{@link parallel.impl.SendReceiveImpl <em>Send Receive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.SendReceiveImpl
		 * @see parallel.impl.ParallelPackageImpl#getSendReceive()
		 * @generated
		 */
		EClass SEND_RECEIVE = eINSTANCE.getSendReceive();

		/**
		 * The meta object literal for the '{@link parallel.impl.MessageImpl <em>Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.impl.MessageImpl
		 * @see parallel.impl.ParallelPackageImpl#getMessage()
		 * @generated
		 */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__ADDRESS = eINSTANCE.getMessage_Address();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__TAG = eINSTANCE.getMessage_Tag();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__TYPE = eINSTANCE.getMessage_Type();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__COUNT = eINSTANCE.getMessage_Count();

		/**
		 * The meta object literal for the '{@link parallel.CommType <em>Comm Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see parallel.CommType
		 * @see parallel.impl.ParallelPackageImpl#getCommType()
		 * @generated
		 */
		EEnum COMM_TYPE = eINSTANCE.getCommType();

	}

} //ParallelPackage
