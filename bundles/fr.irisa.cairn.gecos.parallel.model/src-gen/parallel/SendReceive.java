/**
 */
package parallel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Receive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getSendReceive()
 * @model
 * @generated
 */
public interface SendReceive extends Communication, ParallelModelVisitable {
} // SendReceive
