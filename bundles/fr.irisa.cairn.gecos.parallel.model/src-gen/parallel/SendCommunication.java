/**
 */
package parallel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.SendCommunication#getReceivers <em>Receivers</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getSendCommunication()
 * @model abstract="true"
 * @generated
 */
public interface SendCommunication extends ICommunicationUnit, ParallelModelVisitable {
	/**
	 * Returns the value of the '<em><b>Receivers</b></em>' reference list.
	 * The list contents are of type {@link parallel.ReceiveCommunication}.
	 * It is bidirectional and its opposite is '{@link parallel.ReceiveCommunication#getSenders <em>Senders</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receivers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receivers</em>' reference list.
	 * @see parallel.ParallelPackage#getSendCommunication_Receivers()
	 * @see parallel.ReceiveCommunication#getSenders
	 * @model opposite="senders"
	 * @generated
	 */
	EList<ReceiveCommunication> getReceivers();

} // SendCommunication
