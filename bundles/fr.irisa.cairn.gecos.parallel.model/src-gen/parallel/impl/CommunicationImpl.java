/**
 */
package parallel.impl;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorsRegistry;

import gecos.blocks.BlocksVisitor;

import gecos.blocks.impl.BasicBlockImpl;

import gecos.core.ProcedureSet;

import org.eclipse.emf.ecore.EClass;

import parallel.Communication;
import parallel.ParallelModelVisitor;
import parallel.ParallelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class CommunicationImpl extends BasicBlockImpl implements Communication {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ParallelPackage.Literals.COMMUNICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public parallel.Process getContiningProcess() {
		ProcedureSet _containingProcedureSet = this.getContainingProcedureSet();
		return ((parallel.Process) _containingProcedureSet);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		if ((visitor instanceof ParallelModelVisitor)) {
			this.accept(((ParallelModelVisitor) visitor));
		}
		else {
			final ParallelModelVisitor adapter = GecosVisitorsRegistry.INSTANCE.<ParallelModelVisitor, BlocksVisitor>getBlocksAdapter(visitor, ParallelModelVisitor.class);
			if ((adapter == null)) {
				super.accept(visitor);
			}
			else {
				this.accept(adapter);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(ParallelModelVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} //CommunicationImpl
