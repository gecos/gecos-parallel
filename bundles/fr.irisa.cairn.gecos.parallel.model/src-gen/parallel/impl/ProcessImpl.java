/**
 */
package parallel.impl;

import architecture.ProcessingResource;

import gecos.core.impl.ProcedureSetImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import parallel.ParallelModelVisitor;
import parallel.ParallelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link parallel.impl.ProcessImpl#getProcessResource <em>Process Resource</em>}</li>
 *   <li>{@link parallel.impl.ProcessImpl#getRank <em>Rank</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessImpl extends ProcedureSetImpl implements parallel.Process {
	/**
	 * The cached value of the '{@link #getProcessResource() <em>Process Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessResource()
	 * @generated
	 * @ordered
	 */
	protected ProcessingResource processResource;

	/**
	 * The default value of the '{@link #getRank() <em>Rank</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRank()
	 * @generated
	 * @ordered
	 */
	protected static final int RANK_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRank() <em>Rank</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRank()
	 * @generated
	 * @ordered
	 */
	protected int rank = RANK_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ParallelPackage.Literals.PROCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessingResource getProcessResource() {
		if (processResource != null && processResource.eIsProxy()) {
			InternalEObject oldProcessResource = (InternalEObject)processResource;
			processResource = (ProcessingResource)eResolveProxy(oldProcessResource);
			if (processResource != oldProcessResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ParallelPackage.PROCESS__PROCESS_RESOURCE, oldProcessResource, processResource));
			}
		}
		return processResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessingResource basicGetProcessResource() {
		return processResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessResource(ProcessingResource newProcessResource) {
		ProcessingResource oldProcessResource = processResource;
		processResource = newProcessResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ParallelPackage.PROCESS__PROCESS_RESOURCE, oldProcessResource, processResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRank(int newRank) {
		int oldRank = rank;
		rank = newRank;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ParallelPackage.PROCESS__RANK, oldRank, rank));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final ParallelModelVisitor visitor) {
		visitor.visitProcess(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _rank = this.getRank();
		return ("Process" + Integer.valueOf(_rank));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ParallelPackage.PROCESS__PROCESS_RESOURCE:
				if (resolve) return getProcessResource();
				return basicGetProcessResource();
			case ParallelPackage.PROCESS__RANK:
				return getRank();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ParallelPackage.PROCESS__PROCESS_RESOURCE:
				setProcessResource((ProcessingResource)newValue);
				return;
			case ParallelPackage.PROCESS__RANK:
				setRank((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ParallelPackage.PROCESS__PROCESS_RESOURCE:
				setProcessResource((ProcessingResource)null);
				return;
			case ParallelPackage.PROCESS__RANK:
				setRank(RANK_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ParallelPackage.PROCESS__PROCESS_RESOURCE:
				return processResource != null;
			case ParallelPackage.PROCESS__RANK:
				return rank != RANK_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //ProcessImpl
