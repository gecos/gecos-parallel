/**
 */
package parallel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import parallel.CommType;
import parallel.Message;
import parallel.ParallelModelVisitor;
import parallel.ParallelPackage;
import parallel.Receive;
import parallel.Send;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Send</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link parallel.impl.SendImpl#getReceives <em>Receives</em>}</li>
 *   <li>{@link parallel.impl.SendImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link parallel.impl.SendImpl#getCommType <em>Comm Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SendImpl extends CommunicationImpl implements Send {
	/**
	 * The cached value of the '{@link #getReceives() <em>Receives</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceives()
	 * @generated
	 * @ordered
	 */
	protected EList<Receive> receives;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected Message message;

	/**
	 * The default value of the '{@link #getCommType() <em>Comm Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommType()
	 * @generated
	 * @ordered
	 */
	protected static final CommType COMM_TYPE_EDEFAULT = CommType.SYNC;

	/**
	 * The cached value of the '{@link #getCommType() <em>Comm Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommType()
	 * @generated
	 * @ordered
	 */
	protected CommType commType = COMM_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ParallelPackage.Literals.SEND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Receive> getReceives() {
		if (receives == null) {
			receives = new EObjectWithInverseResolvingEList.ManyInverse<Receive>(Receive.class, this, ParallelPackage.SEND__RECEIVES, ParallelPackage.RECEIVE__SENDS);
		}
		return receives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMessage(Message newMessage, NotificationChain msgs) {
		Message oldMessage = message;
		message = newMessage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ParallelPackage.SEND__MESSAGE, oldMessage, newMessage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(Message newMessage) {
		if (newMessage != message) {
			NotificationChain msgs = null;
			if (message != null)
				msgs = ((InternalEObject)message).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ParallelPackage.SEND__MESSAGE, null, msgs);
			if (newMessage != null)
				msgs = ((InternalEObject)newMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ParallelPackage.SEND__MESSAGE, null, msgs);
			msgs = basicSetMessage(newMessage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ParallelPackage.SEND__MESSAGE, newMessage, newMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommType getCommType() {
		return commType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommType(CommType newCommType) {
		CommType oldCommType = commType;
		commType = newCommType == null ? COMM_TYPE_EDEFAULT : newCommType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ParallelPackage.SEND__COMM_TYPE, oldCommType, commType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<parallel.Process> getReceivingProcesses() {
		final Function1<Receive, parallel.Process> _function = new Function1<Receive, parallel.Process>() {
			public parallel.Process apply(final Receive it) {
				return it.getContiningProcess();
			}
		};
		return ECollections.<parallel.Process>unmodifiableEList(ECollections.<parallel.Process>asEList(XcoreEListExtensions.<Receive, parallel.Process>map(this.getReceives(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final ParallelModelVisitor visitor) {
		visitor.visitSend(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _number = this.getNumber();
		String _plus = ("Snd_" + Integer.valueOf(_number));
		String _plus_1 = (_plus + " ");
		Message _message = this.getMessage();
		String _plus_2 = (_plus_1 + _message);
		String _plus_3 = (_plus_2 + " from ");
		parallel.Process _continingProcess = this.getContiningProcess();
		return (_plus_3 + _continingProcess);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getReceives()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				return ((InternalEList<?>)getReceives()).basicRemove(otherEnd, msgs);
			case ParallelPackage.SEND__MESSAGE:
				return basicSetMessage(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				return getReceives();
			case ParallelPackage.SEND__MESSAGE:
				return getMessage();
			case ParallelPackage.SEND__COMM_TYPE:
				return getCommType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				getReceives().clear();
				getReceives().addAll((Collection<? extends Receive>)newValue);
				return;
			case ParallelPackage.SEND__MESSAGE:
				setMessage((Message)newValue);
				return;
			case ParallelPackage.SEND__COMM_TYPE:
				setCommType((CommType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				getReceives().clear();
				return;
			case ParallelPackage.SEND__MESSAGE:
				setMessage((Message)null);
				return;
			case ParallelPackage.SEND__COMM_TYPE:
				setCommType(COMM_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ParallelPackage.SEND__RECEIVES:
				return receives != null && !receives.isEmpty();
			case ParallelPackage.SEND__MESSAGE:
				return message != null;
			case ParallelPackage.SEND__COMM_TYPE:
				return commType != COMM_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //SendImpl
