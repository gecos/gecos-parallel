/**
 */
package parallel.impl;

import architecture.ArchitecturePackage;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import parallel.CommType;
import parallel.Communication;
import parallel.Message;
import parallel.ParallelApplication;
import parallel.ParallelFactory;
import parallel.ParallelModelVisitable;
import parallel.ParallelModelVisitor;
import parallel.ParallelPackage;
import parallel.Receive;
import parallel.Send;
import parallel.SendReceive;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ParallelPackageImpl extends EPackageImpl implements ParallelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parallelModelVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parallelModelVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parallelApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sendReceiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum commTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see parallel.ParallelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ParallelPackageImpl() {
		super(eNS_URI, ParallelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ParallelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ParallelPackage init() {
		if (isInited) return (ParallelPackage)EPackage.Registry.INSTANCE.getEPackage(ParallelPackage.eNS_URI);

		// Obtain or create and register package
		ParallelPackageImpl theParallelPackage = (ParallelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ParallelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ParallelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		ArchitecturePackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theParallelPackage.createPackageContents();

		// Initialize created meta-data
		theParallelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theParallelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ParallelPackage.eNS_URI, theParallelPackage);
		return theParallelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParallelModelVisitor() {
		return parallelModelVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParallelModelVisitable() {
		return parallelModelVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParallelApplication() {
		return parallelApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParallelApplication_Processes() {
		return (EReference)parallelApplicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcess() {
		return processEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcess_ProcessResource() {
		return (EReference)processEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcess_Rank() {
		return (EAttribute)processEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunication() {
		return communicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSend() {
		return sendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSend_Receives() {
		return (EReference)sendEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSend_Message() {
		return (EReference)sendEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSend_CommType() {
		return (EAttribute)sendEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceive() {
		return receiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceive_Sends() {
		return (EReference)receiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceive_Message() {
		return (EReference)receiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReceive_CommType() {
		return (EAttribute)receiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSendReceive() {
		return sendReceiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessage() {
		return messageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Address() {
		return (EReference)messageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessage_Tag() {
		return (EAttribute)messageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Type() {
		return (EReference)messageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Count() {
		return (EReference)messageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCommType() {
		return commTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParallelFactory getParallelFactory() {
		return (ParallelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		parallelModelVisitorEClass = createEClass(PARALLEL_MODEL_VISITOR);

		parallelModelVisitableEClass = createEClass(PARALLEL_MODEL_VISITABLE);

		parallelApplicationEClass = createEClass(PARALLEL_APPLICATION);
		createEReference(parallelApplicationEClass, PARALLEL_APPLICATION__PROCESSES);

		processEClass = createEClass(PROCESS);
		createEReference(processEClass, PROCESS__PROCESS_RESOURCE);
		createEAttribute(processEClass, PROCESS__RANK);

		communicationEClass = createEClass(COMMUNICATION);

		sendEClass = createEClass(SEND);
		createEReference(sendEClass, SEND__RECEIVES);
		createEReference(sendEClass, SEND__MESSAGE);
		createEAttribute(sendEClass, SEND__COMM_TYPE);

		receiveEClass = createEClass(RECEIVE);
		createEReference(receiveEClass, RECEIVE__SENDS);
		createEReference(receiveEClass, RECEIVE__MESSAGE);
		createEAttribute(receiveEClass, RECEIVE__COMM_TYPE);

		sendReceiveEClass = createEClass(SEND_RECEIVE);

		messageEClass = createEClass(MESSAGE);
		createEReference(messageEClass, MESSAGE__ADDRESS);
		createEAttribute(messageEClass, MESSAGE__TAG);
		createEReference(messageEClass, MESSAGE__TYPE);
		createEReference(messageEClass, MESSAGE__COUNT);

		// Create enums
		commTypeEEnum = createEEnum(COMM_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ArchitecturePackage theArchitecturePackage = (ArchitecturePackage)EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		parallelApplicationEClass.getESuperTypes().add(theCorePackage.getProcedureSet());
		parallelApplicationEClass.getESuperTypes().add(this.getParallelModelVisitable());
		processEClass.getESuperTypes().add(theCorePackage.getProcedureSet());
		processEClass.getESuperTypes().add(this.getParallelModelVisitable());
		communicationEClass.getESuperTypes().add(theBlocksPackage.getBasicBlock());
		communicationEClass.getESuperTypes().add(this.getParallelModelVisitable());
		sendEClass.getESuperTypes().add(this.getCommunication());
		sendEClass.getESuperTypes().add(this.getParallelModelVisitable());
		receiveEClass.getESuperTypes().add(this.getCommunication());
		receiveEClass.getESuperTypes().add(this.getParallelModelVisitable());
		sendReceiveEClass.getESuperTypes().add(this.getCommunication());
		sendReceiveEClass.getESuperTypes().add(this.getParallelModelVisitable());

		// Initialize classes and features; add operations and parameters
		initEClass(parallelModelVisitorEClass, ParallelModelVisitor.class, "ParallelModelVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(parallelModelVisitorEClass, null, "visitParallelApplication", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelApplication(), "application", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(parallelModelVisitorEClass, null, "visitProcess", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcess(), "process", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(parallelModelVisitorEClass, null, "visitSend", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSend(), "send", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(parallelModelVisitorEClass, null, "visitReceive", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getReceive(), "receive", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(parallelModelVisitableEClass, ParallelModelVisitable.class, "ParallelModelVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(parallelModelVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelModelVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(parallelApplicationEClass, ParallelApplication.class, "ParallelApplication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParallelApplication_Processes(), this.getProcess(), null, "processes", null, 0, -1, ParallelApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(parallelApplicationEClass, theCorePackage.getProcedure(), "listProcedures", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(parallelApplicationEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelModelVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(parallelApplicationEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(processEClass, parallel.Process.class, "Process", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcess_ProcessResource(), theArchitecturePackage.getProcessingResource(), null, "processResource", null, 0, 1, parallel.Process.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcess_Rank(), theEcorePackage.getEInt(), "rank", null, 0, 1, parallel.Process.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(processEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelModelVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(processEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(communicationEClass, Communication.class, "Communication", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(communicationEClass, this.getProcess(), "getContiningProcess", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(communicationEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sendEClass, Send.class, "Send", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSend_Receives(), this.getReceive(), this.getReceive_Sends(), "receives", null, 0, -1, Send.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSend_Message(), this.getMessage(), null, "message", null, 0, 1, Send.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSend_CommType(), this.getCommType(), "commType", "SYNC", 0, 1, Send.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(sendEClass, this.getProcess(), "getReceivingProcesses", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(sendEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelModelVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(sendEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(receiveEClass, Receive.class, "Receive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReceive_Sends(), this.getSend(), this.getSend_Receives(), "sends", null, 0, -1, Receive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReceive_Message(), this.getMessage(), null, "message", null, 0, 1, Receive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReceive_CommType(), this.getCommType(), "commType", "SYNC", 0, 1, Receive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(receiveEClass, this.getProcess(), "getSendingProcesses", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(receiveEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParallelModelVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(receiveEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sendReceiveEClass, SendReceive.class, "SendReceive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(messageEClass, Message.class, "Message", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessage_Address(), theInstrsPackage.getInstruction(), null, "address", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessage_Tag(), theEcorePackage.getEInt(), "tag", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Type(), theTypesPackage.getType(), null, "type", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Count(), theInstrsPackage.getInstruction(), null, "count", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(messageEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(commTypeEEnum, CommType.class, "CommType");
		addEEnumLiteral(commTypeEEnum, CommType.SYNC);
		addEEnumLiteral(commTypeEEnum, CommType.ASYNC);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //ParallelPackageImpl
