/**
 */
package parallel.impl;

import org.eclipse.emf.ecore.EClass;

import parallel.ParallelPackage;
import parallel.SendReceive;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Send Receive</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SendReceiveImpl extends CommunicationImpl implements SendReceive {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SendReceiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ParallelPackage.Literals.SEND_RECEIVE;
	}

} //SendReceiveImpl
