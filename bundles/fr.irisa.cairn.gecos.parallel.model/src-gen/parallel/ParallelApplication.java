/**
 */
package parallel;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.ParallelApplication#getProcesses <em>Processes</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getParallelApplication()
 * @model
 * @generated
 */
public interface ParallelApplication extends ProcedureSet, ParallelModelVisitable {
	/**
	 * Returns the value of the '<em><b>Processes</b></em>' containment reference list.
	 * The list contents are of type {@link parallel.Process}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processes</em>' containment reference list.
	 * @see parallel.ParallelPackage#getParallelApplication_Processes()
	 * @model containment="true"
	 * @generated
	 */
	EList<parallel.Process> getProcesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%parallel.Process%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%parallel.Process%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt; apply(final &lt;%parallel.Process%&gt; it)\n\t{\n\t\treturn it.listProcedures();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;asEList(((&lt;%gecos.core.Procedure%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%parallel.Process%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt;map(this.getProcesses(), _function)), &lt;%gecos.core.Procedure%&gt;.class)));'"
	 * @generated
	 */
	EList<Procedure> listProcedures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitParallelApplication(this);'"
	 * @generated
	 */
	void accept(ParallelModelVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return \"ParallelApp\";'"
	 * @generated
	 */
	String toString();

} // ParallelApplication
