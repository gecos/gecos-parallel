/**
 */
package parallel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getParallelModelVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ParallelModelVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model applicationUnique="false"
	 * @generated
	 */
	void visitParallelApplication(ParallelApplication application);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model processUnique="false"
	 * @generated
	 */
	void visitProcess(parallel.Process process);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sendUnique="false"
	 * @generated
	 */
	void visitSend(Send send);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model receiveUnique="false"
	 * @generated
	 */
	void visitReceive(Receive receive);

} // ParallelModelVisitor
