/**
 */
package parallel;

import architecture.ProcessingResource;

import gecos.core.ProcedureSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.Process#getProcessResource <em>Process Resource</em>}</li>
 *   <li>{@link parallel.Process#getRank <em>Rank</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getProcess()
 * @model
 * @generated
 */
public interface Process extends ProcedureSet, ParallelModelVisitable {
	/**
	 * Returns the value of the '<em><b>Process Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Resource</em>' reference.
	 * @see #setProcessResource(ProcessingResource)
	 * @see parallel.ParallelPackage#getProcess_ProcessResource()
	 * @model
	 * @generated
	 */
	ProcessingResource getProcessResource();

	/**
	 * Sets the value of the '{@link parallel.Process#getProcessResource <em>Process Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Resource</em>' reference.
	 * @see #getProcessResource()
	 * @generated
	 */
	void setProcessResource(ProcessingResource value);

	/**
	 * Returns the value of the '<em><b>Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rank</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rank</em>' attribute.
	 * @see #setRank(int)
	 * @see parallel.ParallelPackage#getProcess_Rank()
	 * @model unique="false"
	 * @generated
	 */
	int getRank();

	/**
	 * Sets the value of the '{@link parallel.Process#getRank <em>Rank</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rank</em>' attribute.
	 * @see #getRank()
	 * @generated
	 */
	void setRank(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProcess(this);'"
	 * @generated
	 */
	void accept(ParallelModelVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _rank = this.getRank();\nreturn (\"Process\" + &lt;%java.lang.Integer%&gt;.valueOf(_rank));'"
	 * @generated
	 */
	String toString();

} // Process
