/**
 */
package parallel;

import gecos.instrs.Instruction;

import gecos.types.Type;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.Message#getAddress <em>Address</em>}</li>
 *   <li>{@link parallel.Message#getTag <em>Tag</em>}</li>
 *   <li>{@link parallel.Message#getType <em>Type</em>}</li>
 *   <li>{@link parallel.Message#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends EObject {
	/**
	 * Returns the value of the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' containment reference.
	 * @see #setAddress(Instruction)
	 * @see parallel.ParallelPackage#getMessage_Address()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getAddress();

	/**
	 * Sets the value of the '{@link parallel.Message#getAddress <em>Address</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' containment reference.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(Instruction value);

	/**
	 * Returns the value of the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag</em>' attribute.
	 * @see #setTag(int)
	 * @see parallel.ParallelPackage#getMessage_Tag()
	 * @model unique="false"
	 * @generated
	 */
	int getTag();

	/**
	 * Sets the value of the '{@link parallel.Message#getTag <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag</em>' attribute.
	 * @see #getTag()
	 * @generated
	 */
	void setTag(int value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see parallel.ParallelPackage#getMessage_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link parallel.Message#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' containment reference.
	 * @see #setCount(Instruction)
	 * @see parallel.ParallelPackage#getMessage_Count()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getCount();

	/**
	 * Sets the value of the '{@link parallel.Message#getCount <em>Count</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' containment reference.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _eIsProxy = this.eIsProxy();\nif (_eIsProxy)\n{\n\treturn super.toString();\n}\n&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.Instruction%&gt; _address = this.getAddress();\n&lt;%java.lang.String%&gt; _string = null;\nif (_address!=null)\n{\n\t_string=_address.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"\";\n}\nreturn _elvis;'"
	 * @generated
	 */
	String toString();

} // Message
