/**
 */
package parallel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receive Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.ReceiveCommunication#getSenders <em>Senders</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getReceiveCommunication()
 * @model abstract="true"
 * @generated
 */
public interface ReceiveCommunication extends ICommunicationUnit, ParallelModelVisitable {
	/**
	 * Returns the value of the '<em><b>Senders</b></em>' reference list.
	 * The list contents are of type {@link parallel.SendCommunication}.
	 * It is bidirectional and its opposite is '{@link parallel.SendCommunication#getReceivers <em>Receivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Senders</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Senders</em>' reference list.
	 * @see parallel.ParallelPackage#getReceiveCommunication_Senders()
	 * @see parallel.SendCommunication#getReceivers
	 * @model opposite="receivers"
	 * @generated
	 */
	EList<SendCommunication> getSenders();

} // ReceiveCommunication
