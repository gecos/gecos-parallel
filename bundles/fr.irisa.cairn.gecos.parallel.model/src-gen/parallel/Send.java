/**
 */
package parallel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link parallel.Send#getReceives <em>Receives</em>}</li>
 *   <li>{@link parallel.Send#getMessage <em>Message</em>}</li>
 *   <li>{@link parallel.Send#getCommType <em>Comm Type</em>}</li>
 * </ul>
 *
 * @see parallel.ParallelPackage#getSend()
 * @model
 * @generated
 */
public interface Send extends Communication, ParallelModelVisitable {
	/**
	 * Returns the value of the '<em><b>Receives</b></em>' reference list.
	 * The list contents are of type {@link parallel.Receive}.
	 * It is bidirectional and its opposite is '{@link parallel.Receive#getSends <em>Sends</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receives</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receives</em>' reference list.
	 * @see parallel.ParallelPackage#getSend_Receives()
	 * @see parallel.Receive#getSends
	 * @model opposite="sends"
	 * @generated
	 */
	EList<Receive> getReceives();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' containment reference.
	 * @see #setMessage(Message)
	 * @see parallel.ParallelPackage#getSend_Message()
	 * @model containment="true"
	 * @generated
	 */
	Message getMessage();

	/**
	 * Sets the value of the '{@link parallel.Send#getMessage <em>Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' containment reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Comm Type</b></em>' attribute.
	 * The default value is <code>"SYNC"</code>.
	 * The literals are from the enumeration {@link parallel.CommType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comm Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comm Type</em>' attribute.
	 * @see parallel.CommType
	 * @see #setCommType(CommType)
	 * @see parallel.ParallelPackage#getSend_CommType()
	 * @model default="SYNC" unique="false"
	 * @generated
	 */
	CommType getCommType();

	/**
	 * Sets the value of the '{@link parallel.Send#getCommType <em>Comm Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comm Type</em>' attribute.
	 * @see parallel.CommType
	 * @see #getCommType()
	 * @generated
	 */
	void setCommType(CommType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%parallel.Receive%&gt;, &lt;%parallel.Process%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%parallel.Receive%&gt;, &lt;%parallel.Process%&gt;&gt;()\n{\n\tpublic &lt;%parallel.Process%&gt; apply(final &lt;%parallel.Receive%&gt; it)\n\t{\n\t\treturn it.getContiningProcess();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%parallel.Process%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%parallel.Process%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%parallel.Receive%&gt;, &lt;%parallel.Process%&gt;&gt;map(this.getReceives(), _function)));'"
	 * @generated
	 */
	EList<parallel.Process> getReceivingProcesses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSend(this);'"
	 * @generated
	 */
	void accept(ParallelModelVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"Snd_\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" \");\n&lt;%parallel.Message%&gt; _message = this.getMessage();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _message);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" from \");\n&lt;%parallel.Process%&gt; _continingProcess = this.getContiningProcess();\nreturn (_plus_3 + _continingProcess);'"
	 * @generated
	 */
	String toString();

} // Send
