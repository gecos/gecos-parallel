/**
 */
package parallel;

import gecos.blocks.Block;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ICommunication Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see parallel.ParallelPackage#getICommunicationUnit()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ICommunicationUnit extends Block, ParallelModelVisitable {
} // ICommunicationUnit
