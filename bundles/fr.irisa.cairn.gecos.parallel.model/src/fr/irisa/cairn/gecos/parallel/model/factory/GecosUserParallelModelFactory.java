package fr.irisa.cairn.gecos.parallel.model.factory;

import java.util.Arrays;
import java.util.Collection;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import gecos.core.Procedure;
import gecos.core.Scope;
import gecos.instrs.Instruction;
import gecos.types.Type;
import parallel.CommType;
import parallel.Message;
import parallel.ParallelApplication;
import parallel.ParallelFactory;
import parallel.Process;
import parallel.Receive;
import parallel.Send;

public class GecosUserParallelModelFactory {

	public static final int TAG_ANY = 0;
	
	private static ParallelFactory _factory = ParallelFactory.eINSTANCE;
	private static int _com_number_ = 0;
	
	public static ParallelApplication parallelApplication(Process... processes) {
		return parallelApplication(Arrays.asList(processes));
	}
	
	public static ParallelApplication parallelApplication(Collection<Process> processes) {
		ParallelApplication app = _factory.createParallelApplication();
		app.getProcesses().addAll(processes); 
		return app;
	}
	
	public static Process process(int rank, Procedure...procs){
		Process process = _factory.createProcess();
		process.setRank(rank);
		process.setScope(GecosUserCoreFactory.scope());
		for (Procedure procedure : procs)
			process.addProcedure(procedure);
		return process;
	}
	
	public static Process process(ProcessingResource pr, Procedure...procs) {
		return process(pr, GecosUserCoreFactory.scope(), procs);
	}
	
	public static Process process(ProcessingResource pr, Scope scope, Procedure...procs){
		Process process = _factory.createProcess();
		process.setRank(pr.getId());
		process.setProcessResource(pr);
		process.setScope(scope);
		for (Procedure procedure : procs)
			process.addProcedure(procedure);
		return process;
	}
	
	/**
	 * Creates a Synchronous Send.
	 * @param Message represents Message to be sent
	 * @param receives represent the corresponding {@link Receive}s
	 */
	public static Send ssend(Message Message, Receive... receives){
		Send send = _factory.createSend();
		send.setCommType(CommType.SYNC);
		send.setMessage(Message);
		send.setNumber(_com_number_ ++);
		for(Receive recv : receives)
			send.getReceives().add(recv);
		return send;
	}
	
	/**
	 * Creates a Asynchronous Send.
	 * @param Message represents Message to be sent
	 * @param receives represent the corresponding {@link Receive}s
	 */
	public static Send asend(Message Message, Receive... receives){
		Send send = _factory.createSend();
		send.setCommType(CommType.ASYNC);
		send.setMessage(Message);
		send.setNumber(_com_number_ ++);
		for(Receive recv : receives)
			send.getReceives().add(recv);
		return send;
	}
	
	/**
	 * Creates a Synchronous Receive.
	 * @param Message receive Message location
	 * @param send the corresponding {@link Send} or null.
	 */
	public static Receive sreceive(Message Message, Send send){
		Receive receive = _factory.createReceive();
		receive.setCommType(CommType.SYNC);
		receive.setMessage(Message);
		receive.setNumber(_com_number_ ++);
		if(send != null)
			receive.getSends().add(send);
		return receive;
	}
	
	/**
	 * Creates a Synchronous Receive.
	 * @param Message receive Message location
	 * @param send the corresponding {@link Send} or null.
	 */
	public static Receive areceive(Message Message, Send send){
		Receive receive = _factory.createReceive();
		receive.setCommType(CommType.ASYNC);
		receive.setMessage(Message);
		receive.setNumber(_com_number_ ++);
		if(send != null)
			receive.getSends().add(send);
		return receive;
	}
	
	/**
	 * Creates a Message used for communications
	 * @param address the address of the buffer to be sent or used to receive Message
	 * @param type Message type
	 * @param size number of elements to send/receive
	 * @param tag tag associated to the message
	 * @return
	 */
	public static Message message(Instruction address, Type type, Instruction size, int tag) {
		Message Message = _factory.createMessage();
		Message.setAddress(address);
		Message.setType(type);
		Message.setCount(size);
		Message.setTag(tag);
		return Message;
	}
	
	/**
	 * Shortcut to {@link #Message(Instruction, Type, int, int)} with {@link #TAG_ANY}
	 */
	public static Message message(Instruction address, Type type, Instruction size) {
		return message(address, type, size, TAG_ANY);
	}

}
