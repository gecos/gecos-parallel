package fr.irisa.cairn.gecos.parallel.model.utils;

import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import parallel.Process;

public class ParallelUtils {

	public static Process getProcess(Procedure procedure) {
		if(procedure != null) {
			ProcedureSet ps = procedure.getContainingProcedureSet();
			if(ps instanceof Process)
				return (Process) ps;
		}
		return null;
	}
	
	public static Process getProcess(Block block) {
		if(block == null)
			return null;
		return getProcess(block.getContainingProcedure());
	}
	
}
