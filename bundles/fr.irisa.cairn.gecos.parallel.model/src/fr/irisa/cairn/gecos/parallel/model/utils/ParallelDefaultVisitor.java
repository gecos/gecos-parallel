package fr.irisa.cairn.gecos.parallel.model.utils;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import parallel.Communication;
import parallel.ParallelApplication;
import parallel.ParallelModelVisitor;
import parallel.Process;
import parallel.Receive;
import parallel.Send;


public class ParallelDefaultVisitor extends GecosBlocksDefaultVisitor implements ParallelModelVisitor {

	@Override
	public void visitParallelApplication(ParallelApplication application) {
		application.getProcesses().forEach(this::visitProcess);
	}
	
	@Override
	public void visitProcess(Process process) {
		super.visit(process);
	}

	@Override
	public void visitSend(Send send) {
		visitCommunication(send);
	}

	@Override
	public void visitReceive(Receive receive) {
		visitCommunication(receive);
	}

	public void visitCommunication(Communication communication) {
		super.visitBasicBlock(communication);
	}

}