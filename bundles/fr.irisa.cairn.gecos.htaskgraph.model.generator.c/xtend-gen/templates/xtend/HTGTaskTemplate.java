package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.htaskgraph.model.generator.c.withlabels.HTGCGeneratorWithASMLabels;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import htaskgraph.ClusterTaskNode;
import htaskgraph.TaskNode;
import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.HTGTemplate;

@SuppressWarnings("all")
public class HTGTaskTemplate extends HTGTemplate {
  public static boolean GENERATE_ASM_LABELS = false;
  
  protected Object _generate(final TaskNode t) {
    StringConcatenation _builder = new StringConcatenation();
    {
      if (HTGTaskTemplate.GENERATE_ASM_LABELS) {
        {
          boolean _isProcBody = this.isProcBody(t);
          if (_isProcBody) {
            _builder.append("{");
          }
        }
        _builder.newLineIfNotEmpty();
        String _taskStartLabel = HTGCGeneratorWithASMLabels.getTaskStartLabel(t);
        _builder.append(_taskStartLabel);
        _builder.newLineIfNotEmpty();
      }
    }
    String _generate = ExtendableBlockCGenerator.eInstance.generate(t.getBlock());
    _builder.append(_generate);
    _builder.newLineIfNotEmpty();
    {
      if (HTGTaskTemplate.GENERATE_ASM_LABELS) {
        String _taskEndLabel = HTGCGeneratorWithASMLabels.getTaskEndLabel(t);
        _builder.append(_taskEndLabel);
        _builder.newLineIfNotEmpty();
        {
          boolean _isProcBody_1 = this.isProcBody(t);
          if (_isProcBody_1) {
            _builder.append("}");
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public boolean isProcBody(final TaskNode t) {
    return ((t instanceof ClusterTaskNode) || ((((!Objects.equal(t.eContainer(), null)) && (t.eContainer() instanceof CompositeBlock)) && 
      (!Objects.equal(t.eContainer().eContainer(), null))) && (t.eContainer().eContainer() instanceof Procedure)));
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public Object generate(final EObject t) {
    if (t instanceof TaskNode) {
      return _generate((TaskNode)t);
    } else if (t != null) {
      return _generate(t);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(t).toString());
    }
  }
}
