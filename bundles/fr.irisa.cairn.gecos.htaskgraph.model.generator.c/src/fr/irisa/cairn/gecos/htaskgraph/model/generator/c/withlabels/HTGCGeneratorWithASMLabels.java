package fr.irisa.cairn.gecos.htaskgraph.model.generator.c.withlabels;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import gecos.annotations.IAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.gecosproject.GecosProject;
import htaskgraph.CoreLevelWCET;
import htaskgraph.ProfilingInformation;
import htaskgraph.TaskNode;
import htaskgraph.WCETInformation;
import templates.xtend.HTGTaskTemplate;

public class HTGCGeneratorWithASMLabels {

	private GecosProject project;
	private static String keystart;
	private static String keyend;
	private String dir;
	

	public HTGCGeneratorWithASMLabels(GecosProject p, String dir, String start, String end) {
		this.project = p;
		HTGCGeneratorWithASMLabels.keystart = start;
		HTGCGeneratorWithASMLabels.keyend = end;
		this.dir = dir;
	}
	
	public void compute() {	
		boolean saved = HTGTaskTemplate.GENERATE_ASM_LABELS;
		HTGTaskTemplate.GENERATE_ASM_LABELS = true;
		new XtendCGenerator(project, dir).compute();
		HTGTaskTemplate.GENERATE_ASM_LABELS = saved;
	}

	public static String getTaskStartLabel(TaskNode t) {
		IAnnotation annotation = t.getAnnotation(keystart);
		String label = "";
		String wcetAnnot = "";
		if (annotation != null)
			if (annotation instanceof StringAnnotation) {
				StringAnnotation annot = (StringAnnotation) annotation;				
					label = label.concat(annot.getContent());
			}
		EList<ProfilingInformation> piList = t.getProfilingInformation();
		for (ProfilingInformation pi : piList) {						
			if (pi instanceof WCETInformation) {
				EList<CoreLevelWCET> coreLevelWCETs = ((WCETInformation) pi).getData();
				if (! coreLevelWCETs.isEmpty())
					wcetAnnot = wcetAnnot.concat(" /*");
				for (CoreLevelWCET coreLevelwcet : coreLevelWCETs) {
					wcetAnnot = wcetAnnot.concat(" WCET_");
					wcetAnnot = wcetAnnot.concat(coreLevelwcet.getArchitecture());
					wcetAnnot = wcetAnnot.concat(": ");
					wcetAnnot = wcetAnnot.concat(String.valueOf(coreLevelwcet.getIsolatedWCET()));
					wcetAnnot = wcetAnnot.concat(" cycles.");
				}
				if (! coreLevelWCETs.isEmpty())
					wcetAnnot = wcetAnnot.concat(" */");
				break;
			}
		}
		return (label.concat(wcetAnnot));
	}
	
	public static String getTaskEndLabel(TaskNode t) {
		IAnnotation annotation = t.getAnnotation(keyend);
		if (annotation != null)
			if (annotation instanceof StringAnnotation) {
				StringAnnotation annot = (StringAnnotation) annotation;				
					return annot.getContent();
			}
		return "";
	}
	
}
