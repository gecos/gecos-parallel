package templates.xtend

import fr.irisa.cairn.gecos.htaskgraph.model.generator.c.withlabels.HTGCGeneratorWithASMLabels
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator
import gecos.blocks.CompositeBlock
import gecos.core.Procedure
import htaskgraph.ClusterTaskNode
import htaskgraph.TaskNode
import org.eclipse.emf.ecore.EObject

class HTGTaskTemplate extends HTGTemplate {
	
	public static var boolean GENERATE_ASM_LABELS = false;
	
	def dispatch generate(TaskNode t) {
		'''
		«IF GENERATE_ASM_LABELS»
			«IF isProcBody(t)»{«ENDIF»
			«HTGCGeneratorWithASMLabels.getTaskStartLabel(t)»
		«ENDIF»
		«ExtendableBlockCGenerator.eInstance.generate(t.block)»
		«IF GENERATE_ASM_LABELS»
			«HTGCGeneratorWithASMLabels.getTaskEndLabel(t)»
			«IF isProcBody(t)»}«ENDIF»
		«ENDIF»
		'''
	}
	
	def isProcBody(TaskNode t) {
		return ((t instanceof ClusterTaskNode) || (t.eContainer != null && t.eContainer instanceof CompositeBlock &&
			t.eContainer.eContainer != null && t.eContainer.eContainer instanceof Procedure))
	}
	
	def dispatch generate(EObject object) {
		null
	}
}