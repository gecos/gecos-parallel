package fr.irisa.cairn.gecos.htaskgraph.adapter.builder;

import java.util.Comparator;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.htaskgraph.adapter.impl.HierarchicalTaskGraphAdapterImpl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.impl.PortAdapterImpl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.impl.TaskDependencyAdapterImpl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.impl.TaskNodeAdapterImpl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IPortAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * Factory od the graph adapter
 * @author <your name>
 *
 */
public class AdapterFactory {

	//	private static AdapterFactory INSTANCE  = new AdapterFactory();

	@Inject
	Comparator<INode> nodeComparator;
	@Inject
	Comparator<IEdge> edgeComparator;
	@Inject
	Comparator<IPort> portComparator;

	@Inject
	private AdapterFactory() {
	}

	//	public static AdapterFactory getINSTANCE() {
	//		return AdapterFactory.INSTANCE;
	//	}

	public IHierarchicalTaskGraphAdapter createGraphAdapter(HierarchicalTaskNode adapted,
			boolean directed) {
		HierarchicalTaskGraphAdapterImpl graph = new HierarchicalTaskGraphAdapterImpl(adapted, directed);
		if (this.nodeComparator != null) {
			graph.setNodeComparator(this.nodeComparator);
		}
		if (this.edgeComparator != null) {
			graph.setEdgeComparator(this.edgeComparator);
		}
		return graph;
	}

	public ITaskNodeAdapter createNodeAdapter(TaskNode adapted) {
		TaskNodeAdapterImpl node = new TaskNodeAdapterImpl(adapted);
		if (this.portComparator != null) {
			node.setPortComparator(portComparator);
		}
		return node;
	}

	public IPortAdapter createPortAdapter() {
		PortAdapterImpl port = new PortAdapterImpl();;
		return port;
	}

	private ITaskDependencyAdapter createEdgeAdapter(TaskDependency adapted,
			IPortAdapter source, IPortAdapter sink) {
		return new TaskDependencyAdapterImpl(adapted, source, sink);
	}

	/**
	 * Connect two ports and add the builded link to the nodes graphs
	 * 
	 * @param source
	 * @param sink
	 * @return the created edge
	 */
	public ITaskDependencyAdapter connect(TaskDependency adapted,
			IPortAdapter source, IPortAdapter sink) {
		ITaskDependencyAdapter e = createEdgeAdapter(adapted, source, sink);
		IHierarchicalTaskGraphAdapter gSource = source.getNode().getGraph();
		IHierarchicalTaskGraphAdapter gSink = sink.getNode().getGraph();
		gSource.addEdge(e);
		if (gSource != gSink) {
			gSink.addEdge(e);
		}
		return e;
	}
	/**
	 * Connect two nodes and add the builded link to the nodes graphs
	 * 
	 * @param source
	 * @param sink
	 * @return the created edge
	 */
	public ITaskDependencyAdapter connect(TaskDependency adapted,
			ITaskNodeAdapter source, ITaskNodeAdapter sink) {
		IPortAdapter sourcePort;
		IPortAdapter sinkPort;
		sourcePort = createPortAdapter();
		source.addOutputPort(sourcePort);
		sinkPort = createPortAdapter();
		sink.addInputPort(sinkPort);
		return connect(adapted, sourcePort, sinkPort);

	}

}
