package fr.irisa.cairn.gecos.htaskgraph.adapter.model;

import java.util.Set;

/**
 * Interface of a port adapter.

 * @author <your name>
 *
 */
public interface IPortAdapter extends fr.irisa.cairn.graph.IPort {

	/** Get the port node */
	/**
	 * @model default=""
	 */
	ITaskNodeAdapter getNode();

	/** Get all connected edges */
	/**
	 * @model default=""
	 */
	Set<ITaskDependencyAdapter> getEdges();

}
