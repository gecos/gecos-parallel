package fr.irisa.cairn.gecos.htaskgraph.adapter.module;

import fr.irisa.cairn.gecos.htaskgraph.adapter.HierarchicalTaskNodeLabelProvider;
import fr.irisa.cairn.gecos.htaskgraph.adapter.ProgramFlowDotAttributsProvider;
import fr.irisa.cairn.gecos.htaskgraph.adapter.TaskDependencyLabelProvider;
import fr.irisa.cairn.graph.guice.modules.providersbinding.LabelProviderModule;
import fr.irisa.cairn.graph.providers.IAttributsProvider;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;

public class ProgramFlowLabelProviderModule extends LabelProviderModule {

	protected void bindIAttributeProvider() {
		this.bind(IAttributsProvider.class).toInstance(new ProgramFlowDotAttributsProvider());
	}

	protected void bindINodeLabelProvider() {
		this.bind(INodeLabelProvider.class).to(HierarchicalTaskNodeLabelProvider.class);
	}
	
	@Override
	protected void bindIEdgeLabelProvider() {
		this.bind(IEdgeLabelProvider.class).to(TaskDependencyLabelProvider.class);
	}

}
