package fr.irisa.cairn.gecos.htaskgraph.adapter.model;

import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * Interface of the adpated container.
 *
 * this interface has method to find the correponding adapter to an adapted object
 * 
 * @author <your name>
 *
 */
public interface IAdaptedContainer {

	public IAdaptedContainer getParent();

	public void setParent(IAdaptedContainer parent);

	public IAdaptedContainer getRoot();

	public IHierarchicalTaskGraphAdapter getAdapterGraph(HierarchicalTaskNode graph);

	public ITaskNodeAdapter getAdapterNode(TaskNode node);

	public ITaskDependencyAdapter getAdapterEdge(TaskDependency edge);

	public void addGraph(IHierarchicalTaskGraphAdapter adapter);

	public void addNode(ITaskNodeAdapter adapter);

	public void addEdge(ITaskDependencyAdapter adapter);

	public void removeGraph(HierarchicalTaskNode adapted);

	public void removeNode(TaskNode adapted);

	public void removeEdge(TaskDependency adapted);

	public void removeGraph(IHierarchicalTaskGraphAdapter adapter);

	public void removeNode(ITaskNodeAdapter adapter);

	public void removeEdge(ITaskDependencyAdapter adapter);

	public boolean containGraph(HierarchicalTaskNode adapted);

	public boolean containNode(TaskNode adapted);

	public boolean containEdge(TaskDependency adapted);

}
