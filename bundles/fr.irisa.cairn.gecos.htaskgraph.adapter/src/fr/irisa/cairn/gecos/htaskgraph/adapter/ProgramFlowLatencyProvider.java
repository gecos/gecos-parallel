package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

public class ProgramFlowLatencyProvider implements
		ILatencyProvider<ITaskNodeAdapter, ITaskDependencyAdapter> {

	public int getNodeLatency(ITaskNodeAdapter n) {
		return 1;
	}

	public int getEdgeLatency(ITaskDependencyAdapter e) {
		return 0;
	}

}
