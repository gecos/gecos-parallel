package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.componentElement.IComponentService;
import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.gecos.htaskgraph.adapter.builder.AdapterBuilder;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.module.AdapterBuilderModule;
import fr.irisa.cairn.gecos.htaskgraph.adapter.module.AdapterFactoryModule;
import fr.irisa.cairn.gecos.htaskgraph.adapter.module.ProgramFlowLabelProviderModule;
import fr.irisa.cairn.graph.guice.GraphComponent;
import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService;
import fr.irisa.cairn.graph.guice.export.IGraphExportService;
import fr.irisa.cairn.graph.guice.modules.providersbinding.LabelProviderModule;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.FactoryServiceModule;
import htaskgraph.HierarchicalTaskNode;

public class TaskGraphAdapterComponent {

	private final static TaskGraphAdapterComponent INSTANCE = new TaskGraphAdapterComponent();
	
	public enum MODE {PROG_DEP_GRAPH,CONTROL_FLOW_GRAPH};
	
	private GraphComponent graphComponent;

	private TaskGraphAdapterComponent() {
		graphComponent = GraphComponent.getINSTANCE();
		configure();
	}

	public static TaskGraphAdapterComponent getInstance() {
		return INSTANCE;
	}

	private void configure() {
		replaceModule(FactoryServiceModule.class, new AdapterFactoryModule());
		replaceModule(LabelProviderModule.class, new ProgramFlowLabelProviderModule());
		installModule(new AdapterBuilderModule());
	}

	public void installModule(ServiceModule serviceModule) {
		graphComponent.installModule(serviceModule);

	}

	public <T extends IComponentService> T getService(Class<T> serviceClassName) {
		return graphComponent.getService(serviceClassName);
	}

	public void installModules(Iterable<ServiceModule> servicesModules) {
		graphComponent.installModules(servicesModules);

	}

	public void installModules(ServiceModule... servicesModules) {
		graphComponent.installModules(servicesModules);

	}

	public void replaceModule(ServiceModule oldServiceModule,
			ServiceModule newServiceModule) {
		graphComponent.replaceModule(oldServiceModule, newServiceModule);

	}

	public void replaceModule(
			Class<? extends ServiceModule> oldServiceModuleClass,
			ServiceModule newServiceModule) {
		graphComponent.replaceModule(oldServiceModuleClass, newServiceModule);

	}

	public void unInstallModule(ServiceModule serviceModule) {
		graphComponent.unInstallService(serviceModule);

	}

	public void unInstallModules(ServiceModule... serviceModules) {
		graphComponent.unInstallServices(serviceModules);

	}

	public IHierarchicalTaskGraphAdapter build(HierarchicalTaskNode adaptedGraph) {
		AdapterBuilder buider = GraphComponent.getINSTANCE().getService(AdapterBuilder.class);
		return buider.buildGraphAdapter(adaptedGraph);
	}


	
	public IGraphAnalysisService getGraphAnalysisService() {
		return this.getService(IGraphAnalysisService.class);
	}

	public IGraphExportService getGraphExportService() {
		return this.getService(IGraphExportService.class);
	}
}

