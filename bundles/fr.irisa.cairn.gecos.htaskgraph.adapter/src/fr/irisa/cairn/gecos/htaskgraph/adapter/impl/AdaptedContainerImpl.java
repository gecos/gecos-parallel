package fr.irisa.cairn.gecos.htaskgraph.adapter.impl;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IAdaptedContainer;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * implementation of the container.
 * @author <your name>
 *
 */
public class AdaptedContainerImpl implements IAdaptedContainer {

	private Map<HierarchicalTaskNode, IHierarchicalTaskGraphAdapter> graphsMap;

	private Map<TaskNode, ITaskNodeAdapter> nodesMap;
	private Map<TaskDependency, ITaskDependencyAdapter> edgesMap;

	private IAdaptedContainer parent;

	public AdaptedContainerImpl() {
		super();
		this.graphsMap = new HashMap<HierarchicalTaskNode, IHierarchicalTaskGraphAdapter>();
		this.nodesMap = new HashMap<TaskNode, ITaskNodeAdapter>();

		this.edgesMap = new HashMap<TaskDependency, ITaskDependencyAdapter>();
		this.parent = null;
	}

	public IAdaptedContainer getParent() {
		return this.parent;
	}

	public void setParent(IAdaptedContainer parent) {
		this.parent = parent;
		for (Iterator<IHierarchicalTaskGraphAdapter> iterator = this.graphsMap.values()
				.iterator(); iterator.hasNext();) {
			IHierarchicalTaskGraphAdapter graphAdapter = iterator.next();
			parent.addGraph(graphAdapter);
		}
		this.graphsMap.clear();
		for (Iterator<ITaskNodeAdapter> iterator = this.nodesMap.values()
				.iterator(); iterator.hasNext();) {
			ITaskNodeAdapter nodeAdapter = iterator.next();
			parent.addNode(nodeAdapter);
		}
		this.nodesMap.clear();

		for (Iterator<ITaskDependencyAdapter> iterator = this.edgesMap.values()
				.iterator(); iterator.hasNext();) {
			ITaskDependencyAdapter edgeAdapter = iterator.next();
			parent.addEdge(edgeAdapter);
		}
		this.edgesMap.clear();

	}

	public IAdaptedContainer getRoot() {
		IAdaptedContainer root;
		if (this.isRoot()) {
			root = this;
		} else {
			root = this.getParent().getRoot();
		}
		return root;
	}

	private boolean isRoot() {
		return this.parent == null;
	}

	public IHierarchicalTaskGraphAdapter getAdapterGraph(HierarchicalTaskNode graph) {
		if (isRoot()) {
			return this.graphsMap.get(graph);
		} else {
			return getRoot().getAdapterGraph(graph);
		}
	}

	public ITaskNodeAdapter getAdapterNode(TaskNode node) {
		if (isRoot()) {
			return nodesMap.get(node);
		} else {
			return getRoot().getAdapterNode(node);
		}
	}

	public ITaskDependencyAdapter getAdapterEdge(TaskDependency edge) {
		if (isRoot()) {
			return edgesMap.get(edge);
		} else {
			return getRoot().getAdapterEdge(edge);
		}
	}

	public void addGraph(IHierarchicalTaskGraphAdapter adapter) {
		if (isRoot()) {
			HierarchicalTaskNode adapted = adapter.getAdaptedGraph();
			if (!graphsMap.containsKey(adapted)) {
				graphsMap.put(adapted, adapter);
			}
		} else {
			getRoot().addGraph(adapter);
		}
	}

	public void addNode(ITaskNodeAdapter adapter) {
		if (isRoot()) {
			TaskNode adapted = adapter.getAdaptedNode();
			if (!nodesMap.containsKey(adapted)) {
				nodesMap.put(adapted, adapter);
			}
		} else {
			getRoot().addNode(adapter);
		}
	}

	public void addEdge(ITaskDependencyAdapter adapter) {
		if (isRoot()) {
			TaskDependency adapted = adapter.getAdaptedEdge();
			if (!edgesMap.containsKey(adapted)) {
				edgesMap.put(adapted, adapter);
			}
		} else {
			getRoot().addEdge(adapter);
		}
	}

	public void removeGraph(IHierarchicalTaskGraphAdapter adapted) {
		if (isRoot()) {
			this.graphsMap.remove(adapted);
		} else {
			getRoot().removeGraph(adapted);
		}
	}

	public void removeGraph(HierarchicalTaskNode adapter) {
		if (isRoot()) {
			this.graphsMap.remove(adapter);
		} else {
			getRoot().removeGraph(adapter);
		}
	}

	public void removeEdge(TaskDependency adapted) {
		if (isRoot()) {
			edgesMap.remove(adapted);
		} else {
			getRoot().removeEdge(adapted);
		}
	}

	public void removeEdge(ITaskDependencyAdapter adapter) {
		if (isRoot()) {
			removeEdge(adapter.getAdaptedEdge());
		} else {
			getRoot().removeEdge(adapter);
		}

	}

	public void removeNode(TaskNode adapted) {
		if (isRoot()) {
			nodesMap.remove(adapted);
		} else {
			getRoot().removeNode(adapted);
		}

	}

	public void removeNode(ITaskNodeAdapter adapter) {
		if (isRoot()) {
			removeNode(adapter.getAdaptedNode());
		} else {
			getRoot().removeNode(adapter);
		}

	}

	public boolean containGraph(HierarchicalTaskNode adapted) {
		if (isRoot()) {
			return this.graphsMap.containsKey(adapted);
		} else {
			return getRoot().containGraph(adapted);
		}
	}

	public boolean containEdge(TaskDependency adapted) {
		if (isRoot()) {
			return edgesMap.containsKey(adapted);
		} else {
			return getRoot().containEdge(adapted);
		}
	}

	public boolean containNode(TaskNode adapted) {
		if (isRoot()) {
			return nodesMap.containsKey(adapted);
		} else {
			return getRoot().containNode(adapted);
		}
	}

}
