package fr.irisa.cairn.gecos.htaskgraph.adapter.impl;
import java.util.Set;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IPortAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;

/**
 * implementation of a node adapter.
 * This graph adapt a Port
 * @author <your name>
 *
 */
public class PortAdapterImpl extends fr.irisa.cairn.graph.implement.Port
		implements
			IPortAdapter {

	public PortAdapterImpl() {
		super();

	}

	/** Get the port node */
	/**
	 * @model default=""
	 */
	@Override
	public ITaskNodeAdapter getNode() {
		return (ITaskNodeAdapter) super.getNode();
	}

	/** Get all connected edges */
	/**
	 * @model default=""
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Set<ITaskDependencyAdapter> getEdges() {
		return (Set<ITaskDependencyAdapter>) super.getEdges();
	}

}
