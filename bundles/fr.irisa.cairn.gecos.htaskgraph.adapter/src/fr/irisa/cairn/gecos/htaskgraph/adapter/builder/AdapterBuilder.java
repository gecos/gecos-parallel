package fr.irisa.cairn.gecos.htaskgraph.adapter.builder;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * Factory od the graph adapter
 * 
 * @author <your name>
 * 
 */
@Singleton
public class AdapterBuilder {

	@Inject
	AdapterFactory factory;

	private Set<TaskNode> visitedsNode;
	private Set<TaskDependency> visitedsEdge;

	@Inject
	private AdapterBuilder() {

		this.visitedsNode = new HashSet<TaskNode>();
		this.visitedsEdge = new HashSet<TaskDependency>();
	}

	public IHierarchicalTaskGraphAdapter buildGraphAdapter(HierarchicalTaskNode adapted) {
		IHierarchicalTaskGraphAdapter graphAdapter = factory.createGraphAdapter(adapted, true);
		initGraphAdapter(graphAdapter, adapted);
		return graphAdapter;
	}

	private void initGraphAdapter(IHierarchicalTaskGraphAdapter graphAdapter, HierarchicalTaskNode adapted) {

		for (TaskNode n : adapted.getChildTasks()) {
			buildNodeAdapter(graphAdapter, n);
		}

		/**
		 * In a Program Dependency Graph, we only keep track on conditional
		 * branch information and data dependencies. ControlEdge that correspond
		 * to simple unconditional jumps, and which model the initial program
		 * execution order, are not included.
		 */
		for (TaskNode r : adapted.getChildTasks()) {
			for (TaskDependency e : r.getPreds()) {
				IHierarchicalTaskGraphAdapter container = graphAdapter;
				buildInternalEdge(container, e);
			}
		}

	}

	public void buildNodeAdapter(IHierarchicalTaskGraphAdapter graphAdapter, TaskNode adapted) {

		ITaskNodeAdapter nodeAdapter = null;
		if (!this.visitedsNode.contains(adapted)) {
			this.visitedsNode.add(adapted);
			nodeAdapter = factory.createNodeAdapter(adapted);
			graphAdapter.addNode(nodeAdapter);

			initNodeAdapter(graphAdapter, nodeAdapter, adapted);

		}
	}

	private void initNodeAdapter(IHierarchicalTaskGraphAdapter graphAdapter, ITaskNodeAdapter nodeAdapter,
			TaskNode adapted) {

	}

	private void buildEdgeAdapter(IHierarchicalTaskGraphAdapter graphAdapter, TaskDependency adapted) {

		ITaskDependencyAdapter edgeAdapter = null;
		initEdgeAdapter(graphAdapter, edgeAdapter, adapted);

		ITaskNodeAdapter sourceAdapter = graphAdapter.getAdapterNode(adapted.getFrom()); // sourceExpression
		ITaskNodeAdapter sinkAdapter = graphAdapter.getAdapterNode(adapted.getTo()); // sinkExpression

		edgeAdapter = factory.connect(adapted, sourceAdapter, sinkAdapter);

	}

	private void buildInternalEdge(IHierarchicalTaskGraphAdapter graphAdapter, TaskDependency adapted) {

		if (!this.visitedsEdge.contains(adapted)) {
			this.visitedsEdge.add(adapted);
			buildEdgeAdapter(graphAdapter, adapted);
		}

	}

	private void initEdgeAdapter(IHierarchicalTaskGraphAdapter graphAdapter, ITaskDependencyAdapter edgeAdapter,
			TaskDependency adapted) {

	}

}
