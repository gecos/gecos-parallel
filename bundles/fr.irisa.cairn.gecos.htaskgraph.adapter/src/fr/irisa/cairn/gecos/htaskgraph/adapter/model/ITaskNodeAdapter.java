package fr.irisa.cairn.gecos.htaskgraph.adapter.model;
import java.util.List;

import htaskgraph.TaskNode;

/**
 * Interface of a graph adapter.
 * This graph adapt a TaskGraph
 * @author <your name>
 *
 */
public interface ITaskNodeAdapter
		extends
		fr.irisa.cairn.graph.INode,
			fr.irisa.cairn.graph.observer.IObservable {


	/**
	 * Get the container of all adapter/adapted Objects.
	 *
	 * @return the container
	 */
	IAdaptedContainer getContainer();

	/**
	 * Get the adapted node.
	 *
	 * @return the adapted node
	 */
	TaskNode getAdaptedNode();

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	public IHierarchicalTaskGraphAdapter getGraph();

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<ITaskDependencyAdapter> getIncomingEdges();

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<ITaskDependencyAdapter> getOutgoingEdges();

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IPortAdapter> getInputPorts();

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IPortAdapter> getOutputPorts();

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	List<ITaskNodeAdapter> getPredecessors();

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	List<ITaskNodeAdapter> getSuccessors();

}
