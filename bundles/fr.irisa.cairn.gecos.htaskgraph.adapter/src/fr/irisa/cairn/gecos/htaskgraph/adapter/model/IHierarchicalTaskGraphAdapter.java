package fr.irisa.cairn.gecos.htaskgraph.adapter.model;
import java.util.Set;

import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * Interface of a graph adapter.
 * This graph adapt a TaskGraph
 * @author <your name>
 *
 */
public interface IHierarchicalTaskGraphAdapter
		extends
		fr.irisa.cairn.graph.IGraph,
			fr.irisa.cairn.graph.observer.IObservable {

	/**
	 * Get the adapted graph.
	 *
	 * @return the adapted graph
	 */
	HierarchicalTaskNode getAdaptedGraph();

	/**
	 * Get the container of all adapter/adapted Objects.
	 *
	 * @return the container
	 */
	IAdaptedContainer getContainer();

	@Override
	Set<ITaskNodeAdapter> getNodes();

	@Override
	Set<ITaskDependencyAdapter> getEdges();

	IHierarchicalTaskGraphAdapter getAdapterGraph(HierarchicalTaskNode graph);

	ITaskNodeAdapter getAdapterNode(TaskNode node);

	ITaskDependencyAdapter getAdapterEdge(TaskDependency edge);


}
