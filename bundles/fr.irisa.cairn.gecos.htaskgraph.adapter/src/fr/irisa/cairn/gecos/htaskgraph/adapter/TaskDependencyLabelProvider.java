package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.graph.implement.providers.EdgeLabelProvider;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.blocks.util.BlocksSwitch;
import gecos.instrs.SSAUseSymbol;

public class TaskDependencyLabelProvider extends EdgeLabelProvider<ITaskDependencyAdapter>{

	private LabelProviderSwitch labelProviderSwitch = new LabelProviderSwitch();

	@Override
	public String getEdgeLabel(ITaskDependencyAdapter edge) {
		return labelProviderSwitch.doSwitch(edge.getAdaptedEdge());
	}
	
	public class LabelProviderSwitch extends BlocksSwitch<String> {

		@Override
		public String caseControlEdge(ControlEdge e) {
			switch (e.getCond()) {
			case UNCONDITIONAL:
				return "J";
			case IF_TRUE:
				return "T";
			case IF_FALSE:
				return "F";
			case DISPATCH:
				return "D";
			default:
				throw new UnsupportedOperationException("Not yet Implemented");
			}

		}

		@Override
		public String caseDataEdge(DataEdge edge) {

			StringBuffer buffer = new StringBuffer();
			boolean first = true;
			for(SSAUseSymbol ssaUse  : edge.getUses()) {
				String name = ssaUse.getSymbol().getName()+"_"+ssaUse.getNumber();
				if(first) {
					buffer.append(name);
					first=false;
				} else {
					buffer.append(","+name);
				}
			}
			return buffer.toString(); 
		}
		
	}
}
