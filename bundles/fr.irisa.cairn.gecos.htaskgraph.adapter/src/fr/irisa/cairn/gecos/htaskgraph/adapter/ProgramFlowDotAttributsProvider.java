package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.io.DOTExport.DotAttributs;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.util.HtaskgraphSwitch;

public class ProgramFlowDotAttributsProvider extends DotAttributsProvider<ITaskNodeAdapter, ITaskDependencyAdapter> {
	
	@Override
	protected void findEdgeAttributs(ITaskDependencyAdapter edge) {
		super.findEdgeAttributs(edge);
		addEdgeProperty(edge, DotAttributs.STYLE,DotAttributsProvider.STYLE_DASHED);

	}

	@Override
	protected void findNodeAttributs(ITaskNodeAdapter node) {
		super.findNodeAttributs(node);
		node.getAdaptedNode();
		addNodeProperty(node, DotAttributs.NODE_SHAPE,DotAttributsProvider.SHAPE_RECTANCLE);
		addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
		addNodeProperty(node, DotAttributs.NODE_COLOR, "grey");
		
	}

	public class DotAttributesProviderSwitch extends HtaskgraphSwitch<Object> {

		@Override
		public Object caseHierarchicalTaskNode(HierarchicalTaskNode object) {
			// TODO Auto-generated method stub
			return super.caseHierarchicalTaskNode(object);
		}

//		@Override
//		public Object caseControlEdge(ControlEdge object) {
//			// TODO Auto-generated method stub
//			//addEdgeProperty(edge, DotAttributs.STYLE,DotAttributsProvider.STYLE_DASHED);
//			return super.caseControlEdge(object);
//		}
//
//		@Override
//		public Object caseDataEdge(DataEdge object) {
//			// TODO Auto-generated method stub
//			return super.caseDataEdge(object);
//		}
		
	}
}
