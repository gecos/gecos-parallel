package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IPortAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGPort;
import gecos.types.Type;

/**
 * Default DAG {@link IEquivalenceProvider}. Two {@link DAGOpNode}s are
 * equivalents if they have the same opcode and works and have the same
 * {@link Type}. Other nodes are equivalents if they are instances of the same
 * class. Two {@link DAGPort} are equivalents if they have the same id.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class ProgramFlowEquivalenceProvider implements
		IEquivalenceProvider<ITaskDependencyAdapter, ITaskNodeAdapter, IPortAdapter> {

	public boolean edgesEquivalence(ITaskDependencyAdapter e1, ITaskDependencyAdapter e2) {
		return e1 == e2;
	}

	public boolean nodesEquivalence(ITaskNodeAdapter n1, ITaskNodeAdapter n2) {
		 return (n1.getAdaptedNode()==n2.getAdaptedNode());
	}

	public boolean portsEquivalence(IPortAdapter p1, IPortAdapter p2) {
		return true;
	}


}
