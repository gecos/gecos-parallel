package fr.irisa.cairn.gecos.htaskgraph.adapter.impl;
import java.util.List;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IAdaptedContainer;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IPortAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.IPort;
import htaskgraph.TaskNode;

/**
 * implementation of a node adapter.
 * This graph adapt a TaskNode
 * @author <your name>
 *
 */
public class TaskNodeAdapterImpl extends fr.irisa.cairn.graph.implement.Node
		implements
			ITaskNodeAdapter {

	private final TaskNode adapted;

	public TaskNodeAdapterImpl(TaskNode adapted) {
		super();
		this.adapted = adapted;
	}

	public TaskNode getAdaptedNode() {
		return this.adapted;
	}

	/**
	 * Add an input port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addInputPort(IPort p) {
		super.addInputPort(p);

	}

	@Override
	public void removeInputPort(IPort p) {
		super.removeInputPort(p);

	}

	/**
	 * Add an output port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addOutputPort(IPort p) {
		super.addOutputPort(p);

	}

	@Override
	public void removeOutputPort(IPort p) {
		super.removeOutputPort(p);

	}

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	@Override
	public IHierarchicalTaskGraphAdapter getGraph() {
		return (IHierarchicalTaskGraphAdapter) super.getGraph();
	}

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ITaskDependencyAdapter> getIncomingEdges() {
		return (List<ITaskDependencyAdapter>) super.getIncomingEdges();
	}

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ITaskDependencyAdapter> getOutgoingEdges() {
		return (List<ITaskDependencyAdapter>) super.getOutgoingEdges();
	}

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IPortAdapter> getInputPorts() {
		return (List<IPortAdapter>) super.getInputPorts();
	}

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IPortAdapter> getOutputPorts() {
		return (List<IPortAdapter>) super.getOutputPorts();
	}

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ITaskNodeAdapter> getPredecessors() {
		return (List<ITaskNodeAdapter>) super.getPredecessors();
	}

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ITaskNodeAdapter> getSuccessors() {
		return (List<ITaskNodeAdapter>) super.getSuccessors();
	}

	@Override
	public IAdaptedContainer getContainer() {
		throw new UnsupportedOperationException("Not yet Implemented");
	}

}
