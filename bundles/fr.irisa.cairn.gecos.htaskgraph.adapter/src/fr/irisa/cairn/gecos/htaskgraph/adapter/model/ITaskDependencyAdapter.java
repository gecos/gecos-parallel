package fr.irisa.cairn.gecos.htaskgraph.adapter.model;

import htaskgraph.TaskDependency;

/**
 * Interface of an edge adapter.

 * This edge adapt a TaskDependency

 * @author <your name>
 *
 */
public interface ITaskDependencyAdapter extends fr.irisa.cairn.graph.IEdge {

	/**
	 * get the adapted edge
	 *
	 * @return the adapted edge
	 */
	TaskDependency getAdaptedEdge();

	/**
	 * @model
	 */
	IPortAdapter getSourcePort();

	/**
	 * @model
	 */
	IPortAdapter getSinkPort();

}
