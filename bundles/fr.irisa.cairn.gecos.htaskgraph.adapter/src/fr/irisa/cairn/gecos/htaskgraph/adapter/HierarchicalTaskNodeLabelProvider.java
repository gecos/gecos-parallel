package fr.irisa.cairn.gecos.htaskgraph.adapter;

import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import htaskgraph.TaskNode;

public class HierarchicalTaskNodeLabelProvider extends
		NodeLabelProvider<ITaskNodeAdapter, ISubGraphNode> {

	@Override
	public String getNodeLabel(ITaskNodeAdapter n) {
		TaskNode bb = n.getAdaptedNode();
		return bb.toShortString()+"[]";//+bb.getChildrenTasks().size()+']';
	}

	
	
	

}
