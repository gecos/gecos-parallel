package fr.irisa.cairn.gecos.htaskgraph.adapter.impl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IPortAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import htaskgraph.TaskDependency;

/**
 * implementation of a node adapter.
 * This graph adapt a TaskDependency
 * @author <your name>
 *
 */
public class TaskDependencyAdapterImpl extends fr.irisa.cairn.graph.implement.Edge
		implements
			ITaskDependencyAdapter {

	private final TaskDependency adapted;

	public TaskDependencyAdapterImpl(TaskDependency adapted, IPortAdapter source,
			IPortAdapter sink) {
		super(source, sink);
		this.adapted = adapted;
	}

	public TaskDependency getAdaptedEdge() {
		return this.adapted;
	}

	/**
	 * @model
	 */
	@Override
	public IPortAdapter getSourcePort() {
		return (IPortAdapter) super.getSourcePort();
	}

	/**
	 * @model
	 */
	@Override
	public IPortAdapter getSinkPort() {
		return (IPortAdapter) super.getSinkPort();
	}

}
