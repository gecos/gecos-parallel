package fr.irisa.cairn.gecos.htaskgraph.adapter.impl;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IAdaptedContainer;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskDependencyAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * implementation of a graph adapter.
 * This graph adapt a HierarchicalTaskGraph
 * @author <your name>
 *
 */
public class HierarchicalTaskGraphAdapterImpl extends fr.irisa.cairn.graph.implement.Graph
		implements
			IHierarchicalTaskGraphAdapter {

	private IAdaptedContainer container;
	private final HierarchicalTaskNode adapted;

	public HierarchicalTaskGraphAdapterImpl(HierarchicalTaskNode adapted, boolean directed) {
		super(directed);
		this.adapted = adapted;
		this.container = new AdaptedContainerImpl();
	}

	public HierarchicalTaskNode getAdaptedGraph() {
		return this.adapted;
	}

	public IAdaptedContainer getContainer() {
		return this.container;
	}

	/**
	 * Add an edge to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            an Edge
	 */
	@Override
	public void addEdge(IEdge edge) {
		super.addEdge(edge);

		if (edge instanceof ITaskDependencyAdapter) {
			this.container.addEdge((ITaskDependencyAdapter) edge);
		}

	}

	/**
	 * Add a node to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            a node
	 */
	@Override
	public void addNode(INode node) {
		super.addNode(node);

		if (node instanceof ITaskNodeAdapter) {
			this.container.addNode((ITaskNodeAdapter) node);
		}
	}

	@Override
	public void removeEdge(IEdge edge) {
		super.removeEdge(edge);

		if (edge instanceof ITaskDependencyAdapter) {
			this.container.removeEdge((ITaskDependencyAdapter) edge);
		}

	}

	@Override
	public void removeNode(INode node) {
		super.removeNode(node);
		if (node instanceof ITaskNodeAdapter) {
			this.container.removeNode((ITaskNodeAdapter) node);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.Set<ITaskNodeAdapter> getNodes() {
		return (java.util.Set<ITaskNodeAdapter>) super.getNodes();
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.Set<ITaskDependencyAdapter> getEdges() {
		return (java.util.Set<ITaskDependencyAdapter>) super.getEdges();
	} 

	public IHierarchicalTaskGraphAdapter getAdapterGraph(HierarchicalTaskNode graph) {
		IHierarchicalTaskGraphAdapter graphAdapter;
		graphAdapter = this.container.getAdapterGraph(graph);
		return graphAdapter;
	}

	public ITaskNodeAdapter getAdapterNode(TaskNode node) {
		ITaskNodeAdapter nodeAdapter;
		nodeAdapter = this.container.getAdapterNode(node);
		return nodeAdapter;
	}

	public ITaskDependencyAdapter getAdapterEdge(TaskDependency edge) {
		ITaskDependencyAdapter edgeAdapter;
		edgeAdapter = this.container.getAdapterEdge(edge);
		return edgeAdapter;
	}

}
