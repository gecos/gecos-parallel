package fr.irisa.cairn.gecos.htaskgraph.adapter.module;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.gecos.htaskgraph.adapter.builder.AdapterFactory;

public class AdapterFactoryModule extends ServiceModule {

	@Override
	protected void configure() {
		this.bindFactory();
	}

	protected void bindFactory() {
		this.bind(AdapterFactory.class).in(Singleton.class);
	}
	
	
}
