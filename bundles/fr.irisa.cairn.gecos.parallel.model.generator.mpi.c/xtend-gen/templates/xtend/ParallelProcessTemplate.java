package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import gecos.annotations.AnnotatedElement;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.types.FunctionType;
import gecos.types.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import parallel.ParallelApplication;
import templates.xtend.ParallelTemplate;

@SuppressWarnings("all")
public class ParallelProcessTemplate extends ParallelTemplate {
  protected String _generate(final ParallelApplication app) {
    String _xblockexpression = null;
    {
      final StringBuilder buffer = new StringBuilder();
      buffer.append("#include <stdio.h>\n");
      buffer.append("#include <mpi.h>\n\n");
      final Function1<Procedure, String> _function = (Procedure it) -> {
        return it.getSymbolName();
      };
      final Map<String, List<Procedure>> procedures_m = IterableExtensions.<String, Procedure>groupBy(app.listProcedures(), _function);
      boolean first = true;
      Set<String> _keySet = procedures_m.keySet();
      for (final String procName : _keySet) {
        {
          final List<Procedure> procs = procedures_m.get(procName);
          final ProcedureSymbol proc0Sym = procs.get(0).getSymbol();
          final Function1<Procedure, Boolean> _function_1 = (Procedure it) -> {
            ProcedureSymbol _symbol = it.getSymbol();
            return Boolean.valueOf(this.isSimilarProcedure(((ProcedureSymbol) _symbol), ((ProcedureSymbol) proc0Sym)));
          };
          ParallelProcessTemplate._assert(IterableExtensions.<Procedure>forall(procs, _function_1));
          if (first) {
            first = false;
            final Procedure procedure = procs.get(0);
            if (((!Objects.equal(procedure.getPragma(), null)) && (!procedure.getPragma().getContent().isEmpty()))) {
              StringConcatenation _builder = new StringConcatenation();
              CharSequence _generatePragma = this.generatePragma(((AnnotatedElement) procedure));
              _builder.append(_generatePragma);
              _builder.newLineIfNotEmpty();
              buffer.append(_builder);
            }
            Type _type = procedure.getSymbol().getType();
            buffer.append(ExtendableTypeCGenerator.eInstance.generate(((FunctionType) _type).getReturnType()));
            buffer.append(" ");
            buffer.append(procedure.getSymbol().getName());
            buffer.append("(");
            EList<Symbol> _symbols = procedure.getScope().getSymbols();
            for (final Symbol symbol : _symbols) {
              {
                if (((!Objects.equal(symbol.getPragma(), null)) && (!symbol.getPragma().getContent().isEmpty()))) {
                  StringConcatenation _builder_1 = new StringConcatenation();
                  _builder_1.newLine();
                  CharSequence _generatePragma_1 = this.generatePragma(((AnnotatedElement) symbol));
                  _builder_1.append(_generatePragma_1);
                  _builder_1.newLineIfNotEmpty();
                  buffer.append(_builder_1);
                  StringConcatenation _builder_2 = new StringConcatenation();
                  _builder_2.append("\t");
                  String _generate = ExtendableCoreCGenerator.eInstance.generate(symbol);
                  _builder_2.append(_generate, "\t");
                  buffer.append(_builder_2);
                } else {
                  StringConcatenation _builder_3 = new StringConcatenation();
                  String _generate_1 = ExtendableCoreCGenerator.eInstance.generate(symbol);
                  _builder_3.append(_generate_1);
                  buffer.append(_builder_3);
                }
                EList<Symbol> _symbols_1 = procedure.getScope().getSymbols();
                int _size = procedure.getScope().getSymbols().size();
                int _minus = (_size - 1);
                boolean _equals = symbol.equals(_symbols_1.get(_minus));
                boolean _not = (!_equals);
                if (_not) {
                  buffer.append(", ");
                }
              }
            }
            Type _type_1 = procedure.getSymbol().getType();
            boolean _isHasElipsis = ((FunctionType) _type_1).isHasElipsis();
            if (_isHasElipsis) {
              buffer.append(", ...");
            }
            buffer.append(")");
          }
          buffer.append(" {\n");
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("\t");
          _builder_1.append("int rank;");
          buffer.append(_builder_1);
          buffer.append("\n\n");
          boolean _equals = Objects.equal(procName, "main");
          if (_equals) {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("\t");
            _builder_2.append("MPI_Init(NULL, NULL);");
            buffer.append(_builder_2);
            buffer.append("\n");
          }
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("\t");
          _builder_3.append("MPI_Comm_rank(MPI_COMM_WORLD, &rank);");
          buffer.append(_builder_3);
          final Function1<Procedure, CharSequence> _function_2 = (Procedure it) -> {
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("\t");
            _builder_4.append("if(rank == ");
            ProcedureSet _containingProcedureSet = it.getContainingProcedureSet();
            int _rank = ((parallel.Process) _containingProcedureSet).getRank();
            _builder_4.append(_rank, "\t");
            _builder_4.append(") ");
            {
              CompositeBlock _body = it.getBody();
              boolean _tripleEquals = (_body == null);
              if (_tripleEquals) {
                _builder_4.append(";");
              } else {
                String _generate = ExtendableBlockCGenerator.eInstance.generate(it.getBody());
                _builder_4.append(_generate, "\t");
              }
            }
            return _builder_4.toString();
          };
          buffer.append(IterableExtensions.<Procedure>join(procs, "\n\n", "\n", "\n\n", _function_2));
          boolean _equals_1 = Objects.equal(procName, "main");
          if (_equals_1) {
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("\t");
            _builder_4.append("MPI_Finalize();");
            buffer.append(_builder_4);
            buffer.append("\n}");
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      String _string = buffer.toString();
      _builder.append(_string);
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final parallel.Process p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Process ");
    int _rank = p.getRank();
    _builder.append(_rank);
    return _builder;
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public CharSequence generatePragma(final AnnotatedElement annotatedElement) {
    StringConcatenation _builder = new StringConcatenation();
    String _xifexpression = null;
    if (((!Objects.equal(annotatedElement.getPragma(), null)) && (!annotatedElement.getPragma().getContent().isEmpty()))) {
      _xifexpression = ExtendableAnnotationCGenerator.eInstance.generate(annotatedElement.getPragma());
    }
    _builder.append(_xifexpression);
    return _builder;
  }
  
  private boolean isSimilarProcedure(final ProcedureSymbol symbol1, final ProcedureSymbol symbol0) {
    return true;
  }
  
  private static void _assert(final boolean b) {
    if ((!b)) {
      throw new RuntimeException("Assertion failed");
    }
  }
  
  public Object generate(final EObject app) {
    if (app instanceof ParallelApplication) {
      return _generate((ParallelApplication)app);
    } else if (app instanceof parallel.Process) {
      return _generate((parallel.Process)app);
    } else if (app != null) {
      return _generate(app);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(app).toString());
    }
  }
}
