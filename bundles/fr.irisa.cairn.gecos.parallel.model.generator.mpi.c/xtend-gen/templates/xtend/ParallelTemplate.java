package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public class ParallelTemplate implements IGecosCodeGenerator {
  public Object generate(final EObject o) {
    return null;
  }
  
  @Override
  public String generate(final Object o) {
    Object _xblockexpression = null;
    {
      final Object object = this.generate(((EObject) o));
      boolean _notEquals = (!Objects.equal(object, null));
      if (_notEquals) {
        return object.toString();
      }
      _xblockexpression = null;
    }
    return ((String)_xblockexpression);
  }
}
