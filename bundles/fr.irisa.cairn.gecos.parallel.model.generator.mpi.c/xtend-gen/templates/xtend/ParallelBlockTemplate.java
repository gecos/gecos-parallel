package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory;
import gecos.instrs.Instruction;
import gecos.types.Type;
import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import parallel.CommType;
import parallel.Message;
import parallel.Receive;
import parallel.Send;
import templates.xtend.ParallelTemplate;

@SuppressWarnings("all")
public class ParallelBlockTemplate extends ParallelTemplate {
  protected Object _generate(final Send send) {
    boolean _isEmpty = send.getReceivingProcesses().isEmpty();
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("/* no receiver is specified! (");
      _builder.append(send);
      _builder.append(") */");
      _builder.newLineIfNotEmpty();
      return _builder;
    }
    final Message msg = send.getMessage();
    CommType _commType = send.getCommType();
    final boolean isAsync = Objects.equal(_commType, CommType.ASYNC);
    final StringBuilder sb = new StringBuilder();
    final Function1<parallel.Process, CharSequence> _function = (parallel.Process it) -> {
      return this.generateSendTo1Process(msg, isAsync, it);
    };
    sb.append(IterableExtensions.<parallel.Process>join(send.getReceivingProcesses(), "", _function));
    final Function1<Instruction, CharSequence> _function_1 = (Instruction it) -> {
      return ExtendableInstructionCGenerator.eInstance.generate(it);
    };
    sb.append(IterableExtensions.<Instruction>join(send.getInstructions(), "", ";\n", ";\n", _function_1));
    return sb.toString();
  }
  
  public String generateSendTo1Process(final Message msg, final boolean isAsync, final parallel.Process to) {
    String _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("MPI_");
      {
        if (isAsync) {
          _builder.append("Is");
        } else {
          _builder.append("S");
        }
      }
      _builder.append("end");
      final String mpi_send = _builder.toString();
      final String buf = ExtendableInstructionCGenerator.eInstance.generate(msg.getAddress());
      final String count = ExtendableInstructionCGenerator.eInstance.generate(msg.getCount());
      Type _type = msg.getType();
      final CharSequence type = this.generateMPIType(((Type) _type));
      final int dest = to.getRank();
      final Object tag = this.generateMPITag(msg.getTag());
      final String communicator = "MPI_COMM_WORLD";
      final String request = "NULL/*request TODO*/";
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(mpi_send);
      _builder_1.append("(");
      _builder_1.append(buf);
      _builder_1.append(", ");
      _builder_1.append(count);
      _builder_1.append(", ");
      _builder_1.append(type);
      _builder_1.append(", ");
      _builder_1.append(dest);
      _builder_1.append(", ");
      _builder_1.append(tag);
      _builder_1.append(", ");
      _builder_1.append(communicator);
      {
        if (isAsync) {
          _builder_1.append(", ");
          _builder_1.append(request);
        }
      }
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      _xblockexpression = _builder_1.toString();
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Receive receive) {
    boolean _isEmpty = receive.getSendingProcesses().isEmpty();
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("/* no sender is specified! (");
      _builder.append(receive);
      _builder.append(") */");
      _builder.newLineIfNotEmpty();
      return _builder;
    }
    final Message msg = receive.getMessage();
    CommType _commType = receive.getCommType();
    final boolean isAsync = Objects.equal(_commType, CommType.ASYNC);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("MPI_");
    {
      if (isAsync) {
        _builder_1.append("Ir");
      } else {
        _builder_1.append("R");
      }
    }
    _builder_1.append("ecv");
    final String mpi_recv = _builder_1.toString();
    final String buf = ExtendableInstructionCGenerator.eInstance.generate(msg.getAddress());
    final String count = ExtendableInstructionCGenerator.eInstance.generate(msg.getCount());
    Type _type = msg.getType();
    final CharSequence type = this.generateMPIType(((Type) _type));
    Object _xifexpression = null;
    int _size = receive.getSendingProcesses().size();
    boolean _equals = (_size == 1);
    if (_equals) {
      _xifexpression = Integer.valueOf(receive.getSendingProcesses().get(0).getRank());
    } else {
      _xifexpression = "MPI_ANY_SOURCE";
    }
    final Object source = ((Object)_xifexpression);
    final Object tag = this.generateMPITag(msg.getTag());
    final String communicator = "MPI_COMM_WORLD";
    String _xifexpression_1 = null;
    if (isAsync) {
      _xifexpression_1 = "NULL/*request TODO*/";
    } else {
      _xifexpression_1 = "MPI_STATUS_IGNORE";
    }
    final String statusOrRequest = _xifexpression_1;
    final StringBuilder sb = new StringBuilder();
    StringConcatenation _builder_2 = new StringConcatenation();
    _builder_2.append(mpi_recv);
    _builder_2.append("(");
    _builder_2.append(buf);
    _builder_2.append(", ");
    _builder_2.append(count);
    _builder_2.append(", ");
    _builder_2.append(type);
    _builder_2.append(", ");
    _builder_2.append(((Object)source));
    _builder_2.append(", ");
    _builder_2.append(tag);
    _builder_2.append(", ");
    _builder_2.append(communicator);
    _builder_2.append(", ");
    _builder_2.append(statusOrRequest);
    _builder_2.append(");");
    _builder_2.newLineIfNotEmpty();
    sb.append(_builder_2);
    final Function1<Instruction, CharSequence> _function = (Instruction it) -> {
      return ExtendableInstructionCGenerator.eInstance.generate(it);
    };
    sb.append(IterableExtensions.<Instruction>join(receive.getInstructions(), "", ";\n", ";\n", _function));
    return sb.toString();
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  /**
   * TODO verify mapping and complete
   */
  public CharSequence generateMPIType(final Type type) {
    CharSequence _xblockexpression = null;
    {
      final TypeAnalyzer ta = new TypeAnalyzer(type);
      CharSequence _switchResult = null;
      boolean _matched = false;
      boolean _isInt8 = ta.getBaseLevel().isInt8();
      if (_isInt8) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("MPI_BYTE");
        _switchResult = _builder;
      }
      if (!_matched) {
        boolean _isInt16 = ta.getBaseLevel().isInt16();
        if (_isInt16) {
          _matched=true;
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("MPI_SHORT");
          _switchResult = _builder_1;
        }
      }
      if (!_matched) {
        boolean _isInt64 = ta.getBaseLevel().isInt64();
        if (_isInt64) {
          _matched=true;
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("MPI_LONG");
          _switchResult = _builder_2;
        }
      }
      if (!_matched) {
        boolean _isInt = ta.getBaseLevel().isInt();
        if (_isInt) {
          _matched=true;
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("MPI_INT");
          _switchResult = _builder_3;
        }
      }
      if (!_matched) {
        boolean _isFloatSingle = ta.getBaseLevel().isFloatSingle();
        if (_isFloatSingle) {
          _matched=true;
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append("MPI_FLOAT");
          _switchResult = _builder_4;
        }
      }
      if (!_matched) {
        boolean _isFloatDouble = ta.getBaseLevel().isFloatDouble();
        if (_isFloatDouble) {
          _matched=true;
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append("MPI_DOUBLE");
          _switchResult = _builder_5;
        }
      }
      if (!_matched) {
        StringConcatenation _builder_6 = new StringConcatenation();
        _builder_6.append("/* unable to generate MPI_TYPE for ");
        _builder_6.append(type);
        _builder_6.append(" */");
        _switchResult = _builder_6;
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  public Object generateMPITag(final int tag) {
    Object _switchResult = null;
    switch (tag) {
      case GecosUserParallelModelFactory.TAG_ANY:
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("MPI_TAG_ANY");
        _switchResult = _builder;
        break;
      default:
        _switchResult = Integer.valueOf(tag);
        break;
    }
    return _switchResult;
  }
  
  public Object generate(final EObject receive) {
    if (receive instanceof Receive) {
      return _generate((Receive)receive);
    } else if (receive instanceof Send) {
      return _generate((Send)receive);
    } else if (receive != null) {
      return _generate(receive);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(receive).toString());
    }
  }
}
