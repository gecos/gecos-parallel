package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import gecos.annotations.AnnotatedElement
import gecos.core.ProcedureSymbol
import gecos.types.FunctionType
import org.eclipse.emf.ecore.EObject
import parallel.ParallelApplication
import parallel.Process

class ParallelProcessTemplate extends ParallelTemplate {
	
	def dispatch String generate(ParallelApplication app) {
		val buffer = new StringBuilder();
		
		//XXX 
//		val project = EMFUtils.eContainerTypeSelect(app, GecosProject);
//		if(project !== null) {
//			buffer.append(project.includes.join("","\n","\n", ['''#include <«name»>''']))
//		}
		buffer.append("#include <stdio.h>\n")
		buffer.append("#include <mpi.h>\n\n")
		

		
//		/* generate processes global symbols */
//		buffer.append(app.processes.map[scope].join("\n", "\n", "\n", [ExtendableCoreCGenerator::eInstance.generate(it)]))

		
		val procedures_m = app.listProcedures.groupBy[symbolName]
		
		//NOTE: procedures defined in different processes with the same name must have the same signature !
		var first = true
		for(String procName : procedures_m.keySet()) {
			val procs = procedures_m.get(procName)
			val proc0Sym = procs.get(0).getSymbol()
//			_assert(procs.stream().allMatch([Procedure pd | isSimilarProcedure(pd.getSymbol(), proc0Sym)]))
			_assert(procs.forall[isSimilarProcedure(it.getSymbol() as ProcedureSymbol, proc0Sym as ProcedureSymbol)])
			
			if(first) {
				first = false
				val procedure = procs.get(0)
				if (procedure.pragma != null && !procedure.pragma.content.empty)
				buffer.append('''«generatePragma(procedure as AnnotatedElement)»
					''')
				buffer.append(ExtendableTypeCGenerator::eInstance.generate((procedure.symbol.type as FunctionType).returnType))
				buffer.append(" ")
				buffer.append(procedure.symbol.name)
				buffer.append("(")
				
				for (symbol : procedure.scope.symbols) {
					if (symbol.pragma != null && !symbol.pragma.content.empty) {
						buffer.append('''
						
						«generatePragma(symbol as AnnotatedElement)»
						''')
						buffer.append('''	«ExtendableCoreCGenerator::eInstance.generate(symbol)»''')
					} else
						buffer.append('''«ExtendableCoreCGenerator::eInstance.generate(symbol)»''')
					if (!symbol.equals(procedure.scope.symbols.get(procedure.scope.symbols.size - 1)))
						buffer.append(", ")
				}
				if ((procedure.symbol.type as FunctionType).hasElipsis)
					buffer.append(", ...");
				buffer.append(")")
			}
			buffer.append(" {\n")
			buffer.append('''	int rank;''')
			buffer.append("\n\n")
			
			if(procName == "main") {
				buffer.append('''	MPI_Init(NULL, NULL);''')
				buffer.append("\n")
			}
			
			//query rank
			buffer.append('''	MPI_Comm_rank(MPI_COMM_WORLD, &rank);''')
			
//			buffer.append(procs.stream()
//				.map([Procedure pd | '''	if(rank == «(pd.getContainingProcedureSet() as Process).getRank()»)''' + 
//					'''	«IF pd.body == null»;«ELSE»«ExtendableBlockCGenerator::eInstance.generate(pd.body)»«ENDIF»'''])
//				.collect(Collectors.joining("\n","\n\n","\n\n")))
			buffer.append(procs.join("\n\n", "\n", "\n\n", [
				'''	if(rank == «(containingProcedureSet as Process).getRank()») «
				IF body === null»;«ELSE»«ExtendableBlockCGenerator::eInstance.generate(body)»«ENDIF»''']))
					
			if(procName == "main") {
				//finilize mpi
				buffer.append('''	MPI_Finalize();''')
				buffer.append("\n}")
			}
		}

		'''«buffer.toString»'''
	}
	
	def dispatch generate(Process p) {
		'''Process «p.rank»'''
	}
	
	def dispatch generate(EObject object) {
		null
	}
	
	def generatePragma(AnnotatedElement annotatedElement) {
		'''«if (annotatedElement.pragma != null && !annotatedElement.pragma.content.empty) 
			ExtendableAnnotationCGenerator::eInstance.generate(annotatedElement.pragma)»'''
	}
	
	def private boolean isSimilarProcedure(ProcedureSymbol symbol1, ProcedureSymbol symbol0) {
		return true; //TODO
	}

	def static private void _assert(boolean b) {
		if(!b) throw new RuntimeException("Assertion failed");
	}
}