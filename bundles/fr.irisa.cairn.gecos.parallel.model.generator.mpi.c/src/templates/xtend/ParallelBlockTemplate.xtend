package templates.xtend

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import gecos.types.Type
import org.eclipse.emf.ecore.EObject
import parallel.CommType
import parallel.Message
import parallel.Receive
import parallel.Send
import parallel.Process
import fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator
import gecos.blocks.BasicBlock

class ParallelBlockTemplate extends ParallelTemplate {
	
	def dispatch generate(Send send) {
		if(send.receivingProcesses.isEmpty)
			return '''/* no receiver is specified! («send») */
			'''
		val msg = send.message
		val isAsync = send.commType == CommType.ASYNC
		
		val sb = new StringBuilder
		
		sb.append(send.receivingProcesses.join("", [generateSendTo1Process(msg, isAsync, it)]))
		
		/* generate profiling instructions if any */
		sb.append(send.instructions.join("", ";\n", ";\n", [ExtendableInstructionCGenerator.eInstance.generate(it)]))
		
		return sb.toString
	}
	
	def String generateSendTo1Process(Message msg, boolean isAsync, Process to) {
		val mpi_send = '''MPI_«IF isAsync»Is«ELSE»S«ENDIF»end'''
		val buf = ExtendableInstructionCGenerator.eInstance.generate(msg.address)
		val count = ExtendableInstructionCGenerator.eInstance.generate(msg.count)
		val type = generateMPIType(msg.type as Type)
		val dest = to.rank
		val tag = generateMPITag(msg.tag)
		val communicator = "MPI_COMM_WORLD" //XXX
		val request = "NULL/*request TODO*/" // TODO
		
		'''
		«mpi_send»(«buf», «count», «type», «dest», «tag», «communicator»«IF isAsync», «request»«ENDIF»);
		'''
	}
	
	def dispatch generate(Receive receive) {
		if(receive.sendingProcesses.isEmpty)
			return '''/* no sender is specified! («receive») */
			'''
		val msg = receive.message
		val isAsync = receive.commType == CommType.ASYNC
		
//		receive.sendingProcesses.join("", [generateRecvFrom1Process(msg, isAsync, it)])
//	}
//	
//	def String generateRecvFrom1Process(Message msg, boolean isAsync, Process from) {
		val mpi_recv = '''MPI_«IF isAsync»Ir«ELSE»R«ENDIF»ecv'''
		val buf = ExtendableInstructionCGenerator.eInstance.generate(msg.address)
		val count = ExtendableInstructionCGenerator.eInstance.generate(msg.count)
		val type = generateMPIType(msg.type as Type)
//		val source = from.rank
		val source = if(receive.sendingProcesses.size == 1) receive.sendingProcesses.get(0).rank else "MPI_ANY_SOURCE" 
		val tag = generateMPITag(msg.tag)
		val communicator = "MPI_COMM_WORLD" //XXX
		
		val statusOrRequest = if(isAsync) "NULL/*request TODO*/" //status
								else "MPI_STATUS_IGNORE"	// request
		
		val sb = new StringBuilder
		
		sb.append(
		'''
		«mpi_recv»(«buf», «count», «type», «source», «tag», «communicator», «statusOrRequest»);
		'''
		)
		
		/* generate profiling instructions if any */
		sb.append(receive.instructions.join("", ";\n", ";\n", [ExtendableInstructionCGenerator.eInstance.generate(it)]))
		
		return sb.toString
	}
	
	def dispatch generate(EObject object) {
		null
	}
	
	/**
	 * TODO verify mapping and complete
	 */
	def generateMPIType(Type type) {
		val ta = new TypeAnalyzer(type)
		switch ta {
			case ta.baseLevel.isInt8: '''MPI_BYTE'''	//XXX
			case ta.baseLevel.isInt16: '''MPI_SHORT''' 	//XXX
			case ta.baseLevel.isInt64: '''MPI_LONG''' 	//XXX
			case ta.baseLevel.isInt: '''MPI_INT'''
			case ta.baseLevel.isFloatSingle: '''MPI_FLOAT'''
			case ta.baseLevel.isFloatDouble: '''MPI_DOUBLE'''
			default: '''/* unable to generate MPI_TYPE for «type» */'''
		}
	}
	
	def generateMPITag(int tag) {
		switch tag {
			case GecosUserParallelModelFactory.TAG_ANY: '''MPI_TAG_ANY'''
			default: tag
		}
	}
	
}