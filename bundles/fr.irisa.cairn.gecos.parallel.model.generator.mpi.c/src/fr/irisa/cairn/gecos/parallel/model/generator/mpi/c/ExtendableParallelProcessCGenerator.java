package fr.irisa.cairn.gecos.parallel.model.generator.mpi.c;

import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;
import templates.xtend.ParallelProcessTemplate;


public class ExtendableParallelProcessCGenerator extends ExtendableGenerator {

	public static ExtendableParallelProcessCGenerator eInstance = new ExtendableParallelProcessCGenerator();

	public ExtendableParallelProcessCGenerator() {
		super(new ParallelProcessTemplate());
	}
	
	public ExtendableParallelProcessCGenerator(IGeneratorDispatchStrategy strategy) {
		super(strategy,new ParallelProcessTemplate());
	}

}
