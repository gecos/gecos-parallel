package fr.irisa.cairn.gecos.parallel.model.generator.mpi.c;

import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;
import templates.xtend.ParallelBlockTemplate;


public class ExtendableParallelBlockCGenerator extends ExtendableGenerator {

	public static ExtendableParallelBlockCGenerator eInstance = new ExtendableParallelBlockCGenerator();

	public ExtendableParallelBlockCGenerator() {
		super(new ParallelBlockTemplate());
	}
	
	public ExtendableParallelBlockCGenerator(IGeneratorDispatchStrategy strategy) {
		super(strategy,new ParallelBlockTemplate());
	}

}
