
int main() {
	int a[10][10], n, r, i, j;

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		n = 12;
		r = 0;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		for(i = 0; i < 4; i++) {
			for(j = 0; j < 4; j++) {
				a[i][j] = i+j;
			}
		}
	}

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		for(i = 0; i < 4; i++) {
			for(j = 0; j < 4; j++) {
				r += a[i][j];
			}
		}
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		printf("3: r=%d, j=%d, n=%d\n", r, j,n);
	}
}
