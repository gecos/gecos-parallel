
int main() {
	int a[10]={2}, s, p, i;


#pragma GECOS_SCHEDULER_PROCESS 0
	{
		s = 0;
		p = 1;
	}


	for(i = 0; i < 4; i++) {
#pragma GECOS_SCHEDULER_PROCESS 1
		{
			s += a[i];
		}
#pragma GECOS_SCHEDULER_PROCESS 2
		{
			p *= a[i];
		}
	}


#pragma GECOS_SCHEDULER_PROCESS 0
	{
		printf("3: s=%d, p=%d\n", s,p);
	}

}
