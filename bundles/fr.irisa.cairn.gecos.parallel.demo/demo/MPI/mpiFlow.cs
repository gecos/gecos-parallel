##############################################################################
# This flow takes a c file (with #pragma GECOS_SCHEDULER_PROCESS N) as input.
# N should be >= 0, each different value represent a different process.
#
# It create and parse a project, build HTG, convert it to ParallelModel, then
# it generates MPI code, compile it, run it and collect Communication profiling
# stats if 'ADD_PROFILING' is true.
#
# To run on some examples use 'DemoMPI.cs' scirpt.
##############################################################################

debug(1);

INPUT_FILE=$1;
NBPROCS=4;
OUTDIR="output/" + basename(INPUT_FILE);

ADD_PROFILING = true;
DO_CHECK = true;


echo("=============================================");
echo("[INFO] Starting mpiFlow.cs with file: " + INPUT_FILE);


## Cleanup
echo("[INFO] Cleanup output directory");
shell("rm -rf " + OUTDIR);

## Create/parse Gecos Project
echo("[INFO] Create/Parse project");
p = CreateGecosProject("test");
AddSourceToGecosProject(p, INPUT_FILE);
CDTFrontend(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/before/cdfg/");

## Create a Dummy Parallel Architecture 
arch = CreateDummyParallelArchitecture(NBPROCS);

## Compute SSA Form
echo("[INFO] Compute SSA form");
ComputeSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/before/cdfg-ssa/");

## Build HTG
echo("[INFO] Build HTG");
HTGTaskExtractor(p, arch);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
HTGDottyExport(p, OUTDIR + "/dotty_htg/before", true);
#output(p,"dot", OUTDIR + "/dotty_cdfg/before/htg/");

## Schedule
echo("[INFO] Propagate tasks allocation (specified by #pragma GECOS_SCHEDULER_PROCESS)");
HTGScheduler(p, arch, OUTDIR + "/scheduler", true);
HTGInsertSyncCom(p);
HTGDottyExport(p, OUTDIR + "/dotty_htg/after", true);

## Remove SSA Form
echo("[INFO] Remove SSA form");
RemoveSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;

## HTG to Parallel Model
echo("[INFO] Convert HTG to Parallel IR");
app = HTG2ParallelModel(p, arch, ADD_PROFILING);
ParallelModelDottyExport(app, OUTDIR + "/dotty_paral");

## MPI codegen
echo("[INFO] Generate MPI code");
CGenerator(app, OUTDIR + "/codegen/mpi");

## Compile & RUN
echo("[INFO] Compile the generated code");
shell("cd " + OUTDIR + "/codegen/mpi && mpicc tmp_gecos_file.c");
echo("[INFO] Execute the generated code");
shell("cd " + OUTDIR + "/codegen/mpi && mpirun -np " + NBPROCS + " ./a.out");
if (ADD_PROFILING) then
	echo("[INFO] Communications Profiling stats:");
	shell("cat " + OUTDIR + "/codegen/mpi/*.stats");
fi;

echo("[INFO] Done");


