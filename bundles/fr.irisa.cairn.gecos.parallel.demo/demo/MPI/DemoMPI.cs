debug(1);

###
# More tests can be found here:
# "../../../../tests/fr.irisa.cairn.gecos.parallel.integration.tests/resources/src-c/parallel/manual_pragma_schedule"
###

FILES = FindFiles("./src-c", ".c");

for file in FILES do
	call("mpiFlow.cs", file);
done;
