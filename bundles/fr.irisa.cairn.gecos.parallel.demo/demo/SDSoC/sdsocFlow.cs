##############################################################################
# This flow takes a c file (with #pragma GECOS_SCHEDULER_PROCESS N) as input.

# All Tasks annotated with '#pragma GECOS_SCHEDULER_PROCESS N' with N > 1
# are considered as HardWare Tasks, N = 0 represent the (only) software Thread.

# It create and parse a project, build HTG, outline HW tasks and add SDSoC 
# pragmas before generating the C code. 
#
# To run on some examples use 'DemoSDSoC.cs' scirpt.
##############################################################################

debug(1);

INPUT_FILE=$1;
NBPROCS=4;
OUTDIR="output/" + basename(INPUT_FILE);

DO_CHECK = true;


echo("=============================================");
echo("[INFO] Starting sdsocFlow.cs with file: " + INPUT_FILE);


## Cleanup
echo("[INFO] Cleanup output directory");
shell("rm -rf " + OUTDIR);

## Create/parse Gecos Project
echo("[INFO] Create/Parse project");
p = CreateGecosProject("test");
AddSourceToGecosProject(p, INPUT_FILE);
CDTFrontend(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/");

## Create a Dummy Parallel Architecture 
arch = CreateDummyParallelArchitecture(NBPROCS);


## Compute SSA Form
echo("[INFO] Compute SSA form");
ComputeSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/ssa/");

## Build HTG
echo("[INFO] Build HTG");
HTGTaskExtractor(p, arch);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
HTGDottyExport(p, OUTDIR + "/dotty_htg/init", true);
output(p,"dot", OUTDIR + "/dotty_htg/init/cdfg/");

## Outline HW tasks and add SDSoC pragmas
echo("[INFO] Ouline HW Tasks and add SDSoC pragmas");
HTGToSdx(p);
#if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
#SaveGecosProject(p, OUTDIR + "/dotty_htg/com");
HTGDottyExport(p, OUTDIR + "/dotty_htg/com", true);
output(p,"dot", OUTDIR + "/dotty_htg/com/cdfg/");

## Remove SSA Form
echo("[INFO] Remove SSA form");
RemoveSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;


## SDSoC codegen
echo("[INFO] Generate C code");
CGenerator(p, OUTDIR + "/codegen/sdx");

echo("[INFO] Done");
