package fr.irisa.cairn.gecos.htaskgraph.scheduling;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import architecture.Architecture;
import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.graph.HTGDependencyEdge;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.graph.HTGResources;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.graph.HTGTask;
import fr.irisa.cairn.gecos.scheduling.graph.IDependencyEdge;
import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

/**
 * Adapt the HTG model to the {@link TaskGraph} model to apply scheduling algorithm.
 * Each {@link LeafTaskNode} of the HTG is adapted as a {@link ITask}.
 * Each {@link TaskDependency} between {@link LeafTaskNode} are adapted as a {@link IDependencyEdge}
 * Each {@link TaskDependency} betwwen a {@link LeafTaskNode}/{@link HierarchicalTaskNode} and a 
 * {@link HierarchicalTaskNode}/{@link LeafTaskNode} are expended and adapted as a {@link IDependencyEdge}  
 * 
 * @author Nicolas Simon
 */
public class HTGToTaskGraph {
	
	public static final String TASK_ALLOCATION_PRAGMA = "GECOS_SCHEDULER_PROCESS";

	private final boolean _DEBUG = false;
	
	private static int _print_inc = 0; 
	private static final void debug(String string) {
		System.out.println(Strings.repeat("   ", _print_inc) + string);
	}
	private static final void incPrint(String string) {
		System.out.println(Strings.repeat("   ", _print_inc) + string);
		_print_inc++;
	}
	private static final void decPrint(String string) {
		_print_inc--;
		System.out.println(Strings.repeat("   ", _print_inc) + string);
	}
	
	private Procedure _procedure;
	private Architecture _architecture;
	private TaskGraph _graph;
	private Map<LeafTaskNode, ITask> created_tasks_map;
	
	public HTGToTaskGraph(Procedure procedure, Architecture architecture){
		_procedure = procedure;
		_architecture = architecture;
		created_tasks_map = new LinkedHashMap<>();
	}
	
	public TaskGraph compute() {
		List<IResources> resources = new LinkedList<IResources>();
		for(ProcessingResource htgResources : _architecture.getProcessingResources())
			resources.add(new HTGResources(htgResources));
		
		_graph = new TaskGraph(HTGDependencyEdge.class, resources);
		_procedure.getBody().accept(new HTGToTaskGraphVisitor());
		
		return _graph;
	}

	private Integer processPragma(Block block){
		PragmaAnnotation pragma = block.getPragma();
		if(pragma != null){
			StringBuilder sBuilder = new StringBuilder();
			for (String str : pragma.getContent()) {
				sBuilder.append(str);
			}
			
			Pattern pattern = Pattern.compile("\\.*" + TASK_ALLOCATION_PRAGMA + "\\s*\\s*([0-9]+)\\s*\\.*");
			Matcher matcher = pattern.matcher(sBuilder);
			if (matcher.find()) {
				int temp = Integer.valueOf(matcher.group(1));
				if(temp < _graph.getResourcesPool().size()){
					return temp;
				}
				else{
					throw new RuntimeException("Specified Processor mapping is not valid! "
							+ "The process id must be < total number of processes given to the scheduler.");
				}
			}
		}
		return null;
	}
	
	private class HTGToTaskGraphVisitor extends HTGDefaultVisitor {

		private Multimap<TaskDependency, LeafTaskNode> inputDependencies = HashMultimap.create(); //<dep, LTN in new garph>
		private Multimap<TaskDependency, LeafTaskNode> outputDependencies = HashMultimap.create();
		private Integer pragmaProcessorNumber;
		private LinkedList<HTGTask> _newVertex;

		@Override
		public void visitHierarchicalTask(HierarchicalTaskNode htn) {
			if(_DEBUG) incPrint("GO IN " + htn);
			
			/* Check whether the task is pragma annotated with scheduling directives */
			Integer _savedPragmaProcessorNumber = pragmaProcessorNumber;
			Integer temp = processPragma(htn);
			if(temp != null)
				pragmaProcessorNumber = temp;
			
			/* Get input and output dependencies of ctn */
			Map<TaskDependency, List<LeafTaskNode>> ctnInputDepdencies = htn.getPreds().stream()
				.collect(toMap(pred -> pred, pred -> pred.getFirstInternalSrcNodes().stream()
							.flatMap(n -> getAllChildLTNInGraph(n).stream()).collect(toList()),
						(x,y) -> {throw new RuntimeException("duplicates!");}, LinkedHashMap::new));
			Map<TaskDependency, List<LeafTaskNode>> ctnOutputDepdencies = htn.getSuccs().stream()
				.collect(toMap(succ -> succ, succ -> succ.getFirstInternalDstNodes().stream()
							.flatMap(n -> getAllChildLTNInGraph(n).stream()).collect(toList()),
						(x,y) -> {throw new RuntimeException("duplicates!");}, LinkedHashMap::new));
			
			LinkedList<HTGTask> childVertex = new LinkedList<HTGTask>();
			for(TaskNode child : htn.getChildTasks()) {
				saveDependencies();
				
				ctnInputDepdencies.forEach((pred,inputs) -> inputDependencies.putAll(pred, inputs));
				ctnOutputDepdencies.forEach((succ,outputs) -> outputDependencies.putAll(succ, outputs));
				
				if(_DEBUG) debug("List Pred of " + htn + " -> " + inputDependencies);
				if(_DEBUG) debug("List Succ of " + htn + " -> " + outputDependencies);
				
				//visit child
				_newVertex = new LinkedList<HTGTask>();
				child.accept((HTaskGraphVisitor)this);
				childVertex.addAll(_newVertex);
				
				for(HTGTask itask : _newVertex) {
					// add external input dependencies
					for(TaskDependency edge : inputDependencies.keySet()){
						for(LeafTaskNode from : inputDependencies.get(edge)){
							ITask iTaskfrom = getCreatedTask((LeafTaskNode) from);
							if(iTaskfrom == null)
								throw new RuntimeException("HTGTask not found which correspond to LTN " + from);
							
							if(_graph.getAllPredecessorOf(iTaskfrom).contains(itask))
								throw new RuntimeException("Impossible to add an edge from " + iTaskfrom + " to " + itask + ". It will be created a cycle");
							_graph.addEdge(iTaskfrom, itask, new HTGDependencyEdge(edge));
							
							if(_DEBUG) debug(iTaskfrom + " to " + itask + " edge created");
						}
					}
					
					// add external output dependencies
					for(TaskDependency edge : outputDependencies.keySet()){
						for(LeafTaskNode to : outputDependencies.get(edge)){
							ITask HTGTaskto = getCreatedTask((LeafTaskNode) to);
							if(HTGTaskto == null)
								throw new RuntimeException("HTGTask not found which correspond to LTN " + to);
							
							if(_graph.getAllPredecessorOf(itask).contains(HTGTaskto))
								throw new RuntimeException("Impossible to add an edge from " + itask + " to " + HTGTaskto + ". It will be created a cycle");
							_graph.addEdge(itask, HTGTaskto, new HTGDependencyEdge(edge));
							
							if(_DEBUG) System.out.println(itask + " to " + HTGTaskto + " edge created");
						}
					}
				}
				
				// Restore dependencies and predecessor state
				restoreDependencies();	
			}
			
			handleIntraLevelDependencies(childVertex);
			
			pragmaProcessorNumber = _savedPragmaProcessorNumber;
			if(_DEBUG) decPrint("GO OUT " + htn);
		}

		protected void handleIntraLevelDependencies(LinkedList<HTGTask> childVertex) {
			for(HTGTask itask : childVertex) {
				// Manage intra-HTN dependencies (between LTN)
				for(TaskDependency taskDep : itask.getLTN().getSuccs()){
					TaskNode to = taskDep.getTo();
					if(to instanceof LeafTaskNode){
						ITask itaskTo = getCreatedTask((LeafTaskNode) to);
						if(itaskTo == null)
							throw new RuntimeException("HTGTask not found which correspond to LTN " + to);
						
						if(_graph.getAllPredecessorOf(itask).contains(itaskTo))
							throw new RuntimeException("Impossible to add an edge from " + itask + " to " + itaskTo + ". It will be created a cycle");
						_graph.addEdge(itask, itaskTo, new HTGDependencyEdge(taskDep));
						
						if(_DEBUG) debug(itask + " to " + itaskTo + " edge created");
					}
				}
			}
		}
		
		@Override
		public void visitLeafTaskNode(LeafTaskNode task) {
			if(getCreatedTask(task) == null){
				HTGTask htgTask = new HTGTask(task);
				created_tasks_map.put(task, htgTask);
				
				if(pragmaProcessorNumber != null)
					htgTask.setResources(_graph.getResourcesPool().get(pragmaProcessorNumber));
//						processConstraint(htgTask, htn);
				
				_newVertex.add(htgTask);
				_graph.addVertex(htgTask);
				
				if(_DEBUG) debug(htgTask + " added in graph");
			}
			else{	
				throw new RuntimeException("Try to add a vertex representing the same leaf task :" + task);
			}
		}
		
		Multimap<TaskDependency, LeafTaskNode> listPredSave = HashMultimap.create();
		Multimap<TaskDependency, LeafTaskNode> listSuccSave = HashMultimap.create();
		
		private void saveDependencies() {
			listPredSave.clear();
			listSuccSave.clear();
			listPredSave.putAll(inputDependencies);
			listSuccSave.putAll(outputDependencies);
		}
		
		private void restoreDependencies() {
			inputDependencies.clear();
			outputDependencies.clear();
			inputDependencies.putAll(listPredSave);
			outputDependencies.putAll(listSuccSave);
		}

		private ITask getCreatedTask(LeafTaskNode ltn){
			return created_tasks_map.get(ltn);
		}
		
		private List<LeafTaskNode> getAllChildLTNInGraph(TaskNode from){
			if(from instanceof HierarchicalTaskNode)
				return ((HierarchicalTaskNode) from).getAllLevelsChildTasks().stream()
					.filter(t -> t instanceof LeafTaskNode && getCreatedTask((LeafTaskNode) t) != null)
					.map(t -> (LeafTaskNode)t)
					.collect(toList());
			else if(from instanceof LeafTaskNode)
				return Stream.of((LeafTaskNode)from)
					.filter(t -> getCreatedTask(t) != null)
					.collect(toList());
			throw new RuntimeException("Unsupported task Type: " + from);
		}
		
	}

}
