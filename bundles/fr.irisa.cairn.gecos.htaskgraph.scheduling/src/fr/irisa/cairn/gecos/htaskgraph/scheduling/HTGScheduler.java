package fr.irisa.cairn.gecos.htaskgraph.scheduling;

import java.io.File;

import com.google.common.base.Strings;

import architecture.Architecture;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.PropagatePRsOutwardDuplicateControl;
import fr.irisa.cairn.gecos.scheduling.IScheduler;
import fr.irisa.cairn.gecos.scheduling.algorithm.ListScheduler;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

@GSModule("Apply a list scheduling algorithm on a Hiearchical Task Graph GeCoS model.\n"
		+ "The scheduler algorithm schedule each procedure task pool independently.\n"
		+ "A task pool contains all LeafTaskNode of procedure.\n"
		+ "Dependencies between HiearchicalTaskNode/LeafTaskNode and LeafTaskNode/HiearchicalTaskNode\n"
		+ "are expended to LeafTaskNode/LeafTaskNode")
public class HTGScheduler {
	private GecosProject _project;
	private IScheduler _scheduler;
	private Architecture architecture;
	private File _outputFolderFile;
	private boolean propagateOnly;
	

	@GSModuleConstructor(""
			+ "-arg1: GecosProject to pass to the frontend.\n"
			+ "   See: 'CreateGecosProject' module."
			+ "\n-arg2: Parallel architecture Model.")
	public HTGScheduler(GecosProject project, Architecture arch){
		this(project, arch, "", false);
	}
	
	@GSModuleConstructor(""
			+ "-arg1: GecosProject to pass to the frontend.\n"
			+ "   See: 'CreateGecosProject' module."
			+ "\n-arg2: Parallel architecture Model."
			+ "\n-arg3: output folder path to generate paraver file and dotty file")
	public HTGScheduler(GecosProject project, Architecture arch, String outputFolderPath, boolean propagateOnly){
		this._project = project;
		this.architecture = arch;
		this._scheduler = new ListScheduler();
		this.propagateOnly = propagateOnly;
		if(!Strings.isNullOrEmpty(outputFolderPath)) {
			this._outputFolderFile = new File(outputFolderPath);
			if(!_outputFolderFile.isDirectory() && !_outputFolderFile.mkdirs())
				throw new RuntimeException(_outputFolderFile.getAbsolutePath() + " is not found or cannot be created");
		}
	}

	public void compute(){	
		for(Procedure p : _project.listProcedures()){
			if(propagateOnly) {
				p.getBody().accept(new PropagatePRsOutwardDuplicateControl());
			} else {
				TaskGraph taskGraph = new HTGToTaskGraph(p, architecture).compute();
	
				File gecosSourceFile = new File(((GecosSourceFile) p.getContainingProcedureSet().eContainer()).getName());
				File outputFolder = new File(_outputFolderFile , gecosSourceFile.getName());
				if(_outputFolderFile != null){
					if(!outputFolder.isDirectory() && !outputFolder.mkdirs())
						throw new RuntimeException("Impossible to create the subfolder " + outputFolder.getName()
								+ " in the output directory " + _outputFolderFile.getAbsolutePath());
					
					taskGraph.SaveAsDot(new File(outputFolder, p.getSymbolName()));
				}
				
				_scheduler.schedule(taskGraph);
				new TaskGraphToHTG(p, taskGraph).compute();
				
				if(_outputFolderFile != null){
					taskGraph.saveAsParaverFile(new File(outputFolder, p.getSymbolName()));
					taskGraph.SaveAsDot(new File(outputFolder, p.getSymbolName() + "_after"));
				}
			}
		}
	}
}
