package fr.irisa.cairn.gecos.htaskgraph.scheduling.graph;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.GenericInstructionSwitch;
import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import htaskgraph.LeafTaskNode;

public class HTGTask implements ITask {
	private LeafTaskNode _ltn;
	private float _cost = 0f;
	private IResources _resources;
	
	public HTGTask(LeafTaskNode ltn){
		_ltn = ltn;
		new CostComputation().doSwitch(ltn.getBlock());
	}
	
	public LeafTaskNode getLTN(){
		return _ltn;
	}

	@Override
	public String toString() {
		return _ltn.toString() + " Cost : " + _cost;
	}
	
	@Override
	public void setResources(IResources resources) {
		_resources = resources;
	}
	
	@Override
	public IResources getResources() {
		return _resources;
	}

	@Override
	public float getCost() {
		return _cost;
	}
		
	private class CostComputation extends BlockInstructionSwitch<Object>{
		private GenericInstCostComputation _genericCostComputer = new GenericInstCostComputation();

		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			_cost += _genericCostComputer.doSwitch(g);
			return super.caseGenericInstruction(g);
		}

		@Override
		public Object caseCallInstruction(CallInstruction g) {
			_cost += 3f;
			return super.caseCallInstruction(g);
		}

		@Override
		public Object caseSetInstruction(SetInstruction s) {
			_cost += 2f;
			return super.caseSetInstruction(s);
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction object) {
			_cost += 2f;
			return super.caseSymbolInstruction(object);
		}

		@Override
		public Object caseArrayInstruction(ArrayInstruction object) {
			_cost += 3f;
			return super.caseArrayInstruction(object);
		}

		@Override
		public Object defaultCase(EObject object) {
			_cost += 1f;
			return null;
		}
		
		private class GenericInstCostComputation extends GenericInstructionSwitch<Float> {
			@Override
			public Float caseADD(GenericInstruction g) {
				return 2f;
			}

			@Override
			public Float caseSUB(GenericInstruction g) {
				return 2f;
			}

			@Override
			public Float caseMUL(GenericInstruction g) {
				return 6f;
			}

			@Override
			public Float caseDIV(GenericInstruction g) {
				return 6f;
			}

			@Override
			public Float caseUnknownGeneric(GenericInstruction g) {
				return 1f;
			}	
		}	
	}
}
