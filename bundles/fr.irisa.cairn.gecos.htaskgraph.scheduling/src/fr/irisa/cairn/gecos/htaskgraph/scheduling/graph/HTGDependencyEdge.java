package fr.irisa.cairn.gecos.htaskgraph.scheduling.graph;

import fr.irisa.cairn.gecos.scheduling.graph.IDependencyEdge;
import htaskgraph.TaskDependency;

public class HTGDependencyEdge implements IDependencyEdge{
	private TaskDependency _taskDependency;
	
	public HTGDependencyEdge(TaskDependency taskDependency){
		_taskDependency = taskDependency;
	}
	
	public TaskDependency getTaskDependency(){
		return _taskDependency;
	}

	@Override
	public String toString() {
		return _taskDependency.toString();
	}
}
