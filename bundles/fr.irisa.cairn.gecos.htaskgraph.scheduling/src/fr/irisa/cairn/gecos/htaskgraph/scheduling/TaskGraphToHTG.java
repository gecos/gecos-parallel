package fr.irisa.cairn.gecos.htaskgraph.scheduling;

import fr.irisa.cairn.gecos.htaskgraph.model.transforms.PropagatePRsOutwardDuplicateControl;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.graph.HTGResources;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.graph.HTGTask;
import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;
import gecos.core.Procedure;

public class TaskGraphToHTG {
	private TaskGraph _taskGraph;
	private Procedure _p;
	
	public TaskGraphToHTG(Procedure p, TaskGraph graph){
		_taskGraph = graph;
		_p = p;
	}
	
	
	public void compute(){
		for(IResources resources : _taskGraph.getResourcesPool()){
			if(!(resources instanceof HTGResources))
				throw new RuntimeException("A resource detected in the task graph scheduler resources pool is not compatible with the HTG");
			
			HTGResources htgResource = (HTGResources) resources;
			for(ITask task : htgResource.getAllTask()){
				if(!(task instanceof HTGTask))
					throw new RuntimeException("A task detected in the task graph scheduler is not compatible with the HTG");
				
				HTGTask htgTask = (HTGTask) task;
				htgTask.getLTN().addProcessingResource(htgResource.getProcessingResources());
			}
		}

		_p.getBody().accept(new PropagatePRsOutwardDuplicateControl());
	}
	
}
