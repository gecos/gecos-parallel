package fr.irisa.cairn.gecos.htaskgraph.scheduling.graph;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.scheduling.graph.tools.ExclusiveTimeResource;

public class HTGResources extends ExclusiveTimeResource {
	private ProcessingResource _htgResources;
	
	public HTGResources(ProcessingResource resources){
		_htgResources = resources;
		
	}

	public ProcessingResource getProcessingResources(){
		return _htgResources;
	}

	@Override
	public String toString() {
		return _htgResources.toString() + " -> " + super.toString();
	}
}
