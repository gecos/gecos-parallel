typedef int int32_t;

int main(int argc, char** argv) {
  int32_t A_data[32][32];
  int32_t B_data[32][32];
  int32_t C1_data[32][8];
  int32_t C2_data[32][8];
  int32_t C3_data[32][8];
  int32_t C4_data[32][8];
  int32_t C_data[32][32];
  static const int32_t C_ssize[2] = {32, 32};
  int32_t i1;
  int32_t i2;
  int32_t i3;
  int32_t i4;
  int32_t i5;
  int32_t i6;
  int i7;
  int32_t i8;
  int32_t i9;
  int i10;
  int32_t i11;
  int32_t i12;
  int i13;
  int32_t i14;
  int32_t i15;
  int i16;
  int32_t i17;
  int32_t i18;

  for (i2 = 0; i2 < 32; ++i2) {
    for (i1 = 0; i1 < 32; ++i1) {
      A_data[i2][i1] = (int32_t)((i1 == i2));
    }
  }
  A_data[1][0] = -3;
  A_data[0][1] = 3;
  for (i4 = 0; i4 < 32; ++i4) {
    for (i3 = 0; i3 < 32; ++i3) {
      B_data[i4][i3] = (int32_t)((3.0 - (double)((i3 == i4))));
    }
  }
  for (i6 = 0; i6 < 32; ++i6) {
    for (i5 = 0; i5 < 8; ++i5) {
      C1_data[i6][i5] = 0;

      for (i7 = 0; i7 < 32; ++i7) {
        C1_data[i6][i5] = (C1_data[i6][i5] + (A_data[i7][i5] * B_data[i6][i7]));
      }
    }
  }
  for (i9 = 0; i9 < 32; ++i9) {
    for (i8 = 0; i8 < 8; ++i8) {
      C2_data[i9][i8] = 0;

      for (i10 = 0; i10 < 32; ++i10) {
        C2_data[i9][i8] = (C2_data[i9][i8] + (A_data[i10][(8 + i8)] * B_data[i9][i10]));
      }
    }
  }
  for (i12 = 0; i12 < 32; ++i12) {
    for (i11 = 0; i11 < 8; ++i11) {
      C3_data[i12][i11] = 0;

      for (i13 = 0; i13 < 32; ++i13) {
        C3_data[i12][i11] = (C3_data[i12][i11] + (A_data[i13][(16 + i11)] * B_data[i12][i13]));
      }
    }
  }
  for (i15 = 0; i15 < 32; ++i15) {
    for (i14 = 0; i14 < 8; ++i14) {
      C4_data[i15][i14] = 0;

      for (i16 = 0; i16 < 32; ++i16) {
        C4_data[i15][i14] = (C4_data[i15][i14] + (A_data[i16][(24 + i14)] * B_data[i15][i16]));
      }
    }
  }
  for (i18 = 0; i18 < 32; ++i18) {
    for (i17 = 0; i17 < 32; ++i17) {
      if ((i17 < 8)) {
        C_data[i18][i17] = C1_data[i18][i17];
      } else if ((i17 < 16)) {
        C_data[i18][i17] = C2_data[i18][(i17 - 8)];
      } else if ((i17 < 24)) {
        C_data[i18][i17] = C3_data[i18][(i17 - 16)];
      } else {
        C_data[i18][i17] = C4_data[i18][(i17 - 24)];
      }
    }
  }
//  printSci_dyn("C", matrix_type_int | matrix_type_make_width(sizeof(int32_t)), 2, C_ssize, C_ssize, (void*)C_data);

  return 0;
}


