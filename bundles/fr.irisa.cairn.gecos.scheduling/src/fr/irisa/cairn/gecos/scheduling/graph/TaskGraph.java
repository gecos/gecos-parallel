package fr.irisa.cairn.gecos.scheduling.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.SimpleDirectedGraph;

import fr.irisa.cairn.gecos.scheduling.graph.tools.internal.ParaverGenerator;

public class TaskGraph extends SimpleDirectedGraph<ITask, IDependencyEdge>{
	private static final long serialVersionUID = 2082436033041856084L;

	
	private List<IResources> _resourcesPool;
	
	public TaskGraph(Class<? extends IDependencyEdge> edgeClass, List<IResources> resources) {
		super(edgeClass);
		
		_resourcesPool = resources;
	}
	
	private DOTExporter<ITask, IDependencyEdge> _dotExporter = new DOTExporter<ITask, IDependencyEdge>(new IntegerNameProvider<ITask>(),
			new LeafTaskNodeVertexNameProvider(),
			new TaskDependencyEdgeNameProvider());

	
	public void SaveAsDot(File file){
		File fileToWrite;
		if(!file.getName().endsWith(".dot"))
			fileToWrite = new File(file.getAbsolutePath() + ".dot");
		else
			fileToWrite = file;
			
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileToWrite);
			_dotExporter.export(writer, this);
			writer.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void saveAsParaverFile(File file){
		File fileToWrite;
		if(!file.getName().endsWith(".prv"))
			fileToWrite = new File(file.getAbsolutePath() + ".prv");
		else
			fileToWrite = file;
		
		try {
			new ParaverGenerator(this).generate(fileToWrite);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<IResources> getResourcesPool(){
		return _resourcesPool;
	}
	
	public List<ITask> getStartTask(){
		List<ITask> ret = new LinkedList<ITask>();
		for(ITask task : this.vertexSet()){
			if(this.inDegreeOf(task) == 0)
				ret.add(task);
		}
		
		return ret;
	}
	
	public List<ITask> getAllSuccessorOf(ITask task){
		List<ITask> successors = new LinkedList<ITask>(); 
		for(IDependencyEdge edge : this.outgoingEdgesOf(task)){
			successors.add(this.getEdgeTarget(edge));
		}
		return successors;
	}
	
	public List<ITask> getAllPredecessorOf(ITask task){
		List<ITask> predecessors = new LinkedList<ITask>(); 
		for(IDependencyEdge edge : this.incomingEdgesOf(task)){
			predecessors.add(this.getEdgeSource(edge));
		}
		return predecessors;	
	}
	
	private class LeafTaskNodeVertexNameProvider implements VertexNameProvider<ITask> {
		@Override
		public String getVertexName(ITask arg0) {
			return arg0.toString();
		}
	}
	
	private class TaskDependencyEdgeNameProvider implements EdgeNameProvider<IDependencyEdge> {
		@Override
		public String getEdgeName(IDependencyEdge arg0) {
			return arg0.toString();
		}
	}
}
