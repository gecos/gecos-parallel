package fr.irisa.cairn.gecos.scheduling.graph;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;

public interface IResources {
	/**
	 * Add a task on this resources executed in a specific time slot
	 * @param task Task to be executed
	 * @param time_start Start of the time slot
	 * @param time_end End of the time slot
	 */
	public void addTask(ITask task, float time_start, float time_end);
	
	/**
	 * Return the time slot of the task
	 * @param task
	 * @return
	 */
	public SimpleEntry<Float, Float> getTimeSlot(ITask task);
	
	/**
	 * @return List containing all ITask process on this resource
	 */
	public List<ITask> getAllTask();
	
	/**
	 * @param time
	 * @return True if the resources have not task schedule, false otherwise
	 */
	public boolean isAvailable(float time);
	
	/**
	 * @return Get the total time of the resource
	 */
	public float getTotalTime();
}
