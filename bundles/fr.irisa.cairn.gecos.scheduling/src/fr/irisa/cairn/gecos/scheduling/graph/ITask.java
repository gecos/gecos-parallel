package fr.irisa.cairn.gecos.scheduling.graph;

public interface ITask {
	//TODO Delete this information. This information is contained in the IResources.
	public void setResources(IResources resources);
	public IResources getResources();
	
	/**
	 * @return Cost of the task
	 */
	public float getCost();
}
