package fr.irisa.cairn.gecos.scheduling.graph.tools;

import java.util.AbstractMap.SimpleEntry;

import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.tools.internal.TimeSlot;

import java.util.LinkedList;
import java.util.List;


/**
 * Implementation of a IResource.
 * This implementation force to have one task by time slot.
 * Adding a task which cover a other task present in the resource, a {@link RuntimeException} is created. 
 */
public class ExclusiveTimeResource implements IResources {
	private List<TimeSlot> _schedule = new LinkedList<TimeSlot>();;
	
	@Override
	public void addTask(ITask task, float time_start, float time_end) {
		if(getAllTask().contains(task))
			throw new RuntimeException("Impossible to add " + task + " because he is already scheduled in this resource");
		
		
		TimeSlot timeSlot = new TimeSlot(task, time_start, time_end);
		for(TimeSlot slot : _schedule){
			if(!slot.isExclusive(timeSlot)){
				new RuntimeException("The new task " + task + " added cover an another task "
							+ slot.getTask() + "(" + slot.getTimeStart() + ", " + slot.getTimeEnd() + " )" );
			}
		}
		
		_schedule.add(timeSlot);
	}

	@Override
	public boolean isAvailable(float time) {
		for(TimeSlot slot : _schedule){
			if(slot.ping(time))
				return false;
		}
		return true;
	}

	@Override
	public float getTotalTime() {
		float ret = Float.NEGATIVE_INFINITY;
		for(TimeSlot slot : _schedule){
			if(slot.getTimeEnd() > ret)
				ret = slot.getTimeEnd();
		}
		
		return ret;
	}

	@Override
	public SimpleEntry<Float, Float> getTimeSlot(ITask task) {
		for(TimeSlot slot : _schedule){
			if(task == slot.getTask())
				return new SimpleEntry<Float, Float>(slot.getTimeStart(), slot.getTimeEnd());
		}
		throw new RuntimeException("Task " + task + " not found in the Resources");
	}
	
	@Override
	public String toString() {
		String ret = "";
		for(TimeSlot slot : _schedule){
			ret += slot.toString() + " " ;
		}
		return ret;
	}

	@Override
	public List<ITask> getAllTask() {
		List<ITask> ret = new LinkedList<ITask>();
		for(TimeSlot slot : _schedule){
			ret.add(slot.getTask());
		}
		return ret;
	}
}
