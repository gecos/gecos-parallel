package fr.irisa.cairn.gecos.scheduling.graph.tools.internal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;

import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;

public class ParaverGenerator {
	private TaskGraph _graph;
	private PrintWriter _writer;
	
	public ParaverGenerator(TaskGraph graph){
		_graph = graph;
	}
	
	public void generate(File file) throws FileNotFoundException{
		_writer = new PrintWriter(file);
		
		generateHeader();
		generateBody();
		
		_writer.close();
	}
	
	
	private void generateHeader(){
		Calendar rightNow = Calendar.getInstance();
		_writer.println("#Paraver (" + rightNow.get(Calendar.DAY_OF_MONTH) + "/" +
				rightNow.get(Calendar.MONTH) + "/" +
				rightNow.get(Calendar.YEAR) + " at " + 
				rightNow.get(Calendar.HOUR_OF_DAY) + ":" + rightNow.get(Calendar.MINUTE) + "):"+
				getTotalTime() + ":" + getNumberOfResources() + ":" + generateApplication());
		
	}
	
	private int getTotalTime() {
		return 0;
	}
	
	private String getNumberOfResources(){		
		return "1("+Integer.toString(_graph.getResourcesPool().size()) + ")";
	}
	
	private String generateApplication(){
		String ret;
		
		ret = "1:";
		ret += Integer.toString(_graph.vertexSet().size()) + "(";
		for(int i = 0 ; i < _graph.vertexSet().size() ; i++){
			ret += "1:1";
			if(i + 1 < _graph.vertexSet().size())
				ret += ",";
		}
		
		return ret+")";
	}
	
	private void generateBody(){
		int resources_number = 1;
		int task_number = 1;
		for(IResources resources : _graph.getResourcesPool()){
			for(ITask task : resources.getAllTask()){	
				_writer.println("1:" + resources_number + ":1:" + task_number + ":1:" + Math.round(resources.getTimeSlot(task).getKey()) + ":" + Math.round(resources.getTimeSlot(task).getValue()) + ":2");
				task_number++;
			}
			resources_number++;
		}
	}
}
