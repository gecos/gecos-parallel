package fr.irisa.cairn.gecos.scheduling.algorithm;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import fr.irisa.cairn.gecos.scheduling.IScheduler;
import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;
import fr.irisa.cairn.gecos.scheduling.graph.tools.TaskGraphStateManager;

public class ListScheduler implements IScheduler {
	
	public static final boolean DEBUG = false;
		
	@Override
	public void schedule(TaskGraph taskGraph) {
		TaskGraphStateManager manager = new TaskGraphStateManager(taskGraph);
		if(DEBUG) System.out.println("START-----");
		if(DEBUG) System.out.println(manager.toString());
		
		while(!manager.isAllTaskIsScheduled()){
			List<ITask> taskReady = manager.getReadyTaskPool();
			sortPriorityTask(taskReady);
//			if(taskReady.size() == 0)
//				throw new RuntimeException("No task is ready");
			
			
			List<IResources> resourcesAvalaible = manager.getAvalaibleResourcePool();
 			
 			//////////// Temporary hack to force resource constraint
 			List<ITask> taskReadyWithConstraint = new LinkedList<ITask>();
 			List<ITask> taskReadyWithoutConstraint = new LinkedList<ITask>();
 			for(ITask task : taskReady){
 				if(task.getResources() != null){
 					taskReadyWithConstraint.add(task);
 				}
 				else{
 					taskReadyWithoutConstraint.add(task);
 				}
 			}
 			
 			for(ITask task : taskReadyWithConstraint){
 				if(resourcesAvalaible.contains(task.getResources())){
 					manager.setTaskAsInProgress(task, resourcesAvalaible.get(resourcesAvalaible.indexOf(task.getResources())));
 					resourcesAvalaible.remove(resourcesAvalaible.indexOf(task.getResources()));
 				}
 			}
 			/////////// END hack
 			
 			int resourceAvalaibleNumber = resourcesAvalaible.size();
 			for(int i = 0 ; i < resourceAvalaibleNumber && i < taskReadyWithoutConstraint.size() ; i++){
 				taskReadyWithoutConstraint.get(i).setResources(resourcesAvalaible.get(i));
 				manager.setTaskAsInProgress(taskReadyWithoutConstraint.get(i), resourcesAvalaible.get(i)); 				
 			}		
 			
 			if(DEBUG) System.out.println(manager.toString());
			manager.nextTimeState();
		}
		
		if(DEBUG) System.out.println("END-----");
		if(DEBUG) System.out.println(manager.toString());
	}
	
	private void sortPriorityTask(List<ITask> readyTask){
		Collections.sort(readyTask, new Comparator<ITask>() {
			@Override
			public int compare(ITask o1, ITask o2) {
				if(o1.getCost() < o2.getCost())
					return -1;
				else if(o1.getCost() == o2.getCost())
					return 0;
				else return 1;
			}
		});		
	}
}
