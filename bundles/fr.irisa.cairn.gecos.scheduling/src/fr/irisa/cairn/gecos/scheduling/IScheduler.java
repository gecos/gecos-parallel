package fr.irisa.cairn.gecos.scheduling;

import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;

public interface IScheduler {
	public void schedule(TaskGraph taskGraph);
}
