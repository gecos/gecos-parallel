package fr.irisa.cairn.gecos.scheduling.graph.tools.internal;

import fr.irisa.cairn.gecos.scheduling.graph.ITask;

public class TimeSlot {
	private ITask _task;
	private float _timeStart, _timeEnd;
	
	public TimeSlot(ITask task, float timeStart, float timeEnd){
		_task = task;
		_timeStart = timeStart;
		_timeEnd = timeEnd;
	}
	
	public float getTimeStart(){
		return _timeStart;
	}
	
	public float getTimeEnd(){
		return _timeEnd;
	}
	
	public boolean ping(float time){
		return time >= _timeStart && time < _timeEnd;		
	}
	
	
	public ITask getTask(){
		return _task;
	}
	
	public boolean isExclusive(TimeSlot other){
		return other.getTimeStart() >= _timeEnd && _timeStart >= other.getTimeEnd();
	}

	@Override
	public String toString() {
		return _task.toString() + " (" + _timeStart + "," + _timeEnd + ")";
	}	
}
