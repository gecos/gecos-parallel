package fr.irisa.cairn.gecos.scheduling.graph.tools;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.scheduling.algorithm.ListScheduler;
import fr.irisa.cairn.gecos.scheduling.graph.IResources;
import fr.irisa.cairn.gecos.scheduling.graph.ITask;
import fr.irisa.cairn.gecos.scheduling.graph.TaskGraph;

/**
 * Create a manager which manage the status of a {@link TaskGraph}.
 * This manager set 4 kind of state on task (NOT_READY, IN_PROGRESS, COMPLETED, NOT_READY).
 * 
 * This manager have a internal time state. The time state is first task which are completed.
 * When we want to go to the next time state, the manager update the internal time state to the
 * next first task completed and update the {@link TaskGraph} status. 
 * 
 * NOT_READY -> task is no ready to be executed
 * READY -> task is ready to be executed
 * IN_PROGRESS -> task is scheduled but not finish yet
 * COMPLETED -> task is finished 
 * 
 * @author Nicolas Simon
 */
public class TaskGraphStateManager {
	private final boolean _DEBUG = ListScheduler.DEBUG;
	
	private enum STATE {READY, IN_PROGRESS, COMPLETED, NOT_READY}
	
	private Map<ITask, STATE> _taskState;
	private TaskGraph _taskGraph;
	private float _currentTimeState = 0f;
	
	public TaskGraphStateManager(TaskGraph taskGraph){
		_taskGraph = taskGraph;
		_taskState = new HashMap<ITask, TaskGraphStateManager.STATE>();

		fillTaskState();
		initializeTaskState();
	}

	/**
	 * @return The {@link TaskGraph} manage by the {@link TaskGraphStateManager}
	 */
	public TaskGraph getAssociatedGraph(){
		return _taskGraph;
	}
	
	/**
	 * Change the state of a {@link ITask} to in progress and associate a resource to it 
	 * @param task
	 * @param resource
	 */
	public void setTaskAsInProgress(ITask task, IResources resource){
		_taskState.put(task, STATE.IN_PROGRESS);
		resource.addTask(task, _currentTimeState, _currentTimeState+task.getCost());
	}
	
	/**
	 * @return Get a list which contain all the {@link ITask} which have a status as ready at this time state
	 */
	public List<ITask> getReadyTaskPool(){
		List<ITask> taskReadyPool = new LinkedList<ITask>();
		for(ITask task : _taskState.keySet())
			if(_taskState.get(task) == STATE.READY)
				taskReadyPool.add(task);
		
		return taskReadyPool;
	}
	
	/**
	 * @return true if all task in the graph task are completed
	 */
	public boolean isAllTaskIsScheduled(){
		for(ITask task : _taskState.keySet())
			if(_taskState.get(task) != STATE.COMPLETED)
				return false;
		
		return true;
	}
	
	/**
	 * @return Get a list which contain all the {@link IResources} available at this time state
	 */
	public List<IResources> getAvalaibleResourcePool(){
		List<IResources> pool = new LinkedList<IResources>();
		
		for(IResources resource : _taskGraph.getResourcesPool()){
			if(resource.isAvailable(_currentTimeState)){
				pool.add(resource);
			}
		}
		return pool;
	}
	
	/**
	 * Update the task graph state to the next time state.
	 * Update the status of tasks from IN_PROGRESS to COMPLETED and NOT_READY to READY if necessary.
	 */
	public void nextTimeState(){
		checkIfTaskIsCompleted();
		
//		while(getAvalaibleResourcePool().size() == 0 || (getReadyTaskPool().size() == 0 && !isAllTaskIsScheduled())){
			List<Float> timeRecources = new LinkedList<Float>();
			for(IResources resource : _taskGraph.getResourcesPool()){
				float timeResource = resource.getTotalTime();
				if(timeResource > _currentTimeState)
					timeRecources.add(timeResource);
			}
			
			if(timeRecources.size() == 0)
				throw new RuntimeException("No resources avalaible for the next time state");
			_currentTimeState = Collections.min(timeRecources);
			checkIfTaskIsCompleted();
//		}
		
	}
	
	/**
	 * check and update of a task to completed in relation to the current time state
	 */
	private void checkIfTaskIsCompleted() {
		for (ITask task : _taskState.keySet()) {
			if (_taskState.get(task) == STATE.IN_PROGRESS) {
				if (task.getResources().isAvailable(_currentTimeState)) {
					setTaskAsCompleted(task);
				}
			}
		}
	}
	
	/**
	 * Change the state of a {@link ITask} to completed and update the TaskGraph task state 
	 * @param task
	 */
	private void setTaskAsCompleted(ITask task){
		if(_DEBUG) System.out.println(task + " is completed");
		_taskState.put(task, STATE.COMPLETED);
		updateGraphState(task);
	}
	
	/**
	 * Check if successor of a task need to be updated as READY state.
	 * Check if each predecessor of the task's successors have the COMPLETED state and update the current state of the successor to READY. 
	 * @param taskUpdated
	 */
	private void updateGraphState(ITask taskUpdated){
		for(ITask succ : _taskGraph.getAllSuccessorOf(taskUpdated)){	
			boolean isReady = true;
			for(ITask predOfSucc : _taskGraph.getAllPredecessorOf(succ)){
				if(_taskState.get(predOfSucc) != STATE.COMPLETED){
					isReady = false;
					break;
				}
					
			}
			
			if(isReady){
				if(_DEBUG) System.out.println(succ + " is ready");
				_taskState.put(succ, STATE.READY);
			}
		}
	}
	
	/**
	 * Add in the state table each task contained in the {@link TaskGraph} and initialize them to NOT_READY
	 */
	private void fillTaskState(){
		for(ITask task : _taskGraph.vertexSet())
			_taskState.put(task, STATE.NOT_READY);
	}
	
	/**
	 * Update as READY the task state which are the input task in the graph.
	 */
	private void initializeTaskState(){
		List<ITask> startTask = _taskGraph.getStartTask();
		if(startTask.isEmpty())
			throw new RuntimeException("No start task in the task graph. Impossible to initialize task as READY to be schedule");
		
		for(ITask task : startTask)
			_taskState.put(task, STATE.READY);
	}
	
	
	@Override
	public String toString() {
		String ret = "";
		
		ret += "\n" + "CURRENT TIME STATE -> " + _currentTimeState + "\n";
		for(ITask node : _taskState.keySet()){
			ret += node.toString() + " -> " + _taskState.get(node) + "\n";
		}
		
		ret += "Task mapped to resources : \n"; 
		for(IResources resource : _taskGraph.getResourcesPool()){
			ret += resource.toString();
			ret += "\n";
		}
		
		ret += "\nReady task pool : " + getReadyTaskPool() + "\nAvalaible resource pool : " + getAvalaibleResourcePool() + "\n";	
		return ret;
	}
}
