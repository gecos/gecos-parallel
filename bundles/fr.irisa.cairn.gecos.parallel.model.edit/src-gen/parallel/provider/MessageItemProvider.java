/**
 */
package parallel.provider;


import gecos.dag.DagFactory;

import gecos.instrs.InstrsFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import parallel.Message;
import parallel.ParallelPackage;

/**
 * This is the item provider adapter for a {@link parallel.Message} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MessageItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Tag feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Message_tag_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Message_tag_feature", "_UI_Message_type"),
				 ParallelPackage.Literals.MESSAGE__TAG,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Message_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Message_type_feature", "_UI_Message_type"),
				 ParallelPackage.Literals.MESSAGE__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ParallelPackage.Literals.MESSAGE__ADDRESS);
			childrenFeatures.add(ParallelPackage.Literals.MESSAGE__COUNT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Message.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Message"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Message message = (Message)object;
		return getString("_UI_Message_type") + " " + message.getTag();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Message.class)) {
			case ParallelPackage.MESSAGE__TAG:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ParallelPackage.MESSAGE__ADDRESS:
			case ParallelPackage.MESSAGE__COUNT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createDummyInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createLabelInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createNumberedSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSSADefSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSSAUseSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSizeofInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSizeofTypeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSizeofValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createIntInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createFloatInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createStringInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createArrayValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createPhiInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createAddressInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createIndirInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createMethodCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimpleArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createRangeArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createCondInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createBreakInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createContinueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createGotoInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createRetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createConvertInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createFieldInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createEnumeratorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createRangeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createCaseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createArithmeticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createComparisonInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createLogicalInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createBitwiseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdPackInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdExtractInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdShuffleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 InstrsFactory.eINSTANCE.createSimdExpandInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__ADDRESS,
				 DagFactory.eINSTANCE.createDAGInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createDummyInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createLabelInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createNumberedSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSSADefSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSSAUseSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSizeofInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSizeofTypeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSizeofValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createIntInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createFloatInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createStringInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createArrayValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createPhiInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createAddressInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createIndirInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createMethodCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimpleArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createRangeArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createCondInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createBreakInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createContinueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createGotoInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createRetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createConvertInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createFieldInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createEnumeratorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createRangeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createCaseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createArithmeticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createComparisonInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createLogicalInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createBitwiseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdPackInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdExtractInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdShuffleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 InstrsFactory.eINSTANCE.createSimdExpandInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(ParallelPackage.Literals.MESSAGE__COUNT,
				 DagFactory.eINSTANCE.createDAGInstruction()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ParallelPackage.Literals.MESSAGE__ADDRESS ||
			childFeature == ParallelPackage.Literals.MESSAGE__COUNT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ParallelEditPlugin.INSTANCE;
	}

}
