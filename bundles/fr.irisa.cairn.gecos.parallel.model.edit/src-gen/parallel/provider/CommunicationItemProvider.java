/**
 */
package parallel.provider;


import gecos.annotations.AnnotationsFactory;
import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;

import gecos.dag.DagFactory;
import gecos.instrs.InstrsFactory;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import parallel.Communication;
import parallel.ParallelPackage;

/**
 * This is the item provider adapter for a {@link parallel.Communication} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CommunicationItemProvider extends ItemProviderAdapter {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addParentPropertyDescriptor(object);
			addNumberPropertyDescriptor(object);
			addUseEdgesPropertyDescriptor(object);
			addInEdgesPropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addFlagsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Parent feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_parent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_parent_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__PARENT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_number_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_number_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__NUMBER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Edges feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BasicBlock_useEdges_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BasicBlock_useEdges_feature", "_UI_BasicBlock_type"),
				 BlocksPackage.Literals.BASIC_BLOCK__USE_EDGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the In Edges feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BasicBlock_inEdges_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BasicBlock_inEdges_feature", "_UI_BasicBlock_type"),
				 BlocksPackage.Literals.BASIC_BLOCK__IN_EDGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BasicBlock_label_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BasicBlock_label_feature", "_UI_BasicBlock_type"),
				 BlocksPackage.Literals.BASIC_BLOCK__LABEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Flags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BasicBlock_flags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BasicBlock_flags_feature", "_UI_BasicBlock_type"),
				 BlocksPackage.Literals.BASIC_BLOCK__FLAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AnnotationsPackage.Literals.ANNOTATED_ELEMENT__ANNOTATIONS);
			childrenFeatures.add(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS);
			childrenFeatures.add(BlocksPackage.Literals.BASIC_BLOCK__DEF_EDGES);
			childrenFeatures.add(BlocksPackage.Literals.BASIC_BLOCK__OUT_EDGES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Communication communication = (Communication)object;
		return getString("_UI_Communication_type") + " " + communication.getNumber();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Communication.class)) {
			case ParallelPackage.COMMUNICATION__NUMBER:
			case ParallelPackage.COMMUNICATION__LABEL:
			case ParallelPackage.COMMUNICATION__FLAGS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ParallelPackage.COMMUNICATION__ANNOTATIONS:
			case ParallelPackage.COMMUNICATION__INSTRUCTIONS:
			case ParallelPackage.COMMUNICATION__DEF_EDGES:
			case ParallelPackage.COMMUNICATION__OUT_EDGES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(AnnotationsPackage.Literals.ANNOTATED_ELEMENT__ANNOTATIONS,
				 AnnotationsFactory.eINSTANCE.create(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createDummyInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createLabelInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createNumberedSymbolInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSSADefSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSSAUseSymbol()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSizeofInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSizeofTypeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSizeofValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createIntInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createFloatInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createStringInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createArrayValueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createPhiInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createAddressInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createIndirInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createMethodCallInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimpleArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createRangeArrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createCondInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createBreakInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createContinueInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createGotoInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createRetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createConvertInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createFieldInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createEnumeratorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createRangeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createCaseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createArithmeticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createComparisonInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createLogicalInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createBitwiseInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdGenericInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdPackInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdExtractInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdShuffleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 InstrsFactory.eINSTANCE.createSimdExpandInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__INSTRUCTIONS,
				 DagFactory.eINSTANCE.createDAGInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__DEF_EDGES,
				 BlocksFactory.eINSTANCE.createDataEdge()));

		newChildDescriptors.add
			(createChildParameter
				(BlocksPackage.Literals.BASIC_BLOCK__OUT_EDGES,
				 BlocksFactory.eINSTANCE.createControlEdge()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ParallelEditPlugin.INSTANCE;
	}

}
