/**
 */
package architecture;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AMemory Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see architecture.ArchitecturePackage#getAMemoryResource()
 * @model
 * @generated
 */
public interface AMemoryResource extends MemoryResource {
} // AMemoryResource
