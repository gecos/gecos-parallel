/**
 */
package architecture;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memory Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see architecture.ArchitecturePackage#getMemoryResource()
 * @model
 * @generated
 */
public interface MemoryResource extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _id = this.getId();\nreturn (\"MR_\" + &lt;%java.lang.Integer%&gt;.valueOf(_id));'"
	 * @generated
	 */
	String toString();

} // MemoryResource
