/**
 */
package architecture;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BMemory Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see architecture.ArchitecturePackage#getBMemoryResource()
 * @model
 * @generated
 */
public interface BMemoryResource extends MemoryResource {
} // BMemoryResource
