/**
 */
package architecture.impl;

import architecture.Architecture;
import architecture.ArchitecturePackage;
import architecture.MemoryResource;
import architecture.ProcessingResource;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Architecture</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link architecture.impl.ArchitectureImpl#getProcessingResources <em>Processing Resources</em>}</li>
 *   <li>{@link architecture.impl.ArchitectureImpl#getMemoryResources <em>Memory Resources</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArchitectureImpl extends MinimalEObjectImpl.Container implements Architecture {
	/**
	 * The cached value of the '{@link #getProcessingResources() <em>Processing Resources</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessingResources()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessingResource> processingResources;

	/**
	 * The cached value of the '{@link #getMemoryResources() <em>Memory Resources</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryResources()
	 * @generated
	 * @ordered
	 */
	protected EList<MemoryResource> memoryResources;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArchitectureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArchitecturePackage.Literals.ARCHITECTURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getProcessingResources() {
		if (processingResources == null) {
			processingResources = new EObjectContainmentEList<ProcessingResource>(ProcessingResource.class, this, ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES);
		}
		return processingResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MemoryResource> getMemoryResources() {
		if (memoryResources == null) {
			memoryResources = new EObjectContainmentEList<MemoryResource>(MemoryResource.class, this, ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES);
		}
		return memoryResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessingResource getProcessingResource(final int prId) {
		final Function1<ProcessingResource, Boolean> _function = new Function1<ProcessingResource, Boolean>() {
			public Boolean apply(final ProcessingResource it) {
				int _id = it.getId();
				return Boolean.valueOf((_id == prId));
			}
		};
		return IterableExtensions.<ProcessingResource>findFirst(this.getProcessingResources(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES:
				return ((InternalEList<?>)getProcessingResources()).basicRemove(otherEnd, msgs);
			case ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES:
				return ((InternalEList<?>)getMemoryResources()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES:
				return getProcessingResources();
			case ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES:
				return getMemoryResources();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES:
				getProcessingResources().clear();
				getProcessingResources().addAll((Collection<? extends ProcessingResource>)newValue);
				return;
			case ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES:
				getMemoryResources().clear();
				getMemoryResources().addAll((Collection<? extends MemoryResource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES:
				getProcessingResources().clear();
				return;
			case ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES:
				getMemoryResources().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArchitecturePackage.ARCHITECTURE__PROCESSING_RESOURCES:
				return processingResources != null && !processingResources.isEmpty();
			case ArchitecturePackage.ARCHITECTURE__MEMORY_RESOURCES:
				return memoryResources != null && !memoryResources.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ArchitectureImpl
