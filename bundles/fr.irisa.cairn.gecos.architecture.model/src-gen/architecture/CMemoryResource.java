/**
 */
package architecture;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CMemory Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see architecture.ArchitecturePackage#getCMemoryResource()
 * @model
 * @generated
 */
public interface CMemoryResource extends MemoryResource {
} // CMemoryResource
