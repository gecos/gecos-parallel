/**
 */
package architecture;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Processing Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see architecture.ArchitecturePackage#getProcessingResource()
 * @model
 * @generated
 */
public interface ProcessingResource extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _id = this.getId();\nreturn (\"PR_\" + &lt;%java.lang.Integer%&gt;.valueOf(_id));'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xifexpression = false;\nif ((other instanceof &lt;%architecture.ProcessingResource%&gt;))\n{\n\tint _id = this.getId();\n\tint _id_1 = ((&lt;%architecture.ProcessingResource%&gt;)other).getId();\n\t_xifexpression = (_id == _id_1);\n}\nelse\n{\n\t_xifexpression = false;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	boolean equals(Object other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getId();'"
	 * @generated
	 */
	int hashCode();

} // ProcessingResource
