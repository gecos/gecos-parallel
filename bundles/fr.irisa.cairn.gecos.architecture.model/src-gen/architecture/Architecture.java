/**
 */
package architecture;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Architecture</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link architecture.Architecture#getProcessingResources <em>Processing Resources</em>}</li>
 *   <li>{@link architecture.Architecture#getMemoryResources <em>Memory Resources</em>}</li>
 * </ul>
 *
 * @see architecture.ArchitecturePackage#getArchitecture()
 * @model
 * @generated
 */
public interface Architecture extends EObject {
	/**
	 * Returns the value of the '<em><b>Processing Resources</b></em>' containment reference list.
	 * The list contents are of type {@link architecture.ProcessingResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processing Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processing Resources</em>' containment reference list.
	 * @see architecture.ArchitecturePackage#getArchitecture_ProcessingResources()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessingResource> getProcessingResources();

	/**
	 * Returns the value of the '<em><b>Memory Resources</b></em>' containment reference list.
	 * The list contents are of type {@link architecture.MemoryResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Resources</em>' containment reference list.
	 * @see architecture.ArchitecturePackage#getArchitecture_MemoryResources()
	 * @model containment="true"
	 * @generated
	 */
	EList<MemoryResource> getMemoryResources();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the first {@link ProcessingResource} that have as ID {@code prId),
	 * {@code null} if no such {@link ProcessingResource} is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" prIdUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%architecture.ProcessingResource%&gt; it)\n\t{\n\t\tint _id = it.getId();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((_id == prId));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%architecture.ProcessingResource%&gt;&gt;findFirst(this.getProcessingResources(), _function);'"
	 * @generated
	 */
	ProcessingResource getProcessingResource(int prId);

} // Architecture
