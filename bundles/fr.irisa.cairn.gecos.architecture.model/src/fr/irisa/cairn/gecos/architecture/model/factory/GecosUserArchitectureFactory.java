package fr.irisa.cairn.gecos.architecture.model.factory;

import architecture.Architecture;
import architecture.ArchitectureFactory;
import architecture.ProcessingResource;

public class GecosUserArchitectureFactory {

	private static ArchitectureFactory _factory = ArchitectureFactory.eINSTANCE;
	
	
	public static ProcessingResource processingResource(int id) {
		ProcessingResource unit = _factory.createProcessingResource();
		
		unit.setId(id);
		
		return unit;
	}
	
	public static Architecture architecture() {
		Architecture arch = _factory.createArchitecture();
		return arch;
	}
	
}
