package fr.irisa.cairn.gecos.architecture.model.architectures;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import architecture.Architecture;
import fr.irisa.cairn.gecos.architecture.model.factory.GecosUserArchitectureFactory;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Creates a dummy Paralle architecture containing a specified"
		+ "number N of ProcessingResources ranked from 0 to N-1.")
public class DummyParallelArchitecture {

	Architecture arch;
	
	@GSModuleConstructor("-arg1: number of ProcessingResources available in the architecture.")
	public DummyParallelArchitecture(int nb) {
		this.arch = GecosUserArchitectureFactory.architecture();
		this.arch.getProcessingResources().addAll(IntStream.range(0, nb)
				.mapToObj(GecosUserArchitectureFactory::processingResource)
				.collect(Collectors.toList()));
	}
	
	public Architecture compute() {
		return getArch();
	}
	
	public Architecture getArch() {
		return arch;
	}
	
}
