/**
 */
package htaskgraph;

import gecos.blocks.IfBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.IfTaskNode#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getIfTaskNode()
 * @model
 * @generated
 */
public interface IfTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(IfBlock)
	 * @see htaskgraph.HtaskgraphPackage#getIfTaskNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	IfBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.IfTaskNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(IfBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIfTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _testBlock = this.getBlock().getTestBlock();\nreturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _testBlock);'"
	 * @generated
	 */
	BasicLeafTaskNode getTest();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getBlock().getThenBlock();\nreturn ((&lt;%htaskgraph.TaskNode%&gt;) _thenBlock);'"
	 * @generated
	 */
	TaskNode getThen();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getBlock().getElseBlock();\nreturn ((&lt;%htaskgraph.TaskNode%&gt;) _elseBlock);'"
	 * @generated
	 */
	TaskNode getElse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"IfTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

} // IfTaskNode
