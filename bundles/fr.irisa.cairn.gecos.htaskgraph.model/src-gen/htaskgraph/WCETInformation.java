/**
 */
package htaskgraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WCET Information</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.WCETInformation#getData <em>Data</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getWCETInformation()
 * @model
 * @generated
 */
public interface WCETInformation extends ProfilingInformation {
	/**
	 * Returns the value of the '<em><b>Data</b></em>' containment reference list.
	 * The list contents are of type {@link htaskgraph.CoreLevelWCET}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' containment reference list.
	 * @see htaskgraph.HtaskgraphPackage#getWCETInformation_Data()
	 * @model containment="true"
	 * @generated
	 */
	EList<CoreLevelWCET> getData();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.CoreLevelWCET%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.CoreLevelWCET%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%htaskgraph.CoreLevelWCET%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _architecture = it.getArchitecture();\n\t\t&lt;%java.lang.String%&gt; _plus = (_architecture + \":\");\n\t\tlong _isolatedWCET = it.getIsolatedWCET();\n\t\treturn (_plus + &lt;%java.lang.Long%&gt;.valueOf(_isolatedWCET));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.CoreLevelWCET%&gt;&gt;join(this.getData(), \" \", _function);'"
	 * @generated
	 */
	String toString();

} // WCETInformation
