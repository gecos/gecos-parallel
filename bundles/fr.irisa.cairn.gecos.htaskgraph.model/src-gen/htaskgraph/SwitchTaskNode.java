/**
 */
package htaskgraph;

import gecos.blocks.SwitchBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.SwitchTaskNode#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getSwitchTaskNode()
 * @model
 * @generated
 */
public interface SwitchTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(SwitchBlock)
	 * @see htaskgraph.HtaskgraphPackage#getSwitchTaskNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	SwitchBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.SwitchTaskNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(SwitchBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSwitchTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"SwitchTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

} // SwitchTaskNode
