/**
 */
package htaskgraph;

import gecos.blocks.ForBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.ForTaskNode#getBlock <em>Block</em>}</li>
 *   <li>{@link htaskgraph.ForTaskNode#getLoopIn <em>Loop In</em>}</li>
 *   <li>{@link htaskgraph.ForTaskNode#getLoopOut <em>Loop Out</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getForTaskNode()
 * @model
 * @generated
 */
public interface ForTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(ForBlock)
	 * @see htaskgraph.HtaskgraphPackage#getForTaskNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	ForBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.ForTaskNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(ForBlock value);

	/**
	 * Returns the value of the '<em><b>Loop In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop In</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop In</em>' containment reference.
	 * @see #setLoopIn(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getForTaskNode_LoopIn()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getLoopIn();

	/**
	 * Sets the value of the '{@link htaskgraph.ForTaskNode#getLoopIn <em>Loop In</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop In</em>' containment reference.
	 * @see #getLoopIn()
	 * @generated
	 */
	void setLoopIn(CommunicationNode value);

	/**
	 * Returns the value of the '<em><b>Loop Out</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Out</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Out</em>' containment reference.
	 * @see #setLoopOut(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getForTaskNode_LoopOut()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getLoopOut();

	/**
	 * Sets the value of the '{@link htaskgraph.ForTaskNode#getLoopOut <em>Loop Out</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Out</em>' containment reference.
	 * @see #getLoopOut()
	 * @generated
	 */
	void setLoopOut(CommunicationNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _stepBlock = this.getBlock().getStepBlock();\nreturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _stepBlock);'"
	 * @generated
	 */
	BasicLeafTaskNode getStep();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; _internalInputs = this.getInternalInputs();\nfor (final &lt;%htaskgraph.TaskDependency%&gt; td : _internalInputs)\n{\n\tif ((td instanceof &lt;%htaskgraph.GlueTaskDependency%&gt;))\n\t{\n\t\t&lt;%htaskgraph.TaskNode%&gt; _from = ((&lt;%htaskgraph.GlueTaskDependency%&gt;)td).getFrom();\n\t\treturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _from);\n\t}\n}\n&lt;%gecos.blocks.BasicBlock%&gt; _initBlock = this.getBlock().getInitBlock();\nreturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _initBlock);'"
	 * @generated
	 */
	BasicLeafTaskNode getInit();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _testBlock = this.getBlock().getTestBlock();\nreturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _testBlock);'"
	 * @generated
	 */
	BasicLeafTaskNode getTest();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBlock().getBodyBlock();\nreturn ((&lt;%htaskgraph.TaskNode%&gt;) _bodyBlock);'"
	 * @generated
	 */
	TaskNode getBody();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitForTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"ForTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

} // ForTaskNode
