/**
 */
package htaskgraph;

import gecos.blocks.WhileBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Do While Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.DoWhileTaskNode#getBlock <em>Block</em>}</li>
 *   <li>{@link htaskgraph.DoWhileTaskNode#getLoopIn <em>Loop In</em>}</li>
 *   <li>{@link htaskgraph.DoWhileTaskNode#getLoopOut <em>Loop Out</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getDoWhileTaskNode()
 * @model
 * @generated
 */
public interface DoWhileTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(WhileBlock)
	 * @see htaskgraph.HtaskgraphPackage#getDoWhileTaskNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	WhileBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.DoWhileTaskNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(WhileBlock value);

	/**
	 * Returns the value of the '<em><b>Loop In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop In</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop In</em>' containment reference.
	 * @see #setLoopIn(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getDoWhileTaskNode_LoopIn()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getLoopIn();

	/**
	 * Sets the value of the '{@link htaskgraph.DoWhileTaskNode#getLoopIn <em>Loop In</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop In</em>' containment reference.
	 * @see #getLoopIn()
	 * @generated
	 */
	void setLoopIn(CommunicationNode value);

	/**
	 * Returns the value of the '<em><b>Loop Out</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Out</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Out</em>' containment reference.
	 * @see #setLoopOut(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getDoWhileTaskNode_LoopOut()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getLoopOut();

	/**
	 * Sets the value of the '{@link htaskgraph.DoWhileTaskNode#getLoopOut <em>Loop Out</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Out</em>' containment reference.
	 * @see #getLoopOut()
	 * @generated
	 */
	void setLoopOut(CommunicationNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _testBlock = this.getBlock().getTestBlock();\nreturn ((&lt;%htaskgraph.BasicLeafTaskNode%&gt;) _testBlock);'"
	 * @generated
	 */
	BasicLeafTaskNode getTest();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBlock().getBodyBlock();\nreturn ((&lt;%htaskgraph.TaskNode%&gt;) _bodyBlock);'"
	 * @generated
	 */
	TaskNode getBody();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDoWhileTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"DoWhileTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

} // DoWhileTaskNode
