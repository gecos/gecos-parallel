/**
 */
package htaskgraph;

import gecos.blocks.BasicBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Leaf Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getBasicLeafTaskNode()
 * @model
 * @generated
 */
public interface BasicLeafTaskNode extends LeafTaskNode, BasicBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"BasicLT\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitBasicLeafTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

} // BasicLeafTaskNode
