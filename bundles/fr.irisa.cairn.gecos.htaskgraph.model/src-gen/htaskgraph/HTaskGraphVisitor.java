/**
 */
package htaskgraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HTask Graph Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getHTaskGraphVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface HTaskGraphVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tdUnique="false"
	 * @generated
	 */
	void visitTaskDependency(TaskDependency td);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ltnUnique="false"
	 * @generated
	 */
	void visitLeafTaskNode(LeafTaskNode ltn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ctnUnique="false"
	 * @generated
	 */
	void visitClusterTaskNode(ClusterTaskNode ctn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ltnUnique="false"
	 * @generated
	 */
	void visitIfTaskNode(IfTaskNode ltn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ltnUnique="false"
	 * @generated
	 */
	void visitForTaskNode(ForTaskNode ltn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ltnUnique="false"
	 * @generated
	 */
	void visitWhileTaskNode(WhileTaskNode ltn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ltnUnique="false"
	 * @generated
	 */
	void visitProcedureTaskNode(ProcedureTaskNode ltn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tdUnique="false"
	 * @generated
	 */
	void visitCommunicationNode(CommunicationNode td);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model blUnique="false"
	 * @generated
	 */
	void visitBasicLeafTaskNode(BasicLeafTaskNode bl);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dwbUnique="false"
	 * @generated
	 */
	void visitDoWhileTaskNode(DoWhileTaskNode dwb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stnUnique="false"
	 * @generated
	 */
	void visitSwitchTaskNode(SwitchTaskNode stn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ctnUnique="false"
	 * @generated
	 */
	void visitCaseTaskNode(CaseTaskNode ctn);

} // HTaskGraphVisitor
