/**
 */
package htaskgraph;

import architecture.ProcessingResource;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HTG Recv</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.HTGRecv#getToProcResources <em>To Proc Resources</em>}</li>
 *   <li>{@link htaskgraph.HTGRecv#getSends <em>Sends</em>}</li>
 *   <li>{@link htaskgraph.HTGRecv#getUse <em>Use</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getHTGRecv()
 * @model
 * @generated
 */
public interface HTGRecv extends HTGCom {
	/**
	 * Returns the value of the '<em><b>To Proc Resources</b></em>' reference list.
	 * The list contents are of type {@link architecture.ProcessingResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Proc Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Proc Resources</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getHTGRecv_ToProcResources()
	 * @model
	 * @generated
	 */
	EList<ProcessingResource> getToProcResources();

	/**
	 * Returns the value of the '<em><b>Sends</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.HTGSend}.
	 * It is bidirectional and its opposite is '{@link htaskgraph.HTGSend#getReceives <em>Receives</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sends</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sends</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getHTGRecv_Sends()
	 * @see htaskgraph.HTGSend#getReceives
	 * @model opposite="receives"
	 * @generated
	 */
	EList<HTGSend> getSends();

	/**
	 * Returns the value of the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use</em>' reference.
	 * @see #setUse(Symbol)
	 * @see htaskgraph.HtaskgraphPackage#getHTGRecv_Use()
	 * @model
	 * @generated
	 */
	Symbol getUse();

	/**
	 * Sets the value of the '{@link htaskgraph.HTGRecv#getUse <em>Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use</em>' reference.
	 * @see #getUse()
	 * @generated
	 */
	void setUse(Symbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.HTGSend%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.HTGSend%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt; apply(final &lt;%htaskgraph.HTGSend%&gt; it)\n\t{\n\t\treturn it.getFromProcResources();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%architecture.ProcessingResource%&gt;&gt;asEList(((&lt;%architecture.ProcessingResource%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%architecture.ProcessingResource%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.HTGSend%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt;&gt;map(this.getSends(), _function)), &lt;%architecture.ProcessingResource%&gt;.class)));'"
	 * @generated
	 */
	EList<ProcessingResource> getFromProcResources();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _use = this.getUse();\n&lt;%java.lang.String%&gt; _plus = (\"HTGRecv \" + _use);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" \");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt; _fromProcResources = this.getFromProcResources();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _fromProcResources);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" -&gt; \");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%architecture.ProcessingResource%&gt;&gt; _toProcResources = this.getToProcResources();\nreturn (_plus_3 + _toProcResources);'"
	 * @generated
	 */
	String toString();

} // HTGRecv
