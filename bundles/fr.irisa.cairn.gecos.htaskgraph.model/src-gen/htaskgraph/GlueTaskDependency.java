/**
 */
package htaskgraph;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Glue Task Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getGlueTaskDependency()
 * @model
 * @generated
 */
public interface GlueTaskDependency extends TaskDependency {
} // GlueTaskDependency
