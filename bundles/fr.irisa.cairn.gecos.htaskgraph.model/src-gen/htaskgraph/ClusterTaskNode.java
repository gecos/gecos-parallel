/**
 */
package htaskgraph;

import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cluster Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.ClusterTaskNode#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getClusterTaskNode()
 * @model
 * @generated
 */
public interface ClusterTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(CompositeBlock)
	 * @see htaskgraph.HtaskgraphPackage#getClusterTaskNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	CompositeBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.ClusterTaskNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(CompositeBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitClusterTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"CTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldBlockUnique="false" newBlockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.CompositeBlock%&gt; _block = this.getBlock();\nboolean _tripleEquals = (oldBlock == _block);\nif (_tripleEquals)\n{\n\tif ((newBlock instanceof &lt;%gecos.blocks.CompositeBlock%&gt;))\n\t{\n\t\tthis.setBlock(((&lt;%gecos.blocks.CompositeBlock%&gt;)newBlock));\n\t}\n\telse\n\t{\n\t\tthis.setBlock(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.CompositeBlock(newBlock));\n\t}\n}\nelse\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;((((\"Block \" + oldBlock) + \" not contained in \") + this));\n}'"
	 * @generated
	 */
	void replace(Block oldBlock, Block newBlock);

} // ClusterTaskNode
