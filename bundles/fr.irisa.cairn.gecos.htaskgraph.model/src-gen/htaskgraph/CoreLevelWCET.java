/**
 */
package htaskgraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Core Level WCET</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.CoreLevelWCET#getArchitecture <em>Architecture</em>}</li>
 *   <li>{@link htaskgraph.CoreLevelWCET#getIsolatedWCET <em>Isolated WCET</em>}</li>
 *   <li>{@link htaskgraph.CoreLevelWCET#getConflictWCET <em>Conflict WCET</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getCoreLevelWCET()
 * @model
 * @generated
 */
public interface CoreLevelWCET extends EObject {
	/**
	 * Returns the value of the '<em><b>Architecture</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Architecture</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Architecture</em>' attribute.
	 * @see #setArchitecture(String)
	 * @see htaskgraph.HtaskgraphPackage#getCoreLevelWCET_Architecture()
	 * @model unique="false"
	 * @generated
	 */
	String getArchitecture();

	/**
	 * Sets the value of the '{@link htaskgraph.CoreLevelWCET#getArchitecture <em>Architecture</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Architecture</em>' attribute.
	 * @see #getArchitecture()
	 * @generated
	 */
	void setArchitecture(String value);

	/**
	 * Returns the value of the '<em><b>Isolated WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Isolated WCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Isolated WCET</em>' attribute.
	 * @see #setIsolatedWCET(long)
	 * @see htaskgraph.HtaskgraphPackage#getCoreLevelWCET_IsolatedWCET()
	 * @model unique="false"
	 * @generated
	 */
	long getIsolatedWCET();

	/**
	 * Sets the value of the '{@link htaskgraph.CoreLevelWCET#getIsolatedWCET <em>Isolated WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Isolated WCET</em>' attribute.
	 * @see #getIsolatedWCET()
	 * @generated
	 */
	void setIsolatedWCET(long value);

	/**
	 * Returns the value of the '<em><b>Conflict WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conflict WCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conflict WCET</em>' attribute.
	 * @see #setConflictWCET(long)
	 * @see htaskgraph.HtaskgraphPackage#getCoreLevelWCET_ConflictWCET()
	 * @model unique="false"
	 * @generated
	 */
	long getConflictWCET();

	/**
	 * Sets the value of the '{@link htaskgraph.CoreLevelWCET#getConflictWCET <em>Conflict WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conflict WCET</em>' attribute.
	 * @see #getConflictWCET()
	 * @generated
	 */
	void setConflictWCET(long value);

} // CoreLevelWCET
