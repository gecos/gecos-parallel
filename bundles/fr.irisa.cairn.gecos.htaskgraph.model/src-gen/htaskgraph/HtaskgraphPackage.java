/**
 */
package htaskgraph;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see htaskgraph.HtaskgraphFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false'"
 * @generated
 */
public interface HtaskgraphPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "htaskgraph";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/htaskgraph";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "htaskgraph";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HtaskgraphPackage eINSTANCE = htaskgraph.impl.HtaskgraphPackageImpl.init();

	/**
	 * The meta object id for the '{@link htaskgraph.HTaskGraphVisitable <em>HTask Graph Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.HTaskGraphVisitable
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTaskGraphVisitable()
	 * @generated
	 */
	int HTASK_GRAPH_VISITABLE = 0;

	/**
	 * The number of structural features of the '<em>HTask Graph Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTASK_GRAPH_VISITABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link htaskgraph.HTaskGraphVisitor <em>HTask Graph Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.HTaskGraphVisitor
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTaskGraphVisitor()
	 * @generated
	 */
	int HTASK_GRAPH_VISITOR = 1;

	/**
	 * The number of structural features of the '<em>HTask Graph Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTASK_GRAPH_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskDependencyImpl <em>Task Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskDependency()
	 * @generated
	 */
	int TASK_DEPENDENCY = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__USE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__FROM = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__TO = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__SRC_DEPENDENCIES = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__DST_DEPENDENCIES = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__ID = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.GlueTaskDependencyImpl <em>Glue Task Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.GlueTaskDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getGlueTaskDependency()
	 * @generated
	 */
	int GLUE_TASK_DEPENDENCY = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__ANNOTATIONS = TASK_DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__USE = TASK_DEPENDENCY__USE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__FROM = TASK_DEPENDENCY__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__TO = TASK_DEPENDENCY__TO;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__SRC_DEPENDENCIES = TASK_DEPENDENCY__SRC_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__DST_DEPENDENCIES = TASK_DEPENDENCY__DST_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY__ID = TASK_DEPENDENCY__ID;

	/**
	 * The number of structural features of the '<em>Glue Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLUE_TASK_DEPENDENCY_FEATURE_COUNT = TASK_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskOutputDependencyImpl <em>Task Output Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskOutputDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskOutputDependency()
	 * @generated
	 */
	int TASK_OUTPUT_DEPENDENCY = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__ANNOTATIONS = TASK_DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__USE = TASK_DEPENDENCY__USE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__FROM = TASK_DEPENDENCY__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__TO = TASK_DEPENDENCY__TO;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__SRC_DEPENDENCIES = TASK_DEPENDENCY__SRC_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__DST_DEPENDENCIES = TASK_DEPENDENCY__DST_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY__ID = TASK_DEPENDENCY__ID;

	/**
	 * The number of structural features of the '<em>Task Output Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OUTPUT_DEPENDENCY_FEATURE_COUNT = TASK_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskAntiDependencyImpl <em>Task Anti Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskAntiDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskAntiDependency()
	 * @generated
	 */
	int TASK_ANTI_DEPENDENCY = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__ANNOTATIONS = TASK_DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__USE = TASK_DEPENDENCY__USE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__FROM = TASK_DEPENDENCY__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__TO = TASK_DEPENDENCY__TO;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__SRC_DEPENDENCIES = TASK_DEPENDENCY__SRC_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__DST_DEPENDENCIES = TASK_DEPENDENCY__DST_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY__ID = TASK_DEPENDENCY__ID;

	/**
	 * The number of structural features of the '<em>Task Anti Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_ANTI_DEPENDENCY_FEATURE_COUNT = TASK_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskTrueDependencyImpl <em>Task True Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskTrueDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskTrueDependency()
	 * @generated
	 */
	int TASK_TRUE_DEPENDENCY = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__ANNOTATIONS = TASK_DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__USE = TASK_DEPENDENCY__USE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__FROM = TASK_DEPENDENCY__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__TO = TASK_DEPENDENCY__TO;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__SRC_DEPENDENCIES = TASK_DEPENDENCY__SRC_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__DST_DEPENDENCIES = TASK_DEPENDENCY__DST_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY__ID = TASK_DEPENDENCY__ID;

	/**
	 * The number of structural features of the '<em>Task True Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_TRUE_DEPENDENCY_FEATURE_COUNT = TASK_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskControlDependencyImpl <em>Task Control Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskControlDependencyImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskControlDependency()
	 * @generated
	 */
	int TASK_CONTROL_DEPENDENCY = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__ANNOTATIONS = TASK_DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__USE = TASK_DEPENDENCY__USE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__FROM = TASK_DEPENDENCY__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__TO = TASK_DEPENDENCY__TO;

	/**
	 * The feature id for the '<em><b>Src Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__SRC_DEPENDENCIES = TASK_DEPENDENCY__SRC_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Dst Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__DST_DEPENDENCIES = TASK_DEPENDENCY__DST_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY__ID = TASK_DEPENDENCY__ID;

	/**
	 * The number of structural features of the '<em>Task Control Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_DEPENDENCY_FEATURE_COUNT = TASK_DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.TaskNodeImpl <em>Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.TaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskNode()
	 * @generated
	 */
	int TASK_NODE = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__ANNOTATIONS = BlocksPackage.BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__PARENT = BlocksPackage.BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__NUMBER = BlocksPackage.BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__DEPTH = BlocksPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__SUCCS = BlocksPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__PREDS = BlocksPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__INPUT_NODE = BlocksPackage.BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__OUTPUT_NODE = BlocksPackage.BLOCK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__PROFILING_INFORMATION = BlocksPackage.BLOCK_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__PROCESSING_RESOURCES = BlocksPackage.BLOCK_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE__MEMORY_RESOURCES = BlocksPackage.BLOCK_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_NODE_FEATURE_COUNT = BlocksPackage.BLOCK_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.CommunicationNodeImpl <em>Communication Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.CommunicationNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCommunicationNode()
	 * @generated
	 */
	int COMMUNICATION_NODE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__ANNOTATIONS = TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__PARENT = TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__NUMBER = TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__DEPTH = TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__SUCCS = TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__PREDS = TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__INPUT_NODE = TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__OUTPUT_NODE = TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__PROFILING_INFORMATION = TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__PROCESSING_RESOURCES = TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__MEMORY_RESOURCES = TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__TYPE = TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__COMMUNICATIONS = TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE__BLOCK = TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Communication Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_NODE_FEATURE_COUNT = TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.LeafTaskNodeImpl <em>Leaf Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.LeafTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getLeafTaskNode()
	 * @generated
	 */
	int LEAF_TASK_NODE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__ANNOTATIONS = TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__PARENT = TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__NUMBER = TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__DEPTH = TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__SUCCS = TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__PREDS = TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__INPUT_NODE = TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__OUTPUT_NODE = TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__PROFILING_INFORMATION = TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__PROCESSING_RESOURCES = TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__MEMORY_RESOURCES = TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE__BLOCK = TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Leaf Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_TASK_NODE_FEATURE_COUNT = TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.BasicLeafTaskNodeImpl <em>Basic Leaf Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.BasicLeafTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getBasicLeafTaskNode()
	 * @generated
	 */
	int BASIC_LEAF_TASK_NODE = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__ANNOTATIONS = LEAF_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__PARENT = LEAF_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__NUMBER = LEAF_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__DEPTH = LEAF_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__SUCCS = LEAF_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__PREDS = LEAF_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__INPUT_NODE = LEAF_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__OUTPUT_NODE = LEAF_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__PROFILING_INFORMATION = LEAF_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__PROCESSING_RESOURCES = LEAF_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__MEMORY_RESOURCES = LEAF_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__BLOCK = LEAF_TASK_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__INSTRUCTIONS = LEAF_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__DEF_EDGES = LEAF_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__OUT_EDGES = LEAF_TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__USE_EDGES = LEAF_TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__IN_EDGES = LEAF_TASK_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__LABEL = LEAF_TASK_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE__FLAGS = LEAF_TASK_NODE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Basic Leaf Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_LEAF_TASK_NODE_FEATURE_COUNT = LEAF_TASK_NODE_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.HierarchicalTaskNodeImpl <em>Hierarchical Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.HierarchicalTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHierarchicalTaskNode()
	 * @generated
	 */
	int HIERARCHICAL_TASK_NODE = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__ANNOTATIONS = TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__PARENT = TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__NUMBER = TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__DEPTH = TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__SUCCS = TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__PREDS = TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__INPUT_NODE = TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__OUTPUT_NODE = TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION = TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES = TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES = TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The number of structural features of the '<em>Hierarchical Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_TASK_NODE_FEATURE_COUNT = TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.ClusterTaskNodeImpl <em>Cluster Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.ClusterTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getClusterTaskNode()
	 * @generated
	 */
	int CLUSTER_TASK_NODE = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cluster Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.IfTaskNodeImpl <em>If Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.IfTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getIfTaskNode()
	 * @generated
	 */
	int IF_TASK_NODE = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>If Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.SwitchTaskNodeImpl <em>Switch Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.SwitchTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getSwitchTaskNode()
	 * @generated
	 */
	int SWITCH_TASK_NODE = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Switch Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.CaseTaskNodeImpl <em>Case Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.CaseTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCaseTaskNode()
	 * @generated
	 */
	int CASE_TASK_NODE = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Case Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.ForTaskNodeImpl <em>For Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.ForTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getForTaskNode()
	 * @generated
	 */
	int FOR_TASK_NODE = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__LOOP_IN = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Loop Out</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE__LOOP_OUT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>For Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.WhileTaskNodeImpl <em>While Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.WhileTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getWhileTaskNode()
	 * @generated
	 */
	int WHILE_TASK_NODE = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__LOOP_IN = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Loop Out</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE__LOOP_OUT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>While Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.DoWhileTaskNodeImpl <em>Do While Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.DoWhileTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getDoWhileTaskNode()
	 * @generated
	 */
	int DO_WHILE_TASK_NODE = 19;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__BLOCK = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__LOOP_IN = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Loop Out</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE__LOOP_OUT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Do While Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.ProcedureTaskNodeImpl <em>Procedure Task Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.ProcedureTaskNodeImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getProcedureTaskNode()
	 * @generated
	 */
	int PROCEDURE_TASK_NODE = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__ANNOTATIONS = HIERARCHICAL_TASK_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__PARENT = HIERARCHICAL_TASK_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__NUMBER = HIERARCHICAL_TASK_NODE__NUMBER;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__DEPTH = HIERARCHICAL_TASK_NODE__DEPTH;

	/**
	 * The feature id for the '<em><b>Succs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__SUCCS = HIERARCHICAL_TASK_NODE__SUCCS;

	/**
	 * The feature id for the '<em><b>Preds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__PREDS = HIERARCHICAL_TASK_NODE__PREDS;

	/**
	 * The feature id for the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__INPUT_NODE = HIERARCHICAL_TASK_NODE__INPUT_NODE;

	/**
	 * The feature id for the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__OUTPUT_NODE = HIERARCHICAL_TASK_NODE__OUTPUT_NODE;

	/**
	 * The feature id for the '<em><b>Profiling Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__PROFILING_INFORMATION = HIERARCHICAL_TASK_NODE__PROFILING_INFORMATION;

	/**
	 * The feature id for the '<em><b>Processing Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__PROCESSING_RESOURCES = HIERARCHICAL_TASK_NODE__PROCESSING_RESOURCES;

	/**
	 * The feature id for the '<em><b>Memory Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__MEMORY_RESOURCES = HIERARCHICAL_TASK_NODE__MEMORY_RESOURCES;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__INPUTS = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__OUTPUTS = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE__PROCEDURE = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Procedure Task Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_TASK_NODE_FEATURE_COUNT = HIERARCHICAL_TASK_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.ProfilingInformation <em>Profiling Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.ProfilingInformation
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getProfilingInformation()
	 * @generated
	 */
	int PROFILING_INFORMATION = 21;

	/**
	 * The number of structural features of the '<em>Profiling Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILING_INFORMATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.CoreLevelWCETImpl <em>Core Level WCET</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.CoreLevelWCETImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCoreLevelWCET()
	 * @generated
	 */
	int CORE_LEVEL_WCET = 22;

	/**
	 * The feature id for the '<em><b>Architecture</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_LEVEL_WCET__ARCHITECTURE = 0;

	/**
	 * The feature id for the '<em><b>Isolated WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_LEVEL_WCET__ISOLATED_WCET = 1;

	/**
	 * The feature id for the '<em><b>Conflict WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_LEVEL_WCET__CONFLICT_WCET = 2;

	/**
	 * The number of structural features of the '<em>Core Level WCET</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_LEVEL_WCET_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.WCETInformationImpl <em>WCET Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.WCETInformationImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getWCETInformation()
	 * @generated
	 */
	int WCET_INFORMATION = 23;

	/**
	 * The feature id for the '<em><b>Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WCET_INFORMATION__DATA = PROFILING_INFORMATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>WCET Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WCET_INFORMATION_FEATURE_COUNT = PROFILING_INFORMATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.HTGComImpl <em>HTG Com</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.HTGComImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGCom()
	 * @generated
	 */
	int HTG_COM = 24;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__ANNOTATIONS = BlocksPackage.BASIC_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__PARENT = BlocksPackage.BASIC_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__NUMBER = BlocksPackage.BASIC_BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__INSTRUCTIONS = BlocksPackage.BASIC_BLOCK__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__DEF_EDGES = BlocksPackage.BASIC_BLOCK__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__OUT_EDGES = BlocksPackage.BASIC_BLOCK__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__USE_EDGES = BlocksPackage.BASIC_BLOCK__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__IN_EDGES = BlocksPackage.BASIC_BLOCK__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__LABEL = BlocksPackage.BASIC_BLOCK__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM__FLAGS = BlocksPackage.BASIC_BLOCK__FLAGS;

	/**
	 * The number of structural features of the '<em>HTG Com</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_COM_FEATURE_COUNT = BlocksPackage.BASIC_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.HTGSendImpl <em>HTG Send</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.HTGSendImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGSend()
	 * @generated
	 */
	int HTG_SEND = 25;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__ANNOTATIONS = HTG_COM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__PARENT = HTG_COM__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__NUMBER = HTG_COM__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__INSTRUCTIONS = HTG_COM__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__DEF_EDGES = HTG_COM__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__OUT_EDGES = HTG_COM__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__USE_EDGES = HTG_COM__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__IN_EDGES = HTG_COM__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__LABEL = HTG_COM__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__FLAGS = HTG_COM__FLAGS;

	/**
	 * The feature id for the '<em><b>From Proc Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__FROM_PROC_RESOURCES = HTG_COM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Receives</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__RECEIVES = HTG_COM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND__USE = HTG_COM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HTG Send</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_SEND_FEATURE_COUNT = HTG_COM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.impl.HTGRecvImpl <em>HTG Recv</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.impl.HTGRecvImpl
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGRecv()
	 * @generated
	 */
	int HTG_RECV = 26;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__ANNOTATIONS = HTG_COM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__PARENT = HTG_COM__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__NUMBER = HTG_COM__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__INSTRUCTIONS = HTG_COM__INSTRUCTIONS;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__DEF_EDGES = HTG_COM__DEF_EDGES;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__OUT_EDGES = HTG_COM__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__USE_EDGES = HTG_COM__USE_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__IN_EDGES = HTG_COM__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__LABEL = HTG_COM__LABEL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__FLAGS = HTG_COM__FLAGS;

	/**
	 * The feature id for the '<em><b>To Proc Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__TO_PROC_RESOURCES = HTG_COM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sends</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__SENDS = HTG_COM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV__USE = HTG_COM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HTG Recv</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTG_RECV_FEATURE_COUNT = HTG_COM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link htaskgraph.CommunicationNodeType <em>Communication Node Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see htaskgraph.CommunicationNodeType
	 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCommunicationNodeType()
	 * @generated
	 */
	int COMMUNICATION_NODE_TYPE = 27;


	/**
	 * Returns the meta object for class '{@link htaskgraph.HTaskGraphVisitable <em>HTask Graph Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTask Graph Visitable</em>'.
	 * @see htaskgraph.HTaskGraphVisitable
	 * @generated
	 */
	EClass getHTaskGraphVisitable();

	/**
	 * Returns the meta object for class '{@link htaskgraph.HTaskGraphVisitor <em>HTask Graph Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTask Graph Visitor</em>'.
	 * @see htaskgraph.HTaskGraphVisitor
	 * @generated
	 */
	EClass getHTaskGraphVisitor();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskDependency <em>Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Dependency</em>'.
	 * @see htaskgraph.TaskDependency
	 * @generated
	 */
	EClass getTaskDependency();

	/**
	 * Returns the meta object for the reference '{@link htaskgraph.TaskDependency#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Use</em>'.
	 * @see htaskgraph.TaskDependency#getUse()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_Use();

	/**
	 * Returns the meta object for the reference '{@link htaskgraph.TaskDependency#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see htaskgraph.TaskDependency#getFrom()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_From();

	/**
	 * Returns the meta object for the container reference '{@link htaskgraph.TaskDependency#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>To</em>'.
	 * @see htaskgraph.TaskDependency#getTo()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_To();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.TaskDependency#getSrcDependencies <em>Src Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Src Dependencies</em>'.
	 * @see htaskgraph.TaskDependency#getSrcDependencies()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_SrcDependencies();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.TaskDependency#getDstDependencies <em>Dst Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Dst Dependencies</em>'.
	 * @see htaskgraph.TaskDependency#getDstDependencies()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_DstDependencies();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.TaskDependency#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see htaskgraph.TaskDependency#getId()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EAttribute getTaskDependency_Id();

	/**
	 * Returns the meta object for class '{@link htaskgraph.GlueTaskDependency <em>Glue Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Glue Task Dependency</em>'.
	 * @see htaskgraph.GlueTaskDependency
	 * @generated
	 */
	EClass getGlueTaskDependency();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskOutputDependency <em>Task Output Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Output Dependency</em>'.
	 * @see htaskgraph.TaskOutputDependency
	 * @generated
	 */
	EClass getTaskOutputDependency();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskAntiDependency <em>Task Anti Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Anti Dependency</em>'.
	 * @see htaskgraph.TaskAntiDependency
	 * @generated
	 */
	EClass getTaskAntiDependency();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskTrueDependency <em>Task True Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task True Dependency</em>'.
	 * @see htaskgraph.TaskTrueDependency
	 * @generated
	 */
	EClass getTaskTrueDependency();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskControlDependency <em>Task Control Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Control Dependency</em>'.
	 * @see htaskgraph.TaskControlDependency
	 * @generated
	 */
	EClass getTaskControlDependency();

	/**
	 * Returns the meta object for class '{@link htaskgraph.TaskNode <em>Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Node</em>'.
	 * @see htaskgraph.TaskNode
	 * @generated
	 */
	EClass getTaskNode();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.TaskNode#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see htaskgraph.TaskNode#getDepth()
	 * @see #getTaskNode()
	 * @generated
	 */
	EAttribute getTaskNode_Depth();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.TaskNode#getSuccs <em>Succs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Succs</em>'.
	 * @see htaskgraph.TaskNode#getSuccs()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_Succs();

	/**
	 * Returns the meta object for the containment reference list '{@link htaskgraph.TaskNode#getPreds <em>Preds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Preds</em>'.
	 * @see htaskgraph.TaskNode#getPreds()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_Preds();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.TaskNode#getInputNode <em>Input Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input Node</em>'.
	 * @see htaskgraph.TaskNode#getInputNode()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_InputNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.TaskNode#getOutputNode <em>Output Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output Node</em>'.
	 * @see htaskgraph.TaskNode#getOutputNode()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_OutputNode();

	/**
	 * Returns the meta object for the containment reference list '{@link htaskgraph.TaskNode#getProfilingInformation <em>Profiling Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Profiling Information</em>'.
	 * @see htaskgraph.TaskNode#getProfilingInformation()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_ProfilingInformation();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.TaskNode#getProcessingResources <em>Processing Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Processing Resources</em>'.
	 * @see htaskgraph.TaskNode#getProcessingResources()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_ProcessingResources();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.TaskNode#getMemoryResources <em>Memory Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Memory Resources</em>'.
	 * @see htaskgraph.TaskNode#getMemoryResources()
	 * @see #getTaskNode()
	 * @generated
	 */
	EReference getTaskNode_MemoryResources();

	/**
	 * Returns the meta object for class '{@link htaskgraph.CommunicationNode <em>Communication Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Node</em>'.
	 * @see htaskgraph.CommunicationNode
	 * @generated
	 */
	EClass getCommunicationNode();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.CommunicationNode#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see htaskgraph.CommunicationNode#getType()
	 * @see #getCommunicationNode()
	 * @generated
	 */
	EAttribute getCommunicationNode_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link htaskgraph.CommunicationNode#getCommunications <em>Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communications</em>'.
	 * @see htaskgraph.CommunicationNode#getCommunications()
	 * @see #getCommunicationNode()
	 * @generated
	 */
	EReference getCommunicationNode_Communications();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.CommunicationNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.CommunicationNode#getBlock()
	 * @see #getCommunicationNode()
	 * @generated
	 */
	EReference getCommunicationNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.LeafTaskNode <em>Leaf Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Task Node</em>'.
	 * @see htaskgraph.LeafTaskNode
	 * @generated
	 */
	EClass getLeafTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.LeafTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.LeafTaskNode#getBlock()
	 * @see #getLeafTaskNode()
	 * @generated
	 */
	EReference getLeafTaskNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.BasicLeafTaskNode <em>Basic Leaf Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Leaf Task Node</em>'.
	 * @see htaskgraph.BasicLeafTaskNode
	 * @generated
	 */
	EClass getBasicLeafTaskNode();

	/**
	 * Returns the meta object for class '{@link htaskgraph.HierarchicalTaskNode <em>Hierarchical Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hierarchical Task Node</em>'.
	 * @see htaskgraph.HierarchicalTaskNode
	 * @generated
	 */
	EClass getHierarchicalTaskNode();

	/**
	 * Returns the meta object for class '{@link htaskgraph.ClusterTaskNode <em>Cluster Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cluster Task Node</em>'.
	 * @see htaskgraph.ClusterTaskNode
	 * @generated
	 */
	EClass getClusterTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.ClusterTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.ClusterTaskNode#getBlock()
	 * @see #getClusterTaskNode()
	 * @generated
	 */
	EReference getClusterTaskNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.IfTaskNode <em>If Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Task Node</em>'.
	 * @see htaskgraph.IfTaskNode
	 * @generated
	 */
	EClass getIfTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.IfTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.IfTaskNode#getBlock()
	 * @see #getIfTaskNode()
	 * @generated
	 */
	EReference getIfTaskNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.SwitchTaskNode <em>Switch Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Task Node</em>'.
	 * @see htaskgraph.SwitchTaskNode
	 * @generated
	 */
	EClass getSwitchTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.SwitchTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.SwitchTaskNode#getBlock()
	 * @see #getSwitchTaskNode()
	 * @generated
	 */
	EReference getSwitchTaskNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.CaseTaskNode <em>Case Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Task Node</em>'.
	 * @see htaskgraph.CaseTaskNode
	 * @generated
	 */
	EClass getCaseTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.CaseTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.CaseTaskNode#getBlock()
	 * @see #getCaseTaskNode()
	 * @generated
	 */
	EReference getCaseTaskNode_Block();

	/**
	 * Returns the meta object for class '{@link htaskgraph.ForTaskNode <em>For Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Task Node</em>'.
	 * @see htaskgraph.ForTaskNode
	 * @generated
	 */
	EClass getForTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.ForTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.ForTaskNode#getBlock()
	 * @see #getForTaskNode()
	 * @generated
	 */
	EReference getForTaskNode_Block();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.ForTaskNode#getLoopIn <em>Loop In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop In</em>'.
	 * @see htaskgraph.ForTaskNode#getLoopIn()
	 * @see #getForTaskNode()
	 * @generated
	 */
	EReference getForTaskNode_LoopIn();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.ForTaskNode#getLoopOut <em>Loop Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop Out</em>'.
	 * @see htaskgraph.ForTaskNode#getLoopOut()
	 * @see #getForTaskNode()
	 * @generated
	 */
	EReference getForTaskNode_LoopOut();

	/**
	 * Returns the meta object for class '{@link htaskgraph.WhileTaskNode <em>While Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Task Node</em>'.
	 * @see htaskgraph.WhileTaskNode
	 * @generated
	 */
	EClass getWhileTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.WhileTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.WhileTaskNode#getBlock()
	 * @see #getWhileTaskNode()
	 * @generated
	 */
	EReference getWhileTaskNode_Block();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.WhileTaskNode#getLoopIn <em>Loop In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop In</em>'.
	 * @see htaskgraph.WhileTaskNode#getLoopIn()
	 * @see #getWhileTaskNode()
	 * @generated
	 */
	EReference getWhileTaskNode_LoopIn();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.WhileTaskNode#getLoopOut <em>Loop Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop Out</em>'.
	 * @see htaskgraph.WhileTaskNode#getLoopOut()
	 * @see #getWhileTaskNode()
	 * @generated
	 */
	EReference getWhileTaskNode_LoopOut();

	/**
	 * Returns the meta object for class '{@link htaskgraph.DoWhileTaskNode <em>Do While Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Do While Task Node</em>'.
	 * @see htaskgraph.DoWhileTaskNode
	 * @generated
	 */
	EClass getDoWhileTaskNode();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.DoWhileTaskNode#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see htaskgraph.DoWhileTaskNode#getBlock()
	 * @see #getDoWhileTaskNode()
	 * @generated
	 */
	EReference getDoWhileTaskNode_Block();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.DoWhileTaskNode#getLoopIn <em>Loop In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop In</em>'.
	 * @see htaskgraph.DoWhileTaskNode#getLoopIn()
	 * @see #getDoWhileTaskNode()
	 * @generated
	 */
	EReference getDoWhileTaskNode_LoopIn();

	/**
	 * Returns the meta object for the containment reference '{@link htaskgraph.DoWhileTaskNode#getLoopOut <em>Loop Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop Out</em>'.
	 * @see htaskgraph.DoWhileTaskNode#getLoopOut()
	 * @see #getDoWhileTaskNode()
	 * @generated
	 */
	EReference getDoWhileTaskNode_LoopOut();

	/**
	 * Returns the meta object for class '{@link htaskgraph.ProcedureTaskNode <em>Procedure Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Task Node</em>'.
	 * @see htaskgraph.ProcedureTaskNode
	 * @generated
	 */
	EClass getProcedureTaskNode();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.ProcedureTaskNode#getInputs <em>Inputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Inputs</em>'.
	 * @see htaskgraph.ProcedureTaskNode#getInputs()
	 * @see #getProcedureTaskNode()
	 * @generated
	 */
	EReference getProcedureTaskNode_Inputs();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.ProcedureTaskNode#getOutputs <em>Outputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outputs</em>'.
	 * @see htaskgraph.ProcedureTaskNode#getOutputs()
	 * @see #getProcedureTaskNode()
	 * @generated
	 */
	EReference getProcedureTaskNode_Outputs();

	/**
	 * Returns the meta object for the reference '{@link htaskgraph.ProcedureTaskNode#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Procedure</em>'.
	 * @see htaskgraph.ProcedureTaskNode#getProcedure()
	 * @see #getProcedureTaskNode()
	 * @generated
	 */
	EReference getProcedureTaskNode_Procedure();

	/**
	 * Returns the meta object for class '{@link htaskgraph.ProfilingInformation <em>Profiling Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Profiling Information</em>'.
	 * @see htaskgraph.ProfilingInformation
	 * @generated
	 */
	EClass getProfilingInformation();

	/**
	 * Returns the meta object for class '{@link htaskgraph.CoreLevelWCET <em>Core Level WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Core Level WCET</em>'.
	 * @see htaskgraph.CoreLevelWCET
	 * @generated
	 */
	EClass getCoreLevelWCET();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.CoreLevelWCET#getArchitecture <em>Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Architecture</em>'.
	 * @see htaskgraph.CoreLevelWCET#getArchitecture()
	 * @see #getCoreLevelWCET()
	 * @generated
	 */
	EAttribute getCoreLevelWCET_Architecture();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.CoreLevelWCET#getIsolatedWCET <em>Isolated WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Isolated WCET</em>'.
	 * @see htaskgraph.CoreLevelWCET#getIsolatedWCET()
	 * @see #getCoreLevelWCET()
	 * @generated
	 */
	EAttribute getCoreLevelWCET_IsolatedWCET();

	/**
	 * Returns the meta object for the attribute '{@link htaskgraph.CoreLevelWCET#getConflictWCET <em>Conflict WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Conflict WCET</em>'.
	 * @see htaskgraph.CoreLevelWCET#getConflictWCET()
	 * @see #getCoreLevelWCET()
	 * @generated
	 */
	EAttribute getCoreLevelWCET_ConflictWCET();

	/**
	 * Returns the meta object for class '{@link htaskgraph.WCETInformation <em>WCET Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>WCET Information</em>'.
	 * @see htaskgraph.WCETInformation
	 * @generated
	 */
	EClass getWCETInformation();

	/**
	 * Returns the meta object for the containment reference list '{@link htaskgraph.WCETInformation#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data</em>'.
	 * @see htaskgraph.WCETInformation#getData()
	 * @see #getWCETInformation()
	 * @generated
	 */
	EReference getWCETInformation_Data();

	/**
	 * Returns the meta object for class '{@link htaskgraph.HTGCom <em>HTG Com</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTG Com</em>'.
	 * @see htaskgraph.HTGCom
	 * @generated
	 */
	EClass getHTGCom();

	/**
	 * Returns the meta object for class '{@link htaskgraph.HTGSend <em>HTG Send</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTG Send</em>'.
	 * @see htaskgraph.HTGSend
	 * @generated
	 */
	EClass getHTGSend();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.HTGSend#getFromProcResources <em>From Proc Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>From Proc Resources</em>'.
	 * @see htaskgraph.HTGSend#getFromProcResources()
	 * @see #getHTGSend()
	 * @generated
	 */
	EReference getHTGSend_FromProcResources();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.HTGSend#getReceives <em>Receives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Receives</em>'.
	 * @see htaskgraph.HTGSend#getReceives()
	 * @see #getHTGSend()
	 * @generated
	 */
	EReference getHTGSend_Receives();

	/**
	 * Returns the meta object for the reference '{@link htaskgraph.HTGSend#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Use</em>'.
	 * @see htaskgraph.HTGSend#getUse()
	 * @see #getHTGSend()
	 * @generated
	 */
	EReference getHTGSend_Use();

	/**
	 * Returns the meta object for class '{@link htaskgraph.HTGRecv <em>HTG Recv</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTG Recv</em>'.
	 * @see htaskgraph.HTGRecv
	 * @generated
	 */
	EClass getHTGRecv();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.HTGRecv#getToProcResources <em>To Proc Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>To Proc Resources</em>'.
	 * @see htaskgraph.HTGRecv#getToProcResources()
	 * @see #getHTGRecv()
	 * @generated
	 */
	EReference getHTGRecv_ToProcResources();

	/**
	 * Returns the meta object for the reference list '{@link htaskgraph.HTGRecv#getSends <em>Sends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sends</em>'.
	 * @see htaskgraph.HTGRecv#getSends()
	 * @see #getHTGRecv()
	 * @generated
	 */
	EReference getHTGRecv_Sends();

	/**
	 * Returns the meta object for the reference '{@link htaskgraph.HTGRecv#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Use</em>'.
	 * @see htaskgraph.HTGRecv#getUse()
	 * @see #getHTGRecv()
	 * @generated
	 */
	EReference getHTGRecv_Use();

	/**
	 * Returns the meta object for enum '{@link htaskgraph.CommunicationNodeType <em>Communication Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Communication Node Type</em>'.
	 * @see htaskgraph.CommunicationNodeType
	 * @generated
	 */
	EEnum getCommunicationNodeType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HtaskgraphFactory getHtaskgraphFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link htaskgraph.HTaskGraphVisitable <em>HTask Graph Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.HTaskGraphVisitable
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTaskGraphVisitable()
		 * @generated
		 */
		EClass HTASK_GRAPH_VISITABLE = eINSTANCE.getHTaskGraphVisitable();

		/**
		 * The meta object literal for the '{@link htaskgraph.HTaskGraphVisitor <em>HTask Graph Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.HTaskGraphVisitor
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTaskGraphVisitor()
		 * @generated
		 */
		EClass HTASK_GRAPH_VISITOR = eINSTANCE.getHTaskGraphVisitor();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskDependencyImpl <em>Task Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskDependency()
		 * @generated
		 */
		EClass TASK_DEPENDENCY = eINSTANCE.getTaskDependency();

		/**
		 * The meta object literal for the '<em><b>Use</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__USE = eINSTANCE.getTaskDependency_Use();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__FROM = eINSTANCE.getTaskDependency_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__TO = eINSTANCE.getTaskDependency_To();

		/**
		 * The meta object literal for the '<em><b>Src Dependencies</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__SRC_DEPENDENCIES = eINSTANCE.getTaskDependency_SrcDependencies();

		/**
		 * The meta object literal for the '<em><b>Dst Dependencies</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__DST_DEPENDENCIES = eINSTANCE.getTaskDependency_DstDependencies();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK_DEPENDENCY__ID = eINSTANCE.getTaskDependency_Id();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.GlueTaskDependencyImpl <em>Glue Task Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.GlueTaskDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getGlueTaskDependency()
		 * @generated
		 */
		EClass GLUE_TASK_DEPENDENCY = eINSTANCE.getGlueTaskDependency();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskOutputDependencyImpl <em>Task Output Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskOutputDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskOutputDependency()
		 * @generated
		 */
		EClass TASK_OUTPUT_DEPENDENCY = eINSTANCE.getTaskOutputDependency();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskAntiDependencyImpl <em>Task Anti Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskAntiDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskAntiDependency()
		 * @generated
		 */
		EClass TASK_ANTI_DEPENDENCY = eINSTANCE.getTaskAntiDependency();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskTrueDependencyImpl <em>Task True Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskTrueDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskTrueDependency()
		 * @generated
		 */
		EClass TASK_TRUE_DEPENDENCY = eINSTANCE.getTaskTrueDependency();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskControlDependencyImpl <em>Task Control Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskControlDependencyImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskControlDependency()
		 * @generated
		 */
		EClass TASK_CONTROL_DEPENDENCY = eINSTANCE.getTaskControlDependency();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.TaskNodeImpl <em>Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.TaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getTaskNode()
		 * @generated
		 */
		EClass TASK_NODE = eINSTANCE.getTaskNode();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK_NODE__DEPTH = eINSTANCE.getTaskNode_Depth();

		/**
		 * The meta object literal for the '<em><b>Succs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__SUCCS = eINSTANCE.getTaskNode_Succs();

		/**
		 * The meta object literal for the '<em><b>Preds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__PREDS = eINSTANCE.getTaskNode_Preds();

		/**
		 * The meta object literal for the '<em><b>Input Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__INPUT_NODE = eINSTANCE.getTaskNode_InputNode();

		/**
		 * The meta object literal for the '<em><b>Output Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__OUTPUT_NODE = eINSTANCE.getTaskNode_OutputNode();

		/**
		 * The meta object literal for the '<em><b>Profiling Information</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__PROFILING_INFORMATION = eINSTANCE.getTaskNode_ProfilingInformation();

		/**
		 * The meta object literal for the '<em><b>Processing Resources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__PROCESSING_RESOURCES = eINSTANCE.getTaskNode_ProcessingResources();

		/**
		 * The meta object literal for the '<em><b>Memory Resources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_NODE__MEMORY_RESOURCES = eINSTANCE.getTaskNode_MemoryResources();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.CommunicationNodeImpl <em>Communication Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.CommunicationNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCommunicationNode()
		 * @generated
		 */
		EClass COMMUNICATION_NODE = eINSTANCE.getCommunicationNode();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_NODE__TYPE = eINSTANCE.getCommunicationNode_Type();

		/**
		 * The meta object literal for the '<em><b>Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_NODE__COMMUNICATIONS = eINSTANCE.getCommunicationNode_Communications();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_NODE__BLOCK = eINSTANCE.getCommunicationNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.LeafTaskNodeImpl <em>Leaf Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.LeafTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getLeafTaskNode()
		 * @generated
		 */
		EClass LEAF_TASK_NODE = eINSTANCE.getLeafTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_TASK_NODE__BLOCK = eINSTANCE.getLeafTaskNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.BasicLeafTaskNodeImpl <em>Basic Leaf Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.BasicLeafTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getBasicLeafTaskNode()
		 * @generated
		 */
		EClass BASIC_LEAF_TASK_NODE = eINSTANCE.getBasicLeafTaskNode();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.HierarchicalTaskNodeImpl <em>Hierarchical Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.HierarchicalTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHierarchicalTaskNode()
		 * @generated
		 */
		EClass HIERARCHICAL_TASK_NODE = eINSTANCE.getHierarchicalTaskNode();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.ClusterTaskNodeImpl <em>Cluster Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.ClusterTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getClusterTaskNode()
		 * @generated
		 */
		EClass CLUSTER_TASK_NODE = eINSTANCE.getClusterTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLUSTER_TASK_NODE__BLOCK = eINSTANCE.getClusterTaskNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.IfTaskNodeImpl <em>If Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.IfTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getIfTaskNode()
		 * @generated
		 */
		EClass IF_TASK_NODE = eINSTANCE.getIfTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_TASK_NODE__BLOCK = eINSTANCE.getIfTaskNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.SwitchTaskNodeImpl <em>Switch Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.SwitchTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getSwitchTaskNode()
		 * @generated
		 */
		EClass SWITCH_TASK_NODE = eINSTANCE.getSwitchTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWITCH_TASK_NODE__BLOCK = eINSTANCE.getSwitchTaskNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.CaseTaskNodeImpl <em>Case Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.CaseTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCaseTaskNode()
		 * @generated
		 */
		EClass CASE_TASK_NODE = eINSTANCE.getCaseTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_TASK_NODE__BLOCK = eINSTANCE.getCaseTaskNode_Block();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.ForTaskNodeImpl <em>For Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.ForTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getForTaskNode()
		 * @generated
		 */
		EClass FOR_TASK_NODE = eINSTANCE.getForTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_TASK_NODE__BLOCK = eINSTANCE.getForTaskNode_Block();

		/**
		 * The meta object literal for the '<em><b>Loop In</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_TASK_NODE__LOOP_IN = eINSTANCE.getForTaskNode_LoopIn();

		/**
		 * The meta object literal for the '<em><b>Loop Out</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_TASK_NODE__LOOP_OUT = eINSTANCE.getForTaskNode_LoopOut();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.WhileTaskNodeImpl <em>While Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.WhileTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getWhileTaskNode()
		 * @generated
		 */
		EClass WHILE_TASK_NODE = eINSTANCE.getWhileTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_TASK_NODE__BLOCK = eINSTANCE.getWhileTaskNode_Block();

		/**
		 * The meta object literal for the '<em><b>Loop In</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_TASK_NODE__LOOP_IN = eINSTANCE.getWhileTaskNode_LoopIn();

		/**
		 * The meta object literal for the '<em><b>Loop Out</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_TASK_NODE__LOOP_OUT = eINSTANCE.getWhileTaskNode_LoopOut();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.DoWhileTaskNodeImpl <em>Do While Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.DoWhileTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getDoWhileTaskNode()
		 * @generated
		 */
		EClass DO_WHILE_TASK_NODE = eINSTANCE.getDoWhileTaskNode();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DO_WHILE_TASK_NODE__BLOCK = eINSTANCE.getDoWhileTaskNode_Block();

		/**
		 * The meta object literal for the '<em><b>Loop In</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DO_WHILE_TASK_NODE__LOOP_IN = eINSTANCE.getDoWhileTaskNode_LoopIn();

		/**
		 * The meta object literal for the '<em><b>Loop Out</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DO_WHILE_TASK_NODE__LOOP_OUT = eINSTANCE.getDoWhileTaskNode_LoopOut();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.ProcedureTaskNodeImpl <em>Procedure Task Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.ProcedureTaskNodeImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getProcedureTaskNode()
		 * @generated
		 */
		EClass PROCEDURE_TASK_NODE = eINSTANCE.getProcedureTaskNode();

		/**
		 * The meta object literal for the '<em><b>Inputs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_TASK_NODE__INPUTS = eINSTANCE.getProcedureTaskNode_Inputs();

		/**
		 * The meta object literal for the '<em><b>Outputs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_TASK_NODE__OUTPUTS = eINSTANCE.getProcedureTaskNode_Outputs();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_TASK_NODE__PROCEDURE = eINSTANCE.getProcedureTaskNode_Procedure();

		/**
		 * The meta object literal for the '{@link htaskgraph.ProfilingInformation <em>Profiling Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.ProfilingInformation
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getProfilingInformation()
		 * @generated
		 */
		EClass PROFILING_INFORMATION = eINSTANCE.getProfilingInformation();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.CoreLevelWCETImpl <em>Core Level WCET</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.CoreLevelWCETImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCoreLevelWCET()
		 * @generated
		 */
		EClass CORE_LEVEL_WCET = eINSTANCE.getCoreLevelWCET();

		/**
		 * The meta object literal for the '<em><b>Architecture</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORE_LEVEL_WCET__ARCHITECTURE = eINSTANCE.getCoreLevelWCET_Architecture();

		/**
		 * The meta object literal for the '<em><b>Isolated WCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORE_LEVEL_WCET__ISOLATED_WCET = eINSTANCE.getCoreLevelWCET_IsolatedWCET();

		/**
		 * The meta object literal for the '<em><b>Conflict WCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORE_LEVEL_WCET__CONFLICT_WCET = eINSTANCE.getCoreLevelWCET_ConflictWCET();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.WCETInformationImpl <em>WCET Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.WCETInformationImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getWCETInformation()
		 * @generated
		 */
		EClass WCET_INFORMATION = eINSTANCE.getWCETInformation();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WCET_INFORMATION__DATA = eINSTANCE.getWCETInformation_Data();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.HTGComImpl <em>HTG Com</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.HTGComImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGCom()
		 * @generated
		 */
		EClass HTG_COM = eINSTANCE.getHTGCom();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.HTGSendImpl <em>HTG Send</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.HTGSendImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGSend()
		 * @generated
		 */
		EClass HTG_SEND = eINSTANCE.getHTGSend();

		/**
		 * The meta object literal for the '<em><b>From Proc Resources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_SEND__FROM_PROC_RESOURCES = eINSTANCE.getHTGSend_FromProcResources();

		/**
		 * The meta object literal for the '<em><b>Receives</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_SEND__RECEIVES = eINSTANCE.getHTGSend_Receives();

		/**
		 * The meta object literal for the '<em><b>Use</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_SEND__USE = eINSTANCE.getHTGSend_Use();

		/**
		 * The meta object literal for the '{@link htaskgraph.impl.HTGRecvImpl <em>HTG Recv</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.impl.HTGRecvImpl
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getHTGRecv()
		 * @generated
		 */
		EClass HTG_RECV = eINSTANCE.getHTGRecv();

		/**
		 * The meta object literal for the '<em><b>To Proc Resources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_RECV__TO_PROC_RESOURCES = eINSTANCE.getHTGRecv_ToProcResources();

		/**
		 * The meta object literal for the '<em><b>Sends</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_RECV__SENDS = eINSTANCE.getHTGRecv_Sends();

		/**
		 * The meta object literal for the '<em><b>Use</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTG_RECV__USE = eINSTANCE.getHTGRecv_Use();

		/**
		 * The meta object literal for the '{@link htaskgraph.CommunicationNodeType <em>Communication Node Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see htaskgraph.CommunicationNodeType
		 * @see htaskgraph.impl.HtaskgraphPackageImpl#getCommunicationNodeType()
		 * @generated
		 */
		EEnum COMMUNICATION_NODE_TYPE = eINSTANCE.getCommunicationNodeType();

	}

} //HtaskgraphPackage
