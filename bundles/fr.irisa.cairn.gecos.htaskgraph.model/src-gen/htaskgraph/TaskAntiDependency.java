/**
 */
package htaskgraph;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Anti Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskAntiDependency()
 * @model
 * @generated
 */
public interface TaskAntiDependency extends TaskDependency {
} // TaskAntiDependency
