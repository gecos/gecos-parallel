/**
 */
package htaskgraph.util;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitable;

import gecos.core.GecosNode;

import htaskgraph.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see htaskgraph.HtaskgraphPackage
 * @generated
 */
public class HtaskgraphAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static HtaskgraphPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = HtaskgraphPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HtaskgraphSwitch<Adapter> modelSwitch =
		new HtaskgraphSwitch<Adapter>() {
			@Override
			public Adapter caseHTaskGraphVisitable(HTaskGraphVisitable object) {
				return createHTaskGraphVisitableAdapter();
			}
			@Override
			public Adapter caseHTaskGraphVisitor(HTaskGraphVisitor object) {
				return createHTaskGraphVisitorAdapter();
			}
			@Override
			public Adapter caseTaskDependency(TaskDependency object) {
				return createTaskDependencyAdapter();
			}
			@Override
			public Adapter caseGlueTaskDependency(GlueTaskDependency object) {
				return createGlueTaskDependencyAdapter();
			}
			@Override
			public Adapter caseTaskOutputDependency(TaskOutputDependency object) {
				return createTaskOutputDependencyAdapter();
			}
			@Override
			public Adapter caseTaskAntiDependency(TaskAntiDependency object) {
				return createTaskAntiDependencyAdapter();
			}
			@Override
			public Adapter caseTaskTrueDependency(TaskTrueDependency object) {
				return createTaskTrueDependencyAdapter();
			}
			@Override
			public Adapter caseTaskControlDependency(TaskControlDependency object) {
				return createTaskControlDependencyAdapter();
			}
			@Override
			public Adapter caseTaskNode(TaskNode object) {
				return createTaskNodeAdapter();
			}
			@Override
			public Adapter caseCommunicationNode(CommunicationNode object) {
				return createCommunicationNodeAdapter();
			}
			@Override
			public Adapter caseLeafTaskNode(LeafTaskNode object) {
				return createLeafTaskNodeAdapter();
			}
			@Override
			public Adapter caseBasicLeafTaskNode(BasicLeafTaskNode object) {
				return createBasicLeafTaskNodeAdapter();
			}
			@Override
			public Adapter caseHierarchicalTaskNode(HierarchicalTaskNode object) {
				return createHierarchicalTaskNodeAdapter();
			}
			@Override
			public Adapter caseClusterTaskNode(ClusterTaskNode object) {
				return createClusterTaskNodeAdapter();
			}
			@Override
			public Adapter caseIfTaskNode(IfTaskNode object) {
				return createIfTaskNodeAdapter();
			}
			@Override
			public Adapter caseSwitchTaskNode(SwitchTaskNode object) {
				return createSwitchTaskNodeAdapter();
			}
			@Override
			public Adapter caseCaseTaskNode(CaseTaskNode object) {
				return createCaseTaskNodeAdapter();
			}
			@Override
			public Adapter caseForTaskNode(ForTaskNode object) {
				return createForTaskNodeAdapter();
			}
			@Override
			public Adapter caseWhileTaskNode(WhileTaskNode object) {
				return createWhileTaskNodeAdapter();
			}
			@Override
			public Adapter caseDoWhileTaskNode(DoWhileTaskNode object) {
				return createDoWhileTaskNodeAdapter();
			}
			@Override
			public Adapter caseProcedureTaskNode(ProcedureTaskNode object) {
				return createProcedureTaskNodeAdapter();
			}
			@Override
			public Adapter caseProfilingInformation(ProfilingInformation object) {
				return createProfilingInformationAdapter();
			}
			@Override
			public Adapter caseCoreLevelWCET(CoreLevelWCET object) {
				return createCoreLevelWCETAdapter();
			}
			@Override
			public Adapter caseWCETInformation(WCETInformation object) {
				return createWCETInformationAdapter();
			}
			@Override
			public Adapter caseHTGCom(HTGCom object) {
				return createHTGComAdapter();
			}
			@Override
			public Adapter caseHTGSend(HTGSend object) {
				return createHTGSendAdapter();
			}
			@Override
			public Adapter caseHTGRecv(HTGRecv object) {
				return createHTGRecvAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseBlocksVisitable(BlocksVisitable object) {
				return createBlocksVisitableAdapter();
			}
			@Override
			public Adapter caseBlock(Block object) {
				return createBlockAdapter();
			}
			@Override
			public Adapter caseBasicBlock(BasicBlock object) {
				return createBasicBlockAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HTaskGraphVisitable <em>HTask Graph Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HTaskGraphVisitable
	 * @generated
	 */
	public Adapter createHTaskGraphVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HTaskGraphVisitor <em>HTask Graph Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HTaskGraphVisitor
	 * @generated
	 */
	public Adapter createHTaskGraphVisitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskDependency <em>Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskDependency
	 * @generated
	 */
	public Adapter createTaskDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.GlueTaskDependency <em>Glue Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.GlueTaskDependency
	 * @generated
	 */
	public Adapter createGlueTaskDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskOutputDependency <em>Task Output Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskOutputDependency
	 * @generated
	 */
	public Adapter createTaskOutputDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskAntiDependency <em>Task Anti Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskAntiDependency
	 * @generated
	 */
	public Adapter createTaskAntiDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskTrueDependency <em>Task True Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskTrueDependency
	 * @generated
	 */
	public Adapter createTaskTrueDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskControlDependency <em>Task Control Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskControlDependency
	 * @generated
	 */
	public Adapter createTaskControlDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.TaskNode <em>Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.TaskNode
	 * @generated
	 */
	public Adapter createTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.CommunicationNode <em>Communication Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.CommunicationNode
	 * @generated
	 */
	public Adapter createCommunicationNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.LeafTaskNode <em>Leaf Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.LeafTaskNode
	 * @generated
	 */
	public Adapter createLeafTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.BasicLeafTaskNode <em>Basic Leaf Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.BasicLeafTaskNode
	 * @generated
	 */
	public Adapter createBasicLeafTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HierarchicalTaskNode <em>Hierarchical Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HierarchicalTaskNode
	 * @generated
	 */
	public Adapter createHierarchicalTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.ClusterTaskNode <em>Cluster Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.ClusterTaskNode
	 * @generated
	 */
	public Adapter createClusterTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.IfTaskNode <em>If Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.IfTaskNode
	 * @generated
	 */
	public Adapter createIfTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.SwitchTaskNode <em>Switch Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.SwitchTaskNode
	 * @generated
	 */
	public Adapter createSwitchTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.CaseTaskNode <em>Case Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.CaseTaskNode
	 * @generated
	 */
	public Adapter createCaseTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.ForTaskNode <em>For Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.ForTaskNode
	 * @generated
	 */
	public Adapter createForTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.WhileTaskNode <em>While Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.WhileTaskNode
	 * @generated
	 */
	public Adapter createWhileTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.DoWhileTaskNode <em>Do While Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.DoWhileTaskNode
	 * @generated
	 */
	public Adapter createDoWhileTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.ProcedureTaskNode <em>Procedure Task Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.ProcedureTaskNode
	 * @generated
	 */
	public Adapter createProcedureTaskNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.ProfilingInformation <em>Profiling Information</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.ProfilingInformation
	 * @generated
	 */
	public Adapter createProfilingInformationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.CoreLevelWCET <em>Core Level WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.CoreLevelWCET
	 * @generated
	 */
	public Adapter createCoreLevelWCETAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.WCETInformation <em>WCET Information</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.WCETInformation
	 * @generated
	 */
	public Adapter createWCETInformationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HTGCom <em>HTG Com</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HTGCom
	 * @generated
	 */
	public Adapter createHTGComAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HTGSend <em>HTG Send</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HTGSend
	 * @generated
	 */
	public Adapter createHTGSendAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link htaskgraph.HTGRecv <em>HTG Recv</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see htaskgraph.HTGRecv
	 * @generated
	 */
	public Adapter createHTGRecvAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BlocksVisitable
	 * @generated
	 */
	public Adapter createBlocksVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Block
	 * @generated
	 */
	public Adapter createBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BasicBlock <em>Basic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BasicBlock
	 * @generated
	 */
	public Adapter createBasicBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //HtaskgraphAdapterFactory
