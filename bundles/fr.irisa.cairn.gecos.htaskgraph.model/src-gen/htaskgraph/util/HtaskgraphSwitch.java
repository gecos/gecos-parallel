/**
 */
package htaskgraph.util;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitable;

import gecos.core.GecosNode;

import htaskgraph.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see htaskgraph.HtaskgraphPackage
 * @generated
 */
public class HtaskgraphSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static HtaskgraphPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphSwitch() {
		if (modelPackage == null) {
			modelPackage = HtaskgraphPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case HtaskgraphPackage.HTASK_GRAPH_VISITABLE: {
				HTaskGraphVisitable hTaskGraphVisitable = (HTaskGraphVisitable)theEObject;
				T result = caseHTaskGraphVisitable(hTaskGraphVisitable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.HTASK_GRAPH_VISITOR: {
				HTaskGraphVisitor hTaskGraphVisitor = (HTaskGraphVisitor)theEObject;
				T result = caseHTaskGraphVisitor(hTaskGraphVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_DEPENDENCY: {
				TaskDependency taskDependency = (TaskDependency)theEObject;
				T result = caseTaskDependency(taskDependency);
				if (result == null) result = caseAnnotatedElement(taskDependency);
				if (result == null) result = caseHTaskGraphVisitable(taskDependency);
				if (result == null) result = caseGecosNode(taskDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.GLUE_TASK_DEPENDENCY: {
				GlueTaskDependency glueTaskDependency = (GlueTaskDependency)theEObject;
				T result = caseGlueTaskDependency(glueTaskDependency);
				if (result == null) result = caseTaskDependency(glueTaskDependency);
				if (result == null) result = caseAnnotatedElement(glueTaskDependency);
				if (result == null) result = caseHTaskGraphVisitable(glueTaskDependency);
				if (result == null) result = caseGecosNode(glueTaskDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_OUTPUT_DEPENDENCY: {
				TaskOutputDependency taskOutputDependency = (TaskOutputDependency)theEObject;
				T result = caseTaskOutputDependency(taskOutputDependency);
				if (result == null) result = caseTaskDependency(taskOutputDependency);
				if (result == null) result = caseAnnotatedElement(taskOutputDependency);
				if (result == null) result = caseHTaskGraphVisitable(taskOutputDependency);
				if (result == null) result = caseGecosNode(taskOutputDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_ANTI_DEPENDENCY: {
				TaskAntiDependency taskAntiDependency = (TaskAntiDependency)theEObject;
				T result = caseTaskAntiDependency(taskAntiDependency);
				if (result == null) result = caseTaskDependency(taskAntiDependency);
				if (result == null) result = caseAnnotatedElement(taskAntiDependency);
				if (result == null) result = caseHTaskGraphVisitable(taskAntiDependency);
				if (result == null) result = caseGecosNode(taskAntiDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_TRUE_DEPENDENCY: {
				TaskTrueDependency taskTrueDependency = (TaskTrueDependency)theEObject;
				T result = caseTaskTrueDependency(taskTrueDependency);
				if (result == null) result = caseTaskDependency(taskTrueDependency);
				if (result == null) result = caseAnnotatedElement(taskTrueDependency);
				if (result == null) result = caseHTaskGraphVisitable(taskTrueDependency);
				if (result == null) result = caseGecosNode(taskTrueDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_CONTROL_DEPENDENCY: {
				TaskControlDependency taskControlDependency = (TaskControlDependency)theEObject;
				T result = caseTaskControlDependency(taskControlDependency);
				if (result == null) result = caseTaskDependency(taskControlDependency);
				if (result == null) result = caseAnnotatedElement(taskControlDependency);
				if (result == null) result = caseHTaskGraphVisitable(taskControlDependency);
				if (result == null) result = caseGecosNode(taskControlDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.TASK_NODE: {
				TaskNode taskNode = (TaskNode)theEObject;
				T result = caseTaskNode(taskNode);
				if (result == null) result = caseBlock(taskNode);
				if (result == null) result = caseHTaskGraphVisitable(taskNode);
				if (result == null) result = caseAnnotatedElement(taskNode);
				if (result == null) result = caseBlocksVisitable(taskNode);
				if (result == null) result = caseGecosNode(taskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.COMMUNICATION_NODE: {
				CommunicationNode communicationNode = (CommunicationNode)theEObject;
				T result = caseCommunicationNode(communicationNode);
				if (result == null) result = caseTaskNode(communicationNode);
				if (result == null) result = caseBlock(communicationNode);
				if (result == null) result = caseHTaskGraphVisitable(communicationNode);
				if (result == null) result = caseAnnotatedElement(communicationNode);
				if (result == null) result = caseBlocksVisitable(communicationNode);
				if (result == null) result = caseGecosNode(communicationNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.LEAF_TASK_NODE: {
				LeafTaskNode leafTaskNode = (LeafTaskNode)theEObject;
				T result = caseLeafTaskNode(leafTaskNode);
				if (result == null) result = caseTaskNode(leafTaskNode);
				if (result == null) result = caseBlock(leafTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(leafTaskNode);
				if (result == null) result = caseAnnotatedElement(leafTaskNode);
				if (result == null) result = caseBlocksVisitable(leafTaskNode);
				if (result == null) result = caseGecosNode(leafTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.BASIC_LEAF_TASK_NODE: {
				BasicLeafTaskNode basicLeafTaskNode = (BasicLeafTaskNode)theEObject;
				T result = caseBasicLeafTaskNode(basicLeafTaskNode);
				if (result == null) result = caseLeafTaskNode(basicLeafTaskNode);
				if (result == null) result = caseBasicBlock(basicLeafTaskNode);
				if (result == null) result = caseTaskNode(basicLeafTaskNode);
				if (result == null) result = caseBlock(basicLeafTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(basicLeafTaskNode);
				if (result == null) result = caseAnnotatedElement(basicLeafTaskNode);
				if (result == null) result = caseBlocksVisitable(basicLeafTaskNode);
				if (result == null) result = caseGecosNode(basicLeafTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.HIERARCHICAL_TASK_NODE: {
				HierarchicalTaskNode hierarchicalTaskNode = (HierarchicalTaskNode)theEObject;
				T result = caseHierarchicalTaskNode(hierarchicalTaskNode);
				if (result == null) result = caseTaskNode(hierarchicalTaskNode);
				if (result == null) result = caseBlock(hierarchicalTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(hierarchicalTaskNode);
				if (result == null) result = caseAnnotatedElement(hierarchicalTaskNode);
				if (result == null) result = caseBlocksVisitable(hierarchicalTaskNode);
				if (result == null) result = caseGecosNode(hierarchicalTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.CLUSTER_TASK_NODE: {
				ClusterTaskNode clusterTaskNode = (ClusterTaskNode)theEObject;
				T result = caseClusterTaskNode(clusterTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(clusterTaskNode);
				if (result == null) result = caseTaskNode(clusterTaskNode);
				if (result == null) result = caseBlock(clusterTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(clusterTaskNode);
				if (result == null) result = caseAnnotatedElement(clusterTaskNode);
				if (result == null) result = caseBlocksVisitable(clusterTaskNode);
				if (result == null) result = caseGecosNode(clusterTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.IF_TASK_NODE: {
				IfTaskNode ifTaskNode = (IfTaskNode)theEObject;
				T result = caseIfTaskNode(ifTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(ifTaskNode);
				if (result == null) result = caseTaskNode(ifTaskNode);
				if (result == null) result = caseBlock(ifTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(ifTaskNode);
				if (result == null) result = caseAnnotatedElement(ifTaskNode);
				if (result == null) result = caseBlocksVisitable(ifTaskNode);
				if (result == null) result = caseGecosNode(ifTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.SWITCH_TASK_NODE: {
				SwitchTaskNode switchTaskNode = (SwitchTaskNode)theEObject;
				T result = caseSwitchTaskNode(switchTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(switchTaskNode);
				if (result == null) result = caseTaskNode(switchTaskNode);
				if (result == null) result = caseBlock(switchTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(switchTaskNode);
				if (result == null) result = caseAnnotatedElement(switchTaskNode);
				if (result == null) result = caseBlocksVisitable(switchTaskNode);
				if (result == null) result = caseGecosNode(switchTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.CASE_TASK_NODE: {
				CaseTaskNode caseTaskNode = (CaseTaskNode)theEObject;
				T result = caseCaseTaskNode(caseTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(caseTaskNode);
				if (result == null) result = caseTaskNode(caseTaskNode);
				if (result == null) result = caseBlock(caseTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(caseTaskNode);
				if (result == null) result = caseAnnotatedElement(caseTaskNode);
				if (result == null) result = caseBlocksVisitable(caseTaskNode);
				if (result == null) result = caseGecosNode(caseTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.FOR_TASK_NODE: {
				ForTaskNode forTaskNode = (ForTaskNode)theEObject;
				T result = caseForTaskNode(forTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(forTaskNode);
				if (result == null) result = caseTaskNode(forTaskNode);
				if (result == null) result = caseBlock(forTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(forTaskNode);
				if (result == null) result = caseAnnotatedElement(forTaskNode);
				if (result == null) result = caseBlocksVisitable(forTaskNode);
				if (result == null) result = caseGecosNode(forTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.WHILE_TASK_NODE: {
				WhileTaskNode whileTaskNode = (WhileTaskNode)theEObject;
				T result = caseWhileTaskNode(whileTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(whileTaskNode);
				if (result == null) result = caseTaskNode(whileTaskNode);
				if (result == null) result = caseBlock(whileTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(whileTaskNode);
				if (result == null) result = caseAnnotatedElement(whileTaskNode);
				if (result == null) result = caseBlocksVisitable(whileTaskNode);
				if (result == null) result = caseGecosNode(whileTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.DO_WHILE_TASK_NODE: {
				DoWhileTaskNode doWhileTaskNode = (DoWhileTaskNode)theEObject;
				T result = caseDoWhileTaskNode(doWhileTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(doWhileTaskNode);
				if (result == null) result = caseTaskNode(doWhileTaskNode);
				if (result == null) result = caseBlock(doWhileTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(doWhileTaskNode);
				if (result == null) result = caseAnnotatedElement(doWhileTaskNode);
				if (result == null) result = caseBlocksVisitable(doWhileTaskNode);
				if (result == null) result = caseGecosNode(doWhileTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.PROCEDURE_TASK_NODE: {
				ProcedureTaskNode procedureTaskNode = (ProcedureTaskNode)theEObject;
				T result = caseProcedureTaskNode(procedureTaskNode);
				if (result == null) result = caseHierarchicalTaskNode(procedureTaskNode);
				if (result == null) result = caseTaskNode(procedureTaskNode);
				if (result == null) result = caseBlock(procedureTaskNode);
				if (result == null) result = caseHTaskGraphVisitable(procedureTaskNode);
				if (result == null) result = caseAnnotatedElement(procedureTaskNode);
				if (result == null) result = caseBlocksVisitable(procedureTaskNode);
				if (result == null) result = caseGecosNode(procedureTaskNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.PROFILING_INFORMATION: {
				ProfilingInformation profilingInformation = (ProfilingInformation)theEObject;
				T result = caseProfilingInformation(profilingInformation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.CORE_LEVEL_WCET: {
				CoreLevelWCET coreLevelWCET = (CoreLevelWCET)theEObject;
				T result = caseCoreLevelWCET(coreLevelWCET);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.WCET_INFORMATION: {
				WCETInformation wcetInformation = (WCETInformation)theEObject;
				T result = caseWCETInformation(wcetInformation);
				if (result == null) result = caseProfilingInformation(wcetInformation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.HTG_COM: {
				HTGCom htgCom = (HTGCom)theEObject;
				T result = caseHTGCom(htgCom);
				if (result == null) result = caseBasicBlock(htgCom);
				if (result == null) result = caseBlock(htgCom);
				if (result == null) result = caseAnnotatedElement(htgCom);
				if (result == null) result = caseBlocksVisitable(htgCom);
				if (result == null) result = caseGecosNode(htgCom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.HTG_SEND: {
				HTGSend htgSend = (HTGSend)theEObject;
				T result = caseHTGSend(htgSend);
				if (result == null) result = caseHTGCom(htgSend);
				if (result == null) result = caseBasicBlock(htgSend);
				if (result == null) result = caseBlock(htgSend);
				if (result == null) result = caseAnnotatedElement(htgSend);
				if (result == null) result = caseBlocksVisitable(htgSend);
				if (result == null) result = caseGecosNode(htgSend);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case HtaskgraphPackage.HTG_RECV: {
				HTGRecv htgRecv = (HTGRecv)theEObject;
				T result = caseHTGRecv(htgRecv);
				if (result == null) result = caseHTGCom(htgRecv);
				if (result == null) result = caseBasicBlock(htgRecv);
				if (result == null) result = caseBlock(htgRecv);
				if (result == null) result = caseAnnotatedElement(htgRecv);
				if (result == null) result = caseBlocksVisitable(htgRecv);
				if (result == null) result = caseGecosNode(htgRecv);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTask Graph Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTask Graph Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTaskGraphVisitable(HTaskGraphVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTask Graph Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTask Graph Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTaskGraphVisitor(HTaskGraphVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskDependency(TaskDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Glue Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Glue Task Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlueTaskDependency(GlueTaskDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Output Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Output Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskOutputDependency(TaskOutputDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Anti Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Anti Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskAntiDependency(TaskAntiDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task True Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task True Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskTrueDependency(TaskTrueDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Control Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Control Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskControlDependency(TaskControlDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskNode(TaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationNode(CommunicationNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafTaskNode(LeafTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Leaf Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Leaf Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicLeafTaskNode(BasicLeafTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hierarchical Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hierarchical Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHierarchicalTaskNode(HierarchicalTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cluster Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cluster Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClusterTaskNode(ClusterTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfTaskNode(IfTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Switch Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Switch Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwitchTaskNode(SwitchTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseTaskNode(CaseTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForTaskNode(ForTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhileTaskNode(WhileTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Do While Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Do While Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDoWhileTaskNode(DoWhileTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Task Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureTaskNode(ProcedureTaskNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Profiling Information</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Profiling Information</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProfilingInformation(ProfilingInformation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Core Level WCET</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Core Level WCET</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoreLevelWCET(CoreLevelWCET object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>WCET Information</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>WCET Information</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWCETInformation(WCETInformation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTG Com</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTG Com</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTGCom(HTGCom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTG Send</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTG Send</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTGSend(HTGSend object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTG Recv</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTG Recv</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTGRecv(HTGRecv object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlocksVisitable(BlocksVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicBlock(BasicBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //HtaskgraphSwitch
