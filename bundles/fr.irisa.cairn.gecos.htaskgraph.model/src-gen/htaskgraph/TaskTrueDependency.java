/**
 */
package htaskgraph;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task True Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskTrueDependency()
 * @model
 * @generated
 */
public interface TaskTrueDependency extends TaskDependency {
} // TaskTrueDependency
