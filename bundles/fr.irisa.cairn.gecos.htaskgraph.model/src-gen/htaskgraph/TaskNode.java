/**
 */
package htaskgraph;

import architecture.MemoryResource;
import architecture.ProcessingResource;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.TaskNode#getDepth <em>Depth</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getSuccs <em>Succs</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getPreds <em>Preds</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getInputNode <em>Input Node</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getOutputNode <em>Output Node</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getProfilingInformation <em>Profiling Information</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getProcessingResources <em>Processing Resources</em>}</li>
 *   <li>{@link htaskgraph.TaskNode#getMemoryResources <em>Memory Resources</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskNode()
 * @model abstract="true"
 * @generated
 */
public interface TaskNode extends Block, HTaskGraphVisitable {
	/**
	 * Returns the value of the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depth</em>' attribute.
	 * @see #setDepth(int)
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_Depth()
	 * @model unique="false"
	 * @generated
	 */
	int getDepth();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskNode#getDepth <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Depth</em>' attribute.
	 * @see #getDepth()
	 * @generated
	 */
	void setDepth(int value);

	/**
	 * Returns the value of the '<em><b>Succs</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.TaskDependency}.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskDependency#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succs</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_Succs()
	 * @see htaskgraph.TaskDependency#getFrom
	 * @model opposite="from"
	 * @generated
	 */
	EList<TaskDependency> getSuccs();

	/**
	 * Returns the value of the '<em><b>Preds</b></em>' containment reference list.
	 * The list contents are of type {@link htaskgraph.TaskDependency}.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskDependency#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preds</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preds</em>' containment reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_Preds()
	 * @see htaskgraph.TaskDependency#getTo
	 * @model opposite="to" containment="true"
	 * @generated
	 */
	EList<TaskDependency> getPreds();

	/**
	 * Returns the value of the '<em><b>Input Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Node</em>' containment reference.
	 * @see #setInputNode(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_InputNode()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getInputNode();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskNode#getInputNode <em>Input Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Node</em>' containment reference.
	 * @see #getInputNode()
	 * @generated
	 */
	void setInputNode(CommunicationNode value);

	/**
	 * Returns the value of the '<em><b>Output Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Node</em>' containment reference.
	 * @see #setOutputNode(CommunicationNode)
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_OutputNode()
	 * @model containment="true"
	 * @generated
	 */
	CommunicationNode getOutputNode();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskNode#getOutputNode <em>Output Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Node</em>' containment reference.
	 * @see #getOutputNode()
	 * @generated
	 */
	void setOutputNode(CommunicationNode value);

	/**
	 * Returns the value of the '<em><b>Profiling Information</b></em>' containment reference list.
	 * The list contents are of type {@link htaskgraph.ProfilingInformation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profiling Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profiling Information</em>' containment reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_ProfilingInformation()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProfilingInformation> getProfilingInformation();

	/**
	 * Returns the value of the '<em><b>Processing Resources</b></em>' reference list.
	 * The list contents are of type {@link architecture.ProcessingResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processing Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processing Resources</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_ProcessingResources()
	 * @model
	 * @generated
	 */
	EList<ProcessingResource> getProcessingResources();

	/**
	 * Returns the value of the '<em><b>Memory Resources</b></em>' reference list.
	 * The list contents are of type {@link architecture.MemoryResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Resources</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskNode_MemoryResources()
	 * @model
	 * @generated
	 */
	EList<MemoryResource> getMemoryResources();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	Block getBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model prUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getProcessingResources().contains(pr);\nboolean _not = (!_contains);\nif (_not)\n{\n\tthis.getProcessingResources().add(pr);\n}'"
	 * @generated
	 */
	void addProcessingResource(ProcessingResource pr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Try to get the immediate parent task of this.
	 * That is the closest {@link HierarchicalTaskNode} in the container hierarchy of this task.
	 * 
	 * @return the parent Task or null if not found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; c = this;\nwhile ((c.eContainer() != null))\n{\n\t{\n\t\tc = c.eContainer();\n\t\tif ((c instanceof &lt;%htaskgraph.HierarchicalTaskNode%&gt;))\n\t\t{\n\t\t\treturn ((&lt;%htaskgraph.HierarchicalTaskNode%&gt;)c);\n\t\t}\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	HierarchicalTaskNode getParentTask();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Try to get the the parent task of this at the specified {@code level}.
	 * 
	 * @return the parent Task at level{@code level} or null if not found.
	 * <!-- end-model-doc -->
	 * @model unique="false" levelUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _depth = this.getDepth();\nboolean _lessThan = (_depth &lt; level);\nif (_lessThan)\n{\n\treturn null;\n}\n&lt;%htaskgraph.TaskNode%&gt; parent = this;\nwhile ((parent.getParentTask() != null))\n{\n\t{\n\t\tparent = parent.getParentTask();\n\t\tint _depth_1 = parent.getDepth();\n\t\tboolean _equals = (_depth_1 == level);\n\t\tif (_equals)\n\t\t{\n\t\t\treturn ((&lt;%htaskgraph.HierarchicalTaskNode%&gt;) parent);\n\t\t}\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	HierarchicalTaskNode getParentTask(int level);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Return a unmodifiable list of the external input {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the external inputs. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isExternal());\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getPreds(), _function), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getExternalInputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Return a unmodifiable list of the external output {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the external outputs. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isExternal());\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getSuccs(), _function), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getExternalOutputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Return a unmodifiable list of the internal input {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the internal inputs. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\tboolean _isExternal = it.isExternal();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((!_isExternal));\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getPreds(), _function), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getInternalInputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Return a unmodifiable list of the internal output {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the internal outputs. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\tboolean _isExternal = it.isExternal();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((!_isExternal));\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getSuccs(), _function), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getInternalOutputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return true if this task is scheduled on the specified ProcessingResource.
	 * <!-- end-model-doc -->
	 * @model unique="false" prUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getProcessingResources().contains(pr);'"
	 * @generated
	 */
	boolean isScheduledOn(ProcessingResource pr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((visitor instanceof &lt;%htaskgraph.HTaskGraphVisitor%&gt;))\n{\n\tthis.accept(((&lt;%htaskgraph.HTaskGraphVisitor%&gt;) visitor));\n}\nelse\n{\n\tfinal &lt;%htaskgraph.HTaskGraphVisitor%&gt; adapter = &lt;%fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorsRegistry%&gt;.INSTANCE.&lt;&lt;%htaskgraph.HTaskGraphVisitor%&gt;, &lt;%gecos.blocks.BlocksVisitor%&gt;&gt;getBlocksAdapter(visitor, &lt;%htaskgraph.HTaskGraphVisitor%&gt;.class);\n\tif ((adapter == null))\n\t{\n\t\tsuper.accept(visitor);\n\t}\n\telse\n\t{\n\t\tthis.accept(adapter);\n\t}\n}'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" toUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getBlock().connectTo(to, cond);'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fromUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getBlock().connectFromBasic(from, cond);'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock from, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"Task\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

} // TaskNode
