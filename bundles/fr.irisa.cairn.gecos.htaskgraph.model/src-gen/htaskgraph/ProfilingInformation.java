/**
 */
package htaskgraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Profiling Information</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getProfilingInformation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ProfilingInformation extends EObject {
} // ProfilingInformation
