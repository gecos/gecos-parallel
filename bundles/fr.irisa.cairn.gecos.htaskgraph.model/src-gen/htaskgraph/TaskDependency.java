/**
 */
package htaskgraph;

import gecos.annotations.AnnotatedElement;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.TaskDependency#getUse <em>Use</em>}</li>
 *   <li>{@link htaskgraph.TaskDependency#getFrom <em>From</em>}</li>
 *   <li>{@link htaskgraph.TaskDependency#getTo <em>To</em>}</li>
 *   <li>{@link htaskgraph.TaskDependency#getSrcDependencies <em>Src Dependencies</em>}</li>
 *   <li>{@link htaskgraph.TaskDependency#getDstDependencies <em>Dst Dependencies</em>}</li>
 *   <li>{@link htaskgraph.TaskDependency#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskDependency()
 * @model
 * @generated
 */
public interface TaskDependency extends AnnotatedElement, HTaskGraphVisitable {
	/**
	 * Returns the value of the '<em><b>Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use</em>' reference.
	 * @see #setUse(Symbol)
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_Use()
	 * @model
	 * @generated
	 */
	Symbol getUse();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskDependency#getUse <em>Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use</em>' reference.
	 * @see #getUse()
	 * @generated
	 */
	void setUse(Symbol value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskNode#getSuccs <em>Succs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(TaskNode)
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_From()
	 * @see htaskgraph.TaskNode#getSuccs
	 * @model opposite="succs"
	 * @generated
	 */
	TaskNode getFrom();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskDependency#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(TaskNode value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskNode#getPreds <em>Preds</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' container reference.
	 * @see #setTo(TaskNode)
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_To()
	 * @see htaskgraph.TaskNode#getPreds
	 * @model opposite="preds" transient="false"
	 * @generated
	 */
	TaskNode getTo();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskDependency#getTo <em>To</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' container reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(TaskNode value);

	/**
	 * Returns the value of the '<em><b>Src Dependencies</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.TaskDependency}.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskDependency#getDstDependencies <em>Dst Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Dependencies</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Dependencies</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_SrcDependencies()
	 * @see htaskgraph.TaskDependency#getDstDependencies
	 * @model opposite="dstDependencies"
	 * @generated
	 */
	EList<TaskDependency> getSrcDependencies();

	/**
	 * Returns the value of the '<em><b>Dst Dependencies</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.TaskDependency}.
	 * It is bidirectional and its opposite is '{@link htaskgraph.TaskDependency#getSrcDependencies <em>Src Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dst Dependencies</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dst Dependencies</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_DstDependencies()
	 * @see htaskgraph.TaskDependency#getSrcDependencies
	 * @model opposite="srcDependencies"
	 * @generated
	 */
	EList<TaskDependency> getDstDependencies();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see htaskgraph.HtaskgraphPackage#getTaskDependency_Id()
	 * @model unique="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link htaskgraph.TaskDependency#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * An External dependency is one that comes from or goes to a {@link CommunicationNode}.
	 * <br>
	 * An Internal dependency is thus one that connects two regular tasks (at the same level).
	 * <br>
	 * Internal means that the dependency originated from inside the parent level, whereas
	 * External indicate that it comes from outside the parent level and is conducted through
	 * via the parent's {@link CommunicationNode}.
	 * 
	 * @return true if this dependency is External, false if Internal.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xblockexpression = false;\n{\n\tif (((this.getFrom() == null) || (this.getTo() == null)))\n\t{\n\t\tthrow new &lt;%java.lang.RuntimeException%&gt;((\"From/To tasks are not valid! \" + this));\n\t}\n\t_xblockexpression = ((this.getFrom() instanceof &lt;%htaskgraph.CommunicationNode%&gt;) || (this.getTo() instanceof &lt;%htaskgraph.CommunicationNode%&gt;));\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	boolean isExternal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Find the source node that originated this dependency by backtracking the
	 * srcDependencies chain.
	 * The original source node is the {@code from} of the last dependency in the chain
	 * (i.e. one with empty srcDependencies).
	 * 
	 * @return a unmodifiable unique list of the original source nodes of this dependency.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; srcDeps = this.getSrcDependencies();\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; _xifexpression = null;\n\tboolean _isEmpty = srcDeps.isEmpty();\n\tif (_isEmpty)\n\t{\n\t\t_xifexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;singletonEList(this.getFrom());\n\t}\n\telse\n\t{\n\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt;()\n\t\t{\n\t\t\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t\t\t{\n\t\t\t\treturn it.getOriginalSourceNodes();\n\t\t\t}\n\t\t};\n\t\t_xifexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(((&lt;%htaskgraph.TaskNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;toSet(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt;map(srcDeps, _function))), &lt;%htaskgraph.TaskNode%&gt;.class))));\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<TaskNode> getOriginalSourceNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Find the final destination node of this dependency by following the
	 * dstDependencies chain.
	 * The final destination node is the {@code to} of the last dependency in the chain
	 * (i.e. one with empty dstDependencies).
	 * 
	 * @return a unmodifiable unique list of the original source nodes of this dependency.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; dstDeps = this.getDstDependencies();\n\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; _xifexpression = null;\n\tboolean _isEmpty = dstDeps.isEmpty();\n\tif (_isEmpty)\n\t{\n\t\t_xifexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;singletonEList(this.getTo());\n\t}\n\telse\n\t{\n\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt;()\n\t\t{\n\t\t\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t\t\t{\n\t\t\t\treturn it.getFinalDestinationNodes();\n\t\t\t}\n\t\t};\n\t\t_xifexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(((&lt;%htaskgraph.TaskNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;toSet(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;&gt;map(dstDeps, _function))), &lt;%htaskgraph.TaskNode%&gt;.class))));\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<TaskNode> getFinalDestinationNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return a unmodifiable unique list. Never null
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isExternal = this.isExternal();\nboolean _not = (!_isExternal);\nif (_not)\n{\n\treturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;singletonEList(this);\n}\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; srcDeps = this.getSrcDependencies();\nboolean _isEmpty = srcDeps.isEmpty();\nif (_isEmpty)\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"unhandeled case\");\n}\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getFirstInternalSrcDependencies();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;toSet(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt;map(srcDeps, _function))), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getFirstInternalSrcDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return a unmodifiable unique list. Never null
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isExternal = this.isExternal();\nboolean _not = (!_isExternal);\nif (_not)\n{\n\treturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;singletonEList(this);\n}\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; dstDeps = this.getDstDependencies();\nboolean _isEmpty = dstDeps.isEmpty();\nif (_isEmpty)\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"unhandeled case\");\n}\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getFirstInternalDstDependencies();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;toSet(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;&gt;map(dstDeps, _function))), &lt;%htaskgraph.TaskDependency%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskDependency> getFirstInternalDstDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;()\n{\n\tpublic &lt;%htaskgraph.TaskNode%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getFrom();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;map(this.getFirstInternalSrcDependencies(), _function));'"
	 * @generated
	 */
	EList<TaskNode> getFirstInternalSrcNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;()\n{\n\tpublic &lt;%htaskgraph.TaskNode%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getTo();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;map(this.getFirstInternalDstDependencies(), _function));'"
	 * @generated
	 */
	EList<TaskNode> getFirstInternalDstNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; srcDeps = this.getSrcDependencies();\nboolean _isEmpty = srcDeps.isEmpty();\nboolean _not = (!_isEmpty);\nif (_not)\n{\n\treturn srcDeps;\n}\n&lt;%gecos.core.Symbol%&gt; _use = this.getUse();\nboolean _tripleNotEquals = (_use != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t\t{\n\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(((it.getUse() != null) &amp;&amp; (it.getUse() == &lt;%this%&gt;.getUse())));\n\t\t}\n\t};\n\treturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;toSet(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getFrom().getOutputNode().getPreds(), _function)), &lt;%htaskgraph.TaskDependency%&gt;.class)));\n}\nreturn new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;();'"
	 * @generated
	 */
	EList<TaskDependency> getEffSrcDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt; dstDeps = this.getDstDependencies();\nboolean _isEmpty = dstDeps.isEmpty();\nboolean _not = (!_isEmpty);\nif (_not)\n{\n\treturn dstDeps;\n}\n&lt;%gecos.core.Symbol%&gt; _use = this.getUse();\nboolean _tripleNotEquals = (_use != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t\t{\n\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(((it.getUse() != null) &amp;&amp; (it.getUse() == &lt;%this%&gt;.getUse())));\n\t\t}\n\t};\n\treturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;asEList(((&lt;%htaskgraph.TaskDependency%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;toSet(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;filter(this.getTo().getInputNode().getSuccs(), _function)), &lt;%htaskgraph.TaskDependency%&gt;.class)));\n}\nreturn new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;&gt;();'"
	 * @generated
	 */
	EList<TaskDependency> getEffDstDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;()\n{\n\tpublic &lt;%htaskgraph.TaskNode%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getFrom();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;map(this.getEffSrcDependencies(), _function));'"
	 * @generated
	 */
	EList<TaskNode> getEffPreds();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;()\n{\n\tpublic &lt;%htaskgraph.TaskNode%&gt; apply(final &lt;%htaskgraph.TaskDependency%&gt; it)\n\t{\n\t\treturn it.getTo();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%htaskgraph.TaskDependency%&gt;, &lt;%htaskgraph.TaskNode%&gt;&gt;map(this.getEffDstDependencies(), _function));'"
	 * @generated
	 */
	EList<TaskNode> getEffSuccs();

} // TaskDependency
