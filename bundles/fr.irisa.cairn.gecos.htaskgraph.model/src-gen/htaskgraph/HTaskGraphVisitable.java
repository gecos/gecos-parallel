/**
 */
package htaskgraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HTask Graph Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getHTaskGraphVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface HTaskGraphVisitable extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

} // HTaskGraphVisitable
