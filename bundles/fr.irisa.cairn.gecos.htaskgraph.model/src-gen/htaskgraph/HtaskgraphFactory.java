/**
 */
package htaskgraph;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see htaskgraph.HtaskgraphPackage
 * @generated
 */
public interface HtaskgraphFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HtaskgraphFactory eINSTANCE = htaskgraph.impl.HtaskgraphFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Dependency</em>'.
	 * @generated
	 */
	TaskDependency createTaskDependency();

	/**
	 * Returns a new object of class '<em>Glue Task Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Glue Task Dependency</em>'.
	 * @generated
	 */
	GlueTaskDependency createGlueTaskDependency();

	/**
	 * Returns a new object of class '<em>Task Output Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Output Dependency</em>'.
	 * @generated
	 */
	TaskOutputDependency createTaskOutputDependency();

	/**
	 * Returns a new object of class '<em>Task Anti Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Anti Dependency</em>'.
	 * @generated
	 */
	TaskAntiDependency createTaskAntiDependency();

	/**
	 * Returns a new object of class '<em>Task True Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task True Dependency</em>'.
	 * @generated
	 */
	TaskTrueDependency createTaskTrueDependency();

	/**
	 * Returns a new object of class '<em>Task Control Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Control Dependency</em>'.
	 * @generated
	 */
	TaskControlDependency createTaskControlDependency();

	/**
	 * Returns a new object of class '<em>Communication Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Node</em>'.
	 * @generated
	 */
	CommunicationNode createCommunicationNode();

	/**
	 * Returns a new object of class '<em>Leaf Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Leaf Task Node</em>'.
	 * @generated
	 */
	LeafTaskNode createLeafTaskNode();

	/**
	 * Returns a new object of class '<em>Basic Leaf Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Leaf Task Node</em>'.
	 * @generated
	 */
	BasicLeafTaskNode createBasicLeafTaskNode();

	/**
	 * Returns a new object of class '<em>Cluster Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cluster Task Node</em>'.
	 * @generated
	 */
	ClusterTaskNode createClusterTaskNode();

	/**
	 * Returns a new object of class '<em>If Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Task Node</em>'.
	 * @generated
	 */
	IfTaskNode createIfTaskNode();

	/**
	 * Returns a new object of class '<em>Switch Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Task Node</em>'.
	 * @generated
	 */
	SwitchTaskNode createSwitchTaskNode();

	/**
	 * Returns a new object of class '<em>Case Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Task Node</em>'.
	 * @generated
	 */
	CaseTaskNode createCaseTaskNode();

	/**
	 * Returns a new object of class '<em>For Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Task Node</em>'.
	 * @generated
	 */
	ForTaskNode createForTaskNode();

	/**
	 * Returns a new object of class '<em>While Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Task Node</em>'.
	 * @generated
	 */
	WhileTaskNode createWhileTaskNode();

	/**
	 * Returns a new object of class '<em>Do While Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Do While Task Node</em>'.
	 * @generated
	 */
	DoWhileTaskNode createDoWhileTaskNode();

	/**
	 * Returns a new object of class '<em>Procedure Task Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Task Node</em>'.
	 * @generated
	 */
	ProcedureTaskNode createProcedureTaskNode();

	/**
	 * Returns a new object of class '<em>Core Level WCET</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Core Level WCET</em>'.
	 * @generated
	 */
	CoreLevelWCET createCoreLevelWCET();

	/**
	 * Returns a new object of class '<em>WCET Information</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>WCET Information</em>'.
	 * @generated
	 */
	WCETInformation createWCETInformation();

	/**
	 * Returns a new object of class '<em>HTG Send</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HTG Send</em>'.
	 * @generated
	 */
	HTGSend createHTGSend();

	/**
	 * Returns a new object of class '<em>HTG Recv</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HTG Recv</em>'.
	 * @generated
	 */
	HTGRecv createHTGRecv();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HtaskgraphPackage getHtaskgraphPackage();

} //HtaskgraphFactory
