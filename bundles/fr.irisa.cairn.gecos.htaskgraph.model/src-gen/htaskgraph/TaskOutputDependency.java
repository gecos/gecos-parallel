/**
 */
package htaskgraph;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Output Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskOutputDependency()
 * @model
 * @generated
 */
public interface TaskOutputDependency extends TaskDependency {
} // TaskOutputDependency
