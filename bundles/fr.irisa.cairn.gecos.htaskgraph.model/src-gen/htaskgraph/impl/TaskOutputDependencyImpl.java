/**
 */
package htaskgraph.impl;

import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskOutputDependency;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Output Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TaskOutputDependencyImpl extends TaskDependencyImpl implements TaskOutputDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskOutputDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_OUTPUT_DEPENDENCY;
	}

} //TaskOutputDependencyImpl
