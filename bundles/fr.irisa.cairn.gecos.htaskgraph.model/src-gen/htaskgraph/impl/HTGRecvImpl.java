/**
 */
package htaskgraph.impl;

import architecture.ProcessingResource;

import com.google.common.collect.Iterables;

import gecos.core.Symbol;

import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HtaskgraphPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HTG Recv</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.HTGRecvImpl#getToProcResources <em>To Proc Resources</em>}</li>
 *   <li>{@link htaskgraph.impl.HTGRecvImpl#getSends <em>Sends</em>}</li>
 *   <li>{@link htaskgraph.impl.HTGRecvImpl#getUse <em>Use</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HTGRecvImpl extends HTGComImpl implements HTGRecv {
	/**
	 * The cached value of the '{@link #getToProcResources() <em>To Proc Resources</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToProcResources()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessingResource> toProcResources;

	/**
	 * The cached value of the '{@link #getSends() <em>Sends</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSends()
	 * @generated
	 * @ordered
	 */
	protected EList<HTGSend> sends;

	/**
	 * The cached value of the '{@link #getUse() <em>Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUse()
	 * @generated
	 * @ordered
	 */
	protected Symbol use;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HTGRecvImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.HTG_RECV;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getToProcResources() {
		if (toProcResources == null) {
			toProcResources = new EObjectResolvingEList<ProcessingResource>(ProcessingResource.class, this, HtaskgraphPackage.HTG_RECV__TO_PROC_RESOURCES);
		}
		return toProcResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HTGSend> getSends() {
		if (sends == null) {
			sends = new EObjectWithInverseResolvingEList.ManyInverse<HTGSend>(HTGSend.class, this, HtaskgraphPackage.HTG_RECV__SENDS, HtaskgraphPackage.HTG_SEND__RECEIVES);
		}
		return sends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUse() {
		if (use != null && use.eIsProxy()) {
			InternalEObject oldUse = (InternalEObject)use;
			use = (Symbol)eResolveProxy(oldUse);
			if (use != oldUse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HtaskgraphPackage.HTG_RECV__USE, oldUse, use));
			}
		}
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetUse() {
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUse(Symbol newUse) {
		Symbol oldUse = use;
		use = newUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.HTG_RECV__USE, oldUse, use));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getFromProcResources() {
		final Function1<HTGSend, EList<ProcessingResource>> _function = new Function1<HTGSend, EList<ProcessingResource>>() {
			public EList<ProcessingResource> apply(final HTGSend it) {
				return it.getFromProcResources();
			}
		};
		return ECollections.<ProcessingResource>asEList(((ProcessingResource[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<ProcessingResource>concat(XcoreEListExtensions.<HTGSend, EList<ProcessingResource>>map(this.getSends(), _function)), ProcessingResource.class)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Symbol _use = this.getUse();
		String _plus = ("HTGRecv " + _use);
		String _plus_1 = (_plus + " ");
		EList<ProcessingResource> _fromProcResources = this.getFromProcResources();
		String _plus_2 = (_plus_1 + _fromProcResources);
		String _plus_3 = (_plus_2 + " -> ");
		EList<ProcessingResource> _toProcResources = this.getToProcResources();
		return (_plus_3 + _toProcResources);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__SENDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSends()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__SENDS:
				return ((InternalEList<?>)getSends()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__TO_PROC_RESOURCES:
				return getToProcResources();
			case HtaskgraphPackage.HTG_RECV__SENDS:
				return getSends();
			case HtaskgraphPackage.HTG_RECV__USE:
				if (resolve) return getUse();
				return basicGetUse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__TO_PROC_RESOURCES:
				getToProcResources().clear();
				getToProcResources().addAll((Collection<? extends ProcessingResource>)newValue);
				return;
			case HtaskgraphPackage.HTG_RECV__SENDS:
				getSends().clear();
				getSends().addAll((Collection<? extends HTGSend>)newValue);
				return;
			case HtaskgraphPackage.HTG_RECV__USE:
				setUse((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__TO_PROC_RESOURCES:
				getToProcResources().clear();
				return;
			case HtaskgraphPackage.HTG_RECV__SENDS:
				getSends().clear();
				return;
			case HtaskgraphPackage.HTG_RECV__USE:
				setUse((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_RECV__TO_PROC_RESOURCES:
				return toProcResources != null && !toProcResources.isEmpty();
			case HtaskgraphPackage.HTG_RECV__SENDS:
				return sends != null && !sends.isEmpty();
			case HtaskgraphPackage.HTG_RECV__USE:
				return use != null;
		}
		return super.eIsSet(featureID);
	}

} //HTGRecvImpl
