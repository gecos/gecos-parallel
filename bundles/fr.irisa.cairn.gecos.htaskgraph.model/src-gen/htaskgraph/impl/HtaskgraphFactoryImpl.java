/**
 */
package htaskgraph.impl;

import htaskgraph.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HtaskgraphFactoryImpl extends EFactoryImpl implements HtaskgraphFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HtaskgraphFactory init() {
		try {
			HtaskgraphFactory theHtaskgraphFactory = (HtaskgraphFactory)EPackage.Registry.INSTANCE.getEFactory(HtaskgraphPackage.eNS_URI);
			if (theHtaskgraphFactory != null) {
				return theHtaskgraphFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HtaskgraphFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HtaskgraphPackage.TASK_DEPENDENCY: return createTaskDependency();
			case HtaskgraphPackage.GLUE_TASK_DEPENDENCY: return createGlueTaskDependency();
			case HtaskgraphPackage.TASK_OUTPUT_DEPENDENCY: return createTaskOutputDependency();
			case HtaskgraphPackage.TASK_ANTI_DEPENDENCY: return createTaskAntiDependency();
			case HtaskgraphPackage.TASK_TRUE_DEPENDENCY: return createTaskTrueDependency();
			case HtaskgraphPackage.TASK_CONTROL_DEPENDENCY: return createTaskControlDependency();
			case HtaskgraphPackage.COMMUNICATION_NODE: return createCommunicationNode();
			case HtaskgraphPackage.LEAF_TASK_NODE: return createLeafTaskNode();
			case HtaskgraphPackage.BASIC_LEAF_TASK_NODE: return createBasicLeafTaskNode();
			case HtaskgraphPackage.CLUSTER_TASK_NODE: return createClusterTaskNode();
			case HtaskgraphPackage.IF_TASK_NODE: return createIfTaskNode();
			case HtaskgraphPackage.SWITCH_TASK_NODE: return createSwitchTaskNode();
			case HtaskgraphPackage.CASE_TASK_NODE: return createCaseTaskNode();
			case HtaskgraphPackage.FOR_TASK_NODE: return createForTaskNode();
			case HtaskgraphPackage.WHILE_TASK_NODE: return createWhileTaskNode();
			case HtaskgraphPackage.DO_WHILE_TASK_NODE: return createDoWhileTaskNode();
			case HtaskgraphPackage.PROCEDURE_TASK_NODE: return createProcedureTaskNode();
			case HtaskgraphPackage.CORE_LEVEL_WCET: return createCoreLevelWCET();
			case HtaskgraphPackage.WCET_INFORMATION: return createWCETInformation();
			case HtaskgraphPackage.HTG_SEND: return createHTGSend();
			case HtaskgraphPackage.HTG_RECV: return createHTGRecv();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case HtaskgraphPackage.COMMUNICATION_NODE_TYPE:
				return createCommunicationNodeTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case HtaskgraphPackage.COMMUNICATION_NODE_TYPE:
				return convertCommunicationNodeTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDependency createTaskDependency() {
		TaskDependencyImpl taskDependency = new TaskDependencyImpl();
		return taskDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GlueTaskDependency createGlueTaskDependency() {
		GlueTaskDependencyImpl glueTaskDependency = new GlueTaskDependencyImpl();
		return glueTaskDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskOutputDependency createTaskOutputDependency() {
		TaskOutputDependencyImpl taskOutputDependency = new TaskOutputDependencyImpl();
		return taskOutputDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskAntiDependency createTaskAntiDependency() {
		TaskAntiDependencyImpl taskAntiDependency = new TaskAntiDependencyImpl();
		return taskAntiDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskTrueDependency createTaskTrueDependency() {
		TaskTrueDependencyImpl taskTrueDependency = new TaskTrueDependencyImpl();
		return taskTrueDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskControlDependency createTaskControlDependency() {
		TaskControlDependencyImpl taskControlDependency = new TaskControlDependencyImpl();
		return taskControlDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNode createCommunicationNode() {
		CommunicationNodeImpl communicationNode = new CommunicationNodeImpl();
		return communicationNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafTaskNode createLeafTaskNode() {
		LeafTaskNodeImpl leafTaskNode = new LeafTaskNodeImpl();
		return leafTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicLeafTaskNode createBasicLeafTaskNode() {
		BasicLeafTaskNodeImpl basicLeafTaskNode = new BasicLeafTaskNodeImpl();
		return basicLeafTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClusterTaskNode createClusterTaskNode() {
		ClusterTaskNodeImpl clusterTaskNode = new ClusterTaskNodeImpl();
		return clusterTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfTaskNode createIfTaskNode() {
		IfTaskNodeImpl ifTaskNode = new IfTaskNodeImpl();
		return ifTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchTaskNode createSwitchTaskNode() {
		SwitchTaskNodeImpl switchTaskNode = new SwitchTaskNodeImpl();
		return switchTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseTaskNode createCaseTaskNode() {
		CaseTaskNodeImpl caseTaskNode = new CaseTaskNodeImpl();
		return caseTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForTaskNode createForTaskNode() {
		ForTaskNodeImpl forTaskNode = new ForTaskNodeImpl();
		return forTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileTaskNode createWhileTaskNode() {
		WhileTaskNodeImpl whileTaskNode = new WhileTaskNodeImpl();
		return whileTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoWhileTaskNode createDoWhileTaskNode() {
		DoWhileTaskNodeImpl doWhileTaskNode = new DoWhileTaskNodeImpl();
		return doWhileTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureTaskNode createProcedureTaskNode() {
		ProcedureTaskNodeImpl procedureTaskNode = new ProcedureTaskNodeImpl();
		return procedureTaskNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreLevelWCET createCoreLevelWCET() {
		CoreLevelWCETImpl coreLevelWCET = new CoreLevelWCETImpl();
		return coreLevelWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WCETInformation createWCETInformation() {
		WCETInformationImpl wcetInformation = new WCETInformationImpl();
		return wcetInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTGSend createHTGSend() {
		HTGSendImpl htgSend = new HTGSendImpl();
		return htgSend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTGRecv createHTGRecv() {
		HTGRecvImpl htgRecv = new HTGRecvImpl();
		return htgRecv;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNodeType createCommunicationNodeTypeFromString(EDataType eDataType, String initialValue) {
		CommunicationNodeType result = CommunicationNodeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCommunicationNodeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphPackage getHtaskgraphPackage() {
		return (HtaskgraphPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HtaskgraphPackage getPackage() {
		return HtaskgraphPackage.eINSTANCE;
	}

} //HtaskgraphFactoryImpl
