/**
 */
package htaskgraph.impl;

import htaskgraph.GlueTaskDependency;
import htaskgraph.HtaskgraphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Glue Task Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GlueTaskDependencyImpl extends TaskDependencyImpl implements GlueTaskDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GlueTaskDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.GLUE_TASK_DEPENDENCY;
	}

} //GlueTaskDependencyImpl
