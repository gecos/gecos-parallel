/**
 */
package htaskgraph.impl;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;

import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.GlueTaskDependency;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Task Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.ForTaskNodeImpl#getBlock <em>Block</em>}</li>
 *   <li>{@link htaskgraph.impl.ForTaskNodeImpl#getLoopIn <em>Loop In</em>}</li>
 *   <li>{@link htaskgraph.impl.ForTaskNodeImpl#getLoopOut <em>Loop Out</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForTaskNodeImpl extends HierarchicalTaskNodeImpl implements ForTaskNode {
	/**
	 * The cached value of the '{@link #getBlock() <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected ForBlock block;

	/**
	 * The cached value of the '{@link #getLoopIn() <em>Loop In</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopIn()
	 * @generated
	 * @ordered
	 */
	protected CommunicationNode loopIn;

	/**
	 * The cached value of the '{@link #getLoopOut() <em>Loop Out</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopOut()
	 * @generated
	 * @ordered
	 */
	protected CommunicationNode loopOut;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForTaskNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.FOR_TASK_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForBlock getBlock() {
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlock(ForBlock newBlock, NotificationChain msgs) {
		ForBlock oldBlock = block;
		block = newBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__BLOCK, oldBlock, newBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlock(ForBlock newBlock) {
		if (newBlock != block) {
			NotificationChain msgs = null;
			if (block != null)
				msgs = ((InternalEObject)block).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__BLOCK, null, msgs);
			if (newBlock != null)
				msgs = ((InternalEObject)newBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__BLOCK, null, msgs);
			msgs = basicSetBlock(newBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__BLOCK, newBlock, newBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNode getLoopIn() {
		return loopIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopIn(CommunicationNode newLoopIn, NotificationChain msgs) {
		CommunicationNode oldLoopIn = loopIn;
		loopIn = newLoopIn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN, oldLoopIn, newLoopIn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopIn(CommunicationNode newLoopIn) {
		if (newLoopIn != loopIn) {
			NotificationChain msgs = null;
			if (loopIn != null)
				msgs = ((InternalEObject)loopIn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN, null, msgs);
			if (newLoopIn != null)
				msgs = ((InternalEObject)newLoopIn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN, null, msgs);
			msgs = basicSetLoopIn(newLoopIn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN, newLoopIn, newLoopIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNode getLoopOut() {
		return loopOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopOut(CommunicationNode newLoopOut, NotificationChain msgs) {
		CommunicationNode oldLoopOut = loopOut;
		loopOut = newLoopOut;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT, oldLoopOut, newLoopOut);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopOut(CommunicationNode newLoopOut) {
		if (newLoopOut != loopOut) {
			NotificationChain msgs = null;
			if (loopOut != null)
				msgs = ((InternalEObject)loopOut).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT, null, msgs);
			if (newLoopOut != null)
				msgs = ((InternalEObject)newLoopOut).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT, null, msgs);
			msgs = basicSetLoopOut(newLoopOut, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT, newLoopOut, newLoopOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicLeafTaskNode getStep() {
		BasicBlock _stepBlock = this.getBlock().getStepBlock();
		return ((BasicLeafTaskNode) _stepBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicLeafTaskNode getInit() {
		EList<TaskDependency> _internalInputs = this.getInternalInputs();
		for (final TaskDependency td : _internalInputs) {
			if ((td instanceof GlueTaskDependency)) {
				TaskNode _from = ((GlueTaskDependency)td).getFrom();
				return ((BasicLeafTaskNode) _from);
			}
		}
		BasicBlock _initBlock = this.getBlock().getInitBlock();
		return ((BasicLeafTaskNode) _initBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicLeafTaskNode getTest() {
		BasicBlock _testBlock = this.getBlock().getTestBlock();
		return ((BasicLeafTaskNode) _testBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNode getBody() {
		Block _bodyBlock = this.getBlock().getBodyBlock();
		return ((TaskNode) _bodyBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final HTaskGraphVisitor visitor) {
		visitor.visitForTaskNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _number = this.getNumber();
		String _plus = ("ForTN" + Integer.valueOf(_number));
		String _plus_1 = (_plus + "(L:");
		int _depth = this.getDepth();
		String _plus_2 = (_plus_1 + Integer.valueOf(_depth));
		return (_plus_2 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.FOR_TASK_NODE__BLOCK:
				return basicSetBlock(null, msgs);
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN:
				return basicSetLoopIn(null, msgs);
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT:
				return basicSetLoopOut(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.FOR_TASK_NODE__BLOCK:
				return getBlock();
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN:
				return getLoopIn();
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT:
				return getLoopOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.FOR_TASK_NODE__BLOCK:
				setBlock((ForBlock)newValue);
				return;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN:
				setLoopIn((CommunicationNode)newValue);
				return;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT:
				setLoopOut((CommunicationNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.FOR_TASK_NODE__BLOCK:
				setBlock((ForBlock)null);
				return;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN:
				setLoopIn((CommunicationNode)null);
				return;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT:
				setLoopOut((CommunicationNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.FOR_TASK_NODE__BLOCK:
				return block != null;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_IN:
				return loopIn != null;
			case HtaskgraphPackage.FOR_TASK_NODE__LOOP_OUT:
				return loopOut != null;
		}
		return super.eIsSet(featureID);
	}

} //ForTaskNodeImpl
