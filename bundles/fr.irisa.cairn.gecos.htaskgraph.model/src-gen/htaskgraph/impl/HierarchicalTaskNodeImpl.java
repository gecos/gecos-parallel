/**
 */
package htaskgraph.impl;

import com.google.common.base.Objects;

import gecos.blocks.CompositeBlock;

import htaskgraph.HierarchicalTaskNode;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskNode;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hierarchical Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class HierarchicalTaskNodeImpl extends TaskNodeImpl implements HierarchicalTaskNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HierarchicalTaskNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.HIERARCHICAL_TASK_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getChildTasks() {
		final BasicEList<TaskNode> res = new BasicEList<TaskNode>();
		final TreeIterator<EObject> iter = this.getBlock().eAllContents();
		while (iter.hasNext()) {
			{
				EObject elt = iter.next();
				if ((elt instanceof TaskNode)) {
					res.add(((TaskNode)elt));
				}
				if ((!(elt instanceof CompositeBlock))) {
					iter.prune();
				}
			}
		}
		final TreeIterator<EObject> iter2 = this.eAllContents();
		while (iter2.hasNext()) {
			{
				EObject elt = iter2.next();
				if (((elt instanceof TaskNode) && (!res.contains(elt)))) {
					res.add(((TaskNode) elt));
				}
				if ((!(elt instanceof CompositeBlock))) {
					iter2.prune();
				}
			}
		}
		return ECollections.<TaskNode>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getAllLevelsChildTasks() {
		BasicEList<TaskNode> res = new BasicEList<TaskNode>();
		EList<TaskNode> _childTasks = this.getChildTasks();
		for (final TaskNode t : _childTasks) {
			if ((t instanceof HierarchicalTaskNode)) {
				res.addAll(((HierarchicalTaskNode)t).getAllLevelsChildTasks());
			}
			else {
				res.add(t);
			}
		}
		return ECollections.<TaskNode>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getSourceChildTasks() {
		final Function1<TaskNode, Boolean> _function = new Function1<TaskNode, Boolean>() {
			public Boolean apply(final TaskNode it) {
				return Boolean.valueOf((it.getPreds().isEmpty() || ((it.getPreds().size() == 1) && Objects.equal(it.getPreds().get(0), it.getInputNode()))));
			}
		};
		return ECollections.<TaskNode>unmodifiableEList(ECollections.<TaskNode>asEList(((TaskNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskNode>filter(this.getChildTasks(), _function), TaskNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getSinkChildTasks() {
		final Function1<TaskNode, Boolean> _function = new Function1<TaskNode, Boolean>() {
			public Boolean apply(final TaskNode it) {
				return Boolean.valueOf((it.getSuccs().isEmpty() || ((it.getSuccs().size() == 1) && Objects.equal(it.getSuccs().get(0), it.getOutputNode()))));
			}
		};
		return ECollections.<TaskNode>unmodifiableEList(ECollections.<TaskNode>asEList(((TaskNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskNode>filter(this.getChildTasks(), _function), TaskNode.class))));
	}

} //HierarchicalTaskNodeImpl
