/**
 */
package htaskgraph.impl;

import architecture.MemoryResource;
import architecture.ProcessingResource;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorsRegistry;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;

import gecos.blocks.impl.BlockImpl;

import gecos.instrs.BranchType;

import htaskgraph.CommunicationNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.ProfilingInformation;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getDepth <em>Depth</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getSuccs <em>Succs</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getPreds <em>Preds</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getInputNode <em>Input Node</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getOutputNode <em>Output Node</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getProfilingInformation <em>Profiling Information</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getProcessingResources <em>Processing Resources</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskNodeImpl#getMemoryResources <em>Memory Resources</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TaskNodeImpl extends BlockImpl implements TaskNode {
	/**
	 * The default value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected int depth = DEPTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuccs() <em>Succs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccs()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDependency> succs;

	/**
	 * The cached value of the '{@link #getPreds() <em>Preds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreds()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDependency> preds;

	/**
	 * The cached value of the '{@link #getInputNode() <em>Input Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputNode()
	 * @generated
	 * @ordered
	 */
	protected CommunicationNode inputNode;

	/**
	 * The cached value of the '{@link #getOutputNode() <em>Output Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputNode()
	 * @generated
	 * @ordered
	 */
	protected CommunicationNode outputNode;

	/**
	 * The cached value of the '{@link #getProfilingInformation() <em>Profiling Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfilingInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<ProfilingInformation> profilingInformation;

	/**
	 * The cached value of the '{@link #getProcessingResources() <em>Processing Resources</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessingResources()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessingResource> processingResources;

	/**
	 * The cached value of the '{@link #getMemoryResources() <em>Memory Resources</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryResources()
	 * @generated
	 * @ordered
	 */
	protected EList<MemoryResource> memoryResources;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepth(int newDepth) {
		int oldDepth = depth;
		depth = newDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_NODE__DEPTH, oldDepth, depth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getSuccs() {
		if (succs == null) {
			succs = new EObjectWithInverseResolvingEList<TaskDependency>(TaskDependency.class, this, HtaskgraphPackage.TASK_NODE__SUCCS, HtaskgraphPackage.TASK_DEPENDENCY__FROM);
		}
		return succs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getPreds() {
		if (preds == null) {
			preds = new EObjectContainmentWithInverseEList<TaskDependency>(TaskDependency.class, this, HtaskgraphPackage.TASK_NODE__PREDS, HtaskgraphPackage.TASK_DEPENDENCY__TO);
		}
		return preds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNode getInputNode() {
		return inputNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputNode(CommunicationNode newInputNode, NotificationChain msgs) {
		CommunicationNode oldInputNode = inputNode;
		inputNode = newInputNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_NODE__INPUT_NODE, oldInputNode, newInputNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputNode(CommunicationNode newInputNode) {
		if (newInputNode != inputNode) {
			NotificationChain msgs = null;
			if (inputNode != null)
				msgs = ((InternalEObject)inputNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.TASK_NODE__INPUT_NODE, null, msgs);
			if (newInputNode != null)
				msgs = ((InternalEObject)newInputNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.TASK_NODE__INPUT_NODE, null, msgs);
			msgs = basicSetInputNode(newInputNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_NODE__INPUT_NODE, newInputNode, newInputNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNode getOutputNode() {
		return outputNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputNode(CommunicationNode newOutputNode, NotificationChain msgs) {
		CommunicationNode oldOutputNode = outputNode;
		outputNode = newOutputNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_NODE__OUTPUT_NODE, oldOutputNode, newOutputNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputNode(CommunicationNode newOutputNode) {
		if (newOutputNode != outputNode) {
			NotificationChain msgs = null;
			if (outputNode != null)
				msgs = ((InternalEObject)outputNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.TASK_NODE__OUTPUT_NODE, null, msgs);
			if (newOutputNode != null)
				msgs = ((InternalEObject)newOutputNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.TASK_NODE__OUTPUT_NODE, null, msgs);
			msgs = basicSetOutputNode(newOutputNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_NODE__OUTPUT_NODE, newOutputNode, newOutputNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilingInformation> getProfilingInformation() {
		if (profilingInformation == null) {
			profilingInformation = new EObjectContainmentEList<ProfilingInformation>(ProfilingInformation.class, this, HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION);
		}
		return profilingInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getProcessingResources() {
		if (processingResources == null) {
			processingResources = new EObjectResolvingEList<ProcessingResource>(ProcessingResource.class, this, HtaskgraphPackage.TASK_NODE__PROCESSING_RESOURCES);
		}
		return processingResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MemoryResource> getMemoryResources() {
		if (memoryResources == null) {
			memoryResources = new EObjectResolvingEList<MemoryResource>(MemoryResource.class, this, HtaskgraphPackage.TASK_NODE__MEMORY_RESOURCES);
		}
		return memoryResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBlock() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addProcessingResource(final ProcessingResource pr) {
		boolean _contains = this.getProcessingResources().contains(pr);
		boolean _not = (!_contains);
		if (_not) {
			this.getProcessingResources().add(pr);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchicalTaskNode getParentTask() {
		EObject c = this;
		while ((c.eContainer() != null)) {
			{
				c = c.eContainer();
				if ((c instanceof HierarchicalTaskNode)) {
					return ((HierarchicalTaskNode)c);
				}
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchicalTaskNode getParentTask(final int level) {
		int _depth = this.getDepth();
		boolean _lessThan = (_depth < level);
		if (_lessThan) {
			return null;
		}
		TaskNode parent = this;
		while ((parent.getParentTask() != null)) {
			{
				parent = parent.getParentTask();
				int _depth_1 = parent.getDepth();
				boolean _equals = (_depth_1 == level);
				if (_equals) {
					return ((HierarchicalTaskNode) parent);
				}
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getExternalInputs() {
		final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
			public Boolean apply(final TaskDependency it) {
				return Boolean.valueOf(it.isExternal());
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>filter(this.getPreds(), _function), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getExternalOutputs() {
		final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
			public Boolean apply(final TaskDependency it) {
				return Boolean.valueOf(it.isExternal());
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>filter(this.getSuccs(), _function), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getInternalInputs() {
		final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
			public Boolean apply(final TaskDependency it) {
				boolean _isExternal = it.isExternal();
				return Boolean.valueOf((!_isExternal));
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>filter(this.getPreds(), _function), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getInternalOutputs() {
		final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
			public Boolean apply(final TaskDependency it) {
				boolean _isExternal = it.isExternal();
				return Boolean.valueOf((!_isExternal));
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>filter(this.getSuccs(), _function), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isScheduledOn(final ProcessingResource pr) {
		return this.getProcessingResources().contains(pr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		if ((visitor instanceof HTaskGraphVisitor)) {
			this.accept(((HTaskGraphVisitor) visitor));
		}
		else {
			final HTaskGraphVisitor adapter = GecosVisitorsRegistry.INSTANCE.<HTaskGraphVisitor, BlocksVisitor>getBlocksAdapter(visitor, HTaskGraphVisitor.class);
			if ((adapter == null)) {
				super.accept(visitor);
			}
			else {
				this.accept(adapter);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		return this.getBlock().connectTo(to, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock from, final BranchType cond) {
		return this.getBlock().connectFromBasic(from, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("Task" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(HTaskGraphVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSuccs()).basicAdd(otherEnd, msgs);
			case HtaskgraphPackage.TASK_NODE__PREDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPreds()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				return ((InternalEList<?>)getSuccs()).basicRemove(otherEnd, msgs);
			case HtaskgraphPackage.TASK_NODE__PREDS:
				return ((InternalEList<?>)getPreds()).basicRemove(otherEnd, msgs);
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
				return basicSetInputNode(null, msgs);
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
				return basicSetOutputNode(null, msgs);
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				return ((InternalEList<?>)getProfilingInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__DEPTH:
				return getDepth();
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				return getSuccs();
			case HtaskgraphPackage.TASK_NODE__PREDS:
				return getPreds();
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
				return getInputNode();
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
				return getOutputNode();
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				return getProfilingInformation();
			case HtaskgraphPackage.TASK_NODE__PROCESSING_RESOURCES:
				return getProcessingResources();
			case HtaskgraphPackage.TASK_NODE__MEMORY_RESOURCES:
				return getMemoryResources();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__DEPTH:
				setDepth((Integer)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				getSuccs().clear();
				getSuccs().addAll((Collection<? extends TaskDependency>)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__PREDS:
				getPreds().clear();
				getPreds().addAll((Collection<? extends TaskDependency>)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
				setInputNode((CommunicationNode)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
				setOutputNode((CommunicationNode)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				getProfilingInformation().clear();
				getProfilingInformation().addAll((Collection<? extends ProfilingInformation>)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__PROCESSING_RESOURCES:
				getProcessingResources().clear();
				getProcessingResources().addAll((Collection<? extends ProcessingResource>)newValue);
				return;
			case HtaskgraphPackage.TASK_NODE__MEMORY_RESOURCES:
				getMemoryResources().clear();
				getMemoryResources().addAll((Collection<? extends MemoryResource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__DEPTH:
				setDepth(DEPTH_EDEFAULT);
				return;
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				getSuccs().clear();
				return;
			case HtaskgraphPackage.TASK_NODE__PREDS:
				getPreds().clear();
				return;
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
				setInputNode((CommunicationNode)null);
				return;
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
				setOutputNode((CommunicationNode)null);
				return;
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				getProfilingInformation().clear();
				return;
			case HtaskgraphPackage.TASK_NODE__PROCESSING_RESOURCES:
				getProcessingResources().clear();
				return;
			case HtaskgraphPackage.TASK_NODE__MEMORY_RESOURCES:
				getMemoryResources().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_NODE__DEPTH:
				return depth != DEPTH_EDEFAULT;
			case HtaskgraphPackage.TASK_NODE__SUCCS:
				return succs != null && !succs.isEmpty();
			case HtaskgraphPackage.TASK_NODE__PREDS:
				return preds != null && !preds.isEmpty();
			case HtaskgraphPackage.TASK_NODE__INPUT_NODE:
				return inputNode != null;
			case HtaskgraphPackage.TASK_NODE__OUTPUT_NODE:
				return outputNode != null;
			case HtaskgraphPackage.TASK_NODE__PROFILING_INFORMATION:
				return profilingInformation != null && !profilingInformation.isEmpty();
			case HtaskgraphPackage.TASK_NODE__PROCESSING_RESOURCES:
				return processingResources != null && !processingResources.isEmpty();
			case HtaskgraphPackage.TASK_NODE__MEMORY_RESOURCES:
				return memoryResources != null && !memoryResources.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (depth: ");
		result.append(depth);
		result.append(')');
		return result.toString();
	}

} //TaskNodeImpl
