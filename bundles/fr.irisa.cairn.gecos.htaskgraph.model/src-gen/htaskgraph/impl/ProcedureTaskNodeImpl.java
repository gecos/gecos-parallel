/**
 */
package htaskgraph.impl;

import gecos.core.Procedure;

import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.ProcedureTaskNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Task Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.ProcedureTaskNodeImpl#getInputs <em>Inputs</em>}</li>
 *   <li>{@link htaskgraph.impl.ProcedureTaskNodeImpl#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link htaskgraph.impl.ProcedureTaskNodeImpl#getProcedure <em>Procedure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureTaskNodeImpl extends HierarchicalTaskNodeImpl implements ProcedureTaskNode {
	/**
	 * The cached value of the '{@link #getInputs() <em>Inputs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcedureTaskNode> inputs;

	/**
	 * The cached value of the '{@link #getOutputs() <em>Outputs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputs()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcedureTaskNode> outputs;

	/**
	 * The cached value of the '{@link #getProcedure() <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedure()
	 * @generated
	 * @ordered
	 */
	protected Procedure procedure;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureTaskNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.PROCEDURE_TASK_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureTaskNode> getInputs() {
		if (inputs == null) {
			inputs = new EObjectResolvingEList<ProcedureTaskNode>(ProcedureTaskNode.class, this, HtaskgraphPackage.PROCEDURE_TASK_NODE__INPUTS);
		}
		return inputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureTaskNode> getOutputs() {
		if (outputs == null) {
			outputs = new EObjectResolvingEList<ProcedureTaskNode>(ProcedureTaskNode.class, this, HtaskgraphPackage.PROCEDURE_TASK_NODE__OUTPUTS);
		}
		return outputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getProcedure() {
		if (procedure != null && procedure.eIsProxy()) {
			InternalEObject oldProcedure = (InternalEObject)procedure;
			procedure = (Procedure)eResolveProxy(oldProcedure);
			if (procedure != oldProcedure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE, oldProcedure, procedure));
			}
		}
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure basicGetProcedure() {
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedure(Procedure newProcedure) {
		Procedure oldProcedure = procedure;
		procedure = newProcedure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE, oldProcedure, procedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final HTaskGraphVisitor visitor) {
		visitor.visitProcedureTaskNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _number = this.getNumber();
		String _plus = ("PTN" + Integer.valueOf(_number));
		String _plus_1 = (_plus + "(L:");
		int _depth = this.getDepth();
		String _plus_2 = (_plus_1 + Integer.valueOf(_depth));
		return (_plus_2 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__INPUTS:
				return getInputs();
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__OUTPUTS:
				return getOutputs();
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE:
				if (resolve) return getProcedure();
				return basicGetProcedure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__INPUTS:
				getInputs().clear();
				getInputs().addAll((Collection<? extends ProcedureTaskNode>)newValue);
				return;
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__OUTPUTS:
				getOutputs().clear();
				getOutputs().addAll((Collection<? extends ProcedureTaskNode>)newValue);
				return;
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE:
				setProcedure((Procedure)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__INPUTS:
				getInputs().clear();
				return;
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__OUTPUTS:
				getOutputs().clear();
				return;
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE:
				setProcedure((Procedure)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__INPUTS:
				return inputs != null && !inputs.isEmpty();
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__OUTPUTS:
				return outputs != null && !outputs.isEmpty();
			case HtaskgraphPackage.PROCEDURE_TASK_NODE__PROCEDURE:
				return procedure != null;
		}
		return super.eIsSet(featureID);
	}

} //ProcedureTaskNodeImpl
