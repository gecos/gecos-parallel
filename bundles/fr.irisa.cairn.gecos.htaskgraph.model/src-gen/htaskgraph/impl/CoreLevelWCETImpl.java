/**
 */
package htaskgraph.impl;

import htaskgraph.CoreLevelWCET;
import htaskgraph.HtaskgraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Core Level WCET</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.CoreLevelWCETImpl#getArchitecture <em>Architecture</em>}</li>
 *   <li>{@link htaskgraph.impl.CoreLevelWCETImpl#getIsolatedWCET <em>Isolated WCET</em>}</li>
 *   <li>{@link htaskgraph.impl.CoreLevelWCETImpl#getConflictWCET <em>Conflict WCET</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CoreLevelWCETImpl extends MinimalEObjectImpl.Container implements CoreLevelWCET {
	/**
	 * The default value of the '{@link #getArchitecture() <em>Architecture</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArchitecture()
	 * @generated
	 * @ordered
	 */
	protected static final String ARCHITECTURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArchitecture() <em>Architecture</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArchitecture()
	 * @generated
	 * @ordered
	 */
	protected String architecture = ARCHITECTURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsolatedWCET() <em>Isolated WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsolatedWCET()
	 * @generated
	 * @ordered
	 */
	protected static final long ISOLATED_WCET_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIsolatedWCET() <em>Isolated WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsolatedWCET()
	 * @generated
	 * @ordered
	 */
	protected long isolatedWCET = ISOLATED_WCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getConflictWCET() <em>Conflict WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConflictWCET()
	 * @generated
	 * @ordered
	 */
	protected static final long CONFLICT_WCET_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getConflictWCET() <em>Conflict WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConflictWCET()
	 * @generated
	 * @ordered
	 */
	protected long conflictWCET = CONFLICT_WCET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoreLevelWCETImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.CORE_LEVEL_WCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArchitecture() {
		return architecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArchitecture(String newArchitecture) {
		String oldArchitecture = architecture;
		architecture = newArchitecture;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.CORE_LEVEL_WCET__ARCHITECTURE, oldArchitecture, architecture));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getIsolatedWCET() {
		return isolatedWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsolatedWCET(long newIsolatedWCET) {
		long oldIsolatedWCET = isolatedWCET;
		isolatedWCET = newIsolatedWCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.CORE_LEVEL_WCET__ISOLATED_WCET, oldIsolatedWCET, isolatedWCET));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getConflictWCET() {
		return conflictWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConflictWCET(long newConflictWCET) {
		long oldConflictWCET = conflictWCET;
		conflictWCET = newConflictWCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.CORE_LEVEL_WCET__CONFLICT_WCET, oldConflictWCET, conflictWCET));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.CORE_LEVEL_WCET__ARCHITECTURE:
				return getArchitecture();
			case HtaskgraphPackage.CORE_LEVEL_WCET__ISOLATED_WCET:
				return getIsolatedWCET();
			case HtaskgraphPackage.CORE_LEVEL_WCET__CONFLICT_WCET:
				return getConflictWCET();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.CORE_LEVEL_WCET__ARCHITECTURE:
				setArchitecture((String)newValue);
				return;
			case HtaskgraphPackage.CORE_LEVEL_WCET__ISOLATED_WCET:
				setIsolatedWCET((Long)newValue);
				return;
			case HtaskgraphPackage.CORE_LEVEL_WCET__CONFLICT_WCET:
				setConflictWCET((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.CORE_LEVEL_WCET__ARCHITECTURE:
				setArchitecture(ARCHITECTURE_EDEFAULT);
				return;
			case HtaskgraphPackage.CORE_LEVEL_WCET__ISOLATED_WCET:
				setIsolatedWCET(ISOLATED_WCET_EDEFAULT);
				return;
			case HtaskgraphPackage.CORE_LEVEL_WCET__CONFLICT_WCET:
				setConflictWCET(CONFLICT_WCET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.CORE_LEVEL_WCET__ARCHITECTURE:
				return ARCHITECTURE_EDEFAULT == null ? architecture != null : !ARCHITECTURE_EDEFAULT.equals(architecture);
			case HtaskgraphPackage.CORE_LEVEL_WCET__ISOLATED_WCET:
				return isolatedWCET != ISOLATED_WCET_EDEFAULT;
			case HtaskgraphPackage.CORE_LEVEL_WCET__CONFLICT_WCET:
				return conflictWCET != CONFLICT_WCET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (architecture: ");
		result.append(architecture);
		result.append(", isolatedWCET: ");
		result.append(isolatedWCET);
		result.append(", conflictWCET: ");
		result.append(conflictWCET);
		result.append(')');
		return result.toString();
	}

} //CoreLevelWCETImpl
