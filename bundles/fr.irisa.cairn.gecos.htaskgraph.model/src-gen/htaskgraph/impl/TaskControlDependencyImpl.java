/**
 */
package htaskgraph.impl;

import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskControlDependency;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Control Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TaskControlDependencyImpl extends TaskDependencyImpl implements TaskControlDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskControlDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_CONTROL_DEPENDENCY;
	}

} //TaskControlDependencyImpl
