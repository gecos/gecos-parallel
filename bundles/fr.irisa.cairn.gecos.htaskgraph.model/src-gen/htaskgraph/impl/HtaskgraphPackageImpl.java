/**
 */
package htaskgraph.impl;

import architecture.ArchitecturePackage;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CaseTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.CommunicationNodeType;
import htaskgraph.CoreLevelWCET;
import htaskgraph.DoWhileTaskNode;
import htaskgraph.ForTaskNode;
import htaskgraph.GlueTaskDependency;
import htaskgraph.HTGCom;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HTaskGraphVisitable;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.HtaskgraphFactory;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.ProfilingInformation;
import htaskgraph.SwitchTaskNode;
import htaskgraph.TaskAntiDependency;
import htaskgraph.TaskControlDependency;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;
import htaskgraph.TaskOutputDependency;
import htaskgraph.TaskTrueDependency;
import htaskgraph.WCETInformation;
import htaskgraph.WhileTaskNode;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HtaskgraphPackageImpl extends EPackageImpl implements HtaskgraphPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hTaskGraphVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hTaskGraphVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass glueTaskDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskOutputDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskAntiDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskTrueDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskControlDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicLeafTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hierarchicalTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clusterTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whileTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doWhileTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureTaskNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass profilingInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coreLevelWCETEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wcetInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass htgComEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass htgSendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass htgRecvEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum communicationNodeTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see htaskgraph.HtaskgraphPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HtaskgraphPackageImpl() {
		super(eNS_URI, HtaskgraphFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link HtaskgraphPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HtaskgraphPackage init() {
		if (isInited) return (HtaskgraphPackage)EPackage.Registry.INSTANCE.getEPackage(HtaskgraphPackage.eNS_URI);

		// Obtain or create and register package
		HtaskgraphPackageImpl theHtaskgraphPackage = (HtaskgraphPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof HtaskgraphPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new HtaskgraphPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AnnotationsPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		ArchitecturePackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theHtaskgraphPackage.createPackageContents();

		// Initialize created meta-data
		theHtaskgraphPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHtaskgraphPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HtaskgraphPackage.eNS_URI, theHtaskgraphPackage);
		return theHtaskgraphPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTaskGraphVisitable() {
		return hTaskGraphVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTaskGraphVisitor() {
		return hTaskGraphVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskDependency() {
		return taskDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_Use() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_From() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_To() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_SrcDependencies() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_DstDependencies() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTaskDependency_Id() {
		return (EAttribute)taskDependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlueTaskDependency() {
		return glueTaskDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskOutputDependency() {
		return taskOutputDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskAntiDependency() {
		return taskAntiDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskTrueDependency() {
		return taskTrueDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskControlDependency() {
		return taskControlDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskNode() {
		return taskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTaskNode_Depth() {
		return (EAttribute)taskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_Succs() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_Preds() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_InputNode() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_OutputNode() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_ProfilingInformation() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_ProcessingResources() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskNode_MemoryResources() {
		return (EReference)taskNodeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationNode() {
		return communicationNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationNode_Type() {
		return (EAttribute)communicationNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationNode_Communications() {
		return (EReference)communicationNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationNode_Block() {
		return (EReference)communicationNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeafTaskNode() {
		return leafTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafTaskNode_Block() {
		return (EReference)leafTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicLeafTaskNode() {
		return basicLeafTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHierarchicalTaskNode() {
		return hierarchicalTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClusterTaskNode() {
		return clusterTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClusterTaskNode_Block() {
		return (EReference)clusterTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfTaskNode() {
		return ifTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfTaskNode_Block() {
		return (EReference)ifTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwitchTaskNode() {
		return switchTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwitchTaskNode_Block() {
		return (EReference)switchTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseTaskNode() {
		return caseTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseTaskNode_Block() {
		return (EReference)caseTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForTaskNode() {
		return forTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForTaskNode_Block() {
		return (EReference)forTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForTaskNode_LoopIn() {
		return (EReference)forTaskNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForTaskNode_LoopOut() {
		return (EReference)forTaskNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhileTaskNode() {
		return whileTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileTaskNode_Block() {
		return (EReference)whileTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileTaskNode_LoopIn() {
		return (EReference)whileTaskNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileTaskNode_LoopOut() {
		return (EReference)whileTaskNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoWhileTaskNode() {
		return doWhileTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoWhileTaskNode_Block() {
		return (EReference)doWhileTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoWhileTaskNode_LoopIn() {
		return (EReference)doWhileTaskNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoWhileTaskNode_LoopOut() {
		return (EReference)doWhileTaskNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureTaskNode() {
		return procedureTaskNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureTaskNode_Inputs() {
		return (EReference)procedureTaskNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureTaskNode_Outputs() {
		return (EReference)procedureTaskNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureTaskNode_Procedure() {
		return (EReference)procedureTaskNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProfilingInformation() {
		return profilingInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoreLevelWCET() {
		return coreLevelWCETEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoreLevelWCET_Architecture() {
		return (EAttribute)coreLevelWCETEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoreLevelWCET_IsolatedWCET() {
		return (EAttribute)coreLevelWCETEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoreLevelWCET_ConflictWCET() {
		return (EAttribute)coreLevelWCETEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWCETInformation() {
		return wcetInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWCETInformation_Data() {
		return (EReference)wcetInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTGCom() {
		return htgComEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTGSend() {
		return htgSendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGSend_FromProcResources() {
		return (EReference)htgSendEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGSend_Receives() {
		return (EReference)htgSendEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGSend_Use() {
		return (EReference)htgSendEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTGRecv() {
		return htgRecvEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGRecv_ToProcResources() {
		return (EReference)htgRecvEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGRecv_Sends() {
		return (EReference)htgRecvEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTGRecv_Use() {
		return (EReference)htgRecvEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCommunicationNodeType() {
		return communicationNodeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtaskgraphFactory getHtaskgraphFactory() {
		return (HtaskgraphFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		hTaskGraphVisitableEClass = createEClass(HTASK_GRAPH_VISITABLE);

		hTaskGraphVisitorEClass = createEClass(HTASK_GRAPH_VISITOR);

		taskDependencyEClass = createEClass(TASK_DEPENDENCY);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__USE);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__FROM);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__TO);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__SRC_DEPENDENCIES);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__DST_DEPENDENCIES);
		createEAttribute(taskDependencyEClass, TASK_DEPENDENCY__ID);

		glueTaskDependencyEClass = createEClass(GLUE_TASK_DEPENDENCY);

		taskOutputDependencyEClass = createEClass(TASK_OUTPUT_DEPENDENCY);

		taskAntiDependencyEClass = createEClass(TASK_ANTI_DEPENDENCY);

		taskTrueDependencyEClass = createEClass(TASK_TRUE_DEPENDENCY);

		taskControlDependencyEClass = createEClass(TASK_CONTROL_DEPENDENCY);

		taskNodeEClass = createEClass(TASK_NODE);
		createEAttribute(taskNodeEClass, TASK_NODE__DEPTH);
		createEReference(taskNodeEClass, TASK_NODE__SUCCS);
		createEReference(taskNodeEClass, TASK_NODE__PREDS);
		createEReference(taskNodeEClass, TASK_NODE__INPUT_NODE);
		createEReference(taskNodeEClass, TASK_NODE__OUTPUT_NODE);
		createEReference(taskNodeEClass, TASK_NODE__PROFILING_INFORMATION);
		createEReference(taskNodeEClass, TASK_NODE__PROCESSING_RESOURCES);
		createEReference(taskNodeEClass, TASK_NODE__MEMORY_RESOURCES);

		communicationNodeEClass = createEClass(COMMUNICATION_NODE);
		createEAttribute(communicationNodeEClass, COMMUNICATION_NODE__TYPE);
		createEReference(communicationNodeEClass, COMMUNICATION_NODE__COMMUNICATIONS);
		createEReference(communicationNodeEClass, COMMUNICATION_NODE__BLOCK);

		leafTaskNodeEClass = createEClass(LEAF_TASK_NODE);
		createEReference(leafTaskNodeEClass, LEAF_TASK_NODE__BLOCK);

		basicLeafTaskNodeEClass = createEClass(BASIC_LEAF_TASK_NODE);

		hierarchicalTaskNodeEClass = createEClass(HIERARCHICAL_TASK_NODE);

		clusterTaskNodeEClass = createEClass(CLUSTER_TASK_NODE);
		createEReference(clusterTaskNodeEClass, CLUSTER_TASK_NODE__BLOCK);

		ifTaskNodeEClass = createEClass(IF_TASK_NODE);
		createEReference(ifTaskNodeEClass, IF_TASK_NODE__BLOCK);

		switchTaskNodeEClass = createEClass(SWITCH_TASK_NODE);
		createEReference(switchTaskNodeEClass, SWITCH_TASK_NODE__BLOCK);

		caseTaskNodeEClass = createEClass(CASE_TASK_NODE);
		createEReference(caseTaskNodeEClass, CASE_TASK_NODE__BLOCK);

		forTaskNodeEClass = createEClass(FOR_TASK_NODE);
		createEReference(forTaskNodeEClass, FOR_TASK_NODE__BLOCK);
		createEReference(forTaskNodeEClass, FOR_TASK_NODE__LOOP_IN);
		createEReference(forTaskNodeEClass, FOR_TASK_NODE__LOOP_OUT);

		whileTaskNodeEClass = createEClass(WHILE_TASK_NODE);
		createEReference(whileTaskNodeEClass, WHILE_TASK_NODE__BLOCK);
		createEReference(whileTaskNodeEClass, WHILE_TASK_NODE__LOOP_IN);
		createEReference(whileTaskNodeEClass, WHILE_TASK_NODE__LOOP_OUT);

		doWhileTaskNodeEClass = createEClass(DO_WHILE_TASK_NODE);
		createEReference(doWhileTaskNodeEClass, DO_WHILE_TASK_NODE__BLOCK);
		createEReference(doWhileTaskNodeEClass, DO_WHILE_TASK_NODE__LOOP_IN);
		createEReference(doWhileTaskNodeEClass, DO_WHILE_TASK_NODE__LOOP_OUT);

		procedureTaskNodeEClass = createEClass(PROCEDURE_TASK_NODE);
		createEReference(procedureTaskNodeEClass, PROCEDURE_TASK_NODE__INPUTS);
		createEReference(procedureTaskNodeEClass, PROCEDURE_TASK_NODE__OUTPUTS);
		createEReference(procedureTaskNodeEClass, PROCEDURE_TASK_NODE__PROCEDURE);

		profilingInformationEClass = createEClass(PROFILING_INFORMATION);

		coreLevelWCETEClass = createEClass(CORE_LEVEL_WCET);
		createEAttribute(coreLevelWCETEClass, CORE_LEVEL_WCET__ARCHITECTURE);
		createEAttribute(coreLevelWCETEClass, CORE_LEVEL_WCET__ISOLATED_WCET);
		createEAttribute(coreLevelWCETEClass, CORE_LEVEL_WCET__CONFLICT_WCET);

		wcetInformationEClass = createEClass(WCET_INFORMATION);
		createEReference(wcetInformationEClass, WCET_INFORMATION__DATA);

		htgComEClass = createEClass(HTG_COM);

		htgSendEClass = createEClass(HTG_SEND);
		createEReference(htgSendEClass, HTG_SEND__FROM_PROC_RESOURCES);
		createEReference(htgSendEClass, HTG_SEND__RECEIVES);
		createEReference(htgSendEClass, HTG_SEND__USE);

		htgRecvEClass = createEClass(HTG_RECV);
		createEReference(htgRecvEClass, HTG_RECV__TO_PROC_RESOURCES);
		createEReference(htgRecvEClass, HTG_RECV__SENDS);
		createEReference(htgRecvEClass, HTG_RECV__USE);

		// Create enums
		communicationNodeTypeEEnum = createEEnum(COMMUNICATION_NODE_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);
		ArchitecturePackage theArchitecturePackage = (ArchitecturePackage)EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		taskDependencyEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		taskDependencyEClass.getESuperTypes().add(this.getHTaskGraphVisitable());
		glueTaskDependencyEClass.getESuperTypes().add(this.getTaskDependency());
		taskOutputDependencyEClass.getESuperTypes().add(this.getTaskDependency());
		taskAntiDependencyEClass.getESuperTypes().add(this.getTaskDependency());
		taskTrueDependencyEClass.getESuperTypes().add(this.getTaskDependency());
		taskControlDependencyEClass.getESuperTypes().add(this.getTaskDependency());
		taskNodeEClass.getESuperTypes().add(theBlocksPackage.getBlock());
		taskNodeEClass.getESuperTypes().add(this.getHTaskGraphVisitable());
		communicationNodeEClass.getESuperTypes().add(this.getTaskNode());
		leafTaskNodeEClass.getESuperTypes().add(this.getTaskNode());
		basicLeafTaskNodeEClass.getESuperTypes().add(this.getLeafTaskNode());
		basicLeafTaskNodeEClass.getESuperTypes().add(theBlocksPackage.getBasicBlock());
		hierarchicalTaskNodeEClass.getESuperTypes().add(this.getTaskNode());
		clusterTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		ifTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		switchTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		caseTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		forTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		whileTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		doWhileTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		procedureTaskNodeEClass.getESuperTypes().add(this.getHierarchicalTaskNode());
		wcetInformationEClass.getESuperTypes().add(this.getProfilingInformation());
		htgComEClass.getESuperTypes().add(theBlocksPackage.getBasicBlock());
		htgSendEClass.getESuperTypes().add(this.getHTGCom());
		htgRecvEClass.getESuperTypes().add(this.getHTGCom());

		// Initialize classes and features; add operations and parameters
		initEClass(hTaskGraphVisitableEClass, HTaskGraphVisitable.class, "HTaskGraphVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(hTaskGraphVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(hTaskGraphVisitorEClass, HTaskGraphVisitor.class, "HTaskGraphVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitTaskDependency", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTaskDependency(), "td", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitLeafTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLeafTaskNode(), "ltn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitClusterTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getClusterTaskNode(), "ctn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitIfTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIfTaskNode(), "ltn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitForTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getForTaskNode(), "ltn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitWhileTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getWhileTaskNode(), "ltn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitProcedureTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedureTaskNode(), "ltn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitCommunicationNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCommunicationNode(), "td", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitBasicLeafTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicLeafTaskNode(), "bl", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitDoWhileTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoWhileTaskNode(), "dwb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitSwitchTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSwitchTaskNode(), "stn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(hTaskGraphVisitorEClass, null, "visitCaseTaskNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCaseTaskNode(), "ctn", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(taskDependencyEClass, TaskDependency.class, "TaskDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTaskDependency_Use(), theCorePackage.getSymbol(), null, "use", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskDependency_From(), this.getTaskNode(), this.getTaskNode_Succs(), "from", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskDependency_To(), this.getTaskNode(), this.getTaskNode_Preds(), "to", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskDependency_SrcDependencies(), this.getTaskDependency(), this.getTaskDependency_DstDependencies(), "srcDependencies", null, 0, -1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskDependency_DstDependencies(), this.getTaskDependency(), this.getTaskDependency_SrcDependencies(), "dstDependencies", null, 0, -1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaskDependency_Id(), theEcorePackage.getEInt(), "id", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(taskDependencyEClass, theEcorePackage.getEBoolean(), "isExternal", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getOriginalSourceNodes", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getFinalDestinationNodes", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskDependency(), "getFirstInternalSrcDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskDependency(), "getFirstInternalDstDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getFirstInternalSrcNodes", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getFirstInternalDstNodes", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskDependency(), "getEffSrcDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskDependency(), "getEffDstDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getEffPreds", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskDependencyEClass, this.getTaskNode(), "getEffSuccs", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(glueTaskDependencyEClass, GlueTaskDependency.class, "GlueTaskDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskOutputDependencyEClass, TaskOutputDependency.class, "TaskOutputDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskAntiDependencyEClass, TaskAntiDependency.class, "TaskAntiDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskTrueDependencyEClass, TaskTrueDependency.class, "TaskTrueDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskControlDependencyEClass, TaskControlDependency.class, "TaskControlDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(taskNodeEClass, TaskNode.class, "TaskNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTaskNode_Depth(), theEcorePackage.getEInt(), "depth", null, 0, 1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_Succs(), this.getTaskDependency(), this.getTaskDependency_From(), "succs", null, 0, -1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_Preds(), this.getTaskDependency(), this.getTaskDependency_To(), "preds", null, 0, -1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_InputNode(), this.getCommunicationNode(), null, "inputNode", null, 0, 1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_OutputNode(), this.getCommunicationNode(), null, "outputNode", null, 0, 1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_ProfilingInformation(), this.getProfilingInformation(), null, "profilingInformation", null, 0, -1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_ProcessingResources(), theArchitecturePackage.getProcessingResource(), null, "processingResources", null, 0, -1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskNode_MemoryResources(), theArchitecturePackage.getMemoryResource(), null, "memoryResources", null, 0, -1, TaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(taskNodeEClass, theBlocksPackage.getBlock(), "getBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, null, "addProcessingResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theArchitecturePackage.getProcessingResource(), "pr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, this.getHierarchicalTaskNode(), "getParentTask", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, this.getHierarchicalTaskNode(), "getParentTask", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "level", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, this.getTaskDependency(), "getExternalInputs", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, this.getTaskDependency(), "getExternalOutputs", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, this.getTaskDependency(), "getInternalInputs", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, this.getTaskDependency(), "getInternalOutputs", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, theEcorePackage.getEBoolean(), "isScheduledOn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theArchitecturePackage.getProcessingResource(), "pr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, theBlocksPackage.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(taskNodeEClass, theBlocksPackage.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(taskNodeEClass, theEcorePackage.getEString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(communicationNodeEClass, CommunicationNode.class, "CommunicationNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCommunicationNode_Type(), this.getCommunicationNodeType(), "type", null, 0, 1, CommunicationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationNode_Communications(), this.getHTGCom(), null, "communications", null, 0, -1, CommunicationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationNode_Block(), theBlocksPackage.getCompositeBlock(), null, "block", null, 0, 1, CommunicationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(communicationNodeEClass, theEcorePackage.getEBoolean(), "isInput", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(communicationNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(communicationNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(leafTaskNodeEClass, LeafTaskNode.class, "LeafTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeafTaskNode_Block(), theBlocksPackage.getBlock(), null, "block", null, 0, 1, LeafTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(leafTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(leafTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(basicLeafTaskNodeEClass, BasicLeafTaskNode.class, "BasicLeafTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(basicLeafTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicLeafTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(hierarchicalTaskNodeEClass, HierarchicalTaskNode.class, "HierarchicalTaskNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(hierarchicalTaskNodeEClass, this.getTaskNode(), "getChildTasks", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(hierarchicalTaskNodeEClass, this.getTaskNode(), "getAllLevelsChildTasks", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(hierarchicalTaskNodeEClass, this.getTaskNode(), "getSourceChildTasks", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(hierarchicalTaskNodeEClass, this.getTaskNode(), "getSinkChildTasks", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(clusterTaskNodeEClass, ClusterTaskNode.class, "ClusterTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClusterTaskNode_Block(), theBlocksPackage.getCompositeBlock(), null, "block", null, 0, 1, ClusterTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(clusterTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(clusterTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(clusterTaskNodeEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlock(), "oldBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlock(), "newBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(ifTaskNodeEClass, IfTaskNode.class, "IfTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIfTaskNode_Block(), theBlocksPackage.getIfBlock(), null, "block", null, 0, 1, IfTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ifTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifTaskNodeEClass, this.getBasicLeafTaskNode(), "getTest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifTaskNodeEClass, this.getTaskNode(), "getThen", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifTaskNodeEClass, this.getTaskNode(), "getElse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(switchTaskNodeEClass, SwitchTaskNode.class, "SwitchTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSwitchTaskNode_Block(), theBlocksPackage.getSwitchBlock(), null, "block", null, 0, 1, SwitchTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(switchTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(caseTaskNodeEClass, CaseTaskNode.class, "CaseTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCaseTaskNode_Block(), theBlocksPackage.getCaseBlock(), null, "block", null, 0, 1, CaseTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(caseTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(caseTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(forTaskNodeEClass, ForTaskNode.class, "ForTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getForTaskNode_Block(), theBlocksPackage.getForBlock(), null, "block", null, 0, 1, ForTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForTaskNode_LoopIn(), this.getCommunicationNode(), null, "loopIn", null, 0, 1, ForTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForTaskNode_LoopOut(), this.getCommunicationNode(), null, "loopOut", null, 0, 1, ForTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(forTaskNodeEClass, this.getBasicLeafTaskNode(), "getStep", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forTaskNodeEClass, this.getBasicLeafTaskNode(), "getInit", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forTaskNodeEClass, this.getBasicLeafTaskNode(), "getTest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forTaskNodeEClass, this.getTaskNode(), "getBody", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(whileTaskNodeEClass, WhileTaskNode.class, "WhileTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWhileTaskNode_Block(), theBlocksPackage.getWhileBlock(), null, "block", null, 0, 1, WhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhileTaskNode_LoopIn(), this.getCommunicationNode(), null, "loopIn", null, 0, 1, WhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhileTaskNode_LoopOut(), this.getCommunicationNode(), null, "loopOut", null, 0, 1, WhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(whileTaskNodeEClass, this.getBasicLeafTaskNode(), "getTest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileTaskNodeEClass, this.getTaskNode(), "getBody", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(doWhileTaskNodeEClass, DoWhileTaskNode.class, "DoWhileTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDoWhileTaskNode_Block(), theBlocksPackage.getWhileBlock(), null, "block", null, 0, 1, DoWhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDoWhileTaskNode_LoopIn(), this.getCommunicationNode(), null, "loopIn", null, 0, 1, DoWhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDoWhileTaskNode_LoopOut(), this.getCommunicationNode(), null, "loopOut", null, 0, 1, DoWhileTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(doWhileTaskNodeEClass, this.getBasicLeafTaskNode(), "getTest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileTaskNodeEClass, this.getTaskNode(), "getBody", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(procedureTaskNodeEClass, ProcedureTaskNode.class, "ProcedureTaskNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedureTaskNode_Inputs(), this.getProcedureTaskNode(), null, "inputs", null, 0, -1, ProcedureTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedureTaskNode_Outputs(), this.getProcedureTaskNode(), null, "outputs", null, 0, -1, ProcedureTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedureTaskNode_Procedure(), theCorePackage.getProcedure(), null, "procedure", null, 0, 1, ProcedureTaskNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(procedureTaskNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHTaskGraphVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureTaskNodeEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(profilingInformationEClass, ProfilingInformation.class, "ProfilingInformation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(coreLevelWCETEClass, CoreLevelWCET.class, "CoreLevelWCET", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCoreLevelWCET_Architecture(), theEcorePackage.getEString(), "architecture", null, 0, 1, CoreLevelWCET.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCoreLevelWCET_IsolatedWCET(), theEcorePackage.getELong(), "isolatedWCET", null, 0, 1, CoreLevelWCET.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCoreLevelWCET_ConflictWCET(), theEcorePackage.getELong(), "conflictWCET", null, 0, 1, CoreLevelWCET.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wcetInformationEClass, WCETInformation.class, "WCETInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWCETInformation_Data(), this.getCoreLevelWCET(), null, "data", null, 0, -1, WCETInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(wcetInformationEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(htgComEClass, HTGCom.class, "HTGCom", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(htgSendEClass, HTGSend.class, "HTGSend", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHTGSend_FromProcResources(), theArchitecturePackage.getProcessingResource(), null, "fromProcResources", null, 0, -1, HTGSend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTGSend_Receives(), this.getHTGRecv(), this.getHTGRecv_Sends(), "receives", null, 0, -1, HTGSend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTGSend_Use(), theCorePackage.getSymbol(), null, "use", null, 0, 1, HTGSend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(htgSendEClass, theArchitecturePackage.getProcessingResource(), "getToProcResources", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(htgSendEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(htgRecvEClass, HTGRecv.class, "HTGRecv", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHTGRecv_ToProcResources(), theArchitecturePackage.getProcessingResource(), null, "toProcResources", null, 0, -1, HTGRecv.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTGRecv_Sends(), this.getHTGSend(), this.getHTGSend_Receives(), "sends", null, 0, -1, HTGRecv.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTGRecv_Use(), theCorePackage.getSymbol(), null, "use", null, 0, 1, HTGRecv.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(htgRecvEClass, theArchitecturePackage.getProcessingResource(), "getFromProcResources", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(htgRecvEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(communicationNodeTypeEEnum, CommunicationNodeType.class, "CommunicationNodeType");
		addEEnumLiteral(communicationNodeTypeEEnum, CommunicationNodeType.INPUT);
		addEEnumLiteral(communicationNodeTypeEEnum, CommunicationNodeType.OUTPUT);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //HtaskgraphPackageImpl
