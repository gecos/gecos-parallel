/**
 */
package htaskgraph.impl;

import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskAntiDependency;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Anti Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TaskAntiDependencyImpl extends TaskDependencyImpl implements TaskAntiDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskAntiDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_ANTI_DEPENDENCY;
	}

} //TaskAntiDependencyImpl
