/**
 */
package htaskgraph.impl;

import architecture.ProcessingResource;

import com.google.common.collect.Iterables;

import gecos.core.Symbol;

import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HtaskgraphPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HTG Send</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.HTGSendImpl#getFromProcResources <em>From Proc Resources</em>}</li>
 *   <li>{@link htaskgraph.impl.HTGSendImpl#getReceives <em>Receives</em>}</li>
 *   <li>{@link htaskgraph.impl.HTGSendImpl#getUse <em>Use</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HTGSendImpl extends HTGComImpl implements HTGSend {
	/**
	 * The cached value of the '{@link #getFromProcResources() <em>From Proc Resources</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromProcResources()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessingResource> fromProcResources;

	/**
	 * The cached value of the '{@link #getReceives() <em>Receives</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceives()
	 * @generated
	 * @ordered
	 */
	protected EList<HTGRecv> receives;

	/**
	 * The cached value of the '{@link #getUse() <em>Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUse()
	 * @generated
	 * @ordered
	 */
	protected Symbol use;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HTGSendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.HTG_SEND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getFromProcResources() {
		if (fromProcResources == null) {
			fromProcResources = new EObjectResolvingEList<ProcessingResource>(ProcessingResource.class, this, HtaskgraphPackage.HTG_SEND__FROM_PROC_RESOURCES);
		}
		return fromProcResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HTGRecv> getReceives() {
		if (receives == null) {
			receives = new EObjectWithInverseResolvingEList.ManyInverse<HTGRecv>(HTGRecv.class, this, HtaskgraphPackage.HTG_SEND__RECEIVES, HtaskgraphPackage.HTG_RECV__SENDS);
		}
		return receives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUse() {
		if (use != null && use.eIsProxy()) {
			InternalEObject oldUse = (InternalEObject)use;
			use = (Symbol)eResolveProxy(oldUse);
			if (use != oldUse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HtaskgraphPackage.HTG_SEND__USE, oldUse, use));
			}
		}
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetUse() {
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUse(Symbol newUse) {
		Symbol oldUse = use;
		use = newUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.HTG_SEND__USE, oldUse, use));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessingResource> getToProcResources() {
		final Function1<HTGRecv, EList<ProcessingResource>> _function = new Function1<HTGRecv, EList<ProcessingResource>>() {
			public EList<ProcessingResource> apply(final HTGRecv it) {
				return it.getToProcResources();
			}
		};
		return ECollections.<ProcessingResource>asEList(((ProcessingResource[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<ProcessingResource>concat(XcoreEListExtensions.<HTGRecv, EList<ProcessingResource>>map(this.getReceives(), _function)), ProcessingResource.class)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Symbol _use = this.getUse();
		String _plus = ("HTGSend " + _use);
		String _plus_1 = (_plus + " ");
		EList<ProcessingResource> _fromProcResources = this.getFromProcResources();
		String _plus_2 = (_plus_1 + _fromProcResources);
		String _plus_3 = (_plus_2 + " -> ");
		EList<ProcessingResource> _toProcResources = this.getToProcResources();
		return (_plus_3 + _toProcResources);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getReceives()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				return ((InternalEList<?>)getReceives()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__FROM_PROC_RESOURCES:
				return getFromProcResources();
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				return getReceives();
			case HtaskgraphPackage.HTG_SEND__USE:
				if (resolve) return getUse();
				return basicGetUse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__FROM_PROC_RESOURCES:
				getFromProcResources().clear();
				getFromProcResources().addAll((Collection<? extends ProcessingResource>)newValue);
				return;
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				getReceives().clear();
				getReceives().addAll((Collection<? extends HTGRecv>)newValue);
				return;
			case HtaskgraphPackage.HTG_SEND__USE:
				setUse((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__FROM_PROC_RESOURCES:
				getFromProcResources().clear();
				return;
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				getReceives().clear();
				return;
			case HtaskgraphPackage.HTG_SEND__USE:
				setUse((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.HTG_SEND__FROM_PROC_RESOURCES:
				return fromProcResources != null && !fromProcResources.isEmpty();
			case HtaskgraphPackage.HTG_SEND__RECEIVES:
				return receives != null && !receives.isEmpty();
			case HtaskgraphPackage.HTG_SEND__USE:
				return use != null;
		}
		return super.eIsSet(featureID);
	}

} //HTGSendImpl
