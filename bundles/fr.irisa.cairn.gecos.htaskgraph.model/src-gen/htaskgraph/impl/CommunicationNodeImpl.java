/**
 */
package htaskgraph.impl;

import com.google.common.base.Objects;

import gecos.blocks.CompositeBlock;

import htaskgraph.CommunicationNode;
import htaskgraph.CommunicationNodeType;
import htaskgraph.HTGCom;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HtaskgraphPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.CommunicationNodeImpl#getType <em>Type</em>}</li>
 *   <li>{@link htaskgraph.impl.CommunicationNodeImpl#getCommunications <em>Communications</em>}</li>
 *   <li>{@link htaskgraph.impl.CommunicationNodeImpl#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationNodeImpl extends TaskNodeImpl implements CommunicationNode {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final CommunicationNodeType TYPE_EDEFAULT = CommunicationNodeType.INPUT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected CommunicationNodeType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCommunications() <em>Communications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunications()
	 * @generated
	 * @ordered
	 */
	protected EList<HTGCom> communications;

	/**
	 * The cached value of the '{@link #getBlock() <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected CompositeBlock block;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.COMMUNICATION_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationNodeType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(CommunicationNodeType newType) {
		CommunicationNodeType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.COMMUNICATION_NODE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HTGCom> getCommunications() {
		if (communications == null) {
			communications = new EObjectContainmentEList<HTGCom>(HTGCom.class, this, HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS);
		}
		return communications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeBlock getBlock() {
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlock(CompositeBlock newBlock, NotificationChain msgs) {
		CompositeBlock oldBlock = block;
		block = newBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.COMMUNICATION_NODE__BLOCK, oldBlock, newBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlock(CompositeBlock newBlock) {
		if (newBlock != block) {
			NotificationChain msgs = null;
			if (block != null)
				msgs = ((InternalEObject)block).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.COMMUNICATION_NODE__BLOCK, null, msgs);
			if (newBlock != null)
				msgs = ((InternalEObject)newBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HtaskgraphPackage.COMMUNICATION_NODE__BLOCK, null, msgs);
			msgs = basicSetBlock(newBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.COMMUNICATION_NODE__BLOCK, newBlock, newBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInput() {
		CommunicationNodeType _type = this.getType();
		return Objects.equal(_type, CommunicationNodeType.INPUT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _number = this.getNumber();
		return ("ComTN" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final HTaskGraphVisitor visitor) {
		visitor.visitCommunicationNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS:
				return ((InternalEList<?>)getCommunications()).basicRemove(otherEnd, msgs);
			case HtaskgraphPackage.COMMUNICATION_NODE__BLOCK:
				return basicSetBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.COMMUNICATION_NODE__TYPE:
				return getType();
			case HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS:
				return getCommunications();
			case HtaskgraphPackage.COMMUNICATION_NODE__BLOCK:
				return getBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.COMMUNICATION_NODE__TYPE:
				setType((CommunicationNodeType)newValue);
				return;
			case HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS:
				getCommunications().clear();
				getCommunications().addAll((Collection<? extends HTGCom>)newValue);
				return;
			case HtaskgraphPackage.COMMUNICATION_NODE__BLOCK:
				setBlock((CompositeBlock)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.COMMUNICATION_NODE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS:
				getCommunications().clear();
				return;
			case HtaskgraphPackage.COMMUNICATION_NODE__BLOCK:
				setBlock((CompositeBlock)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.COMMUNICATION_NODE__TYPE:
				return type != TYPE_EDEFAULT;
			case HtaskgraphPackage.COMMUNICATION_NODE__COMMUNICATIONS:
				return communications != null && !communications.isEmpty();
			case HtaskgraphPackage.COMMUNICATION_NODE__BLOCK:
				return block != null;
		}
		return super.eIsSet(featureID);
	}

} //CommunicationNodeImpl
