/**
 */
package htaskgraph.impl;

import com.google.common.collect.Iterables;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.core.Symbol;

import htaskgraph.CommunicationNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getUse <em>Use</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getFrom <em>From</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getTo <em>To</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getSrcDependencies <em>Src Dependencies</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getDstDependencies <em>Dst Dependencies</em>}</li>
 *   <li>{@link htaskgraph.impl.TaskDependencyImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskDependencyImpl extends AnnotatedElementImpl implements TaskDependency {
	/**
	 * The cached value of the '{@link #getUse() <em>Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUse()
	 * @generated
	 * @ordered
	 */
	protected Symbol use;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected TaskNode from;

	/**
	 * The cached value of the '{@link #getSrcDependencies() <em>Src Dependencies</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDependency> srcDependencies;

	/**
	 * The cached value of the '{@link #getDstDependencies() <em>Dst Dependencies</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDstDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDependency> dstDependencies;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUse() {
		if (use != null && use.eIsProxy()) {
			InternalEObject oldUse = (InternalEObject)use;
			use = (Symbol)eResolveProxy(oldUse);
			if (use != oldUse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HtaskgraphPackage.TASK_DEPENDENCY__USE, oldUse, use));
			}
		}
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetUse() {
		return use;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUse(Symbol newUse) {
		Symbol oldUse = use;
		use = newUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_DEPENDENCY__USE, oldUse, use));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNode getFrom() {
		if (from != null && from.eIsProxy()) {
			InternalEObject oldFrom = (InternalEObject)from;
			from = (TaskNode)eResolveProxy(oldFrom);
			if (from != oldFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HtaskgraphPackage.TASK_DEPENDENCY__FROM, oldFrom, from));
			}
		}
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNode basicGetFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrom(TaskNode newFrom, NotificationChain msgs) {
		TaskNode oldFrom = from;
		from = newFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_DEPENDENCY__FROM, oldFrom, newFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(TaskNode newFrom) {
		if (newFrom != from) {
			NotificationChain msgs = null;
			if (from != null)
				msgs = ((InternalEObject)from).eInverseRemove(this, HtaskgraphPackage.TASK_NODE__SUCCS, TaskNode.class, msgs);
			if (newFrom != null)
				msgs = ((InternalEObject)newFrom).eInverseAdd(this, HtaskgraphPackage.TASK_NODE__SUCCS, TaskNode.class, msgs);
			msgs = basicSetFrom(newFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_DEPENDENCY__FROM, newFrom, newFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNode getTo() {
		if (eContainerFeatureID() != HtaskgraphPackage.TASK_DEPENDENCY__TO) return null;
		return (TaskNode)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskNode basicGetTo() {
		if (eContainerFeatureID() != HtaskgraphPackage.TASK_DEPENDENCY__TO) return null;
		return (TaskNode)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTo(TaskNode newTo, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTo, HtaskgraphPackage.TASK_DEPENDENCY__TO, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(TaskNode newTo) {
		if (newTo != eInternalContainer() || (eContainerFeatureID() != HtaskgraphPackage.TASK_DEPENDENCY__TO && newTo != null)) {
			if (EcoreUtil.isAncestor(this, newTo))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTo != null)
				msgs = ((InternalEObject)newTo).eInverseAdd(this, HtaskgraphPackage.TASK_NODE__PREDS, TaskNode.class, msgs);
			msgs = basicSetTo(newTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_DEPENDENCY__TO, newTo, newTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getSrcDependencies() {
		if (srcDependencies == null) {
			srcDependencies = new EObjectWithInverseResolvingEList.ManyInverse<TaskDependency>(TaskDependency.class, this, HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES, HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES);
		}
		return srcDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getDstDependencies() {
		if (dstDependencies == null) {
			dstDependencies = new EObjectWithInverseResolvingEList.ManyInverse<TaskDependency>(TaskDependency.class, this, HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES, HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES);
		}
		return dstDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HtaskgraphPackage.TASK_DEPENDENCY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternal() {
		boolean _xblockexpression = false;
		{
			if (((this.getFrom() == null) || (this.getTo() == null))) {
				throw new RuntimeException(("From/To tasks are not valid! " + this));
			}
			_xblockexpression = ((this.getFrom() instanceof CommunicationNode) || (this.getTo() instanceof CommunicationNode));
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getOriginalSourceNodes() {
		EList<TaskNode> _xblockexpression = null;
		{
			final EList<TaskDependency> srcDeps = this.getSrcDependencies();
			EList<TaskNode> _xifexpression = null;
			boolean _isEmpty = srcDeps.isEmpty();
			if (_isEmpty) {
				_xifexpression = ECollections.<TaskNode>singletonEList(this.getFrom());
			}
			else {
				final Function1<TaskDependency, EList<TaskNode>> _function = new Function1<TaskDependency, EList<TaskNode>>() {
					public EList<TaskNode> apply(final TaskDependency it) {
						return it.getOriginalSourceNodes();
					}
				};
				_xifexpression = ECollections.<TaskNode>unmodifiableEList(ECollections.<TaskNode>asEList(((TaskNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskNode>toSet(Iterables.<TaskNode>concat(XcoreEListExtensions.<TaskDependency, EList<TaskNode>>map(srcDeps, _function))), TaskNode.class))));
			}
			_xblockexpression = _xifexpression;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getFinalDestinationNodes() {
		EList<TaskNode> _xblockexpression = null;
		{
			final EList<TaskDependency> dstDeps = this.getDstDependencies();
			EList<TaskNode> _xifexpression = null;
			boolean _isEmpty = dstDeps.isEmpty();
			if (_isEmpty) {
				_xifexpression = ECollections.<TaskNode>singletonEList(this.getTo());
			}
			else {
				final Function1<TaskDependency, EList<TaskNode>> _function = new Function1<TaskDependency, EList<TaskNode>>() {
					public EList<TaskNode> apply(final TaskDependency it) {
						return it.getFinalDestinationNodes();
					}
				};
				_xifexpression = ECollections.<TaskNode>unmodifiableEList(ECollections.<TaskNode>asEList(((TaskNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskNode>toSet(Iterables.<TaskNode>concat(XcoreEListExtensions.<TaskDependency, EList<TaskNode>>map(dstDeps, _function))), TaskNode.class))));
			}
			_xblockexpression = _xifexpression;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getFirstInternalSrcDependencies() {
		boolean _isExternal = this.isExternal();
		boolean _not = (!_isExternal);
		if (_not) {
			return ECollections.<TaskDependency>singletonEList(this);
		}
		final EList<TaskDependency> srcDeps = this.getSrcDependencies();
		boolean _isEmpty = srcDeps.isEmpty();
		if (_isEmpty) {
			throw new RuntimeException("unhandeled case");
		}
		final Function1<TaskDependency, EList<TaskDependency>> _function = new Function1<TaskDependency, EList<TaskDependency>>() {
			public EList<TaskDependency> apply(final TaskDependency it) {
				return it.getFirstInternalSrcDependencies();
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>toSet(Iterables.<TaskDependency>concat(XcoreEListExtensions.<TaskDependency, EList<TaskDependency>>map(srcDeps, _function))), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getFirstInternalDstDependencies() {
		boolean _isExternal = this.isExternal();
		boolean _not = (!_isExternal);
		if (_not) {
			return ECollections.<TaskDependency>singletonEList(this);
		}
		final EList<TaskDependency> dstDeps = this.getDstDependencies();
		boolean _isEmpty = dstDeps.isEmpty();
		if (_isEmpty) {
			throw new RuntimeException("unhandeled case");
		}
		final Function1<TaskDependency, EList<TaskDependency>> _function = new Function1<TaskDependency, EList<TaskDependency>>() {
			public EList<TaskDependency> apply(final TaskDependency it) {
				return it.getFirstInternalDstDependencies();
			}
		};
		return ECollections.<TaskDependency>unmodifiableEList(ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>toSet(Iterables.<TaskDependency>concat(XcoreEListExtensions.<TaskDependency, EList<TaskDependency>>map(dstDeps, _function))), TaskDependency.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getFirstInternalSrcNodes() {
		final Function1<TaskDependency, TaskNode> _function = new Function1<TaskDependency, TaskNode>() {
			public TaskNode apply(final TaskDependency it) {
				return it.getFrom();
			}
		};
		return ECollections.<TaskNode>asEList(XcoreEListExtensions.<TaskDependency, TaskNode>map(this.getFirstInternalSrcDependencies(), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getFirstInternalDstNodes() {
		final Function1<TaskDependency, TaskNode> _function = new Function1<TaskDependency, TaskNode>() {
			public TaskNode apply(final TaskDependency it) {
				return it.getTo();
			}
		};
		return ECollections.<TaskNode>asEList(XcoreEListExtensions.<TaskDependency, TaskNode>map(this.getFirstInternalDstDependencies(), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getEffSrcDependencies() {
		final EList<TaskDependency> srcDeps = this.getSrcDependencies();
		boolean _isEmpty = srcDeps.isEmpty();
		boolean _not = (!_isEmpty);
		if (_not) {
			return srcDeps;
		}
		Symbol _use = this.getUse();
		boolean _tripleNotEquals = (_use != null);
		if (_tripleNotEquals) {
			final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
				public Boolean apply(final TaskDependency it) {
					return Boolean.valueOf(((it.getUse() != null) && (it.getUse() == TaskDependencyImpl.this.getUse())));
				}
			};
			return ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>toSet(IterableExtensions.<TaskDependency>filter(this.getFrom().getOutputNode().getPreds(), _function)), TaskDependency.class)));
		}
		return new BasicEList<TaskDependency>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getEffDstDependencies() {
		final EList<TaskDependency> dstDeps = this.getDstDependencies();
		boolean _isEmpty = dstDeps.isEmpty();
		boolean _not = (!_isEmpty);
		if (_not) {
			return dstDeps;
		}
		Symbol _use = this.getUse();
		boolean _tripleNotEquals = (_use != null);
		if (_tripleNotEquals) {
			final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
				public Boolean apply(final TaskDependency it) {
					return Boolean.valueOf(((it.getUse() != null) && (it.getUse() == TaskDependencyImpl.this.getUse())));
				}
			};
			return ECollections.<TaskDependency>asEList(((TaskDependency[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<TaskDependency>toSet(IterableExtensions.<TaskDependency>filter(this.getTo().getInputNode().getSuccs(), _function)), TaskDependency.class)));
		}
		return new BasicEList<TaskDependency>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getEffPreds() {
		final Function1<TaskDependency, TaskNode> _function = new Function1<TaskDependency, TaskNode>() {
			public TaskNode apply(final TaskDependency it) {
				return it.getFrom();
			}
		};
		return ECollections.<TaskNode>asEList(XcoreEListExtensions.<TaskDependency, TaskNode>map(this.getEffSrcDependencies(), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskNode> getEffSuccs() {
		final Function1<TaskDependency, TaskNode> _function = new Function1<TaskDependency, TaskNode>() {
			public TaskNode apply(final TaskDependency it) {
				return it.getTo();
			}
		};
		return ECollections.<TaskNode>asEList(XcoreEListExtensions.<TaskDependency, TaskNode>map(this.getEffDstDependencies(), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(HTaskGraphVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				if (from != null)
					msgs = ((InternalEObject)from).eInverseRemove(this, HtaskgraphPackage.TASK_NODE__SUCCS, TaskNode.class, msgs);
				return basicSetFrom((TaskNode)otherEnd, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTo((TaskNode)otherEnd, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSrcDependencies()).basicAdd(otherEnd, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDstDependencies()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				return basicSetFrom(null, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				return basicSetTo(null, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				return ((InternalEList<?>)getSrcDependencies()).basicRemove(otherEnd, msgs);
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				return ((InternalEList<?>)getDstDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				return eInternalContainer().eInverseRemove(this, HtaskgraphPackage.TASK_NODE__PREDS, TaskNode.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__USE:
				if (resolve) return getUse();
				return basicGetUse();
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				if (resolve) return getFrom();
				return basicGetFrom();
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				if (resolve) return getTo();
				return basicGetTo();
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				return getSrcDependencies();
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				return getDstDependencies();
			case HtaskgraphPackage.TASK_DEPENDENCY__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__USE:
				setUse((Symbol)newValue);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				setFrom((TaskNode)newValue);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				setTo((TaskNode)newValue);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				getSrcDependencies().clear();
				getSrcDependencies().addAll((Collection<? extends TaskDependency>)newValue);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				getDstDependencies().clear();
				getDstDependencies().addAll((Collection<? extends TaskDependency>)newValue);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__ID:
				setId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__USE:
				setUse((Symbol)null);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				setFrom((TaskNode)null);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				setTo((TaskNode)null);
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				getSrcDependencies().clear();
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				getDstDependencies().clear();
				return;
			case HtaskgraphPackage.TASK_DEPENDENCY__ID:
				setId(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HtaskgraphPackage.TASK_DEPENDENCY__USE:
				return use != null;
			case HtaskgraphPackage.TASK_DEPENDENCY__FROM:
				return from != null;
			case HtaskgraphPackage.TASK_DEPENDENCY__TO:
				return basicGetTo() != null;
			case HtaskgraphPackage.TASK_DEPENDENCY__SRC_DEPENDENCIES:
				return srcDependencies != null && !srcDependencies.isEmpty();
			case HtaskgraphPackage.TASK_DEPENDENCY__DST_DEPENDENCIES:
				return dstDependencies != null && !dstDependencies.isEmpty();
			case HtaskgraphPackage.TASK_DEPENDENCY__ID:
				return id != ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //TaskDependencyImpl
