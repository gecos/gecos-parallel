/**
 */
package htaskgraph.impl;

import htaskgraph.HtaskgraphPackage;
import htaskgraph.TaskTrueDependency;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task True Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TaskTrueDependencyImpl extends TaskDependencyImpl implements TaskTrueDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskTrueDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HtaskgraphPackage.Literals.TASK_TRUE_DEPENDENCY;
	}

} //TaskTrueDependencyImpl
