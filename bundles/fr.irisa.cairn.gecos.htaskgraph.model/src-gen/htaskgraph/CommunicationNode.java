/**
 */
package htaskgraph;

import gecos.blocks.CompositeBlock;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.CommunicationNode#getType <em>Type</em>}</li>
 *   <li>{@link htaskgraph.CommunicationNode#getCommunications <em>Communications</em>}</li>
 *   <li>{@link htaskgraph.CommunicationNode#getBlock <em>Block</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getCommunicationNode()
 * @model
 * @generated
 */
public interface CommunicationNode extends TaskNode {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link htaskgraph.CommunicationNodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see htaskgraph.CommunicationNodeType
	 * @see #setType(CommunicationNodeType)
	 * @see htaskgraph.HtaskgraphPackage#getCommunicationNode_Type()
	 * @model unique="false"
	 * @generated
	 */
	CommunicationNodeType getType();

	/**
	 * Sets the value of the '{@link htaskgraph.CommunicationNode#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see htaskgraph.CommunicationNodeType
	 * @see #getType()
	 * @generated
	 */
	void setType(CommunicationNodeType value);

	/**
	 * Returns the value of the '<em><b>Communications</b></em>' containment reference list.
	 * The list contents are of type {@link htaskgraph.HTGCom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communications</em>' containment reference list.
	 * @see htaskgraph.HtaskgraphPackage#getCommunicationNode_Communications()
	 * @model containment="true"
	 * @generated
	 */
	EList<HTGCom> getCommunications();

	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(CompositeBlock)
	 * @see htaskgraph.HtaskgraphPackage#getCommunicationNode_Block()
	 * @model containment="true"
	 * @generated
	 */
	CompositeBlock getBlock();

	/**
	 * Sets the value of the '{@link htaskgraph.CommunicationNode#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(CompositeBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%htaskgraph.CommunicationNodeType%&gt; _type = this.getType();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_type, &lt;%htaskgraph.CommunicationNodeType%&gt;.INPUT);'"
	 * @generated
	 */
	boolean isInput();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"ComTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCommunicationNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

} // CommunicationNode
