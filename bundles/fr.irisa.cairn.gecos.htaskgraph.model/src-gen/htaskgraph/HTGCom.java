/**
 */
package htaskgraph;

import gecos.blocks.BasicBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HTG Com</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getHTGCom()
 * @model abstract="true"
 * @generated
 */
public interface HTGCom extends BasicBlock {
} // HTGCom
