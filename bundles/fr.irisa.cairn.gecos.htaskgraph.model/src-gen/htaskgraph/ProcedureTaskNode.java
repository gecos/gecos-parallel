/**
 */
package htaskgraph;

import gecos.core.Procedure;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link htaskgraph.ProcedureTaskNode#getInputs <em>Inputs</em>}</li>
 *   <li>{@link htaskgraph.ProcedureTaskNode#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link htaskgraph.ProcedureTaskNode#getProcedure <em>Procedure</em>}</li>
 * </ul>
 *
 * @see htaskgraph.HtaskgraphPackage#getProcedureTaskNode()
 * @model
 * @generated
 */
public interface ProcedureTaskNode extends HierarchicalTaskNode {
	/**
	 * Returns the value of the '<em><b>Inputs</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.ProcedureTaskNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getProcedureTaskNode_Inputs()
	 * @model
	 * @generated
	 */
	EList<ProcedureTaskNode> getInputs();

	/**
	 * Returns the value of the '<em><b>Outputs</b></em>' reference list.
	 * The list contents are of type {@link htaskgraph.ProcedureTaskNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputs</em>' reference list.
	 * @see htaskgraph.HtaskgraphPackage#getProcedureTaskNode_Outputs()
	 * @model
	 * @generated
	 */
	EList<ProcedureTaskNode> getOutputs();

	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' reference.
	 * @see #setProcedure(Procedure)
	 * @see htaskgraph.HtaskgraphPackage#getProcedureTaskNode_Procedure()
	 * @model
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link htaskgraph.ProcedureTaskNode#getProcedure <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' reference.
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProcedureTaskNode(this);'"
	 * @generated
	 */
	void accept(HTaskGraphVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"PTN\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(L:\");\nint _depth = this.getDepth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_depth));\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

} // ProcedureTaskNode
