/**
 */
package htaskgraph;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Control Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getTaskControlDependency()
 * @model
 * @generated
 */
public interface TaskControlDependency extends TaskDependency {
} // TaskControlDependency
