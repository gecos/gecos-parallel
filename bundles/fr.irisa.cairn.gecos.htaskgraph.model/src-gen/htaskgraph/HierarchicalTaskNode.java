/**
 */
package htaskgraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hierarchical Task Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see htaskgraph.HtaskgraphPackage#getHierarchicalTaskNode()
 * @model abstract="true"
 * @generated
 */
public interface HierarchicalTaskNode extends TaskNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return a unmodifiable unique list of the direct (at level +1) child tasks of this.
	 * Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;();\nfinal &lt;%org.eclipse.emf.common.util.TreeIterator%&gt;&lt;&lt;%org.eclipse.emf.ecore.EObject%&gt;&gt; iter = this.getBlock().eAllContents();\nwhile (iter.hasNext())\n{\n\t{\n\t\t&lt;%org.eclipse.emf.ecore.EObject%&gt; elt = iter.next();\n\t\tif ((elt instanceof &lt;%htaskgraph.TaskNode%&gt;))\n\t\t{\n\t\t\tres.add(((&lt;%htaskgraph.TaskNode%&gt;)elt));\n\t\t}\n\t\tif ((!(elt instanceof &lt;%gecos.blocks.CompositeBlock%&gt;)))\n\t\t{\n\t\t\titer.prune();\n\t\t}\n\t}\n}\nfinal &lt;%org.eclipse.emf.common.util.TreeIterator%&gt;&lt;&lt;%org.eclipse.emf.ecore.EObject%&gt;&gt; iter2 = this.eAllContents();\nwhile (iter2.hasNext())\n{\n\t{\n\t\t&lt;%org.eclipse.emf.ecore.EObject%&gt; elt = iter2.next();\n\t\tif (((elt instanceof &lt;%htaskgraph.TaskNode%&gt;) &amp;&amp; (!res.contains(elt))))\n\t\t{\n\t\t\tres.add(((&lt;%htaskgraph.TaskNode%&gt;) elt));\n\t\t}\n\t\tif ((!(elt instanceof &lt;%gecos.blocks.CompositeBlock%&gt;)))\n\t\t{\n\t\t\titer2.prune();\n\t\t}\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<TaskNode> getChildTasks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return a unmodifiable unique list of the all (at all levels) child tasks of this.
	 * Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;&gt; _childTasks = this.getChildTasks();\nfor (final &lt;%htaskgraph.TaskNode%&gt; t : _childTasks)\n{\n\tif ((t instanceof &lt;%htaskgraph.HierarchicalTaskNode%&gt;))\n\t{\n\t\tres.addAll(((&lt;%htaskgraph.HierarchicalTaskNode%&gt;)t).getAllLevelsChildTasks());\n\t}\n\telse\n\t{\n\t\tres.add(t);\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<TaskNode> getAllLevelsChildTasks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A Source task is one that has either no predecessors or only this.inputNode as predecessor.
	 * 
	 * @return a unmodifiable unique list direct child sink tasks. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskNode%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((it.getPreds().isEmpty() || ((it.getPreds().size() == 1) &amp;&amp; &lt;%com.google.common.base.Objects%&gt;.equal(it.getPreds().get(0), it.getInputNode()))));\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(((&lt;%htaskgraph.TaskNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;filter(this.getChildTasks(), _function), &lt;%htaskgraph.TaskNode%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskNode> getSourceChildTasks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A sink task is one that has either no successors or only this.outputNode as successor.
	 * 
	 * @return a unmodifiable unique list direct child sink tasks. Never null.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%htaskgraph.TaskNode%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%htaskgraph.TaskNode%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((it.getSuccs().isEmpty() || ((it.getSuccs().size() == 1) &amp;&amp; &lt;%com.google.common.base.Objects%&gt;.equal(it.getSuccs().get(0), it.getOutputNode()))));\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;asEList(((&lt;%htaskgraph.TaskNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%htaskgraph.TaskNode%&gt;&gt;filter(this.getChildTasks(), _function), &lt;%htaskgraph.TaskNode%&gt;.class))));'"
	 * @generated
	 */
	EList<TaskNode> getSinkChildTasks();

} // HierarchicalTaskNode
