@GenModel(
	importerID="org.eclipse.emf.importer.ecore"
	,operationReflection="false"
//	,editDirectory="/fr.irisa.cairn.gecos.htaskgraph.model.edit/src-gen"
//	,editorDirectory="/fr.irisa.cairn.gecos.htaskgraph.model.editor/src-gen"
)
@Ecore(nsURI="http://www.gecos.org/htaskgraph")
package htaskgraph

import architecture.MemoryResource
import architecture.ProcessingResource
import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorsRegistry
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory
import gecos.annotations.AnnotatedElement
import gecos.blocks.BasicBlock
import gecos.blocks.Block
import gecos.blocks.BlocksVisitor
import gecos.blocks.CompositeBlock
import gecos.blocks.ControlEdge
import gecos.blocks.ForBlock
import gecos.blocks.IfBlock
import gecos.blocks.WhileBlock
import gecos.core.Procedure
import gecos.core.Symbol
import gecos.instrs.BranchType
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EObject
import gecos.blocks.SwitchBlock
import gecos.blocks.CaseBlock


interface HTaskGraphVisitable {
	op void accept(HTaskGraphVisitor visitor)
}

interface HTaskGraphVisitor {
	op void visitTaskDependency(TaskDependency td)
	op void visitLeafTaskNode(LeafTaskNode ltn)
	op void visitClusterTaskNode(ClusterTaskNode ctn)
	op void visitIfTaskNode(IfTaskNode ltn)
	op void visitForTaskNode(ForTaskNode ltn)
	op void visitWhileTaskNode(WhileTaskNode ltn)
	op void visitProcedureTaskNode(ProcedureTaskNode ltn)
	op void visitCommunicationNode(CommunicationNode td)
	op void visitBasicLeafTaskNode(BasicLeafTaskNode bl)
	op void visitDoWhileTaskNode(DoWhileTaskNode dwb)
	op void visitSwitchTaskNode(SwitchTaskNode stn)
	op void visitCaseTaskNode(CaseTaskNode ctn)
}

class TaskDependency extends AnnotatedElement , HTaskGraphVisitable {
	refers Symbol use
	refers TaskNode from opposite succs
	container TaskNode to opposite preds
	refers TaskDependency[] srcDependencies opposite dstDependencies
	refers TaskDependency[] dstDependencies opposite srcDependencies
	int ^id
	
	
	/*
	 * An External dependency is one that comes from or goes to a {@link CommunicationNode}.
	 * <br>
	 * An Internal dependency is thus one that connects two regular tasks (at the same level).
	 * <br>
	 * Internal means that the dependency originated from inside the parent level, whereas
	 * External indicate that it comes from outside the parent level and is conducted through
	 * via the parent's {@link CommunicationNode}.
	 * 
	 * @return true if this dependency is External, false if Internal.
	 */
	op boolean isExternal() {
		if(from === null || to === null)
			throw new RuntimeException("From/To tasks are not valid! " + this);
			
		from instanceof CommunicationNode || to instanceof CommunicationNode
	}
	
	/*
	 * Find the source node that originated this dependency by backtracking the
	 * srcDependencies chain.
	 * The original source node is the {@code from} of the last dependency in the chain
	 * (i.e. one with empty srcDependencies).
	 * 
	 * @return a unmodifiable unique list of the original source nodes of this dependency.  
	 */
	op unique TaskNode[] getOriginalSourceNodes() {
		val srcDeps = srcDependencies
		if(srcDeps.isEmpty) from.singletonEList
		else srcDeps.map[getOriginalSourceNodes].flatten.toSet.asEList.unmodifiableEList
	}
	
	/*
	 * Find the final destination node of this dependency by following the
	 * dstDependencies chain.
	 * The final destination node is the {@code to} of the last dependency in the chain
	 * (i.e. one with empty dstDependencies).
	 * 
	 * @return a unmodifiable unique list of the original source nodes of this dependency.  
	 */
	op unique TaskNode[] getFinalDestinationNodes() {
		val dstDeps = dstDependencies
		if(dstDeps.isEmpty) to.singletonEList
		else dstDeps.map[getFinalDestinationNodes].flatten.toSet.asEList.unmodifiableEList
	}
	
	/*
	 * @return a unmodifiable unique list. Never null
	 */
	op unique TaskDependency[] getFirstInternalSrcDependencies() {
		if(!isExternal())
			return this.singletonEList
		
		val srcDeps = srcDependencies
		if(srcDeps.isEmpty())
			throw new RuntimeException("unhandeled case"); //unknown source
			
		return srcDeps.map[firstInternalSrcDependencies].flatten.toSet.asEList.unmodifiableEList
	}

	/*
	 * @return a unmodifiable unique list. Never null
	 */
	op unique TaskDependency[] getFirstInternalDstDependencies() {
		if(!isExternal())
			return this.singletonEList
		
		val dstDeps = dstDependencies
		if(dstDeps.isEmpty())
			throw new RuntimeException("unhandeled case"); //unknown destination
			
		return dstDeps.map[firstInternalDstDependencies].flatten.toSet.asEList.unmodifiableEList
	}

	op unique TaskNode[] getFirstInternalSrcNodes() {
		firstInternalSrcDependencies.map[from].asEList
	}

	op unique TaskNode[] getFirstInternalDstNodes() {
		firstInternalDstDependencies.map[to].asEList
	}
	
	//XXX let src/dstDependencies be the effective ones ?
	op unique TaskDependency[] getEffSrcDependencies() {
		val srcDeps = srcDependencies
		if(!srcDeps.isEmpty())
			return srcDeps;
		
		if(use !== null) { 
			return from.outputNode.preds
				.filter[it.use !== null && it.use === this.use]
				.toSet.asEList
		}
		return new BasicEList
	}
	op unique TaskDependency[] getEffDstDependencies() {
		val dstDeps = dstDependencies
		if(!dstDeps.isEmpty())
			return dstDeps;
		
		if(use !== null) { 
			return to.inputNode.succs
				.filter[it.use !== null && it.use === this.use]
				.toSet.asEList
		}
		return new BasicEList
	}
	op unique TaskNode[] getEffPreds() {
		effSrcDependencies.map[from].asEList
	}
	op unique TaskNode[] getEffSuccs() {
		effDstDependencies.map[to].asEList
	}
}

class GlueTaskDependency extends TaskDependency{
}

class TaskOutputDependency extends TaskDependency {
}

class TaskAntiDependency extends TaskDependency {
}

class TaskTrueDependency extends TaskDependency {
}

class TaskControlDependency extends TaskDependency {
}

abstract class TaskNode extends Block , HTaskGraphVisitable {
	int depth
	refers TaskDependency[] succs opposite from
	contains TaskDependency[] preds opposite to
	contains CommunicationNode inputNode
	contains CommunicationNode outputNode
	contains ProfilingInformation[] profilingInformation
	refers unique ProcessingResource[] processingResources //XXX must be unique
	refers unique MemoryResource[] memoryResources

	
	op Block getBlock()
	
	op void addProcessingResource(ProcessingResource pr) {
		if(!processingResources.contains(pr))
			processingResources.add(pr)
	}
	
	/*
	 * Try to get the immediate parent task of this.
	 * That is the closest {@link HierarchicalTaskNode} in the container hierarchy of this task.
	 * 
	 * @return the parent Task or null if not found.
	 */
	op HierarchicalTaskNode getParentTask() {
		var EObject c = this
		while(c.eContainer() !== null) {
			c = c.eContainer
			if(c instanceof HierarchicalTaskNode) 
				return c
		}
		return null
	}
	
	/*
	 * Try to get the the parent task of this at the specified {@code level}.
	 * 
	 * @return the parent Task at level{@code level} or null if not found.
	 */
	op HierarchicalTaskNode getParentTask(int level) {
		if(depth < level) 
			return null
		var TaskNode parent = this
		while(parent.getParentTask() !== null) {
			parent = parent.getParentTask
			if(parent.depth == level) 
				return parent as HierarchicalTaskNode
		}
		return null
	}
	
	/*
	 * Return a unmodifiable list of the external input {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the external inputs. Never null.
	 */
	op unique TaskDependency[] getExternalInputs() {
		preds.filter[isExternal].asEList.unmodifiableEList
	}
	
	/*
	 * Return a unmodifiable list of the external output {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the external outputs. Never null.
	 */
	op unique TaskDependency[] getExternalOutputs() {
		succs.filter[isExternal].asEList.unmodifiableEList
	}
	
	/*
	 * Return a unmodifiable list of the internal input {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the internal inputs. Never null.
	 */	
	op unique TaskDependency[] getInternalInputs() {
		preds.filter[!isExternal].asEList.unmodifiableEList
	}
	
	/*
	 * Return a unmodifiable list of the internal output {@link TaskDependency}
	 * of this task.
	 * 
	 * @return unmodifiable list of the internal outputs. Never null.
	 */
	op unique TaskDependency[] getInternalOutputs() {
		succs.filter[!isExternal].asEList.unmodifiableEList
	}
	
	/*
	 * @return true if this task is scheduled on the specified ProcessingResource.
	 */
	op boolean isScheduledOn(ProcessingResource pr) {
		processingResources.contains(pr)
	}
	
	//--- Override Block ---
	
	op void accept(BlocksVisitor visitor) {
		if(visitor instanceof HTaskGraphVisitor)
			this.accept(visitor as HTaskGraphVisitor)
		else {
			val adapter = GecosVisitorsRegistry.INSTANCE.getBlocksAdapter(visitor, HTaskGraphVisitor)
			if (adapter === null) 
				super.accept(visitor)
			else
				this.accept(adapter)
		}
	}
	
	op ControlEdge[] connectTo(Block to, BranchType cond) {
		block.connectTo(to, cond)
	}
	
	op ControlEdge connectFromBasic(BasicBlock from, BranchType cond) {
		block.connectFromBasic(from, cond)
	}
	
	op String toShortString() {
		"Task" + number
	}
}

class CommunicationNode extends TaskNode {
	CommunicationNodeType ^type
	contains HTGCom[] communications
	contains CompositeBlock block
	
	op boolean isInput() {
		^type == CommunicationNodeType.INPUT
	}
	
	op String toString() {
//		if (eIsProxy()) return super.toString()
		"ComTN" + number
	}
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitCommunicationNode(this)
	}
}

enum CommunicationNodeType {
	INPUT = 0
	OUTPUT = 1
}

class LeafTaskNode extends TaskNode {
	contains Block block
	
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitLeafTaskNode(this)
	}
	
	op String toString() {
		"LTN" + number + "(L:" + depth + ")"
	}
}

class BasicLeafTaskNode extends LeafTaskNode, BasicBlock {
	op String toString() {
		"BasicLT" + number
	}
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitBasicLeafTaskNode(this)
	}
}


abstract class HierarchicalTaskNode extends TaskNode {
	
	/*
	 * @return a unmodifiable unique list of the direct (at level +1) child tasks of this. 
	 * Never null.
	 */
	op unique TaskNode[] getChildTasks() {
		val res = new BasicEList<TaskNode>
		val iter = getBlock().eAllContents();
		while(iter.hasNext()) {
			var elt = iter.next();
			if(elt instanceof TaskNode)
				res.add(elt);
			if(!(elt instanceof CompositeBlock)) //XXX only in case cluster tasks are not created !!
				iter.prune();
		}
		
		val iter2 = this.eAllContents();
		while(iter2.hasNext()) {
			var elt = iter2.next();
			if(elt instanceof TaskNode && !(res.contains(elt)))
				res.add(elt as TaskNode);
			if(!(elt instanceof CompositeBlock)) //XXX only in case cluster tasks are not created !!
				iter2.prune();
		}
		
		
		return res.unmodifiableEList
	}
	
	/*
	 * @return a unmodifiable unique list of the all (at all levels) child tasks of this. 
	 * Never null.
	 */
	op unique TaskNode[] getAllLevelsChildTasks() {
		var res = new BasicEList<TaskNode>
		for(t : childTasks) {
			if(t instanceof HierarchicalTaskNode)
				res.addAll(t.allLevelsChildTasks)
			else
				res.add(t)
		}
		return res.unmodifiableEList
	}
	
	/*
	 * A Source task is one that has either no predecessors or only this.inputNode as predecessor.
	 * 
	 * @return a unmodifiable unique list direct child sink tasks. Never null. 
	 */
	op unique TaskNode[] getSourceChildTasks() {
		childTasks.filter[preds.isEmpty || (preds.size() == 1 && preds.get(0) == inputNode)]
			.asEList.unmodifiableEList
	}
	
	/*
	 * A sink task is one that has either no successors or only this.outputNode as successor.
	 * 
	 * @return a unmodifiable unique list direct child sink tasks. Never null. 
	 */
	op unique TaskNode[] getSinkChildTasks() {
		childTasks.filter[succs.isEmpty || (succs.size() == 1 && succs.get(0) == outputNode)]
			.asEList.unmodifiableEList
	}
	
}

class ClusterTaskNode extends HierarchicalTaskNode {
	contains CompositeBlock block
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitClusterTaskNode(this)
	}
	
	op String toString() {
		"CTN" + number + "(L:" + depth + ")"
	}
	
	//--- Override  Block ---
	
	op void replace(Block oldBlock, Block newBlock) {
		if(oldBlock === getBlock()) {
			if(newBlock instanceof CompositeBlock)
				setBlock(newBlock)
			else
				setBlock(GecosUserBlockFactory.CompositeBlock(newBlock))
		}
		else
			throw new RuntimeException("Block " + oldBlock + " not contained in " + this)
	}
	
}

class IfTaskNode extends HierarchicalTaskNode {
	contains IfBlock block
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitIfTaskNode(this)
	}
	
	op BasicLeafTaskNode getTest() {
		return block.testBlock as BasicLeafTaskNode
	}
	
	op TaskNode getThen() {
		return block.thenBlock as TaskNode
	}
	
	op TaskNode getElse() {
		return block.elseBlock as TaskNode
	}
	
	op String toString() {
		"IfTN" + number + "(L:" + depth + ")"
	}
}

class SwitchTaskNode extends HierarchicalTaskNode {
	contains SwitchBlock block
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitSwitchTaskNode(this)
	}
	
	op String toString() {
		"SwitchTN" + number + "(L:" + depth + ")"
	}
}

class CaseTaskNode extends HierarchicalTaskNode {
	contains CaseBlock block
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitCaseTaskNode(this)
	}
	
	op String toString() {
		"CaseTN" + number + "(L:" + depth + ")"
	}
}

class ForTaskNode extends HierarchicalTaskNode {
	contains ForBlock block
	contains CommunicationNode loopIn
	contains CommunicationNode loopOut
	
	op BasicLeafTaskNode getStep() {
		return block.stepBlock as BasicLeafTaskNode
	}
	
	op BasicLeafTaskNode getInit() {
		for (td : this.internalInputs) {
			if (td instanceof GlueTaskDependency) {
				return td.from as BasicLeafTaskNode
			}
		}
		
		return block.initBlock as BasicLeafTaskNode
	}
	
	op BasicLeafTaskNode getTest() {
		return block.testBlock as BasicLeafTaskNode
	}
	
	op TaskNode getBody() {
		return block.bodyBlock as TaskNode
	}
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitForTaskNode(this)
	}

	op String toString() {
		"ForTN" + number + "(L:" + depth + ")"
	}
}

class WhileTaskNode extends HierarchicalTaskNode {
	contains WhileBlock block
	contains CommunicationNode loopIn
	contains CommunicationNode loopOut
	
	op BasicLeafTaskNode getTest() {
		return block.testBlock as BasicLeafTaskNode
	}
	
	op TaskNode getBody() {
		return block.bodyBlock as TaskNode
	}
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitWhileTaskNode(this)
	}
	
	op String toString() {
		return "WhileTN" + number + "(L:" + depth + ")"
	}
}

class DoWhileTaskNode extends HierarchicalTaskNode {
	contains WhileBlock block
	contains CommunicationNode loopIn
	contains CommunicationNode loopOut
	
	op BasicLeafTaskNode getTest() {
		return block.testBlock as BasicLeafTaskNode
	}
	
	op TaskNode getBody() {
		return block.bodyBlock as TaskNode
	}
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitDoWhileTaskNode(this)
	}
	
	op String toString() {
		return "DoWhileTN" + number + "(L:" + depth + ")"
	}
}



class ProcedureTaskNode extends HierarchicalTaskNode {
	refers ProcedureTaskNode[] inputs
	refers ProcedureTaskNode[] outputs
	refers Procedure procedure
	
	op void accept(HTaskGraphVisitor visitor) {
		visitor.visitProcedureTaskNode(this)
	}
	
	op String toString() {
		"PTN" + number + "(L:" + depth + ")"
	}
}


interface ProfilingInformation {
}

class CoreLevelWCET {
	String architecture
	long isolatedWCET
	long conflictWCET
}

class WCETInformation extends ProfilingInformation {
	contains CoreLevelWCET[] data
	
	op String toString() {
//		data.toString
		data.join(" ", [architecture + ":" + isolatedWCET])	
	}
}


abstract class HTGCom extends BasicBlock {
}

class HTGSend extends HTGCom {
	refers ProcessingResource[] fromProcResources
//	refers ProcessingResource[] toProcResources
	refers HTGRecv[] receives opposite sends
	refers Symbol use
	
	op ProcessingResource[] getToProcResources() {
		receives.map[toProcResources].flatten.asEList
	}
	
	op String toString() {
		"HTGSend " + use + " " + fromProcResources + " -> " + toProcResources
	}
}

class HTGRecv extends HTGCom {
//	refers ProcessingResource[] fromProcResources
	refers ProcessingResource[] toProcResources
	refers HTGSend[] sends opposite receives
	refers Symbol use
	
	op ProcessingResource[] getFromProcResources() {
		sends.map[fromProcResources].flatten.asEList
	}
	
	op String toString() {
		"HTGRecv " + use + " " + fromProcResources + " -> " + toProcResources
	}
}
