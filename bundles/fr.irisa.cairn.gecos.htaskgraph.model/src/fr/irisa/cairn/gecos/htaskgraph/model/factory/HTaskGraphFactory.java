package fr.irisa.cairn.gecos.htaskgraph.model.factory;

import java.util.Arrays;
import java.util.Collection;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Symbol;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.CommunicationNodeType;
import htaskgraph.CoreLevelWCET;
import htaskgraph.ForTaskNode;
import htaskgraph.GlueTaskDependency;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HtaskgraphFactory;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.TaskAntiDependency;
import htaskgraph.TaskControlDependency;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;
import htaskgraph.TaskOutputDependency;
import htaskgraph.TaskTrueDependency;
import htaskgraph.WCETInformation;
import htaskgraph.WhileTaskNode;

public class HTaskGraphFactory {

	private static HtaskgraphFactory _factory = HtaskgraphFactory.eINSTANCE;
	
	
	protected static CommunicationNode comInputNode(int depth) {
		CommunicationNode t = _factory.createCommunicationNode();
		t.setType(CommunicationNodeType.INPUT);
		t.setDepth(depth);
		t.setBlock(GecosUserBlockFactory.CompositeBlock());
		return t;
	}
	
	protected static CommunicationNode comOutputNode(int depth) {
		CommunicationNode t = _factory.createCommunicationNode();
		t.setType(CommunicationNodeType.OUTPUT);
		t.setDepth(depth);
		t.setBlock(GecosUserBlockFactory.CompositeBlock());
		return t;
	}
	
	public static LeafTaskNode leafTask(int depth) {
		LeafTaskNode ltn = _factory.createLeafTaskNode();
		ltn.setInputNode(comInputNode(depth+1));
		ltn.setOutputNode(comOutputNode(depth+1));
		ltn.setDepth(depth);
		ltn.setNumber(ltn.hashCode());
		return ltn;
	}
	
	public static LeafTaskNode leafTask(int depth, Block block){
		LeafTaskNode ltn = leafTask(depth);
		ltn.setBlock(block);
		return ltn;
	}
	
	public static BasicLeafTaskNode basicLeafTask(int depth) {
		BasicLeafTaskNode blt = _factory.createBasicLeafTaskNode();
		blt.setInputNode(comInputNode(depth+1));
		blt.setOutputNode(comOutputNode(depth+1));
		blt.setDepth(depth);
		blt.setNumber(blt.hashCode());
		blt.setBlock(GecosUserBlockFactory.BBlock());
		return blt;
	}
	
	public static ClusterTaskNode clusterTask(int depth) {
		ClusterTaskNode htn = _factory.createClusterTaskNode();
		htn.setInputNode(comInputNode(depth+1));
		htn.setOutputNode(comOutputNode(depth+1));
		htn.setDepth(depth);
		htn.setNumber(htn.hashCode());
		return htn;
	}
	
	public static ClusterTaskNode clusterTask(int depth, CompositeBlock cb){
		ClusterTaskNode cltn = clusterTask(depth);
		cltn.setBlock(cb);
		return cltn;
	}
	
	public static IfTaskNode ifTask(int depth) {
		IfTaskNode htn = _factory.createIfTaskNode();
		htn.setDepth(depth);
		htn.setNumber(htn.hashCode());
		htn.setInputNode(comInputNode(depth+1));
		htn.setOutputNode(comOutputNode(depth+1));
		return htn;
	}
	
	public static IfTaskNode ifTask(int depth, IfBlock ib) {
		IfTaskNode itn = ifTask(depth);
		itn.setBlock(ib);
		return itn;
	}
	
	public static ForTaskNode forTask(int depth) {
		ForTaskNode htn = _factory.createForTaskNode();
		htn.setDepth(depth);
		htn.setNumber(htn.hashCode());
		htn.setInputNode(comInputNode(depth+1));
		htn.setOutputNode(comOutputNode(depth+1));
		htn.setLoopIn(comInputNode(depth+1));
		htn.setLoopOut(comOutputNode(depth+1));
		return htn;
	}
	
	public static ForTaskNode forTask(int depth, ForBlock fb) {
		ForTaskNode ftn = forTask(depth);
		ftn.setBlock(fb);
		return ftn;
	}
	
	public static WhileTaskNode whileTask(int depth) {
		WhileTaskNode htn = _factory.createWhileTaskNode();
		htn.setDepth(depth);
		htn.setNumber(htn.hashCode());
		htn.setInputNode(comInputNode(depth+1));
		htn.setOutputNode(comOutputNode(depth+1));
		htn.setLoopIn(comInputNode(depth+1));
		htn.setLoopOut(comOutputNode(depth+1));
		return htn;
	}
	
	public static WhileTaskNode whileTask(int depth, WhileBlock wb){
		WhileTaskNode wtn = whileTask(depth);
		wtn.setBlock(wb);
		return wtn;
	}
	
	public static ProcedureTaskNode procedureTask(int depth) {
		ProcedureTaskNode tproc = _factory.createProcedureTaskNode();
		tproc.setDepth(depth);
		tproc.setNumber(tproc.hashCode());
		tproc.setInputNode(comInputNode(depth+1));
		tproc.setOutputNode(comOutputNode(depth+1));
		return tproc;
	}
	
	private static int taskDepId = 0; 
	public static TaskDependency taskDependency(TaskNode from, TaskNode to) {
		TaskDependency tdep = _factory.createTaskDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;	
	}
	
	public static TaskTrueDependency trueDependency(TaskNode from, TaskNode to) {
		TaskTrueDependency tdep = _factory.createTaskTrueDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;	
	}
	
	public static TaskTrueDependency trueDependency(TaskNode from, TaskNode to, Symbol use) {
		TaskTrueDependency tdep = trueDependency(from, to);
		tdep.setUse(use);
		return tdep;
	}
	
	public static GlueTaskDependency glueTaskDependency(TaskNode from, TaskNode to) {
		GlueTaskDependency tdep = _factory.createGlueTaskDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;
	}
	
	public static TaskAntiDependency antiDependency(TaskNode from, TaskNode to) {
		TaskAntiDependency tdep = _factory.createTaskAntiDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;
	}
	
	public static TaskOutputDependency outputDependency(TaskNode from, TaskNode to) {
		TaskOutputDependency tdep = _factory.createTaskOutputDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;
	}
	
	public static TaskControlDependency controlDependency(TaskNode from, TaskNode to) {
		TaskControlDependency tdep = _factory.createTaskControlDependency();
		tdep.setFrom(from);
		tdep.setTo(to);
		tdep.setId(taskDepId++);
		return tdep;
	}
	
	public static TaskDependency taskDependency(TaskNode from, TaskNode to, Symbol use) {
		TaskDependency tdep = taskDependency(from, to);
		tdep.setUse(use);
		return tdep;
	}
	
	public static HTGSend htgSend(Symbol use) {
		HTGSend s = _factory.createHTGSend();
		s.setUse(use);
		return s;
	}
	
	public static HTGSend htgSend(Symbol use, Collection<? extends ProcessingResource> senders) {
		HTGSend s = htgSend(use);
		s.getFromProcResources().addAll(senders);
		return s;
	}
	
	public static HTGRecv htgRecv(Symbol use) {
		HTGRecv r = _factory.createHTGRecv();
		r.setUse(use);
		return r;
	}
	
	public static HTGRecv htgRecv(Symbol use, Collection<? extends ProcessingResource> receivers) {
		HTGRecv r = htgRecv(use);
		r.getToProcResources().addAll(receivers);
		return r;
	}
	
	public static CoreLevelWCET coreLevelWCET(String architecture, long isolatedWCET, long conflictWCET){
		CoreLevelWCET clwcet = _factory.createCoreLevelWCET();
		clwcet.setArchitecture(architecture);
		clwcet.setIsolatedWCET(isolatedWCET);
		clwcet.setConflictWCET(conflictWCET);
		
		return clwcet;
	}
	
	public static WCETInformation wcetInformation(CoreLevelWCET ... coreLevelWCETs){
		WCETInformation info = _factory.createWCETInformation();
		info.getData().addAll(Arrays.asList(coreLevelWCETs));
		return info;
	}
	
}
