package fr.irisa.cairn.gecos.htaskgraph.model.utils;

import static java.util.stream.Collectors.toCollection;

import java.security.InvalidParameterException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.IAnnotation;
import gecos.core.Procedure;
import htaskgraph.HTGCom;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

public class HTGUtils {

	/**
	 * Compute the Set of all {@link ProcessingResource}s  used by all the children
	 * (@see {@link HierarchicalTaskNode#getAllLevelsChildTasks()} ) of the specified task. 
	 * @param n
	 * @return
	 */
	public static Set<ProcessingResource> getAllProcessingResourcesFor(HierarchicalTaskNode n) {
		Set<ProcessingResource> res = new LinkedHashSet<>(); 
		for (TaskNode c : n.getAllLevelsChildTasks()) {
			res.addAll(c.getProcessingResources());
			
			res.addAll(c.getInputNode().getProcessingResources());
			res.addAll(c.getOutputNode().getProcessingResources());
		}
		res.addAll(n.getInputNode().getProcessingResources());
		res.addAll(n.getOutputNode().getProcessingResources());
		
		return res;
	}
	
	/**
	 * @param p
	 * @return the set of all {@link ProcessingResource}s used by all tasks in the 
	 * specified procedure.
	 */
	public static Set<ProcessingResource> getAllProcessingResourcesFor(Procedure p) {
		//XXX 
		EList<HierarchicalTaskNode> hTasks = EMFUtils.eAllContentsInstancesOf(p.getBody(), HierarchicalTaskNode.class);
		return hTasks.stream()
			.flatMap(ht -> ht.getProcessingResources().stream())
			.collect(toCollection(LinkedHashSet::new));
	}
	
	/**
	 * @param com HTG communication
	 * @param procRes processing resource
	 * 
	 * @return true if com is a {@link HTGRecv} and has {@code procRes} as receiver
	 * or if com is a {@link HTGSend} and has {@code procRes} as sender
	 */
	public static boolean isScheduledOn(HTGCom com, ProcessingResource procRes) {
		if(com instanceof HTGRecv)
			return ((HTGRecv) com).getToProcResources().contains(procRes);
		else if(com instanceof HTGSend)
			return ((HTGSend) com).getFromProcResources().contains(procRes);
		
		throw new RuntimeException("Unknown HTGCom type: " + com);
	}
	
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//FIXME: If a loopTask uses a symbol in its control blocks (hidden in the fortask)
//	 		as well as in a child task.
//	 		In this case, for is also an effective final destination !!!!! 
	/**
	 * {@link TaskDependency#getOriginalSourceNodes()} does not cross the boundary of
	 * Loop tasks! i.e. a loop task is always the original source of all its output 
	 * dependencies (that are used by its control blocks)
	 * even if the actual original source is a child task of the loop!
	 * 
	 * This method go through Loop task boundaries to find the effective original sources.
	 * 
	 * @param dep
	 * @return unique list of effective original source nodes. Never null, never empty.
	 */
	public static EList<TaskNode> getEffectiveOriginalSourceNodes(TaskDependency dep) {
//				return dep.getOriginalSourceNodes();
		EList<TaskDependency> srcDep = dep.getEffSrcDependencies();
		Stream<TaskNode> res = srcDep.isEmpty() ? 
				Stream.of(dep.getFrom()) : 
				srcDep.stream().flatMap(d -> getEffectiveOriginalSourceNodes(d).stream());
		return res.collect(Collectors.toCollection(UniqueEList::new));
	}
		
	public static EList<TaskNode> getEffectiveFinalDestinationNodes(TaskDependency dep) {
//				return dep.getFinalDestinationNodes();
		EList<TaskDependency> dstDep = dep.getEffDstDependencies();
		Stream<TaskNode> res = dstDep.isEmpty() ? 
				Stream.of(dep.getTo()) : 
				dstDep.stream().flatMap(d -> getEffectiveFinalDestinationNodes(d).stream());
		return res.collect(Collectors.toCollection(UniqueEList::new));
	}
	
	
	/**
	 * Merge {@link TaskDependency} {@code d} with dependency {@code with} by:
	 * <li> moving {@code d}'s src/dst Dependencies to {@code with}.
	 * <li> removing {@code d} by invoking {@link #removeDep(TaskDependency)}.
	 * 
	 * @param d
	 * @param with
	 * @throws InvalidParameterException in case both tasks does not connect the same {@link TaskNode}s.
	 */
	public static void mergeDependency(TaskDependency d, TaskDependency with) {
		if(d.getFrom() != with.getFrom() || d.getTo() != with.getTo())
			throw new InvalidParameterException("Cannot merge task dependencies: they have different from/to. " + d + "  " + with);
		with.getDstDependencies().addAll(d.getDstDependencies());
		with.getSrcDependencies().addAll(d.getSrcDependencies());
		
		removeDep(d);
	}
	
	public static void removeDep(TaskDependency d) {
		d.setTo(null); 
		d.setFrom(null);
		d.getDstDependencies().clear();
		d.getSrcDependencies().clear();
	}

	
//	/**
//	 * Find the first non-{@link DependencyTaskNode} source of the specified dependency
//	 * and returns the corresponding {@link TaskDependency}.
//	 * The source task can then be obtained as the returned task dependency.getFrom().
//	 * 
//	 * @param dep
//	 * @return source dependency skipping {@link DependencyTaskNode}s.
//	 */
//	@Deprecated
//	public static Set<TaskDependency> getSourceDependencySkipDepNodes(TaskDependency dep) {
//		TaskNode src = dep.getFrom();
//		while(src instanceof DependencyTaskNode) { //dep is coming from an external source
//			HierarchicalTaskNode parent = src.getParentTask();
//			_assert(parent.getInputNode() == src);
//			
//			dep = findMatchingExternalInputDep(dep, parent);
//			src = dep.getFrom();
//		}
//		return dep;
//	}
	
//	/**
//	 * Find the first non-{@link CommunicationNode} destination of the specified dependency
//	 * and returns the corresponding {@link TaskDependency}.
//	 * The destination task can then be obtaibned as the returned task dependency.getFrom().
//	 * 
//	 * @param dep
//	 * @return destination dependency skipping {@link CommunicationNode}s.
//	 */
//	public static TaskDependency getDestDependencySkipDepNodes(TaskDependency dep) {
//		TaskNode dst = dep.getTo();
//		while(dst instanceof CommunicationNode) { //dep is coming from an external source
//			HierarchicalTaskNode parent = dst.getParentTask();
//			_assert(parent.getOutputNode() == dst);
//			
//			dep = findMatchingExternalOutputDep(dep, parent);
//			dst = dep.getFrom();
//		}
//		return dep;
//	}
//	
//	/**
//	 * Search the specified parent {@link HierarchicalTaskNode}'s input dependencies and returns
//	 * the source of the specified (internal) input dependency.
//	 * @param internalDep (internal) input dependency in {@code parent}
//	 * @param parent
//	 * @return {@code parent} 's input task dependency source of {@code internalDep} 
//	 */
//	public static Set<TaskDependency> findMatchingExternalInputDep(TaskDependency internalDep, HierarchicalTaskNode parent) {
//		Set<TaskDependency> matchingExternalInputDeps = parent.getPreds().stream()
//			.filter(in -> isMatchingDep(in, internalDep))
//			.collect(toCollection(LinkedHashSet::new));
//		return matchingExternalInputDeps;
//	}
//	
//	/**
//	 * Search the specified parent {@link HierarchicalTaskNode}'s output dependencies and returns
//	 * the destination of the specified (internal) output dependency.
//	 * @param internalDep (internal) output dependency in {@code parent}
//	 * @param parent
//	 * @return {@code parent} 's output task dependency destination of {@code internalDep} 
//	 */
//	public static TaskDependency findMatchingExternalOutputDep(TaskDependency internalDep, HierarchicalTaskNode parent) {
//		List<TaskDependency> matchingExternalOutputDeps = parent.getSuccs().stream()
//			.filter(out -> isMatchingDep(out, internalDep))
//			.collect(toList());
//		HTGUtils._assert(matchingExternalOutputDeps.size() == 1);
//		return matchingExternalOutputDeps.get(0);
//	}
//	
//	/**
//	 * Returns the internal output dependency in {@code src} matching the specified external output dependency.
//	 * 
//	 * @param externalDep (external) output task dependency of {@code src}
//	 * @param src
//	 */
//	public static TaskDependency findMatchingInternalOutputDep(TaskDependency externalDep, HierarchicalTaskNode src) {
//		HTGUtils._assert(src != null && src.getOutputNode() != null);
//		List<TaskDependency> matchingInternalOutputDeps = src.getOutputNode().getPreds().stream()
//			.filter(out -> isMatchingDep(out, externalDep))
//			.collect(toList());
//		HTGUtils._assert(matchingInternalOutputDeps.size() == 1); //XXX can it be empty for HTNs different than ClusterTN ??
//		return matchingInternalOutputDeps.get(0);
//	}

//	/**
//	 * Checks whether the specified task dependencies represent the same dependency
//	 * i.e. return true if one is the external dependency corresponding to the other (internal) dependency
//	 * 
//	 * TODO make this easier to identify by tracking the correspondence between 
//	 * external and internal dependencies when building the HTG.
//	 */
//	public static boolean isMatchingDep(TaskDependency d1, TaskDependency d2) {
//		EList<Symbol> uses1 = d1.getUses();
//		EList<Symbol> uses2 = d2.getUses();
//		return uses1.containsAll(uses2) && uses2.containsAll(uses1); //FIXME
//	}
//	
//	/**
//	 * Find the effective source of the specified {@link TaskDependency}
//	 * i.e. trace upward the first matching dependency originated from a source 
//	 * which is not a {@link DependencyTaskNode}, then trace inward, from there,
//	 * the matching dependency originated from a non- {@link ClusterTaskNode} source. 
//	 * 
//	 * @param dep
//	 * @return the original dependency corresponding to the specified one.
//	 * The Original source can thus be obtained by returned dep.getFrom().
//	 */
//	@Deprecated
//	public static TaskDependency traceOriginalDependencySource(TaskDependency dep) {
//		/* trace upward */
//		dep = HTGUtils.getSourceDependencySkipDepNodes(dep);
//		TaskNode src = dep.getFrom();
//		
//		/* trace inward */
//		while(src instanceof ClusterTaskNode) {
//			//lookup the corresponding internal dependency coming from src.outputNode
//			TaskDependency intDep = HTGUtils.findMatchingInternalOutputDep(dep, (HierarchicalTaskNode)src);
//			
//			dep = intDep;
//			src = dep.getFrom();
//		}
//		
//		return dep;
//	}
	
	public static final void _assert(boolean b) {
		_assert(b, "");
	}
	
	public static final void _assert(boolean b, String msg) {
		if(!b) throw new RuntimeException("Assertion failed: " + msg);
	}

	public static final String DUPLICATION_ANNOT_KEY = "HTG_UTILS::TASK_DUPLICATION_";
	public static void markAsDuplicatedForCommunicationOn(TaskNode task, ProcessingResource pr) {
		IAnnotation an = task.getAnnotations().get(DUPLICATION_ANNOT_KEY);
		ExtendedAnnotation x;
		if(an instanceof ExtendedAnnotation) {
			x = (ExtendedAnnotation) an;
		} else {
			x = GecosUserAnnotationFactory.extendedContains();
			task.getAnnotations().put(DUPLICATION_ANNOT_KEY, x);
		}
		
		x.getRefs().add(pr);
	}
	
	/**
	 * @param task
	 * @return the set of {@link ProcessingResource} were task was duplicated only to place communications. Never null.
	 */
	public static Set<ProcessingResource> getPRsWhereTaskIsDuplicatedForCommunication(TaskNode task) {
		IAnnotation an = task.getAnnotations().get(DUPLICATION_ANNOT_KEY);
		if(an instanceof ExtendedAnnotation)
			return ((ExtendedAnnotation) an).getRefs().stream()
					.map(ProcessingResource.class::cast).collect(Collectors.toSet());
		return new HashSet<>();
	}
	
	
}

