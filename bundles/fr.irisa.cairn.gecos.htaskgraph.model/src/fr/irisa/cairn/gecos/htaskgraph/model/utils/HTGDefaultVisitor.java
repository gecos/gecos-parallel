package fr.irisa.cairn.gecos.htaskgraph.model.utils;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CaseTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.DoWhileTaskNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.SwitchTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;
import htaskgraph.WhileTaskNode;

/**
 * Depth-first visitor
 */
public class HTGDefaultVisitor extends GecosBlocksDefaultVisitor implements HTaskGraphVisitor {

	public void visitTask(TaskNode task) {
		task.getBlock().accept(this);
	}

	public void visitHierarchicalTask(HierarchicalTaskNode task) {
		task.getInputNode().accept((HTaskGraphVisitor) this);
		visitTask(task);
		task.getOutputNode().accept((HTaskGraphVisitor) this);
	}

	@Override
	public void visitLeafTaskNode(LeafTaskNode task) {
		task.getInputNode().accept((HTaskGraphVisitor) this);
		visitTask(task);
		task.getOutputNode().accept((HTaskGraphVisitor) this);
	}

	@Override
	public void visitClusterTaskNode(ClusterTaskNode task) {
		visitHierarchicalTask(task);
	}

	@Override
	public void visitIfTaskNode(IfTaskNode task) {
		visitHierarchicalTask(task);
	}

	@Override
	public void visitForTaskNode(ForTaskNode task) {
		task.getInputNode().accept((HTaskGraphVisitor) this);
		task.getLoopIn().accept((HTaskGraphVisitor) this);
		visitTask(task);
		task.getOutputNode().accept((HTaskGraphVisitor) this);
		task.getLoopOut().accept((HTaskGraphVisitor) this);
	}

	@Override
	public void visitWhileTaskNode(WhileTaskNode task) {
		task.getInputNode().accept((HTaskGraphVisitor) this);
		task.getLoopIn().accept((HTaskGraphVisitor) this);
		visitTask(task);
		task.getOutputNode().accept((HTaskGraphVisitor) this);
		task.getLoopOut().accept((HTaskGraphVisitor) this);
	}

	@Override
	public void visitProcedureTaskNode(ProcedureTaskNode task) {
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public void visitTaskDependency(TaskDependency dep) {
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public void visitCommunicationNode(CommunicationNode com) {
		// com.getBlock().accept(this);
	}

	@Override
	public void visitBasicLeafTaskNode(BasicLeafTaskNode bl) {
		bl.getInputNode().accept((HTaskGraphVisitor)this);
		visitBasicBlock(bl);
		bl.getOutputNode().accept((HTaskGraphVisitor)this);
	}

	@Override
	public void visitDoWhileTaskNode(DoWhileTaskNode task) {
		task.getInputNode().accept((HTaskGraphVisitor) this);
		task.getLoopIn().accept((HTaskGraphVisitor) this);
		visitTask(task);
		task.getOutputNode().accept((HTaskGraphVisitor) this);
		task.getLoopOut().accept((HTaskGraphVisitor) this);
	}

	@Override
	public void visitSwitchTaskNode(SwitchTaskNode stn) {
		visitHierarchicalTask(stn);
	}

	@Override
	public void visitCaseTaskNode(CaseTaskNode ctn) {
		visitTask(ctn);
	}

}
