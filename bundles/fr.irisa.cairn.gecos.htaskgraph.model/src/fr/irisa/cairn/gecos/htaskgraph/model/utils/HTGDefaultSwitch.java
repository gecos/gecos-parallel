package fr.irisa.cairn.gecos.htaskgraph.model.utils;

import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.WhileTaskNode;
import htaskgraph.util.HtaskgraphSwitch;

public class HTGDefaultSwitch<T> extends HtaskgraphSwitch<T> {


	@Override
	public T caseHierarchicalTaskNode(HierarchicalTaskNode object) {
		doSwitch(object.getInputNode());
		T r = doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}
	
	@Override
	public T caseLeafTaskNode(LeafTaskNode object) {
		doSwitch(object.getInputNode());
		T r = doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}

	@Override
	public T caseCommunicationNode(CommunicationNode object) {
		//return caseBlock(object);
		return doSwitch(object.getBlock());
	}

	@Override
	public T caseBasicLeafTaskNode(BasicLeafTaskNode object) {
		doSwitch(object.getInputNode());
		caseBasicBlock(object);
		T r = doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}
	

	@Override
	public T caseForTaskNode(ForTaskNode object) {
		doSwitch(object.getLoopIn());
		T r = super.caseForTaskNode(object);
		doSwitch(object.getLoopOut());
		return r;
	}

	@Override
	public T caseWhileTaskNode(WhileTaskNode object) {
		doSwitch(object.getLoopIn());
		T r = super.caseWhileTaskNode(object);
		doSwitch(object.getLoopOut());
		return r;
	}
	
}
