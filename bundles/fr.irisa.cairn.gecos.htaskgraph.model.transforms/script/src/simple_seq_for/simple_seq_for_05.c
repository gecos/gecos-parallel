void simple_seq_for_05() {
	int x[256];
	int y[256];
	int a, b, c, i;

	for (i = 1; i < 128; i++) {
		x[i] = x[i] + x[i - 1];
	}

	for (i = 1; i < 128; i++) {
		y[i] = x[i];
	}
}
