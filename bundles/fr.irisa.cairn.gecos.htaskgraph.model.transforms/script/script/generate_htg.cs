debug(1);

p = CreateGecosProject("generate_htg"); # Create a empty GeCoS project
AddSourceToGecosProject(p,"../src/test4.c"); # Add new C sources in the project
CDTFrontend(p); # Generate the GeCoS IR from the C source added

output(p,"dot","./_output/dotty_cdfg/cdfg/");
ComputeSSAForm(p);
output(p,"dot","./_output/dotty_cdfg/ssa/");
HTGTaskExtractor(p);
FixBlockNumbering(p);
output(p,"dot","./_output/dotty_cdfg/htg/");
HTGDottyExport(p,"./_output/dotty_htg/before");