//package fr.irisa.cairn.gecos.htaskgraph.model.transforms.allocation
//
//import fr.irisa.cairn.gecos.htaskgraph.adapter.TaskGraphAdapterComponent
//import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter
//import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter
//import fr.irisa.cairn.gecos.htaskgraph.model.factory.HTaskGraphFactory
//import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory
//import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
//import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService
//import fr.irisa.cairn.tools.ecore.DanglingAnalyzer
//import fr.irisa.cairn.tools.ecore.query.EMFUtils
//import gecos.blocks.BasicBlock
//import gecos.blocks.Block
//import gecos.blocks.CompositeBlock
//import gecos.blocks.DoWhileBlock
//import gecos.blocks.ForBlock
//import gecos.blocks.WhileBlock
//import gecos.core.Procedure
//import gecos.core.ProcedureSet
//import gecos.core.Symbol
//import gecos.gecosproject.GecosProject
//import htaskgraph.HierarchicalTaskNode
//import htaskgraph.LeafTaskNode
//import htaskgraph.TaskNode
//import htaskgraph.TaskProcess
//import java.util.ArrayList
//import java.util.List
//import java.util.Stack
//
///**
// * This pass assign a random processor to each task in the GecosProject.
// * To be use as example/teaching.
// */
//class RandomMapper {
//	
//	var List<TaskProcess> processes = new ArrayList<TaskProcess>;
//	var List<Stack<CompositeBlock>> blockStack = new ArrayList<Stack<CompositeBlock>>;
//	val int MAXPROC = 3
////	val htaskgraphFactory factory = htaskgraphFactory::eINSTANCE;
//		
//		
//	val IGraphAnalysisService graphAnalysisService = TaskGraphAdapterComponent::getInstance().graphAnalysisService;
//	val TaskGraphAdapterComponent instance = TaskGraphAdapterComponent::getInstance();
//
//	GecosProject p;
//	
//	new(GecosProject _p) {
//		p=_p;
//	}
//	
//	def void compute() {
//		// generate random allocation
//		for(ps : p.listProcedureSets) {
//			
//			for(proc : new ArrayList(ps.listProcedures)) {
//				// Create TaskProcesses 
//				for(i : 0..MAXPROC-1) {
//					createProcess(proc,i)
//				} 
//				for(i : 0..MAXPROC-1) {
//					for(j : 0..MAXPROC-1) {
//						createLink(i,j);
//					}
//				}
//				
//				searchTaskRoot(proc)
//				
//				ps.scope.symbols.addAll(EMFUtils.searchInstancesOfInPath(ps, typeof(Symbol), typeof(Block)))	
//
//				ps.removeProcedure(proc);
//				ps.scope.symbols.remove(proc.symbol);
//
//				val DanglingAnalyzer analyzer = new DanglingAnalyzer(p);
//				analyzer.hasDangling(p);
//			}
//		}
//	}
//	
//	/*
//	 * Define Processor interconnect
//	 * 
//	 */
//	def void createLink(int i, int j) {
//		if(i!=j) {
//			processes.get(i).resource.links.add(processes.get(j).resource)	
//		}
//	}
//
//
//	def void createProcess(Procedure proc, int i) {
//		val stack  =new Stack<CompositeBlock>
//		val ps =(proc.eContainer as ProcedureSet);
//		var tp = HTaskGraphFactory::TaskProcess
//		ps.addProcedure(tp)
//
//		// Creating Processor
//		var pe = factory.createProcessingResource
//		pe.setName("PE"+i)
//		ps.scope.types.add(pe);		
//		tp.resource=pe;
//		
//		tp.symbol= proc.symbol.copy
//		ps.scope.symbols.add(tp.symbol)
//		
//		// Updating Task symbol and type
//		tp.symbol.setName(tp.symbol.name+"_"+i)
//		
//		// reconstructing Task body
//		val cb= GecosUserBlockFactory::CompositeBlock();
//		tp.start= GecosUserBlockFactory::BBlock();
//		tp.end= GecosUserBlockFactory::BBlock();
//		tp.symbol.scope = GecosUserCoreFactory::scope
//		tp.body=GecosUserBlockFactory::CompositeBlock(tp.start,cb,tp.end);
//					
//		stack.push(cb);
//		processes.add(tp)
//		blockStack.add(stack)
//	}
//	
//	
//	def searchTaskRoot(Procedure node) {
//		if(node.body instanceof HierarchicalTaskNode) {
//			schedule(node.body as HierarchicalTaskNode);
//		} else {
//			throw new RuntimeException("Not in HTG mode");
//		}
//	}
//
//	def void addProcess(int id, HierarchicalTaskNode node) {
//		(processes.get(id).body as CompositeBlock).children.add(node)
//	}
//	
//	
//
//	def dispatch void schedule(LeafTaskNode node) {
//		val int pid= (MAXPROC*(Math::random)) as int;
//		val stack = blockStack.get(pid)
//		println("Peek "+stack.peek.children)
//		stack.peek.children.add(node)
//		println("Mapping LeafTask "+node+" at depth " +node.depth+" on Processor "+pid)
//	}
//	
//	def int allocateProcessor(TaskNode node) {
//		val int pid= (MAXPROC*(Math::random)) as int;
//		node.processorContainer.add(processes.get(pid).resource);
//		pid
//	}
//
//	def dispatch void schedule(HierarchicalTaskNode node) {
//		val pid = allocateProcessor(node);
//		val stack = blockStack.get(pid)
//		
//		println("Mapping Task "+node+" at depth " +node.depth+" on Processor "+pid)
//		println("Peek "+stack.peek.children)
//		stack.peek.children.add(node)
//		if(node.children.size==1) {
//			val firstChild =node.children.get(0)
//			if(!(firstChild instanceof BasicBlock)) {
//				pushTaskContainer(firstChild,pid);
//				/** Create an adapter for the DAG at hand **/
//				var IHierarchicalTaskGraphAdapter adapter = instance.build(node);
//		
//				/** Topological sort **/
//				var	List<ITaskNodeAdapter> r = graphAnalysisService.topologicalSort(adapter);
//				for(child : r) {
//				 	schedule(child.getAdaptedNode)
//				}
//				stack.pop
//			}
//			
//		} else if(node.children.size>1) {
//			
//		} else {
//			println("Done for "+node)
//		}
//		println("Done for "+node)
//	
//					
//	}
//	
//	/*
//	 * 
//	 */	
//
//	def dispatch pushTaskContainer(Block b, int pid) { }
//
//	def dispatch pushTaskContainer(CompositeBlock b, int pid) {
//		val stack = blockStack.get(pid)
//		stack.push(b as CompositeBlock)
//	}
//
//	def dispatch pushTaskContainer(ForBlock b, int pid) {
//		val stack = blockStack.get(pid)
//		stack.push(b.bodyBlock as CompositeBlock)
//	}
//
//	def dispatch pushTaskContainer(WhileBlock b, int pid) {
//		val stack = blockStack.get(pid)
//		stack.push(b.bodyBlock as CompositeBlock)
//	}
//
//	def dispatch pushTaskContainer(DoWhileBlock b, int pid) {
//		val stack = blockStack.get(pid)
//		stack.push(b.getBodyBlock as CompositeBlock)
//	}
//
//
//}
