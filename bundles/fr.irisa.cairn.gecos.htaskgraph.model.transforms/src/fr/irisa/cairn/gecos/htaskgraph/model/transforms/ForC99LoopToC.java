package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.ForC99Block;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.SetInstruction;

/**
 * Transforms all ForC99 loops to C89 loops by moving all symbols in the scope
 * of the for-loop into the scope of the parent
 * 
 * @author oliver.oey
 *
 */
public class ForC99LoopToC extends BlocksDefaultSwitch<EObject> {
	public static final String WAS_C99_ANNOTATION = "WAS_C99_LOOP";
	
	private GecosProject proj;
	
	public ForC99LoopToC(GecosProject _p) {
		this.proj = _p;
	}
	
	public void compute() {
		doSwitch(proj);
	}

	@Override
	public EObject caseForC99Block(ForC99Block object) {
		if (object.getScope() != null) {
			Scope parentScope = object.getScope().getParent();
			
			ArrayList<Symbol> movedSym = new ArrayList<Symbol>();
			for (Symbol sym : object.getScope().getSymbols()) {
				if (sym.getValue() != null) {
					SetInstruction setInst = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(sym), sym.getValue());
					object.getInitBlock().addInstruction(setInst);
					sym.setValue(null);
					movedSym.add(sym);
				}
			}
			parentScope.getSymbols().addAll(movedSym);
			parentScope.getTypes().addAll(object.getScope().getTypes());
			
			object.setAnnotation(WAS_C99_ANNOTATION, GecosUserAnnotationFactory.STRING("true"));
		}
		
		return super.caseForC99Block(object);
	}
	
	

}
