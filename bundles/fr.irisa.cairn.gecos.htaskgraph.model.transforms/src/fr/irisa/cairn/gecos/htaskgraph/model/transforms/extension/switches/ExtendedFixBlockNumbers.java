package fr.irisa.cairn.gecos.htaskgraph.model.transforms.extension.switches;

import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultSwitch;
import fr.irisa.cairn.gecos.model.extensions.switchs.IAdaptableSwitch;
import fr.irisa.cairn.gecos.model.extensions.switchs.SwitchDelegate;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers.BlockNumberFixer;
import htaskgraph.CommunicationNode;
import htaskgraph.TaskNode;

public class ExtendedFixBlockNumbers extends HTGDefaultSwitch<Object> implements IAdaptableSwitch<Object> {

	private SwitchDelegate<Object> delegate = new SwitchDelegate<Object>();
	private BlockNumberFixer adaptable;

	public ExtendedFixBlockNumbers(FixBlockNumbers.BlockNumberFixer adaptable) {
		this.adaptable = adaptable;
	}
	
	@Override
	public SwitchDelegate<Object> getDelegate() {
		return delegate ;
	}

	@Override
	public Object caseTaskNode(TaskNode object) {
		doSwitch(object.getInputNode());
		Object r = adaptable.doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}

	@Override
	public Object caseCommunicationNode(CommunicationNode object) {
		adaptable.caseBlock(object);
		return object;
	}
	
}
