package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import java.util.List;
import java.util.Set;

import fr.irisa.cairn.gecos.htaskgraph.adapter.TaskGraphAdapterComponent;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.IHierarchicalTaskGraphAdapter;
import fr.irisa.cairn.gecos.htaskgraph.adapter.model.ITaskNodeAdapter;
import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;
import htaskgraph.HierarchicalTaskNode;

/**
 * C2SiliciumFlow is a sample module. When the script evaluator encounters the
 * 'C2SiliciumFlow' function, it calls the compute method.
 */
public class HierarchicalTaskGraphSplitter {

//	private String inArg;
	GecosProject p;
	IGraphAnalysisService graphAnalysisService;
	static TaskGraphAdapterComponent instance = null;

	public HierarchicalTaskGraphSplitter(GecosProject p) {
		this.p = p;
	}

	public void compute() {
		/** White magic code **/
		instance = TaskGraphAdapterComponent.getInstance();
		graphAnalysisService = instance.getGraphAnalysisService();

		for (Procedure proc : p.listProcedures()) {
			System.out.println("Procedure " + proc.getSymbol().getName());
			List<HierarchicalTaskNode> nodes = EMFUtils.eAllContentsInstancesOf(p,HierarchicalTaskNode.class);
			for (HierarchicalTaskNode bb : nodes) {
				splitHierarchicalTaskNode(bb);
			}
		}
	}

	private void splitHierarchicalTaskNode(HierarchicalTaskNode htaskNode) {
		System.out.println("\nSplit HierarchicalTaskNode " + htaskNode);
		if (htaskNode.getChildTasks().size() > 0) {
			/** Create an adapter for the DAG at hand **/
			IHierarchicalTaskGraphAdapter adapter = instance.build(htaskNode);

			/** Call the IGraph connected Set finder **/
			List<Set<ITaskNodeAdapter>> r = graphAnalysisService.findConnectedSets(adapter);
			System.out.println("HTaskNode " + htaskNode.toShortString() + " contains " + r.size() + " connected sets" );
			
			for (Set<ITaskNodeAdapter> set : r) {
				System.out.println("\tSet " + r.indexOf(set));
				for (ITaskNodeAdapter elt : set) {
					System.out.println("\t\t -" + elt.getAdaptedNode().toShortString());
				}

				// / TODO ALI

			}
			// / TODO ALI

		}
		System.out.println("End of Split HierarchicalTaskNode " + htaskNode);
	}

}