package fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty

import fr.irisa.r2d2.gecos.framework.GSModule
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor
import gecos.blocks.Block
import gecos.blocks.WhileBlock
import gecos.core.Procedure
import gecos.gecosproject.GecosProject
import htaskgraph.CommunicationNode
import htaskgraph.ForTaskNode
import htaskgraph.HTGCom
import htaskgraph.HTGRecv
import htaskgraph.HTGSend
import htaskgraph.IfTaskNode
import htaskgraph.LeafTaskNode
import htaskgraph.TaskNode
import java.io.File
import java.io.PrintStream
import java.util.HashMap
import htaskgraph.TaskDependency
import htaskgraph.WhileTaskNode

@GSModule("Generates a Graphiz DOT file representing an HTG.")
class TaskGraphDottyExport {
	
	val static String COLOR_HIERARCHICAL = "black"
	val static String COLOR_FOR = "blue"
	val static String COLOR_WHILE = "purple"
	val static String COLOR_IF = "orange"
	val static String COLOR_DEPEND_INTERNAL = "red"
	val static String COLOR_DEPEND_EXTERNAL = "blue"
	val static String COLOR_COM = "red"
	val static String COLOR_LEAF = "green"
	val static String COLOR_INVISIBLE = "white"
	
	val static String STYLE_HIERARCHICAL = "dashed"
	val static String STYLE_FOR = "solid"
	val static String STYLE_WHILE = "solid"
	val static String STYLE_IF = "solid"
	val static String STYLE_LEAF = "solid"
	val static String STYLE_COM = "solid"
	
	GecosProject project;
	String path;
	boolean verbose;
	
	@GSModuleConstructor("-arg1: the GecosProject containing the HTG to be exported as dot file.\n
						-arg2: the path representing the dot file to be generated.")
	new(GecosProject gProj, String dotFilePath, boolean verbose) {
		this.project = gProj
		this.path = dotFilePath
		this.verbose = verbose
	}
	 
	def compute() {
		var File dest = new File(path);
		if (dest.exists && dest.file)
			throw new RuntimeException()
		dest.mkdirs;
		
		project.listProcedures.forEach([e | generate(e)])
	}
	
	
	def generate(Procedure p) {
		var ps= new PrintStream(path+File::separatorChar+p.symbol.name+".dot")
		ps.append(
		'''
		digraph G {
			compound=true;
			rankdir=TB;
			
			«generateNodes(p.body)»
			
		}
		''');
		ps.close
	}

	def  dispatch String generateNodes(Block p) {
		'''
		«FOR c :p.eContents»
		«if (c instanceof Block) generateNodes(c as Block)» 
		«ENDFOR»
		'''
	}

	def dispatch String generateNodes(TaskNode t) {
		val id = t.number
		var String label
		var String color
		var String style
		var loopIn = (null as CommunicationNode);
		var loopOut = (null as CommunicationNode);
		
		if(t instanceof LeafTaskNode) {
			label = "LT"
			color = COLOR_LEAF
			style = TaskGraphDottyExport.STYLE_LEAF
		} else if(t instanceof ForTaskNode) {
			label = "FOR"
			color = COLOR_FOR
			style = TaskGraphDottyExport.STYLE_FOR
			loopIn = (t as ForTaskNode).loopIn;
			loopOut = (t as ForTaskNode).loopOut;
		} else if(t instanceof WhileTaskNode) {
			label = "WHILE"
			color = COLOR_WHILE
			style = TaskGraphDottyExport.STYLE_WHILE
			loopIn = (t as WhileTaskNode).loopIn;
			loopOut = (t as WhileTaskNode).loopOut;
		} else if(t instanceof IfTaskNode) {
			label = "IF"
			color = COLOR_IF
			style = TaskGraphDottyExport.STYLE_IF
		} else {
			label = "HT"
			color = COLOR_HIERARCHICAL
			style = STYLE_HIERARCHICAL
		} 
		
		
		val wcet = if (!t.getProfilingInformation.nullOrEmpty) '''WCET=«t.getProfilingInformation»'''
		val proc = printProcRes(t) 
		
		label += '''«id» L«t.depth» «proc» «wcet»'''
		
		
		'''
		subgraph cluster«id» {
			compound=true;
			style=«style»;
			color=«color»;
			label="«label»"
				
				task«id»[color="«COLOR_INVISIBLE»",label="",width=0,height=0];
				«IF t.inputNode !== null»«generateNodes(t.inputNode)»«ENDIF»
				«IF loopIn !== null»«generateNodes(loopIn)»«ENDIF»
				«generateNodes(t.block)»
				«IF loopOut !== null»«generateNodes(loopOut)»«ENDIF»
				«IF t.outputNode !== null»«generateNodes(t.outputNode)»«ENDIF»
		}
			«generateEdges(t)»
		'''
	}
	
	def printProcRes(TaskNode t) {
		if (!t.processingResources.nullOrEmpty) '''«t.processingResources»'''
	}
	
	def dispatch String generateNodes(CommunicationNode cn) {
		val id = cn.number
		val label = '''«id»«IF !cn.communications.isEmpty» «printProcRes(cn)»«ENDIF»«cn.communications.join("\\n", "\\n", "", [generateNodes])»'''
		val color = COLOR_COM
		val style = STYLE_COM
//		val shape = '''«IF cn.getType == CommunicationNodeType.INPUT»inv«ENDIF»triangle'''

		if(cn.preds.empty && cn.succs.empty && cn.communications.empty)
			''''''
		else
			'''
			subgraph cluster«id» {
				compound=true;
				style=«style»;
				color=«color»;
				label="«label»"
					
					task«id»[color="«COLOR_INVISIBLE»",label="",width=0,height=0];
			}
			«generateEdges(cn)»
			'''
	}
	
	def dispatch String generateNodes(HTGSend send) {
		val id = getUID(send)
		'''S«id» -> «send.receives.join("[R", ",R", "]", [getUID])»: («send.use») «send.fromProcResources» -> «send.toProcResources»'''
	}

	def dispatch String generateNodes(HTGRecv recv) {
		val id = getUID(recv)
		'''R«id» <- «recv.sends.join("[S", ",S", "]", [getUID])»: («recv.use») «recv.toProcResources» <- «recv.fromProcResources»'''
	}
	
	
	var int comId = 0
	val comId_map = new HashMap<HTGCom, Integer>
	def String getUID(HTGCom com) {
		var id = comId_map.get(com)
		if(id === null) {
			id = comId++
			comId_map.put(com, id)
		}
		'''«id»'''
	}

//	def generateCommunicationEdges(Send send) {
//		'''
//			«FOR Receive recv : send.receives»
//			Send«getUID(send)» -> Receive«getUID(recv)»[color="red",label="«send.message»"]; 
//			«ENDFOR»
//		'''
//	}

	//!!! xdot is throwing segmentation fault in some cases when using '\n' in labels !!! 
	def generateEdges(TaskNode to) {
		val buf = new StringBuffer
		to.preds.groupBy[from].forEach[from, deps| 
			val edge = '''task«from.number» -> task«to.number»[lhead=cluster«to.number»,ltail=cluster«from.number»'''
			if(verbose)
				for( d : deps)
					buf.append('''«edge»,color="«d.getColor»",label="«d.id»,«d.use.name»,«d.srcDependencies.map[id]»->«d.dstDependencies.map[id]»"];
					''')
			else
				buf.append('''«edge»,color="«deps.get(0).getColor»",label="«IF !deps.empty»«deps.map[use]»«ENDIF»"];
				''')
		]
		
	'''«buf.toString»'''
	}
	
	def getColor(TaskDependency dep) {
		if(dep.isExternal) TaskGraphDottyExport.COLOR_DEPEND_INTERNAL
		else TaskGraphDottyExport.COLOR_DEPEND_EXTERNAL
	}
}
