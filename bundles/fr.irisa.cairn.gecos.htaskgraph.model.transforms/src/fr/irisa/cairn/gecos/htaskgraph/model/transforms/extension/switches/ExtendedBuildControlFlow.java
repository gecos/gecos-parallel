package fr.irisa.cairn.gecos.htaskgraph.model.transforms.extension.switches;

import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultSwitch;
import fr.irisa.cairn.gecos.model.extensions.switchs.IAdaptableSwitch;
import fr.irisa.cairn.gecos.model.extensions.switchs.SwitchDelegate;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.LeafTaskNode;

public class ExtendedBuildControlFlow extends HTGDefaultSwitch<Object> implements IAdaptableSwitch<Object>{
	private SwitchDelegate<Object> delegate = new SwitchDelegate<Object>();
	private BuildControlFlow adaptable;
	
	public ExtendedBuildControlFlow(BuildControlFlow adaptable) {
		this.adaptable = adaptable;
	}
	
	@Override
	public SwitchDelegate<Object> getDelegate() {
		return delegate;
	}
	
	@Override
	public Object caseLeafTaskNode(LeafTaskNode object) {
		return adaptable.doSwitch(object.getBlock());
	}
	
	@Override
	public Object caseHierarchicalTaskNode(HierarchicalTaskNode object) {
		doSwitch(object.getInputNode());
		Object r = adaptable.doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}

	@Override
	public Object caseCommunicationNode(CommunicationNode object) {
		return adaptable.caseBlock(object);
	}

	@Override
	public Object caseBasicLeafTaskNode(BasicLeafTaskNode object) {
		doSwitch(object.getInputNode());
		Object r = adaptable.doSwitch(object.getBlock());
		doSwitch(object.getOutputNode());
		return r;
	}
	
	

}
