package fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorAdapter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotEdgePrinter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotPrinter;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CaseTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.DoWhileTaskNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.SwitchTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.WhileTaskNode;

public class ExtendedGecosDotEdgePrinter extends GecosVisitorAdapter<GecosDotEdgePrinter> implements HTaskGraphVisitor {

	private GecosDotPrinter printer;

	public ExtendedGecosDotEdgePrinter(GecosDotEdgePrinter visitor) {
		super(visitor);
		printer = visitor.getGecosDotPrinter();
	}

	public void printTaskDependency(TaskDependency pred) {
		int from = pred.getFrom().getNumber();
		int to = pred.getTo().getNumber();
		String prefix = ("task" + from + " -> task" + to);
		String tail ="";
		String head ="";
		if(pred.getFrom() instanceof HierarchicalTaskNode || pred.getFrom() instanceof LeafTaskNode) {
			prefix = "task" + from ;
			tail= "ltail=cluster" + from + ",";
//		} else if(pred.getFrom() instanceof LeafTaskNode){
//			prefix = "task" + from ;
//			tail= "ltail=leaf" + from + ",";
		} else {
			prefix = "n" + from ;
		}
		prefix +=  " -> " ;
		if(pred.getTo() instanceof HierarchicalTaskNode || pred.getTo() instanceof LeafTaskNode) {
			prefix += "task" + to ;
			head ="lhead=cluster" + to + ",";
//		} else if(pred.getTo() instanceof LeafTaskNode){
//			prefix = "task" + to ;
//			tail= "ltail=leaf" + to + ",";
		} else {
			prefix += "n" + to ;
		}


		if(pred.getUse() == null){
			prefix += "["+tail+head+"color=\"" + GecosDotPrinter.COLOR_LOOP
					+ "\" label=\"T" + from + "->T" + to
					+ "[ ]\"];\n";
		}
		else{
			prefix += "["+tail+head+"color=\"" + GecosDotPrinter.COLOR_LOOP
					+ "\" label=\"T" + from + "->T" + to
					+ "[" + pred.getUse().getName() + "]\"];\n";
		}
		printer.print(prefix);
	}

	@Override
	public void visitLeafTaskNode(LeafTaskNode b) {
		b.getBlock().accept(visitor);
		for (TaskDependency pred : b.getPreds()) {
			printTaskDependency(pred);
		}
	}

	@Override
	public void visitClusterTaskNode(ClusterTaskNode ctn) {
		visitHierarchicalTask(ctn);
	}

	private void visitHierarchicalTask(HierarchicalTaskNode ctn) {
		ctn.getInputNode().accept(visitor);
		ctn.getOutputNode().accept(visitor);
		ctn.getBlock().accept(visitor);
		for (TaskDependency pred : ctn.getPreds()) {
			printTaskDependency(pred);
		}
	}

	@Override
	public void visitIfTaskNode(IfTaskNode ltn) {
		visitHierarchicalTask(ltn);		
	}

	@Override
	public void visitForTaskNode(ForTaskNode ltn) {
		ltn.getInputNode().accept(visitor);
		ltn.getOutputNode().accept(visitor);
		ltn.getLoopIn().accept(visitor);
		ltn.getLoopOut().accept(visitor);
		ltn.getBlock().accept(visitor);
		for (TaskDependency pred : ltn.getPreds()) {
			printTaskDependency(pred);
		}
	}

	@Override
	public void visitWhileTaskNode(WhileTaskNode ltn) {
		ltn.getInputNode().accept(visitor);
		ltn.getOutputNode().accept(visitor);
		ltn.getLoopIn().accept(visitor);
		ltn.getLoopOut().accept(visitor);
		ltn.getBlock().accept(visitor);
		for (TaskDependency pred : ltn.getPreds()) {
			printTaskDependency(pred);
		}
	}

	@Override
	public void visitProcedureTaskNode(ProcedureTaskNode ltn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitTaskDependency(TaskDependency td) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitCommunicationNode(CommunicationNode td) {
		for (TaskDependency pred : td.getPreds()) {
			printTaskDependency(pred);
		}
		
	}

	@Override
	public void visitBasicLeafTaskNode(BasicLeafTaskNode bl) {
		for (TaskDependency pred : bl.getPreds()) {
			printTaskDependency(pred);
		}		
	}

	@Override
	public void visitDoWhileTaskNode(DoWhileTaskNode ltn) {
		ltn.getInputNode().accept(visitor);
		ltn.getOutputNode().accept(visitor);
		ltn.getLoopIn().accept(visitor);
		ltn.getLoopOut().accept(visitor);
		ltn.getBlock().accept(visitor);
		for (TaskDependency pred : ltn.getPreds()) {
			printTaskDependency(pred);
		}
	}

	@Override
	public void visitSwitchTaskNode(SwitchTaskNode stn) {
		visitHierarchicalTask(stn);		
	}

	@Override
	public void visitCaseTaskNode(CaseTaskNode ctn) {
		visitHierarchicalTask(ctn);
	}

}