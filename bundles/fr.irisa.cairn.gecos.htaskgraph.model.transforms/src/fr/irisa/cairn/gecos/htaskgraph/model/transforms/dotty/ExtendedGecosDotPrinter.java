package fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty;

import architecture.Resource;
import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorAdapter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotPrinter;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CaseTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.DoWhileTaskNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.SwitchTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.WhileTaskNode;
 
public class ExtendedGecosDotPrinter extends GecosVisitorAdapter<GecosDotPrinter> implements HTaskGraphVisitor {
 
	//XXX use same as TaskGraphDottyExport
	public static String COLOR_HIERARCHICAL = "red";
	public static String COLOR_LEAF = "dodgerblue";
	public static String COLOR_FOR = "green";
	public static String COLOR_IF = "orange";
	public static String COLOR_WHILE = "forestgreen";
	public static String COLOR_DTN = "purple";
	
	public ExtendedGecosDotPrinter(GecosDotPrinter visitor) {
		super(visitor);
	}

	@Override
	public void visitClusterTaskNode(ClusterTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_HIERARCHICAL + ";");		
		
		visitor.print("label=\"ClusterTask " + g.getNumber()+ " at level "+g.getDepth() + " "); 
		
		visitor.print(" resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
	}
	
	@Override
	public void visitForTaskNode(ForTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_FOR + ";");
		visitor.print("label=\"ForTask " + g.getNumber()+ " at level "+g.getDepth() + " resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
	}

	@Override
	public void visitLeafTaskNode(LeafTaskNode b) {
//		int number = b.getNumber();
//		visitor.println("");
//		visitor.print("n" + number + " [shape=box,color=\""+COLOR_LEAF+"\",label=\""  + "Leaf Task" + number  + " resources -> [");
//		visitor.printNoTabulation(" " + b.getProcessingResource());
//		visitor.printNoTabulation("]\"];\n");
		
		visitor.println("subgraph cluster" + b.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=solid;");
		visitor.println("color=" + COLOR_LEAF + ";");
		visitor.print("label=\"LeafTask " + b.getNumber()+ " at level "+b.getDepth() + " resources -> [");
		visitor.printNoTabulation(" " + b.getProcessingResources());
		visitor.printNoTabulation(" ]");
		
		if (b.getProfilingInformation() != null)
			visitor.print("WCET=" + b.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = b.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		b.getBlock().accept(visitor);
		visitor.printClusterTail();
	}

//	@Override
//	public void visitClusterTaskNode(ClusterTaskNode ctn) {	
//		visitor.println("subgraph cluster" + ctn.getNumber() + " {");
//		visitor.tabulateMore();
//		visitor.println("compound=true;");
//		visitor.println("style=solid;");
//		visitor.println("color=" + COLOR_CLUSTER + ";");
//		visitor.print("label=\"Cluster " + ctn.getNumber()+ " at level "+ctn.getDepth() + " resources -> [");
//		for(Resource resource : ctn.getResources()){
//			visitor.printNoTabulation(" " + resource);
//		}
//		visitor.printNoTabulation(" ]\";\n");
//		int number = ctn.getNumber();
//		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
//		for(Block child : ctn.getChildren()) {
//			child.accept(visitor);
//		}
//		visitor.printClusterTail();
//	}

	@Override
	public void visitIfTaskNode(IfTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_IF + ";");
		visitor.print("label=\"IFTask " + g.getNumber()+ " at level "+g.getDepth() + " resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
	}

	@Override
	public void visitWhileTaskNode(WhileTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_WHILE + ";");
		visitor.print("label=\"WhileTask " + g.getNumber()+ " at level "+g.getDepth() + " resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
	}

	@Override
	public void visitProcedureTaskNode(ProcedureTaskNode ltn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitTaskDependency(TaskDependency td) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitCommunicationNode(CommunicationNode b) {
		if(b.getPreds().isEmpty() && b.getSuccs().isEmpty())
			return;	
		
		int number = b.getNumber();
		visitor.println("");
		visitor.print("n" + number + " [shape=box,color=\""+COLOR_DTN+"\",label=\""  + "CommunicationNode" + number + " " + b.getType());
		visitor.printNoTabulation("\"];\n");
	}

	@Override
	public void visitBasicLeafTaskNode(BasicLeafTaskNode bl) {
		visitor.println("BasicLeafTaskNode currently not supported");		
	}

	@Override
	public void visitDoWhileTaskNode(DoWhileTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_WHILE + ";");
		visitor.print("label=\"DoWhileTask " + g.getNumber()+ " at level "+g.getDepth() + " resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
		
	}

	@Override
	public void visitSwitchTaskNode(SwitchTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_HIERARCHICAL + ";");		
		
		visitor.print("label=\"SwitchTask " + g.getNumber()+ " at level "+g.getDepth() + " "); 
		
		visitor.print(" resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
	}

	@Override
	public void visitCaseTaskNode(CaseTaskNode g) {
		visitor.println("subgraph cluster" + g.getNumber() + " {");
		visitor.tabulateMore();
		visitor.println("compound=true;");
		visitor.println("style=dashed;");
		visitor.println("color=" + COLOR_HIERARCHICAL + ";");		
		
		visitor.print("label=\"CaseTask " + g.getNumber()+ " at level "+g.getDepth() + " "); 
		
		visitor.print(" resources -> [");
		for(Resource resource : g.getProcessingResources()){
			visitor.printNoTabulation(" " + resource);
		}
		visitor.printNoTabulation(" ]");
		
		if (g.getProfilingInformation() != null)
			visitor.print("WCET=" + g.getProfilingInformation());
		visitor.printNoTabulation("\";\n");
		
		int number = g.getNumber();
		visitor.print("task" + number + " [color=\"white\",label=\"\"];\n");
		g.getInputNode().accept(visitor);
		g.getOutputNode().accept(visitor);
		g.getBlock().accept(visitor);
		visitor.printClusterTail();
		
	} 
}