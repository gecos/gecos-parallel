package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.model.analysis.forloops.ModelNormalForInformation;
import gecos.annotations.StringAnnotation;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;

public class ForCLoopToC99 extends HTGDefaultVisitor {
	private GecosProject proj;

	public ForCLoopToC99(GecosProject _p) {
		this.proj = _p;
	}

	public void compute() {
		for (Procedure proc : proj.listProcedures()) {
			visit(proc);
		}
	}

	@Override
	public void visitForBlock(ForBlock f) {
		super.visitForBlock(f);

		StringAnnotation anno = (StringAnnotation) f.getAnnotation(ForC99LoopToC.WAS_C99_ANNOTATION);
		if (anno != null && anno.getContent().equals("true")) {
			ModelNormalForInformation forInfo = new ModelNormalForInformation(f);
			Symbol index = forInfo.getIterationIndex();
			if (index != null) {
				for (Instruction inst : f.getInitBlock().getInstructions()) {
					if (inst instanceof SetInstruction) {
						f.getScope().addSymbol(index);
						index.setValue(((SetInstruction) inst).getSource());
						inst.getBasicBlock().removeInstruction(inst);
						break;
					}
				}
			}
		}

	}

}
