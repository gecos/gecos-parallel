package fr.irisa.cairn.gecos.htaskgraph.model.transforms.extension.visitors;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorAdapter;
import fr.irisa.cairn.gecos.model.transforms.ssa.SSAArrayUpdateRemover;
import gecos.blocks.BasicBlock;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.CaseTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.DoWhileTaskNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProcedureTaskNode;
import htaskgraph.SwitchTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.WhileTaskNode;

public class ExtendedRemoveSSA  extends GecosVisitorAdapter<SSAArrayUpdateRemover> implements HTaskGraphVisitor {

	public ExtendedRemoveSSA(SSAArrayUpdateRemover visitor) {
		super(visitor);
	}

	@Override
	public void visitClusterTaskNode(ClusterTaskNode ctn) {
		ctn.getBlock().accept(visitor);		
	}

	@Override
	public void visitIfTaskNode(IfTaskNode ltn) {
		ltn.getBlock().accept(visitor);		
	}

	@Override
	public void visitForTaskNode(ForTaskNode ltn) {
		ltn.getBlock().accept(visitor);		
	}

	@Override
	public void visitWhileTaskNode(WhileTaskNode ltn) {
		ltn.getBlock().accept(visitor);		
	}

	@Override
	public void visitProcedureTaskNode(ProcedureTaskNode ltn) {
		ltn.getBlock().accept(visitor);		
	}

	@Override
	public void visitCommunicationNode(CommunicationNode td) {
		
	}

	@Override
	public void visitTaskDependency(TaskDependency td) {
		
	}

	@Override
	public void visitLeafTaskNode(LeafTaskNode ltn) {
		ltn.getBlock().accept(visitor);		
	}

	@Override
	public void visitBasicLeafTaskNode(BasicLeafTaskNode bl) {
		((BasicBlock) bl).accept(visitor);		
	}

	@Override
	public void visitDoWhileTaskNode(DoWhileTaskNode dwb) {
		dwb.getBlock().accept(visitor);		
	}

	@Override
	public void visitSwitchTaskNode(SwitchTaskNode stn) {
		stn.getBlock().accept(visitor);		
	}

	@Override
	public void visitCaseTaskNode(CaseTaskNode ctn) {
		ctn.getBlock().accept(visitor);		
	}

}
