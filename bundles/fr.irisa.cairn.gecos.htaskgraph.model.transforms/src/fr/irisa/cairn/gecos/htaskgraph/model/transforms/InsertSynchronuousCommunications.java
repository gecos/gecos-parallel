package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;


/**
 * @author aelmouss
 */
public class InsertSynchronuousCommunications {

	private GecosProject gProj;

	public InsertSynchronuousCommunications(GecosProject gProj) {
		this.gProj = gProj;
	}
	
	public void compute() {
		gProj.listProcedures().forEach(this::insertCom);
		new FixBlockNumbers(gProj).compute();
	}
	
	private void insertCom(Procedure p) {
		new SynchComInsertor().compute(p);
	}
	
}
