package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import java.util.Set;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGUtils;
import htaskgraph.ForTaskNode;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.IfTaskNode;

/**
 * 
 * @author aelmouss
 */
public class PropagatePRsOutwardDuplicateControl extends HTGDefaultVisitor {
		
	@Override
	public void visitHierarchicalTask(HierarchicalTaskNode htn) {
		super.visitHierarchicalTask(htn);
		Set<ProcessingResource> resources = HTGUtils.getAllProcessingResourcesFor(htn);
		// control tasks are duplicated on all its children processes
		if(isControlTask(htn))
			htn.getProcessingResources().clear();
		htn.getProcessingResources().addAll(resources);
	}

	private boolean isControlTask(HierarchicalTaskNode htn) {
		return htn instanceof IfTaskNode || htn instanceof ForTaskNode;
	}
	
}