package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import architecture.Architecture;
import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.factory.HTaskGraphFactory;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGUtils;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.cairn.gecos.model.transforms.ssa.BlockWrapper;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.CoreFactory;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.BranchType;
import gecos.instrs.Instruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import htaskgraph.BasicLeafTaskNode;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;
import htaskgraph.WhileTaskNode;


@GSModule("Converts a CDFG into a Hierarchical Task Graph (HTG) structure.")
public class HTGExtraction extends BlocksDefaultSwitch<Block> {
	public static final String TASK_MAPPING_PRAGMA = "GECOS_SCHEDULER_PROCESS";
	
	private static final boolean VERBOSE = false;
	private static final boolean GEN_ALL_CLUSTERS = true; // false is experimental !!!
	/*
	 * MERGE_DEPENDENCIES_BY_USE = true: 
	 * keep only one TaskDependecy for a given USE between 2 tasks 
	 * (i.e. merge multiple dependencies with the same use)
	 */
	private static final boolean MERGE_DEPENDENCIES_BY_USE = true;
				
	private static void debug(Object o) {
		if (VERBOSE) System.out.println(o);
	}
	
	private GecosProject p;
	private int depth = 0;
	private Architecture arch;


	@Deprecated
	@GSModuleConstructor("-arg1: the GecosProject containing the CDFG to be converted.")
	public HTGExtraction(GecosProject p) {
		this(p, null);
	}
	
	@GSModuleConstructor("-arg1: the GecosProject containing the CDFG to be converted.\n"
			+ "-arg2: Parallel Architecture Model. (e.g. see Modlue 'CreateDummyParallelArchitecture').")
	public HTGExtraction(GecosProject p, Architecture arch) {
		this.p = p;
		this.arch = arch;
		
		//XXX contain arch in project containment structure to avoid dangling references
		p.getAnnotations().put("HTGScheduler::Architecture", GecosUserAnnotationFactory.extendedContains(arch));
	}

	public void compute() {
		for (Procedure proc : p.listProcedures()) {
			debug("Procedure " + proc.getSymbol().getName());
			
			CompositeBlock oldBody = proc.getBody();
			if(oldBody.getChildren().size() > 2 && oldBody.getChildren().get(0) == proc.getStart() &&
					oldBody.getChildren().get(1) instanceof CompositeBlock)
				oldBody = (CompositeBlock) oldBody.getChildren().get(1);
			
			/* Build task nodes */
			doSwitch(oldBody);
			new FixBlockNumbers(p).compute(); //TODO try not to change old block numbers for debugging purposes

			/* Add task dependencies */
			for (BasicBlock bb : proc.getBasicBlocks()) {
				if (bb instanceof BasicLeafTaskNode)
					continue;
				
				debug("Analyzing " + bb + " " + bb.getUseEdges().size() + "/" + bb.getOutEdges().size());

				// Create a dependency for each (input) use dependency by the BB
				for (DataEdge de : bb.getUseEdges()) {
					if (de.getFrom() instanceof BasicLeafTaskNode)
						continue;
					buildTaskDependencies(bb, de.getFrom(), new BasicEList<SymbolInstruction>(de.getUses()));
				}
				
				// Create a dependency for each conditional branch into the BB
				for (ControlEdge ce : bb.getInEdges()) {
					if (ce.getFrom() instanceof BasicLeafTaskNode)
						continue;
					if(ce.getCond() != BranchType.UNCONDITIONAL)
						buildTaskDependencies(bb, ce.getFrom(),null);
				}
			}
			
			/* Combine all dependencies with the same use between 2 tasks into 1 */
			if(MERGE_DEPENDENCIES_BY_USE) {
				proc.getBody().accept(new GroupDependenciesByUse());
			}
		}
	}
	
	/**
	 * This methods creates a TaskDependency and makes sure the dependency occurs 
	 * between tasks at the same depth.
	 */
	private void buildTaskDependencies(final BasicBlock dstBB, final BasicBlock srcBB, final EList<SymbolInstruction> uses) {
		debug("Build dependencies from " + srcBB.toShortString() + " to " + dstBB.toShortString() + " (uses = " + uses +")");
		
		TaskNode origSrcTask = findEnclosingTaskOf(srcBB);
		TaskNode origDstTask = findEnclosingTaskOf(dstBB);
		if (origSrcTask != null && origDstTask != null)
			buildDepChainBetween(origSrcTask, origDstTask, uses);
	}
	
	private final Symbol DUMMY_NULL_USE_SYMBOL_KEY = CoreFactory.eINSTANCE.createSymbol(); // Dummy Symbol

	
	private void buildDepChainBetween(final TaskNode origSrcTask, final TaskNode origDstTask, final EList<SymbolInstruction> uses) {
		debug("\torigSrcTask = " + origSrcTask);
		debug("\torigDstTask = " + origDstTask);
		
		if(origSrcTask == origDstTask) {
			debug("\t-Ignoring: self dependdency.");
			return;
		}
		
		/*
		 * Find the outermost tasks which can carry the dependency 
		 * (i.e. they must be at the same depth).
		 */
		TaskNode srcTask = origSrcTask;
		TaskNode dstTask = origDstTask;
		int commonDepth = Math.min(dstTask.getDepth(), srcTask.getDepth());
		if(srcTask.getDepth() != commonDepth)
			srcTask = srcTask.getParentTask(commonDepth);
		if(dstTask.getDepth() != commonDepth)
			dstTask = dstTask.getParentTask(commonDepth);
		
		while(srcTask != dstTask && srcTask.getParentTask(commonDepth-1) != dstTask.getParentTask(commonDepth-1)) {
		    commonDepth--;
		    srcTask = srcTask.getParentTask(commonDepth);
		    dstTask = dstTask.getParentTask(commonDepth);
		}
		
		//usedSymbols must be unique !!!
		List<Symbol> usedSymbols = getUsedSymbols(uses);
		
		debug("\tDeepest outer common depth = " + commonDepth + ": outerSrcTask = " + srcTask + ", outerDstTask = " + dstTask + ", used symbols = " + usedSymbols);
		
		if (srcTask != dstTask && dstTask != null && srcTask != null) {
			debug("\t-Adding dependency " + srcTask + " to " + dstTask+ " inset="+((uses==null)?"":uses));
			
			/* Create a Task dependency per use at outer commonDepth */
			Map<Symbol, TaskDependency> taskDeps = new LinkedHashMap<>();
			if(usedSymbols == null) // add an empty dependency XXX ?
				taskDeps.put(DUMMY_NULL_USE_SYMBOL_KEY, HTaskGraphFactory.taskDependency(srcTask, dstTask));
			else for(Symbol u : usedSymbols)
				taskDeps.put(u, HTaskGraphFactory.taskDependency(srcTask, dstTask, u));
			
			/* Add/connect dependencies chain from 'origSrcTask' up to 'srcTask' */
			if(srcTask != origSrcTask) {
				Map<Symbol, TaskDependency> previousDeps = addOutputDepChainUpToParent(origSrcTask, srcTask, usedSymbols);
				taskDeps.forEach((u,d) -> {
					TaskDependency dep = previousDeps.get(u);
					if(dep != null) d.getSrcDependencies().add(dep);
				});
			}
			
			/* Add/connect dependencies chain from dstTask down to origDstTask */
			if(dstTask != origDstTask) {
				Map<Symbol, TaskDependency> previousDeps = addInputDepChainDownToChild(dstTask, origDstTask, usedSymbols);
				taskDeps.forEach((u,d) -> {
					TaskDependency dep = previousDeps.get(u);
					if(dep != null) d.getDstDependencies().add(dep);
				});
			}
		} else { // srcTask == dstTask (&& origSrcTask != origDstTask)
			if(uses == null || uses.isEmpty()) {
				//FIXME do we need to add a task dependency in this case (merge with else) ???  
				debug("\t-Ignoring self control dependdency from " + origSrcTask + " to " + origDstTask);
			} else {
				if(dstTask != origDstTask) {
					debug("\t-Adding an input intra-cluster dependency chain from " + dstTask + " down to " + origDstTask + " uses: " + uses);
					/*Map<Symbol, TaskDependency> previousDeps = */addInputDepChainDownToChild(dstTask, origDstTask, usedSymbols);
					//TODO find/connect source dependencies of previousDeps ???
				}
				if(srcTask != origSrcTask) {
					debug("\t-Adding an output intra-cluster dependency chain from " + origSrcTask + " up to " + srcTask + " uses: " + uses);
					/*Map<Symbol, TaskDependency> previousDeps = */addOutputDepChainUpToParent(origSrcTask, srcTask, usedSymbols);
					//TODO find/connect dest dependencies of previousDeps ???
				}
			}
		}
	}

	/**
	 * Add/connect input dependencies chain from {@code parentDstTask} down to its
	 * children (not necessarily at depth-1) {@code childDstTask}.
	 * The chain is build starting from {@code childDstTask} to its direct parent
	 * (through its input {@link CommunicationNode}) until reaching {@code parentDstTask}.
	 * <p>
	 * At each link in the dependency chain, one separate {@link TaskDependency} 
	 * is added per use, in case {@code usedSymbols} is not null or empty, 
	 * otherwise only one (symbol-less) {@link TaskDependency} is added.
	 * 
	 * @param parentDstTask
	 * @param childDstTask
	 * @param usedSymbols
	 * @return the list of all created OUTERMOST dependencies (i.e. from 
	 * {@code parentDstTask} to its direct child in the the chain). Never {@code null}.
	 */
	private Map<Symbol, TaskDependency> addInputDepChainDownToChild(final TaskNode parentDstTask, final TaskNode childDstTask, final List<Symbol> usedSymbols) {
		TaskNode dest = childDstTask;
		TaskNode source = childDstTask.getParentTask();
		Map<Symbol, TaskDependency> previousDeps = new LinkedHashMap<>();
		while(source != null) {
			debug("\t\t-Adding intra-cluster dependencies " + source + " to " + dest + " uses = "+((usedSymbols==null)?"":usedSymbols));
			
			Map<Symbol, TaskDependency> tmp = new LinkedHashMap<>();
			if(usedSymbols == null || usedSymbols.isEmpty()) {
				TaskDependency dep = HTaskGraphFactory.taskDependency(source.getInputNode(), dest);
				tmp.put(DUMMY_NULL_USE_SYMBOL_KEY, dep);
				TaskDependency dstDep = previousDeps.get(DUMMY_NULL_USE_SYMBOL_KEY);
				if(dstDep != null)
					dep.getDstDependencies().add(dstDep);
			} else for(Symbol u : usedSymbols) {
				TaskDependency dep = HTaskGraphFactory.taskDependency(source.getInputNode(), dest, u);
				tmp.put(u, dep);
				TaskDependency dstDep = previousDeps.get(u);
				if(dstDep != null)
					dep.getDstDependencies().add(dstDep);
			}
			previousDeps = tmp;
			
			if(source == parentDstTask)
				break;
			dest = source;
			source = source.getParentTask();
		}
		return previousDeps;
	}

	/**
	 * Add/connect output dependencies chain from {@code childSrcTask} up to its
	 * parent (not necessarily at depth+1) {@code parentSrcTask}.
	 * The chain is build starting from {@code childSrcTask} to its direct parent
	 * (through its output {@link CommunicationNode}) until reaching {@code parentSrcTask}.
	 * <p>
	 * At each link in the dependency chain, one separate {@link TaskDependency} 
	 * is added per use, in case {@code usedSymbols} is not null or empty, 
	 * otherwise only one (symbol-less) {@link TaskDependency} is added.
	 * 
	 * @param childSrcTask
	 * @param parentSrcTask
	 * @param usedSymbols list of UNIQUE symbols. May be null or empty.
	 * @return the list of all created OUTERMOST dependencies (i.e. to 
	 * {@code parentSrcTask} from its direct child in the the chain). Never {@code null}.
	 */
	private Map<Symbol, TaskDependency> addOutputDepChainUpToParent(final TaskNode childSrcTask, final TaskNode parentSrcTask, final List<Symbol> usedSymbols) {
		TaskNode source = childSrcTask;
		TaskNode dest = childSrcTask.getParentTask();
		Map<Symbol, TaskDependency> previousDeps = new LinkedHashMap<>();
		while(dest != null) {
			debug("\t\t-Adding intra-cluster dependencies " + source + " to " + dest + " uses = " + ((usedSymbols==null)?"":usedSymbols));

			Map<Symbol, TaskDependency> tmp = new LinkedHashMap<>();
			if(usedSymbols == null || usedSymbols.isEmpty()) {
				TaskDependency dep = HTaskGraphFactory.taskDependency(source, dest.getOutputNode());
				tmp.put(DUMMY_NULL_USE_SYMBOL_KEY, dep);
				TaskDependency srcDep = previousDeps.get(DUMMY_NULL_USE_SYMBOL_KEY);
				if(srcDep != null)
					dep.getSrcDependencies().add(srcDep);
			} else for(Symbol u : usedSymbols) {
				TaskDependency dep = HTaskGraphFactory.taskDependency(source, dest.getOutputNode(), u);
				tmp.put(u, dep);
				TaskDependency srcDep = previousDeps.get(u);
				if(srcDep != null)
					dep.getSrcDependencies().add(srcDep);
			}
			previousDeps = tmp;
			
			if(dest == parentSrcTask)
				break;
			source = dest;
			dest = dest.getParentTask();
		}
		return previousDeps;
	}

	/**
	 * @param uses
	 * @return list of unique {@link Symbol}s referenced by {@code uses}, 
	 * or {@code null} if {@code uses} is  {@code null}.
	 */
	private List<Symbol> getUsedSymbols(Collection<SymbolInstruction> uses) {
		if(uses == null)
			return null;
		return uses.stream()
				.map(u -> u.getSymbol())
				.distinct()
				.collect(Collectors.toList());
	}

	/**
	 * Find the Task enclosing the specified basic block.
	 * <p>
	 * If the block is a Dummy Phi block (that was generated by ComputeSSA
	 * see {@link BlockWrapper}),
	 * then we consider it as part of its corresponding Control Task.
	 * (such dummy block does not have a Task associated with it)
	 * <br>
	 * Otherwise, it returns the first {@link TaskNode} it encounters
	 * in the block's container hierarchy. 
	 * 
	 * @return the enclosing Task of the specified {@link BasicBlock}.
	 */
	private TaskNode findEnclosingTaskOf(BasicBlock bb) {
		Block dummyControl = BlockWrapper.getDummyPhiBlockSource(bb);
		
		Block current = dummyControl != null? dummyControl : bb;
		while (current != null) {
			if (current instanceof TaskNode)
				return (TaskNode) current;
			current = current.getParent();
		}
		return null;
	}
	
	@Override
	public Block caseBasicBlock(BasicBlock b) {
		if (b.getInstructionCount() == 0) {
			debug("empty basicblock found and skipped: " + b);
			return b;
		} else {
			Block source = BlockWrapper.getDummyPhiBlockSource(b);
			if(source != null) {
				debug("dummy phi basicblock found and skipped: " + b);
				return b;
			}
		}
		
		return wrapInLeafTaskNode(b);
	}
	
	@Override
	public Block caseCompositeBlock(CompositeBlock b) {
		updatePragmaProcessingResources(b);
		
		Block res;
		if(skipCompositeBlock(b)) {
			super.caseCompositeBlock(b);
			res = b;
		} else {
			Block t = wrapInClusterTaskNode(b);
			for(Block block : new ArrayList<>(b.getChildren())){
				depth++;
				doSwitch(block);
				depth--;
			}
			res = t;
		}
		
		restorePragmaProcessingResources();
		return res;
	}

	private boolean skipCompositeBlock(CompositeBlock b) {
		if(BlockWrapper.isWrapperBlock(b))
			return true;
		
		if(GEN_ALL_CLUSTERS)
			return false;
		
		//FIXME b should not define typedefs or records neither. 
		//Also check in case it sets global variables ?..
		boolean skip = b.getScope().getSymbols().isEmpty();
		
		return skip;
	}

	@Override
	public Block caseIfBlock(IfBlock b) {
		updatePragmaProcessingResources(b);
		
		Block testBlk = b.getTestBlock();
		if(hasSideEffects(testBlk)) {
			//TODO: if the test block has side effects (e.g. write to a variable)
			// if MUST be extracted out !!!
			throw new RuntimeException("TODO: IF test block must not have side effects! " + b);
		}
		
		Block t = wrapInIfTaskNode(b);
		depth++;
		doSwitch(b.getElseBlock());
		doSwitch(b.getThenBlock());
		depth--;
		
		restorePragmaProcessingResources();
		return t;
	}

	private boolean hasSideEffects(Block testBlk) {
		EList<Instruction> instructions = EMFUtils.<Instruction>eAllContentsInstancesOf(testBlk, Instruction.class);
		return instructions.stream().anyMatch(i -> i instanceof SetInstruction); //FIXME also check for presence of other side effects like calls to non-pure functions ...
	}

	@Override
	public Block caseWhileBlock(WhileBlock b) {
		updatePragmaProcessingResources(b);
		
		WhileTaskNode t = wrapInWhileTaskNode(b);
		depth++;
		wrapInBasicLeafTaskNode((BasicBlock) b.getTestBlock());
		doSwitch(b.getBodyBlock());
		depth--;
		
		handleWhileLoopDependencies(t);
		
		restorePragmaProcessingResources();
		return t;
	}

	@Override
	public Block caseForBlock(ForBlock b) {
		updatePragmaProcessingResources(b);
				
		ForTaskNode t = wrapInForTaskNode(b);
		
		/* move init task before ForTaskNode and add a dependency*/
		BasicLeafTaskNode init = wrapInBasicLeafTaskNode(b.getInitBlock());
		b.setInitBlock(GecosUserBlockFactory.BBlock());
		Block parent = t.getParent();
		if (!(parent instanceof CompositeBlock)) {
			parent = GecosUserBlockFactory.CompositeBlock();
			t.getParent().replace(t, parent);
			((CompositeBlock) parent).getChildren().add(t);
		}
		((CompositeBlock) parent).addBlockBefore(init, t);
		HTaskGraphFactory.glueTaskDependency(init, t);
			
		depth++;
		wrapInBasicLeafTaskNode(b.getTestBlock());
		wrapInBasicLeafTaskNode(b.getStepBlock());
		doSwitch(b.getBodyBlock());
		depth--;
		
		handleForLoopDependencies(t);
		
		restorePragmaProcessingResources();
		return t;
	}
	
	private void handleForLoopDependencies(ForTaskNode ftn) {
		BasicLeafTaskNode init = ftn.getInit();
		BasicLeafTaskNode step = ftn.getStep();
		BasicLeafTaskNode test = ftn.getTest();
		
		HTaskGraphFactory.taskDependency(step, test);
		HTaskGraphFactory.taskDependency(test, ftn.getLoopOut());
		HTaskGraphFactory.taskDependency(ftn.getLoopIn(), ftn.getBody());
		HTaskGraphFactory.taskDependency(test, ftn.getOutputNode());
		
		//dependencies caused by PHI nodes should go directly to the body
		for (DataEdge de : test.getUseEdges()) {
			if (de.getFrom().equals(init)) {
				EList<SymbolInstruction> uses = null;
								
				uses =  new BasicEList<SymbolInstruction>(de.getUses());
				
				List<PhiInstruction> phi = getRelatedPhis(test, uses);
				if (phi.size() > 0) {
					Set<Symbol> syms = new HashSet<Symbol>();
					for (PhiInstruction phiInst : phi) {
						syms.add(((SymbolInstruction) phiInst.getChild(0)).getSymbol());
					}
					
					for (DataEdge outEdge : test.getUseEdges() ) {
						EList<SymbolInstruction> usedSyms = referencesSymbol(outEdge, syms);
						if (usedSyms.size() > 0) {
							buildTaskDependencies(outEdge.getTo(), init, usedSyms);
						}
					}
				} else {
					buildTaskDependencies(test, init, uses);
				}
			} else {
				//dependency caused by body block, is handled with the def edges
			}
		}
		
		//defines caused by PHIs should create dependencies between the body and loop out
		for (DataEdge de : test.getDefEdges()) {
			if (isBBInBlock(de.getTo(), ftn.getBody().getBlock())) {
				EList<SymbolInstruction> uses = null;
				
				uses =  new BasicEList<SymbolInstruction>(de.getUses());
				
				for (SymbolInstruction use : uses) {
					TaskDependency tdOut = HTaskGraphFactory.taskDependency(test, ftn.getLoopOut());
					tdOut.setUse(use.getSymbol());
				}
				
				buildDepChainBetween(ftn.getLoopIn(), findEnclosingTaskOf(de.getTo()), uses);			
			}
		}		
	}
	
	private void handleWhileLoopDependencies(WhileTaskNode wtn) {
		BasicLeafTaskNode test = wtn.getTest();
		
		HTaskGraphFactory.taskDependency(test, wtn.getLoopOut());
		HTaskGraphFactory.taskDependency(wtn.getLoopIn(), wtn.getBody());
		HTaskGraphFactory.taskDependency(test, wtn.getOutputNode());
				
		//defines caused by PHIs should create dependencies between the body and loop out
		for (DataEdge de : test.getDefEdges()) {
			if (isBBInBlock(de.getTo(), wtn.getBody().getBlock())) {
				EList<SymbolInstruction> uses = null;
				
				uses =  new BasicEList<SymbolInstruction>(de.getUses());
							
				buildTaskDependencies(test, (BasicBlock) wtn.getLoopOut().getBlock(), uses);
				buildTaskDependencies((BasicBlock) wtn.getLoopIn().getBlock(), de.getTo(), uses);			
			}
		}		
	}
	
	private List<PhiInstruction> getRelatedPhis(BasicBlock blk, List<SymbolInstruction> uses) {
		final List<PhiInstruction> phis = new ArrayList<PhiInstruction>();
		Set<Symbol> usedSymbols = new HashSet<Symbol>();
		for (SymbolInstruction inst : uses) {
			usedSymbols.add(inst.getSymbol());
		}
		
		(new BlockInstructionSwitch<Object>() {

			@Override
			public Object caseSymbolInstruction(SymbolInstruction object) {
				if (usedSymbols.contains(object.getSymbol())) {
					Instruction parent = object.getParent();
					while (parent != null) {
						if (parent instanceof PhiInstruction) {
							phis.add((PhiInstruction) parent);
							break;
						}
					}
				}
				return super.caseSymbolInstruction(object);
			}
			
		}).doSwitch(blk);
		
		return phis;
	}
	
	private EList<SymbolInstruction> referencesSymbol(DataEdge de, Set<Symbol> sym) {
		EList<SymbolInstruction> symInst = new BasicEList<SymbolInstruction>();
		for (SSAUseSymbol use : de.getUses()) {
			if (sym.contains(use.getSymbol())) {
				symInst.add(use);
			}
		}
		return symInst;		
	}
	
	private boolean isBBInBlock(BasicBlock bb, Block blk) {
		if (bb.equals(blk)) {
			return true;
		}
		Block parent = bb.getParent();
		while(parent != null) {
			if (parent.equals(blk)) {
				return true;
			} else {
				parent = parent.getParent();
			}
		}
		
		return false;
	}
	
	@Override
	public Block caseForC99Block(ForC99Block object) {		
		return caseForBlock(object);
	}

	@Override
	public Block caseSimpleForBlock(SimpleForBlock object) {
		return caseForBlock(object);
	}
	
	@Override
	public Block caseDoWhileBlock(DoWhileBlock object) {
		throw new UnsupportedOperationException("Do while control structure not manage yet in the HTG extraction");
	}

	@Override
	public Block caseSwitchBlock(SwitchBlock object) {
		throw new UnsupportedOperationException("Case/Switch control structure not manage yet in the HTG extraction");
	}

	@Override
	public Block caseCaseBlock(CaseBlock object) {
		throw new UnsupportedOperationException("Case/Switch control structure not manage yet in the HTG extraction");
	}
	

	//TODO support specifying multiple processing resources 
	private ProcessingResource pragmaProcessingResource = null;
	private ProcessingResource savedPragmaProcessingResource = null;
	
	private void updatePragmaProcessingResources(Block b) {
		this.savedPragmaProcessingResource = pragmaProcessingResource;
		if(arch != null) {
			Integer prId = processPragma(b);
			if(prId != null) {
				ProcessingResource pr = arch.getProcessingResource(prId);
				if(pr != null)
					pragmaProcessingResource = arch.getProcessingResource(prId);
			}
		}
	}
	
	private void restorePragmaProcessingResources() {
		this.pragmaProcessingResource = savedPragmaProcessingResource;
	}
	
	private void setResourceConstraints(TaskNode t) {
		if(arch != null && pragmaProcessingResource != null)
			t.addProcessingResource(pragmaProcessingResource);
	}
	
	//TODO handle mapping restriction to a set of Processing resources.
	private static Integer processPragma(Block block){
		PragmaAnnotation pragma = block.getPragma();
		if(pragma != null){
			StringBuilder sBuilder = new StringBuilder();
			for (String str : pragma.getContent()) {
				sBuilder.append(str);
			}
			
			Pattern pattern = Pattern.compile("\\.*" + TASK_MAPPING_PRAGMA + "\\s*\\s*([0-9]+)\\s*\\.*");
			Matcher matcher = pattern.matcher(sBuilder);
			if (matcher.find()) {
				return Integer.valueOf(matcher.group(1));
			}
		}
		return null;
	}
	
	
	private LeafTaskNode wrapInLeafTaskNode(BasicBlock b) {
		debug("Wrapping " + b + " into ");
		LeafTaskNode t = HTaskGraphFactory.leafTask(depth, null);
		b.getParent().replace(b, t);
		t.setBlock(b);
		setResourceConstraints(t);
		debug("\t" + t);
		return t;
	}
	
	private BasicLeafTaskNode wrapInBasicLeafTaskNode(BasicBlock b) {
		debug("Wrapping " + b + " into ");
		BasicLeafTaskNode t = HTaskGraphFactory.basicLeafTask(depth);
		t.getInstructions().addAll(b.getInstructions());
		t.getAnnotations().addAll(b.getAnnotations());
		t.getDefEdges().addAll(b.getDefEdges());
		t.getUseEdges().addAll(b.getUseEdges());
		t.setLabel(b.getLabel());
		
		for (ControlEdge e : new ArrayList<ControlEdge>(b.getInEdges())) {
			e.setTo(t);
		}
		for (ControlEdge e : new ArrayList<ControlEdge>(b.getOutEdges())) {
			e.setFrom(t);
		}
		
		for (DataEdge e : new ArrayList<DataEdge>(b.getUseEdges())) {
			e.setTo(t);
		}
		
		b.getParent().replace(b, t);
		t.setBlock(b);
		setResourceConstraints(t);
		debug("\t" + t);
		return t;
	}

	private ClusterTaskNode wrapInClusterTaskNode(CompositeBlock object) {
		ClusterTaskNode t = HTaskGraphFactory.clusterTask(depth, null);
		debug("Wrapping " + object + " into ");
		object.getParent().replace(object, t);
		t.setBlock(object);
		setResourceConstraints(t);
		debug("\t ClusterTaskNode " + t + "");
		GecosUserAnnotationFactory.copyAnnotations(object, t);
		return t;
	}
	
	private ForTaskNode wrapInForTaskNode(ForBlock object) {
		ForTaskNode t = HTaskGraphFactory.forTask(depth, null);
		debug("Wrapping " + object + " into ");
		object.getParent().replace(object, t);
		t.setBlock(object);
		setResourceConstraints(t);
		debug("\t ForTaskNode " + t + "");
		GecosUserAnnotationFactory.copyAnnotations(object, t);
		return t;
	}
	
	private WhileTaskNode wrapInWhileTaskNode(WhileBlock object) {
		WhileTaskNode t = HTaskGraphFactory.whileTask(depth, null);
		debug("Wrapping " + object + " into ");
		object.getParent().replace(object, t);
		t.setBlock(object);
		setResourceConstraints(t);
		debug("\t WhileTaskNode " + t + "");		
		GecosUserAnnotationFactory.copyAnnotations(object, t);
		return t;
	}
	
	private IfTaskNode wrapInIfTaskNode(IfBlock object) {
		IfTaskNode t = HTaskGraphFactory.ifTask(depth, null);
		debug("Wrapping " + object + " into ");
		object.getParent().replace(object, t);
		
		t.setBlock(object);
		setResourceConstraints(t);
		debug("\t IfTaskNode " + t + "");
		GecosUserAnnotationFactory.copyAnnotations(object, t);
		return t;
	}
	
	
	
	public class GroupDependenciesByUse extends HTGDefaultVisitor {
		@Override
		public void visitTask(TaskNode task) {
			groupInputDepsByUse(task);
			super.visitTask(task);
		}
		
		@Override
		public void visitCommunicationNode(CommunicationNode com) {
			groupInputDepsByUse(com);
		}

		private void groupInputDepsByUse(TaskNode task) {
			try {
				Map<TaskNode, List<TaskDependency>> groupByPredTask = task.getPreds().stream()
					.collect(Collectors.groupingBy(d -> d.getFrom()));
				
				for(Entry<TaskNode, List<TaskDependency>> predTask : groupByPredTask.entrySet()) {
					Map<Symbol, List<TaskDependency>> groupByUse = predTask.getValue().stream().collect(Collectors.groupingBy(d -> d.getUse()));
					for(Entry<Symbol, List<TaskDependency>> use : groupByUse.entrySet()) {
						List<TaskDependency> depsPerUse = use.getValue();
						if(depsPerUse.size() > 1) {
							// merge all deps to deps[0]
							TaskDependency mergedDep = depsPerUse.get(0);
							depsPerUse.stream().skip(1).forEach(d -> HTGUtils.mergeDependency(d, mergedDep));
						}
					}
				}
			} catch (NullPointerException n) {
				//occurs when the task dependency has no use symbol
			}
		}
	}
	
}
