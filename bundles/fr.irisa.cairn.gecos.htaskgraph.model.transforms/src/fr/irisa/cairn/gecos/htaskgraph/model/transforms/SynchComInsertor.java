package fr.irisa.cairn.gecos.htaskgraph.model.transforms;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.factory.HTaskGraphFactory;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGUtils;
import gecos.core.Procedure;
import gecos.core.Symbol;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HierarchicalTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;

public class SynchComInsertor extends HTGDefaultVisitor {

	protected static final boolean DEBUG = false;
	
	protected boolean duplicateControl;
	
	public void compute(Procedure p) {
		/* 
		 * First pass to duplicate necessary tasks for placing communications.
		 * 
		 * This will update CommunicationNode PRs, which in turn may lead to duplicating some tasks
		 * after propagating PRS.
		 * 
		 * This is required to do before creating htg communications in order to determine the
		 * correct set of sender/receiver Processing Resources.
		 */
		duplicateControl = true;
		p.getBody().accept(this);
		propagatePRsOutward(p);
		
		/* Second pass to add communications */
		duplicateControl = false;
		p.getBody().accept(this);
		// re-propagate PRs to update in case the PRs of a CommunicationNode has changed.
		propagatePRsOutward(p);
	}

	protected void propagatePRsOutward(Procedure p) {
		p.getBody().accept(new PropagatePRsOutwardDuplicateControl());
//		p.getBody().accept(new DuplicateTasksForCom());
	}

//	class DuplicateTasksForCom extends PropagatePRsOutwardDuplicateControl {
//		@Override
//		public void visitHierarchicalTask(HierarchicalTaskNode htn) {
//			List<ProcessingResource> before = new ArrayList<>(htn.getProcessingResources());
//			super.visitHierarchicalTask(htn);
//			
//			/* mark task as being replicated on all the newly added processing resources */
//			htn.getProcessingResources().stream()
//				.filter(pr -> !before.contains(pr))
//				.peek(pr -> {if(DEBUG) System.out.println("- Duplicating (for communication only) task '" + htn + "' on " + pr);})
//				.forEach(pr -> HTGUtils.markAsDuplicatedForCommunicationOn(htn, pr));
//		}
//	}
	
	@Override
	public void visitHierarchicalTask(HierarchicalTaskNode task) {
		visitTask(task);
	}
	
	@Override
	public void visitTask(TaskNode task) {
		/* 
		 * Receive all task Internal inputs (i.e. from other non-CommunicationNode tasks at the same level)
		 * The placement of the send(s)/receive(s) is determined based on the dependency path..
		 */
		for(TaskDependency intInput : task.getInternalInputs()) {
			//add com from 'srcTask' to 'task'
			Symbol use = intInput.getUse();
			if(use != null) {
				// compute PR sender -> [receivers] map
				Set<ProcessingResource> srcPRs = getEffectiveSenderPRs(intInput);
				Set<ProcessingResource> dstPRs = getEffectiveReceiverPRs(intInput);
				Multimap<ProcessingResource, ProcessingResource> sendRecvsMap = HashMultimap.create();
				for(ProcessingResource s : srcPRs)
					dstPRs.stream()
						.filter(r -> r != s) // eliminate self communication
						.forEach(r -> sendRecvsMap.put(s, r));
				
				placeSendRecvs(intInput, sendRecvsMap);
			} else {
				//TODO add a useless (i.e. control) dependency 
				throw new RuntimeException("TODO: unhandled case where use is null");
			}
		}
		
		
		
		// FIXME !!!!!!!!!!!!!
		
		/*
		 * If the parent task is a ForTask,
		 * all loop carried dependencies MUST be sent and received after the task.
		 * 
		 * NOTE: If a Symbol appears in the InputNode.Outputs of a ForTask as well as in its 
		 * OutputNode.Inputs, then this represent a loop-carried dependency.
		 */
		if(task.getParentTask() instanceof ForTaskNode) {
			ForTaskNode parentFor = (ForTaskNode) task.getParentTask();
			CommunicationNode forOutNode = parentFor.getOutputNode();
			
			for(TaskDependency extInput : task.getExternalInputs()) {
				Symbol use = extInput.getUse();
				Set<ProcessingResource> dstPRs = getEffectiveReceiverPRs(extInput);
				
				for(TaskDependency forOutNodeInput : forOutNode.getPreds()) {
					if(use != null && forOutNodeInput.getUse() == use) { //XXX equals ???
						// extInput is a loop Carried dependency
						
						Set<ProcessingResource> srcPRs = getEffectiveSenderPRs(forOutNodeInput);
						Multimap<ProcessingResource, ProcessingResource> sendRecvsMap = HashMultimap.create();
						for(ProcessingResource s : srcPRs)
							dstPRs.stream()
								.filter(r -> r != s) // eliminate self communication
								.forEach(r -> sendRecvsMap.put(s, r));
						
						placeSendRecvs(forOutNodeInput, sendRecvsMap); //XXX forOutNodeInput is not an Internal Input !!!
					}
				}
			}
		}
		
		task.getBlock().accept(this);
	}


	protected void placeSendRecvs(TaskDependency intInput, Multimap<ProcessingResource, ProcessingResource> sendRecvsMap) {
		TaskNode sender = intInput.getFrom();
		Symbol use = intInput.getUse();
		EList<TaskDependency> effSrcDependencies = intInput.getEffSrcDependencies();
		
		//TODO: avoid send/receive inside loops to reduce communications:
		//
		// - When propagating PRs (duplicateControl=true), do not duplicate FOR tasks 
		// used for receiving only (i.e. has no computation on the PR on which to duplicate)
		// - Place all sends after the FOR.
		// - Place receives in the effective sources.
		if(sender instanceof LeafTaskNode || effSrcDependencies.isEmpty() /*|| sender instanceof ForTaskNode*/) {
			/* 
			 * Place communications right after this task
			 * Senders are only those that actually own the latest data value.
			 */
			CommunicationNode sendLocation = sender.getOutputNode();
			CommunicationNode recvLocation = sender.getOutputNode();
			Set<ProcessingResource> senderPRs = getEffectiveSenderPRs(intInput);
			Set<ProcessingResource> receiverPRs = senderPRs.stream()
					.flatMap(s -> sendRecvsMap.get(s).stream())
					.collect(Collectors.toSet());
			
			placeSendRecv(use, sendLocation, senderPRs, recvLocation, receiverPRs);
		} else {
			/* place communications closer toward original (effective sources) */
			for(TaskDependency pred : effSrcDependencies)
				placeSendRecvs(pred, sendRecvsMap);
		}
	}

	protected void placeSendRecv(Symbol use, 
			CommunicationNode sendLocation, Set<ProcessingResource> senders, 
			CommunicationNode recvLocation, Set<ProcessingResource> recvers) {
		
		if(senders.isEmpty() || recvers.isEmpty())
			return;
		
		// update processing resources on communication nodes
		senders.forEach(sendLocation::addProcessingResource);
		recvers.forEach(recvLocation::addProcessingResource);
		
		if(!duplicateControl) {
			HTGSend send = HTaskGraphFactory.htgSend(use, senders);
			sendLocation.getCommunications().add(send);
			
			HTGRecv recv = HTaskGraphFactory.htgRecv(use, recvers);
			recvLocation.getCommunications().add(recv);
			recv.getSends().add(send);
		}
	}
	
	protected Set<ProcessingResource> getEffectiveReceiverPRs(TaskDependency intInput) {
		return HTGUtils.getEffectiveFinalDestinationNodes(intInput).stream()
				.flatMap(t -> t.getProcessingResources().stream())
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	/**
	 * Find the set of {@link ProcessingResource} that should send the specified dependency.
	 * i.e. the PR on which the original source is scheduled on.
	 * 
	 * @param intInput
	 * @return
	 */
	protected Set<ProcessingResource> getEffectiveSenderPRs(TaskDependency intInput) {
		return HTGUtils.getEffectiveOriginalSourceNodes(intInput).stream()
			.flatMap(t -> t.getProcessingResources().stream())
			.collect(Collectors.toCollection(LinkedHashSet::new));
	}

}