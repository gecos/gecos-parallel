package fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty;

import architecture.ProcessingResource;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTGCom;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.ProfilingInformation;
import htaskgraph.TaskDependency;
import htaskgraph.TaskNode;
import htaskgraph.WhileTaskNode;
import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@GSModule("Generates a Graphiz DOT file representing an HTG.")
@SuppressWarnings("all")
public class TaskGraphDottyExport {
  private final static String COLOR_HIERARCHICAL = "black";
  
  private final static String COLOR_FOR = "blue";
  
  private final static String COLOR_WHILE = "purple";
  
  private final static String COLOR_IF = "orange";
  
  private final static String COLOR_DEPEND_INTERNAL = "red";
  
  private final static String COLOR_DEPEND_EXTERNAL = "blue";
  
  private final static String COLOR_COM = "red";
  
  private final static String COLOR_LEAF = "green";
  
  private final static String COLOR_INVISIBLE = "white";
  
  private final static String STYLE_HIERARCHICAL = "dashed";
  
  private final static String STYLE_FOR = "solid";
  
  private final static String STYLE_WHILE = "solid";
  
  private final static String STYLE_IF = "solid";
  
  private final static String STYLE_LEAF = "solid";
  
  private final static String STYLE_COM = "solid";
  
  private GecosProject project;
  
  private String path;
  
  private boolean verbose;
  
  @GSModuleConstructor("-arg1: the GecosProject containing the HTG to be exported as dot file.\n\n\t\t\t\t\t\t-arg2: the path representing the dot file to be generated.")
  public TaskGraphDottyExport(final GecosProject gProj, final String dotFilePath, final boolean verbose) {
    this.project = gProj;
    this.path = dotFilePath;
    this.verbose = verbose;
  }
  
  public void compute() {
    File dest = new File(this.path);
    if ((dest.exists() && dest.isFile())) {
      throw new RuntimeException();
    }
    dest.mkdirs();
    final Consumer<Procedure> _function = (Procedure e) -> {
      this.generate(e);
    };
    this.project.listProcedures().forEach(_function);
  }
  
  public void generate(final Procedure p) {
    try {
      String _name = p.getSymbol().getName();
      String _plus = ((this.path + Character.valueOf(File.separatorChar)) + _name);
      String _plus_1 = (_plus + ".dot");
      PrintStream ps = new PrintStream(_plus_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("digraph G {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rankdir=TB;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      String _generateNodes = this.generateNodes(p.getBody());
      _builder.append(_generateNodes, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ps.append(_builder);
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected String _generateNodes(final Block p) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<EObject> _eContents = p.eContents();
      for(final EObject c : _eContents) {
        String _xifexpression = null;
        if ((c instanceof Block)) {
          _xifexpression = this.generateNodes(((Block) c));
        }
        _builder.append(_xifexpression);
        _builder.append(" ");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  protected String _generateNodes(final TaskNode t) {
    String _xblockexpression = null;
    {
      final int id = t.getNumber();
      String label = null;
      String color = null;
      String style = null;
      CommunicationNode loopIn = ((CommunicationNode) null);
      CommunicationNode loopOut = ((CommunicationNode) null);
      if ((t instanceof LeafTaskNode)) {
        label = "LT";
        color = TaskGraphDottyExport.COLOR_LEAF;
        style = TaskGraphDottyExport.STYLE_LEAF;
      } else {
        if ((t instanceof ForTaskNode)) {
          label = "FOR";
          color = TaskGraphDottyExport.COLOR_FOR;
          style = TaskGraphDottyExport.STYLE_FOR;
          loopIn = ((ForTaskNode) t).getLoopIn();
          loopOut = ((ForTaskNode) t).getLoopOut();
        } else {
          if ((t instanceof WhileTaskNode)) {
            label = "WHILE";
            color = TaskGraphDottyExport.COLOR_WHILE;
            style = TaskGraphDottyExport.STYLE_WHILE;
            loopIn = ((WhileTaskNode) t).getLoopIn();
            loopOut = ((WhileTaskNode) t).getLoopOut();
          } else {
            if ((t instanceof IfTaskNode)) {
              label = "IF";
              color = TaskGraphDottyExport.COLOR_IF;
              style = TaskGraphDottyExport.STYLE_IF;
            } else {
              label = "HT";
              color = TaskGraphDottyExport.COLOR_HIERARCHICAL;
              style = TaskGraphDottyExport.STYLE_HIERARCHICAL;
            }
          }
        }
      }
      String _xifexpression = null;
      boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(t.getProfilingInformation());
      boolean _not = (!_isNullOrEmpty);
      if (_not) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("WCET=");
        EList<ProfilingInformation> _profilingInformation = t.getProfilingInformation();
        _builder.append(_profilingInformation);
        _xifexpression = _builder.toString();
      }
      final String wcet = _xifexpression;
      final CharSequence proc = this.printProcRes(t);
      String _label = label;
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(id);
      _builder_1.append(" L");
      int _depth = t.getDepth();
      _builder_1.append(_depth);
      _builder_1.append(" ");
      _builder_1.append(proc);
      _builder_1.append(" ");
      _builder_1.append(wcet);
      label = (_label + _builder_1);
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("subgraph cluster");
      _builder_2.append(id);
      _builder_2.append(" {");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t");
      _builder_2.append("compound=true;");
      _builder_2.newLine();
      _builder_2.append("\t");
      _builder_2.append("style=");
      _builder_2.append(style, "\t");
      _builder_2.append(";");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t");
      _builder_2.append("color=");
      _builder_2.append(color, "\t");
      _builder_2.append(";");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t");
      _builder_2.append("label=\"");
      _builder_2.append(label, "\t");
      _builder_2.append("\"");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      _builder_2.newLine();
      _builder_2.append("\t\t");
      _builder_2.append("task");
      _builder_2.append(id, "\t\t");
      _builder_2.append("[color=\"");
      _builder_2.append(TaskGraphDottyExport.COLOR_INVISIBLE, "\t\t");
      _builder_2.append("\",label=\"\",width=0,height=0];");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      {
        CommunicationNode _inputNode = t.getInputNode();
        boolean _tripleNotEquals = (_inputNode != null);
        if (_tripleNotEquals) {
          String _generateNodes = this.generateNodes(t.getInputNode());
          _builder_2.append(_generateNodes, "\t\t");
        }
      }
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      {
        if ((loopIn != null)) {
          String _generateNodes_1 = this.generateNodes(loopIn);
          _builder_2.append(_generateNodes_1, "\t\t");
        }
      }
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      String _generateNodes_2 = this.generateNodes(t.getBlock());
      _builder_2.append(_generateNodes_2, "\t\t");
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      {
        if ((loopOut != null)) {
          String _generateNodes_3 = this.generateNodes(loopOut);
          _builder_2.append(_generateNodes_3, "\t\t");
        }
      }
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t\t");
      {
        CommunicationNode _outputNode = t.getOutputNode();
        boolean _tripleNotEquals_1 = (_outputNode != null);
        if (_tripleNotEquals_1) {
          String _generateNodes_4 = this.generateNodes(t.getOutputNode());
          _builder_2.append(_generateNodes_4, "\t\t");
        }
      }
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("}");
      _builder_2.newLine();
      _builder_2.append("\t");
      CharSequence _generateEdges = this.generateEdges(t);
      _builder_2.append(_generateEdges, "\t");
      _builder_2.newLineIfNotEmpty();
      _xblockexpression = _builder_2.toString();
    }
    return _xblockexpression;
  }
  
  public CharSequence printProcRes(final TaskNode t) {
    CharSequence _xifexpression = null;
    boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(t.getProcessingResources());
    boolean _not = (!_isNullOrEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      EList<ProcessingResource> _processingResources = t.getProcessingResources();
      _builder.append(_processingResources);
      _xifexpression = _builder;
    }
    return _xifexpression;
  }
  
  protected String _generateNodes(final CommunicationNode cn) {
    String _xblockexpression = null;
    {
      final int id = cn.getNumber();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(id);
      {
        boolean _isEmpty = cn.getCommunications().isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          _builder.append(" ");
          CharSequence _printProcRes = this.printProcRes(cn);
          _builder.append(_printProcRes);
        }
      }
      final Function1<HTGCom, CharSequence> _function = (HTGCom it) -> {
        return this.generateNodes(it);
      };
      String _join = IterableExtensions.<HTGCom>join(cn.getCommunications(), "\\n", "\\n", "", _function);
      _builder.append(_join);
      final String label = _builder.toString();
      final String color = TaskGraphDottyExport.COLOR_COM;
      final String style = TaskGraphDottyExport.STYLE_COM;
      String _xifexpression = null;
      if (((cn.getPreds().isEmpty() && cn.getSuccs().isEmpty()) && cn.getCommunications().isEmpty())) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _xifexpression = _builder_1.toString();
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("subgraph cluster");
        _builder_2.append(id);
        _builder_2.append(" {");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("compound=true;");
        _builder_2.newLine();
        _builder_2.append("\t");
        _builder_2.append("style=");
        _builder_2.append(style, "\t");
        _builder_2.append(";");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("color=");
        _builder_2.append(color, "\t");
        _builder_2.append(";");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("label=\"");
        _builder_2.append(label, "\t");
        _builder_2.append("\"");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t\t");
        _builder_2.newLine();
        _builder_2.append("\t\t");
        _builder_2.append("task");
        _builder_2.append(id, "\t\t");
        _builder_2.append("[color=\"");
        _builder_2.append(TaskGraphDottyExport.COLOR_INVISIBLE, "\t\t");
        _builder_2.append("\",label=\"\",width=0,height=0];");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("}");
        _builder_2.newLine();
        CharSequence _generateEdges = this.generateEdges(cn);
        _builder_2.append(_generateEdges);
        _builder_2.newLineIfNotEmpty();
        _xifexpression = _builder_2.toString();
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected String _generateNodes(final HTGSend send) {
    String _xblockexpression = null;
    {
      final String id = this.getUID(send);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("S");
      _builder.append(id);
      _builder.append(" -> ");
      final Function1<HTGRecv, CharSequence> _function = (HTGRecv it) -> {
        return this.getUID(it);
      };
      String _join = IterableExtensions.<HTGRecv>join(send.getReceives(), "[R", ",R", "]", _function);
      _builder.append(_join);
      _builder.append(": (");
      Symbol _use = send.getUse();
      _builder.append(_use);
      _builder.append(") ");
      EList<ProcessingResource> _fromProcResources = send.getFromProcResources();
      _builder.append(_fromProcResources);
      _builder.append(" -> ");
      EList<ProcessingResource> _toProcResources = send.getToProcResources();
      _builder.append(_toProcResources);
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNodes(final HTGRecv recv) {
    String _xblockexpression = null;
    {
      final String id = this.getUID(recv);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("R");
      _builder.append(id);
      _builder.append(" <- ");
      final Function1<HTGSend, CharSequence> _function = (HTGSend it) -> {
        return this.getUID(it);
      };
      String _join = IterableExtensions.<HTGSend>join(recv.getSends(), "[S", ",S", "]", _function);
      _builder.append(_join);
      _builder.append(": (");
      Symbol _use = recv.getUse();
      _builder.append(_use);
      _builder.append(") ");
      EList<ProcessingResource> _toProcResources = recv.getToProcResources();
      _builder.append(_toProcResources);
      _builder.append(" <- ");
      EList<ProcessingResource> _fromProcResources = recv.getFromProcResources();
      _builder.append(_fromProcResources);
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  private int comId = 0;
  
  private final HashMap<HTGCom, Integer> comId_map = new HashMap<HTGCom, Integer>();
  
  public String getUID(final HTGCom com) {
    String _xblockexpression = null;
    {
      Integer id = this.comId_map.get(com);
      if ((id == null)) {
        int _plusPlus = this.comId++;
        id = Integer.valueOf(_plusPlus);
        this.comId_map.put(com, id);
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(id);
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public CharSequence generateEdges(final TaskNode to) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buf = new StringBuffer();
      final Function1<TaskDependency, TaskNode> _function = (TaskDependency it) -> {
        return it.getFrom();
      };
      final BiConsumer<TaskNode, List<TaskDependency>> _function_1 = (TaskNode from, List<TaskDependency> deps) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("task");
        int _number = from.getNumber();
        _builder.append(_number);
        _builder.append(" -> task");
        int _number_1 = to.getNumber();
        _builder.append(_number_1);
        _builder.append("[lhead=cluster");
        int _number_2 = to.getNumber();
        _builder.append(_number_2);
        _builder.append(",ltail=cluster");
        int _number_3 = from.getNumber();
        _builder.append(_number_3);
        final String edge = _builder.toString();
        if (this.verbose) {
          for (final TaskDependency d : deps) {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(edge);
            _builder_1.append(",color=\"");
            String _color = this.getColor(d);
            _builder_1.append(_color);
            _builder_1.append("\",label=\"");
            int _id = d.getId();
            _builder_1.append(_id);
            _builder_1.append(",");
            String _name = d.getUse().getName();
            _builder_1.append(_name);
            _builder_1.append(",");
            final Function1<TaskDependency, Integer> _function_2 = (TaskDependency it) -> {
              return Integer.valueOf(it.getId());
            };
            List<Integer> _map = ListExtensions.<TaskDependency, Integer>map(d.getSrcDependencies(), _function_2);
            _builder_1.append(_map);
            _builder_1.append("->");
            final Function1<TaskDependency, Integer> _function_3 = (TaskDependency it) -> {
              return Integer.valueOf(it.getId());
            };
            List<Integer> _map_1 = ListExtensions.<TaskDependency, Integer>map(d.getDstDependencies(), _function_3);
            _builder_1.append(_map_1);
            _builder_1.append("\"];");
            _builder_1.newLineIfNotEmpty();
            buf.append(_builder_1);
          }
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append(edge);
          _builder_2.append(",color=\"");
          String _color_1 = this.getColor(deps.get(0));
          _builder_2.append(_color_1);
          _builder_2.append("\",label=\"");
          {
            boolean _isEmpty = deps.isEmpty();
            boolean _not = (!_isEmpty);
            if (_not) {
              final Function1<TaskDependency, Symbol> _function_4 = (TaskDependency it) -> {
                return it.getUse();
              };
              List<Symbol> _map_2 = ListExtensions.<TaskDependency, Symbol>map(deps, _function_4);
              _builder_2.append(_map_2);
            }
          }
          _builder_2.append("\"];");
          _builder_2.newLineIfNotEmpty();
          buf.append(_builder_2);
        }
      };
      IterableExtensions.<TaskNode, TaskDependency>groupBy(to.getPreds(), _function).forEach(_function_1);
      StringConcatenation _builder = new StringConcatenation();
      String _string = buf.toString();
      _builder.append(_string);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public String getColor(final TaskDependency dep) {
    String _xifexpression = null;
    boolean _isExternal = dep.isExternal();
    if (_isExternal) {
      _xifexpression = TaskGraphDottyExport.COLOR_DEPEND_INTERNAL;
    } else {
      _xifexpression = TaskGraphDottyExport.COLOR_DEPEND_EXTERNAL;
    }
    return _xifexpression;
  }
  
  public String generateNodes(final Block recv) {
    if (recv instanceof HTGRecv) {
      return _generateNodes((HTGRecv)recv);
    } else if (recv instanceof HTGSend) {
      return _generateNodes((HTGSend)recv);
    } else if (recv instanceof CommunicationNode) {
      return _generateNodes((CommunicationNode)recv);
    } else if (recv instanceof TaskNode) {
      return _generateNodes((TaskNode)recv);
    } else if (recv != null) {
      return _generateNodes(recv);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(recv).toString());
    }
  }
}
