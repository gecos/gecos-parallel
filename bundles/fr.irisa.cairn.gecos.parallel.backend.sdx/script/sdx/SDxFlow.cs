debug(1);

INPUT_FILE=$1;
NBPROCS=4;
OUTDIR="output/" + basename(INPUT_FILE);
PROPAGATE_ONLY = true; #$2; # true:= do not schedule just propagate pragma specified schedule..
DO_CHECK = true;


echo("=============================================");
echo("== Starting SDxFlow with file: " + INPUT_FILE);


## Cleanup
shell("rm -rf " + OUTDIR);

## Create/parse Gecos Project
p = CreateGecosProject("test");
AddSourceToGecosProject(p, INPUT_FILE);
CDTFrontend(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/");


## Create a Dummy Parallel Architecture 
arch = CreateDummyParallelArchitecture(NBPROCS);


## Generate HTG
ComputeSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
output(p,"dot", OUTDIR + "/dotty_cdfg/ssa/");

HTGTaskExtractor(p, arch);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
HTGDottyExport(p, OUTDIR + "/dotty_htg/init", true);
output(p,"dot", OUTDIR + "/dotty_htg/init/cdfg/");

## 
HTGToSdx(p);
#if (DO_CHECK) then DanglingRefPrinter(p, true); fi;
#SaveGecosProject(p, OUTDIR + "/dotty_htg/com");
HTGDottyExport(p, OUTDIR + "/dotty_htg/com", true);
output(p,"dot", OUTDIR + "/dotty_htg/com/cdfg/");

RemoveSSAForm(p);
if (DO_CHECK) then DanglingRefPrinter(p, true); fi;


## MPI codegen
CGenerator(p, OUTDIR + "/codegen/sdx");

## Compile & RUN
#shell("cd " + OUTDIR + "/codegen/mpi && mpicc tmp_gecos_file.c");
#echo("-------------------------");
#shell("cd " + OUTDIR + "/codegen/mpi; mpirun -np " + NBPROCS + " ./a.out");
#echo("-------------------------");
### Print profiling stats
#shell("cd " + OUTDIR + "/codegen/mpi;
#	STAT_FILES=`ls *.stats 2> /dev/null`;
#	for f in ${STAT_FILES}; do echo $f; cat $f; done;
#	echo '-------------------------';
#	printf 'total recv instructions count = '; head -1 ${STAT_FILES} | awk -F '=' '{s+=$2} END {print s}';
#	printf 'total send instructions count = '; head -2 ${STAT_FILES} | awk -F '=' '{s+=$2} END {print s}';
#	printf 'total recv data size (Bytes) = ' ; head -3 ${STAT_FILES} | awk -F '=' '{s+=$2} END {print s}';
#	printf 'total send data size (Bytes) = ' ; head -4 ${STAT_FILES} | awk -F '=' '{s+=$2} END {print s}';
#");

#shell("cd " + OUTDIR + "/codegen/mpi; printf 'total recv instructions count = '; head -1 `ls *.stats 2> /dev/null` | cut -d '=' -f 2 | awk '{s+=$1} END {print s}'");

