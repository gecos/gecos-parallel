package fr.irisa.cairn.gecos.parallel.backend.sdx;

import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.HTGExtraction;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.transforms.procedures.outlining.BlockOutliner;
import gecos.annotations.PragmaAnnotation;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;
import htaskgraph.TaskNode;

public class GenerateSDSHwSync extends HTGDefaultVisitor {

	private GecosProject htg;

	public GenerateSDSHwSync(GecosProject htg) {
		this.htg = htg;
	}

	public void compute() {
		htg.listProcedures().forEach(this::visit);
	}
	
	@Override
	public void visitTask(TaskNode task) {
		if(isOnHwPR(task.getProcessingResources())) {
			removePrPragma(task);
			outline(task);
		} else {
			super.visitTask(task);
			removePrPragma(task);
		}
	}

	private boolean isOnHwPR(EList<ProcessingResource> processingResources) {
		return processingResources.size() == 1 && processingResources.get(0).getId() != 0; //XXX
	}
	
	private void outline(TaskNode task) {
		Procedure proc = new BlockOutliner(task.getBlock(), htg,true).outline();
		
		// add SDS pragmas:
		// - data zero-copy: to allow hw to access data directly (i.e. shared memory).
		//   NOTE: this requires data to be allocated with sds_malloc (to provide virtual-2-physical mapping)!
		// - data sys_port: use ACP port to enable cache coherence between hw and sw.
		
		String zeroCopyPragma = proc.listParameters().stream()
			.map(p -> p.getName())// + "[0:" + p.getType().getSize() + "]")
			.collect(Collectors.joining(", ", "SDS data zero_copy(", ")"));
		String sysPortPragma = proc.listParameters().stream()
			.map(p -> p.getName() + ":ACP")
			.collect(Collectors.joining(", ", "SDS data sys_port(", ")"));
			
		
		GecosUserAnnotationFactory.pragma(proc, zeroCopyPragma);
		GecosUserAnnotationFactory.pragma(proc, sysPortPragma);
	}

	private void removePrPragma(TaskNode task) {
		PragmaAnnotation pragma = task.getBlock().getPragma();
		if(pragma != null){
			pragma.getContent().stream()
				.filter(c -> c.startsWith(HTGExtraction.TASK_MAPPING_PRAGMA))
				.collect(Collectors.toList()).forEach(pragma.getContent()::remove);
			
			if(pragma.getContent().isEmpty())
				EcoreUtil.delete(pragma);
			
		}
	}
}
