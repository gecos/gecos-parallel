package fr.irisa.cairn.gecos.parallel.model.transforms.dotprinter;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorAdapter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotPrinter;
import parallel.ParallelApplication;
import parallel.ParallelModelVisitor;
import parallel.Process;
import parallel.Receive;
import parallel.Send;
 
public class ExtendedGecosDotPrinter extends GecosVisitorAdapter<GecosDotPrinter> implements ParallelModelVisitor {
 
	public ExtendedGecosDotPrinter(GecosDotPrinter visitor) {
		super(visitor);
	}

	@Override
	public void visitParallelApplication(ParallelApplication app) {
		// TODO Auto-generated method stub
	
	} 
	
	@Override
	public void visitProcess(Process p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitSend(Send send) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitReceive(Receive receive) {
		// TODO Auto-generated method stub
		
	}

}