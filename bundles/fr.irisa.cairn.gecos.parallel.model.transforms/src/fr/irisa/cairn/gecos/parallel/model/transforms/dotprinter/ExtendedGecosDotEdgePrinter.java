package fr.irisa.cairn.gecos.parallel.model.transforms.dotprinter;

import fr.irisa.cairn.gecos.model.extensions.visitors.GecosVisitorAdapter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotEdgePrinter;
import fr.irisa.cairn.gecos.model.utils.dotprinter.GecosDotPrinter;
import parallel.Communication;
import parallel.ParallelApplication;
import parallel.ParallelModelVisitor;
import parallel.Process;
import parallel.Receive;
import parallel.Send;

/**
 * TODO
 */
public class ExtendedGecosDotEdgePrinter extends GecosVisitorAdapter<GecosDotEdgePrinter> implements ParallelModelVisitor {

	private GecosDotPrinter printer;

	public ExtendedGecosDotEdgePrinter(GecosDotEdgePrinter visitor) {
		super(visitor);
		printer = visitor.getGecosDotPrinter();
	}

	public void printCommunication(Communication com) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visitParallelApplication(ParallelApplication application) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void visitProcess(Process process) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitSend(Send send) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitReceive(Receive receive) {
		// TODO Auto-generated method stub
		
	}

}