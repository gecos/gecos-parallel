package fr.irisa.cairn.gecos.parallel.model.transforms.dotprinter

import fr.irisa.r2d2.gecos.framework.GSModule
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor
import gecos.blocks.BasicBlock
import gecos.blocks.Block
import gecos.blocks.CompositeBlock
import gecos.blocks.ForBlock
import gecos.blocks.IfBlock
import gecos.blocks.WhileBlock
import gecos.core.Procedure
import java.io.File
import java.io.PrintStream
import parallel.ParallelApplication
import parallel.Process
import parallel.Receive
import parallel.Send
import fr.irisa.cairn.gecos.parallel.model.utils.ParallelUtils

@GSModule("Generates a Graphiz DOT graph representing a ParallelModel.")
class ParallelModelDottyExport {
	
	val static COLOR_PROCESS = "black"
	val static COLOR_PROCEDURE = "blue"
	val static COLOR_SEND = "red"
	val static COLOR_RECV = "red"
	val static COLOR_COMPUTE = "green"
	val static COLOR_COMPOSITEBLK = "green"
	
	ParallelApplication app;
	String path;
	StringBuilder comEdgesBuffer
	
	@GSModuleConstructor("-arg1: ParallelApplication.\n" +
						 "-arg2: location where the .dot file will be generated at.")
	new(ParallelApplication parallelApp, String location) {
		app = parallelApp;
		path = location;
	}
	 
	def compute() {
		var File dest = new File(path);
		if (dest.exists && dest.file)
			throw new RuntimeException()
		dest.mkdirs;
		
		generate(app);
	}
	
	def void generate(ParallelApplication app) {
		comEdgesBuffer = new StringBuilder()
		var ps = new PrintStream(path+File::separatorChar+"app.dot")
		ps.append(
		'''
		digraph G {
			compound=true;
			
			«FOR p : app.processes»
			«generate(p)»
			«ENDFOR»
			
			«comEdgesBuffer»
			
			rankdir="LR"
		}
		''');
		ps.close
	}
	
	def String generate(Process p) {
		val id = getUID(p)
		val label = "Process"+id
		'''
		subgraph clusterProcess«id» {
			compound=true;
			style=line;
			color=«COLOR_PROCESS»;
			label="«label»"
			
			«FOR proc : p.listProcedures»
				«generate(proc as Procedure)»
			«ENDFOR»
		}
		'''
	}

	def generate(Procedure p) {
		val id = getUID(p)
		val label = p.symbolName
		'''
		subgraph cluster«id» {
			compound=true;
			style=line;
			color=«COLOR_PROCEDURE»;
			label="«label»"
			
			«generateNode(p.body)»
		}
		'''
	}
	
	def dispatch String generateNode(CompositeBlock b) {
		val id = getUID(b)
		val label = ""
		'''
		subgraph cluster«id» {
			compound=true;
			style=dashed;
			color=«COLOR_COMPOSITEBLK»;
			label="«label»"
			
			«FOR c : b.children» 
			«generateNode(c)»
			«ENDFOR»
		}
		«generateControlEdges(b)»
		'''
	}
	
	def dispatch String generateNode(ForBlock b) {
		val id = getUID(b)
		val label = "For" //TODO (i..)
		'''
		subgraph cluster«id» {
			compound=true;
			style=line;
			color=«COLOR_COMPOSITEBLK»;
			label="«label»"
			
			«generateNode(b.bodyBlock)»
		}
		'''
	}
	
	def dispatch String generateNode(IfBlock b) {
		val id = getUID(b)
		val label = "If" //TODO (condition)
		'''
		subgraph cluster«id» {
			compound=true;
			style=dashed;
			color=«COLOR_COMPOSITEBLK»;
			label="«label»"
			
			«if (b.thenBlock !== null) generateNode(b.thenBlock)»
			«if (b.elseBlock !== null) generateNode(b.elseBlock)»
		}
		'''
	}
	
	def dispatch String generateNode(WhileBlock b) {
		val id = getUID(b)
		val label = "While" //TODO (condition)
		'''
		subgraph cluster«id» {
			compound=true;
			style=line;
			color=«COLOR_COMPOSITEBLK»;
			label="«label»"
			
			«generateNode(b.bodyBlock)»
		}
		'''
	}

	def dispatch String generateNode(BasicBlock b) {
		if(b.instructionCount == 0)
			return ""
		val id = getUID(b)
		val label = '''Compute«id»'''
		
		'''
		subgraph cluster«id» {
			compound=true;
			style=line;
			shape="rectangle"
			color=«COLOR_COMPUTE»;
			label="«label»"
			
			Compute«id»[shape="rectangle",color="white",label=""];
		}
		'''
	}
	
	def dispatch String generateNode(Block b) {
//		throw new RuntimeException("unsupported block type: " + b.class)
	}
	
	def dispatch String generateNode(Send send) {
		comEdgesBuffer.append(generateCommunicationEdges(send));
		val id = getUID(send)
		val label = '''Send«id»'''
		'''
«««		Send«id» [shape="rectangle",color="«COLOR_SEND»",width=1,height=1,label="«label»"];
		subgraph cluster«id» {
			compound=true;
			style=line;
			shape="rectangle"
			color=«COLOR_SEND»;
			label="«label»"
			
			Send«id»[shape="rectangle",color="white",label="«send.message»"];
		}
		'''
	}

	def  dispatch String generateNode(Receive recv) {
		val id = getUID(recv)
		val label = '''Recv«id»'''
		'''
«««		Receive«id» [shape="rectangle",color="«COLOR_RECV»",width=1,height=1,label="«label»"];
		subgraph cluster«id» {
			compound=true;
			style=line;
			shape="rectangle"
			color=«COLOR_RECV»;
			label="«label»"
			
			Receive«id»[shape="rectangle",color="white",label="«recv.message»"];
		}
		'''
	}

	def generateCommunicationEdges(Send send) {
		'''
			«FOR Receive recv : send.receives»
			Send«getUID(send)» -> Receive«getUID(recv)»[color="red",label=""]; 
			«ENDFOR»
		'''
	}
	
	def String generateControlEdges(CompositeBlock block) {
//		val buf = new StringBuffer
//		
//		val children = block.children
//		for(i : 0..< children.size-1)
//			buf.append('''
//			cluster«getUID(children.get(i))» -> cluster«getUID(children.get(i+1))»[color="black",label=""];
//		''')
//		
//		'''«buf.toString»'''
	}
	
	def dispatch getUID(Procedure p) {
		'''«p.symbolName»_«ParallelUtils.getProcess(p).rank»'''
	}
	
	def dispatch getUID(Block c) {
		'''«c.number»_«ParallelUtils.getProcess(c).rank»'''
	}
	
	def dispatch getUID(Process p) {
		'''«p.rank»'''
	}
}
				