package fr.irisa.cairn.gecos.parallel.model.transforms;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.add;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.call;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.mul;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sizeOf;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.string;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FUNCTION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.PTR;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.parallel.model.utils.ParallelDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.Type;
import parallel.Communication;
import parallel.Message;
import parallel.ParallelApplication;
import parallel.Process;
import parallel.Receive;
import parallel.Send;

public class RuntimeProfilingInsertor extends ParallelDefaultVisitor {

	private static final String PROFILING_FILES_EXTENSION = ".stats";

	private ParallelApplication application;
	
	private Map<Process, Symbol> recvCountSymbols;
	private Map<Process, Symbol> sendCountSymbols;
	private Map<Process, Symbol> recvSizeSymbols;
	private Map<Process, Symbol> sendSizeSymbols;

	private Process currentProcess;
	
	
	
	public RuntimeProfilingInsertor(ParallelApplication parallelApp) {
		this.application = parallelApp;
	}
	
	public void compute() {
		this.recvCountSymbols = new LinkedHashMap<>();
		this.sendCountSymbols = new LinkedHashMap<>();
		this.recvSizeSymbols  = new LinkedHashMap<>();
		this.sendSizeSymbols  = new LinkedHashMap<>();
		
		/* insert profiling instructions */
		application.accept(this);
	}


	@Override
	public void visitProcess(Process process) {
		this.currentProcess = process;
		
		Procedure main = getMainProc(process);
		CompositeBlock mainBody = ((CompositeBlock)main.getBody().getChildren().get(1));
		
		
		int rank = process.getRank();

		Scope scope = mainBody.getScope();
		GecosUserTypeFactory.setScope(scope);
		
		/* add profiling symbols */
		Symbol recvCount  = createProfilingSymbol("__RECV_COUNT_"  + rank + "__", scope, 0); 
		Symbol sendCount  = createProfilingSymbol("__SEND_COUNT_"  + rank + "__", scope, 0);
		Symbol recvAmount = createProfilingSymbol("__RECV_AMOUNT_" + rank + "__", scope, 0);
		Symbol sendAmount = createProfilingSymbol("__SEND_AMOUNT_" + rank + "__", scope, 0);
		
		recvCountSymbols.put(process, recvCount);
		sendCountSymbols.put(process, sendCount);
		recvSizeSymbols.put(process, recvAmount);
		sendSizeSymbols.put(process, sendAmount);
		
		/* add profiling printing intructions */
		BasicBlock printBlock = BBlock();
		CompositeBlock profilingBlk = CompositeBlock(printBlock);
		mainBody.addBlock(profilingBlk);
		scope = profilingBlk.getScope();
		GecosUserTypeFactory.setScope(scope);
		
		
		String outputFilePath = "PR" + rank + PROFILING_FILES_EXTENSION;

		Type fileType = GecosUserTypeFactory.ALIAS(INT(), "FILE");
		GecosUserAnnotationFactory.pragma(fileType, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		
		Symbol outputFileSymbol = symbol("__profiling_output_file__", PTR(fileType ), scope);
		printBlock.addInstruction(set(outputFileSymbol, call(getProcedureSymbol(scope, "fopen"), string(outputFilePath), string("w+"))));
//		Symbol outputFileSymbol = symbol("stderr", PTR(fileType), scope);
//		GecosUserAnnotationFactory.pragma(outputFileSymbol, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		
		ProcedureSymbol fprintfSymbol = getProcedureSymbol(scope, "fprintf");
		printBlock.addInstruction(call(fprintfSymbol, symbref(outputFileSymbol), string("[Stats][PR" + rank + "] issued recv instructions = %d\\n"), symbref(recvCount)));
		printBlock.addInstruction(call(fprintfSymbol, symbref(outputFileSymbol), string("[Stats][PR" + rank + "] issued send instructions = %d\\n"), symbref(sendCount)));
		printBlock.addInstruction(call(fprintfSymbol, symbref(outputFileSymbol), string("[Stats][PR" + rank + "] total recv data size = %d Bytes\\n"), symbref(recvAmount)));
		printBlock.addInstruction(call(fprintfSymbol, symbref(outputFileSymbol), string("[Stats][PR" + rank + "] total send data size = %d Bytes\\n"), symbref(sendAmount)));
		
		printBlock.addInstruction(call(getProcedureSymbol(scope, "fclose"), symbref(outputFileSymbol)));
				
		super.visitProcess(process);
	}

	private Symbol createProfilingSymbol(String namePrefix, Scope scope, int initValue) {
		Symbol recvCount = GecosUserCoreFactory.tmpSymbol(namePrefix, GecosUserTypeFactory.INT(), scope);
		recvCount.setValue(GecosUserInstructionFactory.Int(initValue));
		return recvCount;
	}
	
	@Override
	public void visitSend(Send send) {
		Symbol count = sendCountSymbols.get(currentProcess);
		Symbol amount = sendSizeSymbols.get(currentProcess);
		Message msg = send.getMessage();
		int nbCommunications = send.getReceivingProcesses().size();
		Instruction communicationSizeBytes = mul(msg.getCount().copy(), sizeOf(msg.getType()));
		
		addProfiling(send, count, amount, nbCommunications, communicationSizeBytes);
	}
	
	@Override
	public void visitReceive(Receive recv) {
		Symbol count = recvCountSymbols.get(currentProcess);
		Symbol amount = recvSizeSymbols.get(currentProcess);
		Message msg = recv.getMessage();
		int nbCommunications = 1;
		Instruction communicationSizeBytes = msg.getCount().copy();
		
		addProfiling(recv, count, amount, nbCommunications, communicationSizeBytes);
	}
	
	private static void addProfiling(Communication com, Symbol count, Symbol amount, int nbCommunications, Instruction communicationSizeBytes) {
		com.addInstruction(set(count,  add(symbref(count), Int(nbCommunications))));
		com.addInstruction(set(amount, add(symbref(amount), mul(Int(nbCommunications), communicationSizeBytes))));
	}
	
	/**
	 * Lookup a procedure symbol with the specified name in {@code scope}.
	 * If not found create a new one and add it to {@code scope}.
	 * 
	 * @param scope
	 * @return
	 */
	private ProcedureSymbol getProcedureSymbol(Scope scope, String procName) {
		return scope.getSymbols().stream()
			.filter(ProcedureSymbol.class::isInstance)
			.filter(s -> s.getName().equals(procName))
			.map(ProcedureSymbol.class::cast)
			.findFirst().orElseGet(() -> {
				GecosUserTypeFactory.setScope(scope);
				ProcedureSymbol s = procSymbol(procName, FUNCTION(INT()));
				GecosUserAnnotationFactory.pragma(s, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
				scope.getSymbols().add(s);
				return s;
			});
	}
	

	/**
	 * Lookup {@code process} procedures for a "main".
	 * If not found create a new one.
	 * 
	 * @param process
	 * @return
	 */
	private Procedure getMainProc(Process process) {
		Procedure main = process.listProcedures().stream()
			.filter(p -> p.getSymbolName().equals("main"))
			.findFirst().orElse(null);
		if(main == null) {
			//TODO
		}
		return main;
	}
}
