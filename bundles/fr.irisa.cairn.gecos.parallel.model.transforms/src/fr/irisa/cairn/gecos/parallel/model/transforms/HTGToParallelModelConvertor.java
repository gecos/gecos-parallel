package fr.irisa.cairn.gecos.parallel.model.transforms;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.For;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.proc;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.address;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.div;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sizeOf;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory.message;
import static fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory.parallelApplication;
import static fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory.process;
import static fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory.sreceive;
import static fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory.ssend;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;

import architecture.Architecture;
import architecture.ProcessingResource;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGDefaultVisitor;
import fr.irisa.cairn.gecos.htaskgraph.model.utils.HTGUtils;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.cairn.gecos.parallel.model.factory.GecosUserParallelModelFactory;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.GenericInstruction;
import htaskgraph.ClusterTaskNode;
import htaskgraph.CommunicationNode;
import htaskgraph.ForTaskNode;
import htaskgraph.HTGCom;
import htaskgraph.HTGRecv;
import htaskgraph.HTGSend;
import htaskgraph.HTaskGraphVisitor;
import htaskgraph.IfTaskNode;
import htaskgraph.LeafTaskNode;
import htaskgraph.WhileTaskNode;
import parallel.Communication;
import parallel.Message;
import parallel.ParallelApplication;
import parallel.Process;
import parallel.Receive;
import parallel.Send;

/**
 * Convert a Scheduled HTG to a multi-process Parallel IR.
 * 
 * @author aelmouss
 */
@GSModule("Converts Hirerarchical Task Graph (HTG) into ParallelApplication.\n"
		+ "The HTG must be scheduled where each task is properly mapped into a computing Ressource.")
public class HTGToParallelModelConvertor {

	private static final boolean DEBUG = false;
	
	private GecosProject project;
	private Architecture arch;
	private boolean addProfiling;
	
	private ParallelApplication app;
	private Map<ProcessingResource, Process> processes_m;
	private Multimap<HTGRecv, Receive> HTGrecv_recv_m = LinkedHashMultimap.create();
	private Multimap<HTGSend, Send> HTGsend_send_m = LinkedHashMultimap.create();
	
	
	@GSModuleConstructor(value = "", args= {
			@GSArg(name="gProject", info="GecosProject containing the HTG."),
			@GSArg(name="Architecture", info="the target architecture."),
			@GSArg(name="addProfiling", info="If 'true' profiling instructions are added.")
	})
	public HTGToParallelModelConvertor(GecosProject gProject, Architecture arch, boolean addProfiling) {
		this.project = gProject;
		this.arch = arch;
		this.addProfiling = addProfiling;
	}
	
	@GSModuleConstructor("default 'addProfile' to false.")
	public HTGToParallelModelConvertor(GecosProject gProject, Architecture arch) {
		this(gProject, arch, false);
	}
	
	public ParallelApplication compute() {
		processes_m = new LinkedHashMap<>();
		
		/* create parallel model */
		project.listProcedures().forEach(this::buildParallelProcedures);
		app = parallelApplication(processes_m.values());
		
		//XXX add HTG and Arch to app's containment structure to avoid dangling references:
		// since, for example, copied Instructions in app currently still refer to Symbols/Types in HTG Scopes..
		app.getAnnotations().put("HTGToParallelModelConvertor::HTG", GecosUserAnnotationFactory.extendedContains(project));
		//!!! arch is already in HTG containment structure 

		
		/* bind send/recv communications */
		bindCummunications();
		
		if(addProfiling)
			new RuntimeProfilingInsertor(app).compute();
		
		//XXX
//		String name = "parallel_" + project.getName();
//		GecosProject parallelProj = GecosUserCoreFactory.project(name);
//		GecosSourceFile source = GecosUserCoreFactory.source("tmp_gecos_file.c"/*name + ".c"*/, app); //XXX
//		parallelProj.getSources().add(source );
//		parallelProj.getIncludes().addAll(project.getIncludes()); //XXX
//		parallelProj.getMacros().addAll(project.getMacros()); //XXX
		
		return app;
	}

	
	private int comTag = GecosUserParallelModelFactory.TAG_ANY + 1;
	private void bindCummunications() {
		for(HTGRecv hr : HTGrecv_recv_m.keySet()) {
			/*
			 * Each HTGCom that is scheduled on multiple processes is
			 * duplicated so that each parallel instance is assigned to 1 and only 1 Process.
			 * the same unique TAG is used for all sends -> recvs instances XXX ???
			 */
			Collection<Receive> recvInstances = HTGrecv_recv_m.get(hr);
			
			Collection<Send> sendInstances = hr.getSends().stream()
				.flatMap(hs -> HTGsend_send_m.get(hs).stream())
				.collect(Collectors.toList());
			
			// each (parallel) send/recv is assigned to 1 and only 1 Process.
			// bing each send in 'sends' to all recvs assigned to a different process.
			// and set a unique TAG for all sends -> recvs instances 
			int tag = comTag++;
			for(Send send : sendInstances) {
				Process sendingProcess = getProcess(send);
				if(sendingProcess == null) {
					throw new RuntimeException("send is not contained in a process: " + send);
				}
				
				// Receives that are to be received by a different process than the sender's.
				// Other receives (i.e. by the same process as sender) are removed since a communication
				// between the same process is not required.
				List<Receive> effectiveRecvs = recvInstances.stream()
					.filter(r -> getProcess(r) != sendingProcess)
					.collect(Collectors.toList());
				
				if(effectiveRecvs.isEmpty())
					send.getParent().removeBlock(send);
				else {
					send.getMessage().setTag(tag);
					for(Receive r : effectiveRecvs) {
						r.getSends().add(send);
						r.getMessage().setTag(tag);
					}
				}
			}
			
			// Remove receives that do not have a associated send (i.e. do not receive from other processes)
			recvInstances.stream()
				.filter(r -> r.getSends().isEmpty())
				.forEach(r -> r.getParent().removeBlock(r));
		}
	}

	private Process getProcess(Communication r) {
		return r.getContiningProcess();
	}
	
	
	/**
	 * Visit {@code htgProc} multiple times !
	 * (one time per processingResource on which it is mapped)
	 * 
	 * @param htgProc
	 */
	protected void buildParallelProcedures(Procedure htgProc) {
		Set<ProcessingResource> processingResources = HTGUtils.getAllProcessingResourcesFor(htgProc);
		for(ProcessingResource pr : processingResources) {
			Process process = processes_m.get(pr);
			if(process == null) {
				process = process(pr);
				processes_m.put(pr, process);
			}
			Procedure parallelProc = new BuildParallelProcedure().compute(htgProc, process);
			new FixBlockNumbers(parallelProc).compute();
		}
	}
	
	private class BuildParallelProcedure extends HTGDefaultVisitor  {
	
		private Procedure procHTG;
		private ProcessingResource currentPR;
		private CompositeBlock currentCB;

		/**
		 * Build the instance of the HTG procedure {@code htgProc} including only 
		 * tasks scheduled on the specified {@code process}.
		 * Also insert the necessary communication blocks but does not bind them!
		 * 
		 * @param htgProc
		 * @param process
		 * @return the procedure instance scheduled on {@code process}.
		 */
		private Procedure compute(Procedure htgProc, Process process) {
			if(DEBUG) System.out.println("Building " + htgProc + " for " + process);
			
			this.procHTG = htgProc;
			this.currentPR = process.getProcessResource();
			this.currentCB = CompositeBlock();
			
			Procedure parallelProc = proc(process, procHTG.getSymbol().copy(), currentCB);
			
			// build procedure: keep only tasks mapped to pr and add proper communication/synchro
			CompositeBlock oldBody = procHTG.getBody();
			if(oldBody.getChildren().size() > 2 && oldBody.getChildren().get(0) == procHTG.getStart() && oldBody.getChildren().get(1) instanceof CompositeBlock)
				oldBody = (CompositeBlock) oldBody.getChildren().get(1);

			oldBody.accept(this); 
			
			// prune unnecessary body composite blocks
			Block body = parallelProc.getBody().getChildren().get(1);
			while(body instanceof CompositeBlock) {
				EList<Block> children = ((CompositeBlock)body).getChildren();
				if(children.size() == 1 && children.get(0) instanceof CompositeBlock 
						&& ((CompositeBlock)body).getScope().getSymbols().isEmpty())
					body = children.get(0);
				else 
					break;
			}
			parallelProc.getBody().getChildren().set(1, body);
			
			return parallelProc;
		}

		
		@Override
		public void visitClusterTaskNode(ClusterTaskNode task) {
			task.getInputNode().accept((HTaskGraphVisitor)this);
			
			if(task.isScheduledOn(currentPR)) {
				CompositeBlock parallelBlk = CompositeBlock(task.getBlock().getScope().copy());
				currentCB.addChildren(parallelBlk);
				
				/* visit children */
				CompositeBlock saved = currentCB;
				currentCB = parallelBlk;
				task.getBlock().accept(this);
				currentCB = saved;
			}
			
			task.getOutputNode().accept((HTaskGraphVisitor)this);
		}
		
		@Override
		public void visitForTaskNode(ForTaskNode task) {
			task.getInputNode().accept((HTaskGraphVisitor)this);
			
			if(task.isScheduledOn(currentPR)) {
				ForBlock taskBlk = task.getBlock();
				BasicBlock init = taskBlk.getInitBlock().copy();
				BasicBlock test = taskBlk.getTestBlock().copy();
				BasicBlock step = taskBlk.getStepBlock().copy();
				CompositeBlock bodyBlk = CompositeBlock();
				
//				if(HTGUtils.getPRsWhereTaskIsDuplicatedForCommunication(task).contains(currentPR)) {
//					currentCB.addChildren(init);
//					IfBlock parallelBlk = GecosUserBlockFactory.IfThen(test, bodyBlk);
//					currentCB.addChildren(parallelBlk);
//				} else {
					ForBlock parallelBlk = For(init, test, step, bodyBlk);
					currentCB.addChildren(parallelBlk);
//				}
				
				/* visit body */
				CompositeBlock saved = currentCB;
				currentCB = bodyBlk;
				taskBlk.getBodyBlock().accept(this);
				currentCB = saved;
			}
			
			task.getOutputNode().accept((HTaskGraphVisitor)this);
		}
		
		@Override
		public void visitIfTaskNode(IfTaskNode task) {
			task.getInputNode().accept((HTaskGraphVisitor)this);
			
			if(task.isScheduledOn(currentPR)) {
				IfBlock taskBlk = task.getBlock();
				if(taskBlk.getThenBlock() != null) {
					BasicBlock testB = taskBlk.getTestBlock().copy();
					
					CompositeBlock thenBlk = CompositeBlock();
					IfBlock parallelBlk = GecosUserBlockFactory.IfThen(testB, thenBlk);
					currentCB.addChildren(parallelBlk);
					
					/* visit then block*/
					CompositeBlock saved = currentCB;
					currentCB = thenBlk;
					taskBlk.getThenBlock().accept(this);
					currentCB = saved;
					
					/* visit else block */
					if(taskBlk.getElseBlock() != null) {
						CompositeBlock elseBlk = CompositeBlock();
						parallelBlk.setElseBlock(elseBlk);
						
						saved = currentCB;
						currentCB = elseBlk;
						taskBlk.getElseBlock().accept(this);
						currentCB = saved;
					}
				}
			}
			
			task.getOutputNode().accept((HTaskGraphVisitor)this);
		}
		
		//FIXME not correctly supported yet
		@Override
		public void visitWhileTaskNode(WhileTaskNode task) {
			task.getInputNode().accept((HTaskGraphVisitor)this);
			
			if(task.isScheduledOn(currentPR)) {
				WhileBlock taskBlk = task.getBlock();
				BasicBlock test = taskBlk.getTestBlock().copy();
				CompositeBlock bodyBlk = CompositeBlock();
				WhileBlock parallelBlk = GecosUserBlockFactory.While(test, bodyBlk);
				currentCB.addChildren(parallelBlk);
				
				/* visit body */
				CompositeBlock saved = currentCB;
				currentCB = bodyBlk;
				taskBlk.getBodyBlock().accept(this);
				currentCB = saved;
			}
			
			task.getOutputNode().accept((HTaskGraphVisitor)this);
		}
		
		@Override
		public void visitLeafTaskNode(LeafTaskNode task) {
			task.getInputNode().accept((HTaskGraphVisitor)this);
			
			if(task.isScheduledOn(currentPR)) {
				Block parallelBlk = task.getBlock().copy();
				currentCB.addChildren(parallelBlk);
			}
			
			task.getOutputNode().accept((HTaskGraphVisitor)this);
		}
		
		@Override
		public void visitCommunicationNode(CommunicationNode task) {
			EList<HTGCom> communications = task.getCommunications();
			
			if(!communications.isEmpty() && task.isScheduledOn(currentPR)) {
//				CompositeBlock parallelBlk = GecosUserBlockFactory.CompositeBlock(); // create a communication block instead ?
//				currentCB.addChildren(parallelBlk);
				CompositeBlock parallelBlk = currentCB;
				
				/* any newly created Type will be placed in the new TaskBlk Scope */
				Scope scope = parallelBlk.getScope();
				if(scope != null)
					GecosUserTypeFactory.setScope(scope);
				
				List<Communication> beforeCom = htgComToParallelCom(communications);
				beforeCom.forEach(parallelBlk::addChildren);
				if(DEBUG) System.out.println("  - Added communications: " + beforeCom);
			}
		}

		private List<Communication> htgComToParallelCom(Collection<HTGCom> htgComs) {
			return htgComs.stream()
				.filter(hr -> HTGUtils.isScheduledOn(hr, currentPR)) //keep only 'com' to be received/send by/to this.pr
				.map(this::createParallelCom)
				.collect(Collectors.toList());
		}
		
		private Communication createParallelCom(HTGCom com) {
			if(com instanceof HTGRecv)
				return createRecv((HTGRecv) com);
			else if(com instanceof HTGSend)
				return createSend((HTGSend) com);
			
			throw new RuntimeException("Unknown HTGCom type: " + com);
		}
		
		private Receive createRecv(HTGRecv htgRecv) {
			Symbol use = htgRecv.getUse();
			TypeAnalyzer analyzer = new TypeAnalyzer(use.getType());
			// tag is set after all communications are added
			GenericInstruction sizeByte = div(sizeOf(symbref(use)), sizeOf(analyzer.getBase()));
			Message recvMsg = message(address(symbref(use)), use.getType(), sizeByte, 0);
			Receive recv = sreceive(recvMsg, null);
			HTGrecv_recv_m.put(htgRecv, recv);
			return recv;
		}
		
		private Send createSend(HTGSend htgSend) {
			Symbol use = htgSend.getUse();
			TypeAnalyzer analyzer = new TypeAnalyzer(use.getType());
			// tag is set after all communications are added
			GenericInstruction sizeByte = div(sizeOf(symbref(use)), sizeOf(analyzer.getBase()));
			Message sendMsg = message(address(symbref(use)), use.getType(), sizeByte, 0);
			Send send = ssend(sendMsg);
			HTGsend_send_m.put(htgSend, send);
			return send;
		}
	}
	
}
