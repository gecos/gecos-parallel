package fr.irisa.cairn.gecos.parallel.model.transforms.dotprinter;

import fr.irisa.cairn.gecos.parallel.model.utils.ParallelUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import parallel.Message;
import parallel.ParallelApplication;
import parallel.Receive;
import parallel.Send;

@GSModule("Generates a Graphiz DOT graph representing a ParallelModel.")
@SuppressWarnings("all")
public class ParallelModelDottyExport {
  private final static String COLOR_PROCESS = "black";
  
  private final static String COLOR_PROCEDURE = "blue";
  
  private final static String COLOR_SEND = "red";
  
  private final static String COLOR_RECV = "red";
  
  private final static String COLOR_COMPUTE = "green";
  
  private final static String COLOR_COMPOSITEBLK = "green";
  
  private ParallelApplication app;
  
  private String path;
  
  private StringBuilder comEdgesBuffer;
  
  @GSModuleConstructor(("-arg1: ParallelApplication.\n" + 
    "-arg2: location where the .dot file will be generated at."))
  public ParallelModelDottyExport(final ParallelApplication parallelApp, final String location) {
    this.app = parallelApp;
    this.path = location;
  }
  
  public void compute() {
    File dest = new File(this.path);
    if ((dest.exists() && dest.isFile())) {
      throw new RuntimeException();
    }
    dest.mkdirs();
    this.generate(this.app);
  }
  
  public void generate(final ParallelApplication app) {
    try {
      StringBuilder _stringBuilder = new StringBuilder();
      this.comEdgesBuffer = _stringBuilder;
      PrintStream ps = new PrintStream(((this.path + Character.valueOf(File.separatorChar)) + "app.dot"));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("digraph G {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      {
        EList<parallel.Process> _processes = app.getProcesses();
        for(final parallel.Process p : _processes) {
          _builder.append("\t");
          String _generate = this.generate(p);
          _builder.append(_generate, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append(this.comEdgesBuffer, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rankdir=\"LR\"");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      ps.append(_builder);
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String generate(final parallel.Process p) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(p);
      final String label = ("Process" + id);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph clusterProcess");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=line;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_PROCESS, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      {
        EList<Procedure> _listProcedures = p.listProcedures();
        for(final Procedure proc : _listProcedures) {
          _builder.append("\t");
          CharSequence _generate = this.generate(((Procedure) proc));
          _builder.append(_generate, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public CharSequence generate(final Procedure p) {
    CharSequence _xblockexpression = null;
    {
      final CharSequence id = this.getUID(p);
      final String label = p.getSymbolName();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph cluster");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=line;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_PROCEDURE, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      String _generateNode = this.generateNode(p.getBody());
      _builder.append(_generateNode, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final CompositeBlock b) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(b);
      final String label = "";
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph cluster");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=dashed;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_COMPOSITEBLK, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      {
        EList<Block> _children = b.getChildren();
        for(final Block c : _children) {
          _builder.append("\t");
          String _generateNode = this.generateNode(c);
          _builder.append(_generateNode, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("}");
      _builder.newLine();
      String _generateControlEdges = this.generateControlEdges(b);
      _builder.append(_generateControlEdges);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final ForBlock b) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(b);
      final String label = "For";
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph cluster");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=line;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_COMPOSITEBLK, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      String _generateNode = this.generateNode(b.getBodyBlock());
      _builder.append(_generateNode, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final IfBlock b) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(b);
      final String label = "If";
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph cluster");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=dashed;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_COMPOSITEBLK, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      String _xifexpression = null;
      Block _thenBlock = b.getThenBlock();
      boolean _tripleNotEquals = (_thenBlock != null);
      if (_tripleNotEquals) {
        _xifexpression = this.generateNode(b.getThenBlock());
      }
      _builder.append(_xifexpression, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      String _xifexpression_1 = null;
      Block _elseBlock = b.getElseBlock();
      boolean _tripleNotEquals_1 = (_elseBlock != null);
      if (_tripleNotEquals_1) {
        _xifexpression_1 = this.generateNode(b.getElseBlock());
      }
      _builder.append(_xifexpression_1, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final WhileBlock b) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(b);
      final String label = "While";
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph cluster");
      _builder.append(id);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("compound=true;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("style=line;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("color=");
      _builder.append(ParallelModelDottyExport.COLOR_COMPOSITEBLK, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("label=\"");
      _builder.append(label, "\t");
      _builder.append("\"");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      String _generateNode = this.generateNode(b.getBodyBlock());
      _builder.append(_generateNode, "\t");
      _builder.newLineIfNotEmpty();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final BasicBlock b) {
    String _xblockexpression = null;
    {
      int _instructionCount = b.getInstructionCount();
      boolean _equals = (_instructionCount == 0);
      if (_equals) {
        return "";
      }
      final CharSequence id = this.getUID(b);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Compute");
      _builder.append(id);
      final String label = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("subgraph cluster");
      _builder_1.append(id);
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("compound=true;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("style=line;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("shape=\"rectangle\"");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("color=");
      _builder_1.append(ParallelModelDottyExport.COLOR_COMPUTE, "\t");
      _builder_1.append(";");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("label=\"");
      _builder_1.append(label, "\t");
      _builder_1.append("\"");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("Compute");
      _builder_1.append(id, "\t");
      _builder_1.append("[shape=\"rectangle\",color=\"white\",label=\"\"];");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("}");
      _builder_1.newLine();
      _xblockexpression = _builder_1.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final Block b) {
    return null;
  }
  
  protected String _generateNode(final Send send) {
    String _xblockexpression = null;
    {
      this.comEdgesBuffer.append(this.generateCommunicationEdges(send));
      final CharSequence id = this.getUID(send);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Send");
      _builder.append(id);
      final String label = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("subgraph cluster");
      _builder_1.append(id);
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("compound=true;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("style=line;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("shape=\"rectangle\"");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("color=");
      _builder_1.append(ParallelModelDottyExport.COLOR_SEND, "\t");
      _builder_1.append(";");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("label=\"");
      _builder_1.append(label, "\t");
      _builder_1.append("\"");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("Send");
      _builder_1.append(id, "\t");
      _builder_1.append("[shape=\"rectangle\",color=\"white\",label=\"");
      Message _message = send.getMessage();
      _builder_1.append(_message, "\t");
      _builder_1.append("\"];");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("}");
      _builder_1.newLine();
      _xblockexpression = _builder_1.toString();
    }
    return _xblockexpression;
  }
  
  protected String _generateNode(final Receive recv) {
    String _xblockexpression = null;
    {
      final CharSequence id = this.getUID(recv);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Recv");
      _builder.append(id);
      final String label = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("subgraph cluster");
      _builder_1.append(id);
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("compound=true;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("style=line;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("shape=\"rectangle\"");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("color=");
      _builder_1.append(ParallelModelDottyExport.COLOR_RECV, "\t");
      _builder_1.append(";");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.append("label=\"");
      _builder_1.append(label, "\t");
      _builder_1.append("\"");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("Receive");
      _builder_1.append(id, "\t");
      _builder_1.append("[shape=\"rectangle\",color=\"white\",label=\"");
      Message _message = recv.getMessage();
      _builder_1.append(_message, "\t");
      _builder_1.append("\"];");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("}");
      _builder_1.newLine();
      _xblockexpression = _builder_1.toString();
    }
    return _xblockexpression;
  }
  
  public CharSequence generateCommunicationEdges(final Send send) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Receive> _receives = send.getReceives();
      for(final Receive recv : _receives) {
        _builder.append("Send");
        CharSequence _uID = this.getUID(send);
        _builder.append(_uID);
        _builder.append(" -> Receive");
        CharSequence _uID_1 = this.getUID(recv);
        _builder.append(_uID_1);
        _builder.append("[color=\"red\",label=\"\"]; ");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public String generateControlEdges(final CompositeBlock block) {
    return null;
  }
  
  protected CharSequence _getUID(final Procedure p) {
    StringConcatenation _builder = new StringConcatenation();
    String _symbolName = p.getSymbolName();
    _builder.append(_symbolName);
    _builder.append("_");
    int _rank = ParallelUtils.getProcess(p).getRank();
    _builder.append(_rank);
    return _builder;
  }
  
  protected CharSequence _getUID(final Block c) {
    StringConcatenation _builder = new StringConcatenation();
    int _number = c.getNumber();
    _builder.append(_number);
    _builder.append("_");
    int _rank = ParallelUtils.getProcess(c).getRank();
    _builder.append(_rank);
    return _builder;
  }
  
  protected CharSequence _getUID(final parallel.Process p) {
    StringConcatenation _builder = new StringConcatenation();
    int _rank = p.getRank();
    _builder.append(_rank);
    return _builder;
  }
  
  public String generateNode(final EObject recv) {
    if (recv instanceof Receive) {
      return _generateNode((Receive)recv);
    } else if (recv instanceof Send) {
      return _generateNode((Send)recv);
    } else if (recv instanceof BasicBlock) {
      return _generateNode((BasicBlock)recv);
    } else if (recv instanceof CompositeBlock) {
      return _generateNode((CompositeBlock)recv);
    } else if (recv instanceof ForBlock) {
      return _generateNode((ForBlock)recv);
    } else if (recv instanceof IfBlock) {
      return _generateNode((IfBlock)recv);
    } else if (recv instanceof WhileBlock) {
      return _generateNode((WhileBlock)recv);
    } else if (recv instanceof Block) {
      return _generateNode((Block)recv);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(recv).toString());
    }
  }
  
  public CharSequence getUID(final EObject p) {
    if (p instanceof parallel.Process) {
      return _getUID((parallel.Process)p);
    } else if (p instanceof Block) {
      return _getUID((Block)p);
    } else if (p instanceof Procedure) {
      return _getUID((Procedure)p);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(p).toString());
    }
  }
}
