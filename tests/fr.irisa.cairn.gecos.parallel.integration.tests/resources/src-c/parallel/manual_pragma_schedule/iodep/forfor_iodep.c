
int main() {
	int a[10][10], n, s, p, i, j;

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		n = 12;
		s = 0;
		p = 1;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		printf("0.1: s=%d, p=%d, n=%d\n", s,p,n);
		for(i = 0; i < 4; i++) {
			for(j = 0; j < 4; j++) {
				a[i][j] = i+j+1;
			}
		}
		printf("0.1: s=%d, p=%d, n=%d\n", s,p,n);
	}

#pragma GECOS_SCHEDULER_PROCESS 2
	{
		printf("1.0: s=%d, p=%d, n=%d\n", s,p,n);
		for(i = 0; i < 4; i++) {
			for(j = 0; j < 4; j++) {
				s += a[i][j];
			}
		}
		printf("1.1: s=%d, p=%d, n=%d\n", s,p,n);
	}

#pragma GECOS_SCHEDULER_PROCESS 3
	{
		printf("2.0: s=%d, p=%d, n=%d\n", s,p,n);
		for(i = 0; i < 4; i++) {
			for(j = 0; j < 4; j++) {
				p *= a[i][j];
			}
		}
		printf("2.1: s=%d, p=%d, n=%d\n", s,p,n);
	}

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		printf("3: s=%d, p=%d, n=%d\n", s,p,n);
	}

}
