int main() {
	int a[8][8]={0}, n, r, i, j;

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		n = 8;
		r = 0;
	}

	for(i = 0; i < n; i++) {
		if(i%3 == 0)
		{
			for(j = 0; j < n; j++) {
				if(j%2 == 0)
#pragma GECOS_SCHEDULER_PROCESS 1
				{
					a[i][j] = j;
				}
				else
#pragma GECOS_SCHEDULER_PROCESS 2
				{
					a[i][j] = 10 +j;
				}
			}
		}
		else if(i%3 == 1)
#pragma GECOS_SCHEDULER_PROCESS 3
		{
			for(j = 0; j < n; j++) {
				a[i][j] = j+100;
			}
		}
		else
		{
			for(j = 0; j < n; j++) {
				if(j != 2)
#pragma GECOS_SCHEDULER_PROCESS 1
				{
					a[i][j] += j;
				}
				else
#pragma GECOS_SCHEDULER_PROCESS 3
				{
					a[i][j] = -j;
				}
			}
		}
	}


#pragma GECOS_SCHEDULER_PROCESS 0
	for(i = 0; i < n; i++) {
		for(j = 0; j < n; j++) {
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}

}
