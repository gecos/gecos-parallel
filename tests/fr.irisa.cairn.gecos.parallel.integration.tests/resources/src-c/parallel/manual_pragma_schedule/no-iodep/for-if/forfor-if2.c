int main() {
	int a[4][4], s, p, i, j;

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		s = 0;
		p = 0;
	}

	for(i = 0; i < 4; i++) {
#pragma GECOS_SCHEDULER_PROCESS 1
		for(j = 0; j < 4; j++)
			a[i][j] = i+j+1;

		for(j = 0; j < 4; j++) {
			if(i%2 == 0)
#pragma GECOS_SCHEDULER_PROCESS 2
			{
				s += a[i][j];
			}
			else
#pragma GECOS_SCHEDULER_PROCESS 3
			{
				p -= a[i][j];
			}
		}
	}

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		printf("s=%d, p=%d\n", s,p);
	}
}
