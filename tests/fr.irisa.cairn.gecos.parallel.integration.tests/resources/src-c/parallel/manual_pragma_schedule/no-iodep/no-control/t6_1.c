#include<stdio.h>

int main() {
	int n = 5;
	int a[4];
	int r0, r1, r2, r3;
	int r;

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		a[0] = 1;
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		r0 = a[0] * n;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		r1 = a[1] * n;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		r2 = a[2] * n;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		r3 = a[3] * n;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		r = r0 + r1 + r2 + r3;
		printf("%d\n", r); // output > 50
	}
}
