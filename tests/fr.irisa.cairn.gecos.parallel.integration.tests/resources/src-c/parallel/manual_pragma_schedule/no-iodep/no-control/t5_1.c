#include<stdio.h>

int main() {
	int n = 5;
	int a[4];
	int r0, r1, r2, r3;
	int s1, s2, s;

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		a[0] = 1;
		a[1] = 2;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
		a[3] = 1;
		a[4] = 2;
	}

#pragma GECOS_SCHEDULER_PROCESS 1
	{
#pragma GECOS_SCHEDULER_PROCESS 1
		{
			r0 = a[0] * n;
		}
#pragma GECOS_SCHEDULER_PROCESS 1
		{
			r1 = a[3] * n;
		}

	}


#pragma GECOS_SCHEDULER_PROCESS 1
	{
		s1 = r0 + r1;
		printf("%d\n", s1); //output: > 10
	}
}
