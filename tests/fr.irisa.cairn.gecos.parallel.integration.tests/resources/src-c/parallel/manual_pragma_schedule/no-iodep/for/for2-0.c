int main() {
	int a[4], s, i;

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		s = 0;
		a[0] = 2; a[1] = 3; a[2] = 4; a[3] = 5;
	}

	for(i = 0; i < 4; i++) {
#pragma GECOS_SCHEDULER_PROCESS 1
		{
			s = a[i];
		}
#pragma GECOS_SCHEDULER_PROCESS 2
		{
			s += a[i];
		}
	}

#pragma GECOS_SCHEDULER_PROCESS 0
	{
		printf("s=%d\n", s);
	}
}
