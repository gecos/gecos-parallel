
int main() {
	int n = 5;
	int a0, a1, a2, a3;
	int r0, r1, r2, r3;
	int s1, s2, s;

	{
		a0 = 1;
		a1 = 2;
	}

	{
		a2 = 3;
		a3 = 4;
	}

	{
		r0 = a0 * n;
	}

	{
		r1 = a1 * n;
	}

	{
		r2 = a2 * n;
	}

	{
		r3 = a3 * n;
	}

	{
		s1 = r0 + r1;
	}

	{
		s2 = r2 + r3;
	}

	{
		s = s1 + s2;
		printf("%d\n", s);
	}

}
