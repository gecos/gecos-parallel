#include<stdio.h>

int main() {
	int n = 5;
	int a[4];
	int r0, r1, r2, r3;
	int r;

	{
		a[0] = 1;
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
	}

	{
		r0 = a[0] * n;
	}

	{
		r1 = a[1] * n;
	}

	{
		r2 = a[2] * n;
	}

	{
		r3 = a[3] * n;
	}

	{
		r = r0 + r1 + r2 + r3;
		printf("%d\n", r); // output > 50
	}
}
