package fr.irisa.cairn.gecos.parallel.flow.tests;

import java.nio.file.Path;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.HTGExtraction;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty.TaskGraphDottyExport;
import fr.irisa.cairn.gecos.model.transforms.ssa.ComputeSSAForm;
import fr.irisa.cairn.gecos.model.transforms.ssa.RemoveSSAForm;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import gecos.gecosproject.GecosProject;

/**
 * Tests {@link HTGExtraction} module.
 * 
 * @author aelmouss
 */
public class HTGExtractorIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c")
	public void testHtgExtract(ICxxProjectData d) {
//		runTest(d, this::transform);
	}
	
	private void transform(GSProjectVersion v) {
		GecosProject gProj = v.getProject();
		Path outDir = getTestMethodOutputDir().resolve(v.getName());
		
		// Generate HTG
		new ComputeSSAForm(gProj).compute();
		
		new HTGExtraction(gProj, null).compute();
		new TaskGraphDottyExport(gProj, outDir.resolve("dotty_htg").toString(), true).compute();
	
		new RemoveSSAForm(gProj).compute();
	}
	
}
