package fr.irisa.cairn.gecos.parallel.flow.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.Compilers.mpicc;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.COMPILE;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.LINK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.RUN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.TRANSFORM;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.VERIFY;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEachPairWithFirst;
import static java.util.Collections.emptyList;

import java.nio.file.Path;
import java.util.Arrays;

import org.junit.Assume;
import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import architecture.Architecture;
import fr.irisa.cairn.gecos.architecture.model.architectures.DummyParallelArchitecture;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.HTGExtraction;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.InsertSynchronuousCommunications;
import fr.irisa.cairn.gecos.htaskgraph.model.transforms.dotty.TaskGraphDottyExport;
import fr.irisa.cairn.gecos.htaskgraph.scheduling.HTGScheduler;
import fr.irisa.cairn.gecos.model.transforms.ssa.ComputeSSAForm;
import fr.irisa.cairn.gecos.model.transforms.ssa.RemoveSSAForm;
import fr.irisa.cairn.gecos.parallel.model.transforms.HTGToParallelModelConvertor;
import fr.irisa.cairn.gecos.parallel.model.transforms.dotprinter.ParallelModelDottyExport;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.s2s.Comparators;
import fr.irisa.cairn.gecos.testframework.s2s.Linkers;
import fr.irisa.cairn.gecos.testframework.s2s.Operators;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;
import parallel.ParallelApplication;

public class ParallelFlowIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	private int nbProcs = 4;
	private boolean propagateOnly;
	
	@Override
	protected void configure() {
		super.configure();
		
		findTestFlow(CxxFileData.class)
			.addStageAfterLast(TRANSFORM, Stages.<GSProjectVersion>forEach(v -> v.setEnableStdRedirection(true)))
			.replaceStage(COMPILE, forEach(mpicc().changeIncDirsProvider((v,p)-> emptyList())))
			.replaceStage(LINK, forEach(Linkers.mpicc().changeIncDirsProvider(v -> emptyList())))
			.replaceStage(RUN, forEach(Operators.mpiExecutor(v -> v.isRoot()? 1 : nbProcs, v -> Arrays.asList(v.getExecutable().toAbsolutePath().toString()))))
			.replaceStage(VERIFY, forEachPairWithFirst(Comparators.stdoutEquals()))
			;
		
		//!!! if other data types are used, register or modify their test flows accordingly.
	}
	
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/parallel/manual_pragma_schedule/no-iodep")
	public void testManualPragmaSchedule(ICxxProjectData d) {
		propagateOnly = true; // schedule is fully specified by pragmas
//		runTest(d, this::transform);
	}
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/parallel/no_pragma_schedule/no-iodep")
	public void testAutoSchedule(ICxxProjectData d) {
		propagateOnly = false;
//		runTest(d, this::transform);
	}
	
	private void transform(GSProjectVersion v) {
		GecosProject gProj = v.getProject();
		Architecture arch = new DummyParallelArchitecture(nbProcs).compute();
		Path outDir = getTestMethodOutputDir().resolve(v.getName());
		
		// Generate HTG
		new ComputeSSAForm(gProj).compute();
		new HTGExtraction(gProj, arch).compute();
		new TaskGraphDottyExport(gProj, outDir.resolve("dotty_htg-before").toString(), true).compute();

		// Schedule
		new HTGScheduler(gProj, arch, outDir.resolve("scheduler").toString(), propagateOnly).compute();
		new TaskGraphDottyExport(gProj, outDir.resolve("dotty_htg-after").toString(), true).compute();
		new InsertSynchronuousCommunications(gProj).compute();
		new RemoveSSAForm(gProj).compute();

		// HTG to Parallel Model
		GecosProject copy = gProj.copy();
		ParallelApplication parallelApp = new HTGToParallelModelConvertor(copy, arch).compute();
		new ParallelModelDottyExport(parallelApp, outDir.resolve("dotty_paral").toString()).compute();
		
		Assume.assumeTrue(gProj.listProcedureSets().size() == 1);
		EMFUtils.substituteByNewObjectInContainer(gProj.listProcedureSets().get(0), parallelApp); 
	}
	
}
